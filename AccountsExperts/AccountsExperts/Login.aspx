﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="AccountsExperts.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <title>Account Experts </title>
    <style>
        .txtUserName placeholder {
            font-style: normal !important;
        }
    </style>
    <!-- Bootstrap Core CSS -->
    <%--    <link href="../ThemeLayouts/css/bootstrap.min.css" rel="stylesheet" />--%>
    <!-- MetisMenu CSS -->
    <link href="../ThemeLayouts/css/plugins/Login/metisMenu.min.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="../ThemeLayouts/css/plugins/Login/sb-admin-2.css" rel="stylesheet" />
    <!-- Custom Fonts -->
    <link href="../ThemeLayouts/css/plugins/Login/font-awesome.min.css" rel="stylesheet" />
    <!-- Custom CSS For Login Page -->
    <link href="../ThemeLayouts/css/plugins/Login/font-face.css" rel="stylesheet" />
    <link href="../ThemeLayouts/css/plugins/Login/login.css" rel="stylesheet" />
    <script src="JavaScripts/Login.js"></script>

    <!-- jQuery -->

    <script src="../ThemeLayouts/js/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->

    <script src="../ThemeLayouts/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="../ThemeLayouts/js/metisMenu.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../ThemeLayouts/js/sb-admin-2.js"></script>


 


    <link href="ThemeLayouts/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="ThemeLayouts/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="ThemeLayouts/assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="ThemeLayouts/assets/css/bootstrap-responsive.css" rel="stylesheet" />
    <link href="ThemeLayouts/assets/css/login1.css" rel="stylesheet" />
    <script src="ThemeLayouts/assets/js/bootstrap.min.js"></script>



    
  
      <style>
    input:-webkit-autofill,
    input:-webkit-autofill:hover,
    input:-webkit-autofill:focus,
    input:-webkit-autofill:active {
        -webkit-box-shadow: 0 0 0px 1000px #bfeefb inset !important;
           -webkit-text-fill-color: black !important;
   }
</style>

</head>




<body class="has-ius en_US ">
    <div id="margin32"></div>
    <div class="leftshadow generalcontainer">
        <div class="maincontainer" id="maincontainer">
            <div class="logocontainer">
                <img class="qblogo" alt="CloudBook" src="images/cloudbooklogo.png" style="margin-bottom: 5px;"/>
            </div>
            <div class="bodycontainer">
                <table>
                    <tbody>
                        <tr>

                            <td class="comingsooncontainer">
                                <div>
                                    <div id="defaultComingSoon" style="display: block;">
                                        <div class="dtPromo">
                                            <div class="dtHeaderContent">Catch your mind</div>
                                            <div class="dtSubheadingContent">Introduction ERP for small and medium scal business.</div>
                                            <div>
                                                <a class="dtFreeDownloadLinkA" target="_blank" href="#">
                                                    <img class="dtImage" src="images/cloudbookimage.png"/></a>
                                            </div>
                                            <div class="dtSubheadingContent2" style="margin-top:-30px;">
                                                • Seamless integration of all the information flowing through a company.<br />
                                                • Integrated, secure, self-service process for business<br />
                                                • Improve productivity and gain control over the business<br />
                                                • To support business goal<br />
                                                • Empower Employees.
                                                <br />
                                                • Lower costs
                                                <br />
                                                <br />
                                                and many more
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </td>

                            <td class="sfLogininside">
                                <form id="frmLogin" runat="server" style="margin-top: -37px">

                                 <span id="ius-sign-in-header" class="ius-header" >Sign in</span>

                                    <label  class="ius-label" >User ID</label>
                                    <p class="sfUserName" style="margin-top:0px; margin-bottom:0px">
                                        <asp:TextBox runat="server" ID="txtUserName" class="sfInputbox" placeholder=""  style="font-size: 17px;"></asp:TextBox>
                                    </p>

                                     <label  class="ius-label" ">Password</label>
                                    <p class="sfPassword" style="margin-top:0px;margin-bottom:10px;" >
                                        <asp:TextBox runat="server" ID="txtPassword" placeholder="" class="sfInputbox" TextMode="Password"   style="font-size: 17px;"></asp:TextBox>
                                    </p>


                                     
                                    <div class="checkbox" >
                                           <asp:CheckBox ID="Chkrememberme" runat="server" AutoPostBack="false"   Text="Remember my user ID" />
                                            &nbsp;&nbsp;
                                    </div>



                                    <div>
                                        <label style="margin-top:0px; color: #eb3432;word-wrap: break-word;line-height: 1.2em; font-size: 12px;">
                                            <asp:Label ID="lblstatus" runat="server" ></asp:Label>
                                        </label>
                                    
                                        </div>




                                    <div style="float: left;margin-top:-10px; margin-bottom:19px;">

                                        <span><span>
                                            <br />
                                            <asp:Button ID="LoginButton" runat="server"  OnClientClick="return FormValidation();"  OnClick="btnlogin_Click" Text="Sign in" class="button primary" Style="cursor: pointer;" TabIndex="4" />
                                    
                             </span></span>
                                    </div>


                               
                                <div style="float: left; line-height: 1.5; font-family: 'Helvetica Neue',Helvetica,Arial,san serif; text-align: left; font-size: 12px; ">
                                        <span>By clicking Sign In, you agree to our License Agreement.
                                        </span>
                                        <br />
                                        <br />

                                    </div>

                                    <div style="float: left; line-height: 1.5; font-family: 'Helvetica Neue',Helvetica,Arial,san serif; text-align: left; font-size: 12px;">

                                        <span>

                                            <a href="frmForgetPassword">I forgot my user ID or Password</a>
                                        </span>
                                    </div>



                                    <div style="float: left; line-height: 2.5; font-family: 'Helvetica Neue',Helvetica,Arial,san serif; text-align: left; font-size: 12px;">
                                        <span id="Span1">New to CloudBook? </span>
                                        <span><a id="A1" href="#">Sign up</a><span id="Span2" style="display: none;"></span></span>

                                    </div>

                                </form>
                            </td>



                        </tr>

                    </tbody>

                </table>
            </div>
        </div>
    </div>
    <div class="footercontainer generalcontainer">
        <div class="copyright">© 2016 Intuit Inc. All rights reserved.</div>
        <div class="footerlinkscontainer">
            <div class="footerlinks">
                <a class="footer_link" href="#" target="_blank" style="font-size: 11px;">Privacy</a>
                &nbsp;&nbsp;|&nbsp;&nbsp;
                <a class="footer_link" href="#" target="_blank" style="font-size: 11px;">Support</a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>




    <iframe id="intuit.ius.xdrIframe" height="100" width="100" frameborder="0" tabindex="-1" title="" style="color: rgb(0,0,0); float: left; position: absolute; top: -200px; left: -200x; border: 0px; display: inline" src="https://accounts.intuit.com/xdr.html?v2=true&amp;corsEnabled"></iframe>
</body>


</html>
