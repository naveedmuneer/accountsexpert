﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using System.Web.Services;
using AccountsExperts.DataAccesslayer;

namespace AccountsExperts
{
    public partial class ReportViewer : System.Web.UI.Page
    {

        public const string reportPath = "~/Reports/";

        public enum ErrorStatus
        {
            NoData = 0,
            Error = 1,
            show = 2
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string id = "";
                string reportType = "";
                if (!string.IsNullOrEmpty(Request["ID"]))
                {
                    id = Request["ID"];
                }
                if (!string.IsNullOrEmpty(Request["ReportType"]))
                {
                    reportType = Request["ReportType"];
                }
                ShowReport(id, reportType);
            }
        }

        public void ShowReport(string id, string reportType)
        {
            rptViewer.ProcessingMode = ProcessingMode.Local;

            if (reportType.ToLower() == "cr" || reportType.ToLower() == "cp")
            {
                string ReportTypeName = "";

                if(reportType.ToLower()=="cr")
                ReportTypeName = "CASH RECEIPT";
                else
                    ReportTypeName = "CASH PAYMENT";

                DataSet ds = ReportDO.GetCashBookReport(Convert.ToInt32(id));
                using (ds)
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        string totalAmountInWords = cls_util.ConvertNumberIntoWord(Convert.ToInt32(ds.Tables[0].Compute("Sum(Amount)", "")), true);

                        rptViewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath(reportPath + "CashBooks.rdlc");
                        ReportDataSource datasource = new ReportDataSource("CashBook", ds.Tables[0]);
                        ReportParameter rp = new ReportParameter("paramCompanyName", ds.Tables[0].Rows[0]["Company_Name"].ToString(), false);
                        ReportParameter rp2 = new ReportParameter("paramReportType", ReportTypeName, false);
                        ReportParameter rp3 = new ReportParameter("paramAmountInWords", totalAmountInWords, false);

                        rptViewer.LocalReport.SetParameters(new ReportParameter[] { rp, rp2, rp3 });
                        rptViewer.LocalReport.DataSources.Clear();
                        rptViewer.LocalReport.DataSources.Add(datasource);
                        // status = ErrorStatus.show.ToString();
                    }
                    else
                    {
                        //  status = ErrorStatus.NoData.ToString();
                    }
                }
            }
            else if (reportType.ToLower() == "br" || reportType.ToLower() == "bp")
            {
                DataSet ds = ReportDO.GetBankBookReport(Convert.ToInt32(id));
                string ReportTypeName = "";
                using (ds)
                {
                    if (reportType == "br")
                        ReportTypeName = "BANK RECEIPT";
                    else
                        ReportTypeName = "BANK PAYMENT";

                    if (ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                    {
                        string totalAmountInWords = cls_util.ConvertNumberIntoWord(Convert.ToInt32(ds.Tables[1].Compute("Sum(Amount)", "")), true);
                        rptViewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath(reportPath + "BankBooks.rdlc");
                        ReportDataSource datasource = new ReportDataSource("BankBook", ds.Tables[1]);
                        ReportParameter rp = new ReportParameter("paramCompanyName", ds.Tables[1].Rows[0]["Company_Name"].ToString(), false);
                        ReportParameter rp2 = new ReportParameter("paramReportType", ReportTypeName, false);
                        ReportParameter rp3 = new ReportParameter("paramAmountInWords", totalAmountInWords, false);

                        rptViewer.LocalReport.SetParameters(new ReportParameter[] { rp, rp2, rp3 });
                        rptViewer.LocalReport.DataSources.Clear();
                        rptViewer.LocalReport.DataSources.Add(datasource);
                        // status = ErrorStatus.show.ToString();
                    }
                    else
                    {
                        //  status = ErrorStatus.NoData.ToString();
                    }
                }
            }
        }



        //private dsReports GetData(string query)
        //{
        //    // DataSet ds=new DataSet();
        //    string conString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
        //    SqlCommand cmd = new SqlCommand(query);
        //    using (SqlConnection con = new SqlConnection(conString))
        //    {
        //        using (SqlDataAdapter sda = new SqlDataAdapter())
        //        {
        //            //DataSet ds;
        //            cmd.Connection = con;
        //            sda.SelectCommand = cmd;
        //            using (dsReports ds = new dsReports())
        //            {
        //                sda.Fill(ds, "DataSet1");
        //                return ds;
        //            }

        //        }
        //    }
        //}

        //[WebMethod]
        //public static string GetReport(string id, string reportType, string reportName)
        //{

        //    return RunReport(reportType, id, reportName).ToString();

        //}

        //public static string RunReport(string reportType, string id, string reportName)
        //{
        //    string status = ErrorStatus.NoData.ToString();
        //    try
        //    {
        //        if (HttpContext.Current != null)
        //        {
        //            Page page = (Page)HttpContext.Current.Handler;
        //            ReportViewer reportViewer = (ReportViewer)page.FindControl("rptViewer");
        //            //rsweb
        //            //ReportViewer reportViewer = new ReportViewer();
        //            reportViewer.rptViewer.ProcessingMode = ProcessingMode.Local;
        //            reportViewer.rptViewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath("~/Reports/" + reportName);

        //            if (reportType == "CR")
        //            {
        //                dsReports ds = ReportDO.GetCashBookReport(Convert.ToInt32(id));
        //                using (ds)
        //                {
        //                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //                    {
        //                        ReportDataSource datasource = new ReportDataSource("DataSet1", ds.Tables[0]);
        //                        ReportParameter rp = new ReportParameter("paramCompanyName", "DEMO", false);
        //                        ReportParameter rp2 = new ReportParameter("paramReportType", "CASH RECEIPT", false);

        //                        reportViewer.rptViewer.LocalReport.SetParameters(new ReportParameter[] { rp, rp2 });
        //                        reportViewer.rptViewer.LocalReport.DataSources.Clear();
        //                        reportViewer.rptViewer.LocalReport.DataSources.Add(datasource);
        //                        status = ErrorStatus.show.ToString();
        //                    }
        //                    else
        //                    {
        //                        status = ErrorStatus.NoData.ToString();
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        status = ErrorStatus.Error.ToString();
        //    }

        //    return status;
        //}

        //protected void btnShowReport_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string status = ErrorStatus.NoData.ToString();
        //        rptViewer.ProcessingMode = ProcessingMode.Local;
        //      //  rptViewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath("~/Reports/" + reportName);

        //        //if (reportType == "CR")
        //        //{
        //        //    dsReports ds = ReportDO.GetCashBookReport(Convert.ToInt32(id));
        //        //    using (ds)
        //        //    {
        //        //        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //        //        {
        //        //            ReportDataSource datasource = new ReportDataSource("DataSet1", ds.Tables[0]);
        //        //            ReportParameter rp = new ReportParameter("paramCompanyName", "DEMO", false);
        //        //            ReportParameter rp2 = new ReportParameter("paramReportType", "CASH RECEIPT", false);

        //        //            reportViewer.rptViewer.LocalReport.SetParameters(new ReportParameter[] { rp, rp2 });
        //        //            reportViewer.rptViewer.LocalReport.DataSources.Clear();
        //        //            reportViewer.rptViewer.LocalReport.DataSources.Add(datasource);
        //        //            status = ErrorStatus.show.ToString();
        //        //        }
        //        //        else
        //        //        {
        //        //            status = ErrorStatus.NoData.ToString();
        //        //        }
        //        //    }
        //        //}
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
    }
}