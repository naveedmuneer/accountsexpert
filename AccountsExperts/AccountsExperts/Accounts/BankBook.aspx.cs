﻿using AccountsExperts.Access_Layers.CommonDataaccess;
using AccountsExperts.Access_Layers.CommonDataAccess;
using AccountsExperts.DataAccesslayer;
using AccountsExperts.DataObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace AccountsExperts.Accounts
{
    public partial class BankBook : PageBase
    {

        #region Constants

        //private const int ColumnWidth = 100;
        private const int PageSize = 10;


        #endregion

        #region MemberVariables
        // bool isChrome = false;

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["formType"]))
            {
                hidFormType.Value = Request["formType"];
            }

            if (hidFormType.Value.ToLower() == "br")
            {
                lblFormTitle.InnerText = "Bank Receipt";
            }
            else
            {
                lblFormTitle.InnerText = "Bank Payment";
            }
        }

        #endregion

        #region Private Methods

        #endregion

        #region WebMethods

        [WebMethod]
        public static string FillGrid(int pageIndex,string formType)
        {
            
            System.Data.DataSet ds = BankBookDO.GetBankBookData(PageSize, pageIndex, formType);
            return ds.GetXml();
        }

        [WebMethod]
        public static List<Company> BindCompany()
        {
            List<Company> lstCompany = new List<Company>();
            DataTable dt = new DataTable();
            dt = BankBookDO.PopulateCompany();
            foreach (DataRow dr in dt.Rows)
            {
                Company objCompany = new Company();
                objCompany.CompanyID = Convert.ToInt32(dr["pkCompany_ID"]);
                objCompany.CompanyName = Convert.ToString(dr["Company_Name"]);
                lstCompany.Add(objCompany);
            }
            return lstCompany;
        }

        [WebMethod]
        public static List<ChartOfAccount> BindVoucherBook()
        {
            List<ChartOfAccount> lstChart = new List<ChartOfAccount>();
            DataTable dt = new DataTable();
            dt = BankBookDO.PopulateBankVoucher();
            foreach (DataRow dr in dt.Rows)
            {
                ChartOfAccount objChart = new ChartOfAccount();
                objChart.pk_ChartID = Convert.ToInt32(dr["pk_ChartID"]);
                objChart.ChartName = Convert.ToString(dr["ChartName"]);
                lstChart.Add(objChart);
            }
            return lstChart;
        }

        [WebMethod]
        public static bool PostedEnteries(string BankBookIDs)
        {
            bool result = BankBookDO.InsertBankBookPostedEntries(BankBookIDs);
            return result;

           
        }

        [WebMethod]
        public static List<ChequeStatus> BindChequeStatus()
        {
            List<ChequeStatus> lstChequeStatus = new List<ChequeStatus>();
            DataTable dt = new DataTable();
            dt = BankBookDO.PopulateChequeStatus();
            foreach (DataRow dr in dt.Rows)
            {
                ChequeStatus objChequeStatus = new ChequeStatus();
                objChequeStatus.ChequeStatusId = Convert.ToInt32(dr["pkEnumerationValueId"]);
                objChequeStatus.ChequeStatusName = Convert.ToString(dr["Value"]);
                lstChequeStatus.Add(objChequeStatus);
            }
            return lstChequeStatus;
        }

        [WebMethod]
        public static List<ChartOfAccount> BindAccountName()
        {
            List<ChartOfAccount> lstCharts = new List<ChartOfAccount>();
            DataTable dt = new DataTable();
            dt = BankBookDO.PopulateAccountName();
            foreach (DataRow dr in dt.Rows)
            {
                ChartOfAccount objChartofAccount = new ChartOfAccount();
                objChartofAccount.pk_ChartID = Convert.ToInt32(dr["pk_ChartID"]);
                objChartofAccount.ChartName = Convert.ToString(dr["ChartName"]);
                lstCharts.Add(objChartofAccount);
            }
            return lstCharts;
        }


        [WebMethod(EnableSession = true)]
        public static string InsertBankBook(List<DataObjects.BankBook> BankBook, List<DataObjects.BankBookDetail> BankBookDetail)
        {
            try
            {

                DataObjects.BankBook objBankBook = new DataObjects.BankBook();
                objBankBook.InputType = BankBook[0].InputType;
                objBankBook.CompanyID = BankBook[0].CompanyID;
                objBankBook.TransactionDate = BankBook[0].TransactionDate;
                objBankBook.FkChartID = BankBook[0].FkChartID;
                objBankBook.ChequeNo = BankBook[0].ChequeNo;
                objBankBook.ChequeDate = BankBook[0].ChequeDate;
                objBankBook.Remarks = BankBook[0].Remarks;
                if (!string.IsNullOrEmpty(SessionHandler.Current.LoginId))
                {
                    objBankBook.FkUserID = Convert.ToInt32(DataAccess.Decrypt((SessionHandler.Current.LoginId), true));
                }
                else {
                    return "redirect";
                }
                objBankBook.Created_By = SessionHandler.Current.LoginName;
                bool result = BankBookDO.InsertBankBookData(objBankBook, BankBookDetail);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }

        }

        [WebMethod]
        public static string DeleteBankBook(string bankBookID)
        {
            try
            {
                bool result = BankBookDO.DeleteBankBookData(Convert.ToInt32(bankBookID));
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        [WebMethod]
        public static string GetDataForUpdate(int bankBookID)
        {
            System.Data.DataSet ds = BankBookDO.GetDataForUpdate(bankBookID);
            return ds.GetXml();
        }

        [WebMethod]
        public static string Update(List<DataObjects.BankBook> BankBook, List<DataObjects.BankBookDetail> BankBookDetail)
        {
            try
            {
           
                DataObjects.BankBook objBankBook = new DataObjects.BankBook();

                
                objBankBook.pk_BankBookID = BankBook[0].pk_BankBookID;
                objBankBook.CompanyID = BankBook[0].CompanyID;
                objBankBook.TransactionDate = BankBook[0].TransactionDate;
                objBankBook.FkChartID = BankBook[0].FkChartID;
                objBankBook.ChequeNo = BankBook[0].ChequeNo;
                objBankBook.ChequeDate = BankBook[0].ChequeDate;
                objBankBook.Remarks = BankBook[0].Remarks;
                objBankBook.Modified_By = SessionHandler.Current.LoginName;
                objBankBook.InputType = BankBook[0].InputType;

                if (!string.IsNullOrEmpty(SessionHandler.Current.LoginId))
                {
                    objBankBook.FkUserID = Convert.ToInt32(DataAccess.Decrypt((SessionHandler.Current.LoginId), true));
                }
                else
                {
                    return "redirect";
                }
               bool result = BankBookDO.UpdateBankBookData(objBankBook, BankBookDetail);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        [WebMethod]
        public static string GetLastInsertedID(string bankBookType)
        {
            try
            {
                int bankBookID = 0;
                bankBookID = BankBookDO.GetLastInsertedID(bankBookType);

                if (bankBookID > 0)
                    return bankBookID.ToString();
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        #endregion


    }
}