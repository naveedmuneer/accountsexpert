﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster1.Master" AutoEventWireup="true" CodeBehind="JournalVoucher.aspx.cs" Inherits="AccountsExperts.Accounts.JournalVoucher" %>
   
   <asp:Content ContentPlaceHolderID="cphBody" runat="Server">
    <%--Css and Java script Sections--%>

    <link href="../ThemeLayouts/css/CustomizeCss.css" rel="stylesheet" />

     <script src="../../JavaScripts/JournalVoucher.js" type="text/javascript"></script>

   <%--End Css and Java script Sections--%>
        <div class="page-title">
         
            
                <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="title-env">
                        <h1 class="title"><strong>
                            <label id="Label2" runat="server">Journal Voucher</label></strong></h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-2" style="float: right;">
                        <button class="btn btn-info" id="btnAddNew"
                            onclick="AddNew();return false;">
                            Add New</button>
                    </div>
                    <%-- <div class="col-md-2" style="float: right;">
                        <button class="btn btn-info" style="width: 100%;">
                            Post</button>
                    </div>--%>
                    <div class="col-md-1" style="float: right;">
                        <button class="btn btn-info" style="margin-left: -47px;" id="btnDelete"  onclick="openDeleteModal(DeleteJournalVoucher,gvwList)">Delete</button>
                    </div>

                </div>
            </div>

        </div>

    </div>
       
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default" id="divViewPanel">
                <div class="panel-heading" style="display: none;">
                    <h3 class="panel-title">
                        <%--<label id="lblFormTitle" runat="server"></label>--%>
                    </h3>

                    <div class="panel-options" style="display: none;">
                        <a href="#">
                            <i class="linecons-cog"></i>
                        </a>

                        <a href="#" data-toggle="panel">
                            <span class="collapse-icon">&ndash;</span>
                            <span class="expand-icon">+</span>
                        </a>

                        <a href="#" data-toggle="reload">
                            <i class="fa-rotate-right"></i>
                        </a>

                        <a href="#" data-toggle="remove">&times;
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="example-3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 7px;">
                            <div class="col-md-6">
                                <div class="col-md-12">

                                    <label class="col-sm-3 control-label" style="padding-top: 7px; margin-left: -27px;">
                                        Date Range</label>

                                    <%--<div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="daterange daterange-inline active" data-format="MMMM D, YYYY" data-start-date="September 19, 2014" data-end-date="October 3, 2014">
											<i class="fa-calendar"></i>
											<span>September 19, 2014 - October 3, 2014</span>
										</div>
                                           <%-- <input type="text" id="txtFromToDate" class="form-control daterange" data-format="YYYY-MM-DD" data-separator=",">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="linecons-calendar"></i></a>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <div class="col-sm-9">

                                        <%--<div  id="divDateRange" class="daterange daterange-inline" data-format="DD-MMM-YYYY" data-start-date="" data-end-date="" >
                                            <i class="fa-calendar"></i>
                                            <span id="txtDateRange"></span>
                                        </div>--%>
                                        <div id="divDateRange" class="input-group">
                                            <input type="text" id="txtDateRange" class="form-control daterange" data-format="DD-MMM-YYYY" data-separator=" | ">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="linecons-calendar"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-xs-12">
                                    <div class="dataTables_length" id="example-3_length" style="float: right;">
                                        <label>
                                            Show
                                        <select id="ddlPageSize" name="example-3_length" aria-controls="example-3" class="form-control input-sm" onchange="SetPageSizeHiddenValue(1,'')">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                            entries</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div data-pattern="priority-columns" data-focus-btn-icon="fa-asterisk" data-sticky-table-header="true" data-add-display-all-btn="true" data-add-focus-btn="true">
                            <div id="dvGridBody"></div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label id="lblResultEntries"></label>
                                </div>
                                <div class="col-xs-6">
                                    <div class="pagination">
                                        <div class="Pager"></div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="panel panel-default" id="divAddEditPanel" style="display: none;">
                <div class="panel-heading" style="display: none;">
                    <h3 class="panel-title">
                        <label id="Label1" runat="server">Add Cash Receipt</label>
                    </h3>

                    <div class="panel-options">
                        <a href="#">
                            <i class="linecons-cog"></i>
                        </a>

                        <a href="#" data-toggle="panel">
                            <span class="collapse-icon">&ndash;</span>
                            <span class="expand-icon">+</span>
                        </a>

                        <a href="#" data-toggle="reload">
                            <i class="fa-rotate-right"></i>
                        </a>

                        <a href="#" data-toggle="remove">&times;
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div style="float: right;">
                                    <input type="submit" class="btn btn-info" id="btnSave" value="Save" onclick="AddEditJournalVoucher(); return false;">
                                    <input type="submit" class="btn  btn-white" id="btnCancel" value="Close" onclick="RefreshModal(); return false;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="col-xs-9 input-group">
                                <div class="form-group">
                                    <label class="control-label" for="field-1">Company</label>

                                    <select name="aaa" id="ddlCompany" class="chosen-select" style="width: 400px;">
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="col-xs-9 input-group">
                                <div class="form-group">
                                    <label class="control-label" for="field-1">Voucher #</label>
                                    <input type="text" class="form-control" id="txtVoucherNo" placeholder="Auto" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="field-1">Voucher Date</label>
                                <div class="col-xs-9 input-group">
                                    <input type="text" class="form-control datepicker" data-format="dd/M/yyyy" id="txtVoucherDate">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="linecons-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-xs-9 input-group" style="width: 100%;">
                                <div class="form-group chosenContainer">
                                    <label class="control-label" for="field-1">Voucher Book</label>
                                    <select id="ddlVoucheBook" data-msg-required="The DropDownRequiredDemo field is required."
                                        data-rule-required="true"
                                        name="DropDownRequiredDemo" class="form-control chosen-select">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="col-xs-9 input-group">
                                <div class="form-group">
                                    <label class="control-label" for="field-1">Cheque #</label>
                                    <input type="text" class="form-control" id="txtChequeNo" name="firstName" data-msg-required="The Cheque Number field is required."
                                        data-rule-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-xs-9 input-group">
                                <div class="form-group">
                                    <label class="control-label" for="field-1">Cheque Date</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" data-format="dd/M/yyyy" id="txtChequeDate" name="firstName">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="linecons-calendar"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="control-label" for="field-1">Remarks</label>
                                <input type="text" class="form-control" id="txtRemarks">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="button" class="btn btn-blue" id="btnAdd" value="Add new line" onclick="AddDetailData();">
                            <table cellspacing="0" rules="all" border="1" id="gvwDetail" class="table table-small-font table-bordered table-stripe">
                                <thead>
                                    <tr class="headerStyle">
                                       <th scope="col" data-hide="expand">Account Name</th>
                                        <th scope="col" data-hide="expand">Description</th>
                                        <th scope="col" data-hide="phone">Debit</th>
                                        <th scope="col" data-hide="phone">Credit</th>
                                         <th scope="col" data-hide="expand"></th>
                                    </tr>
                                </thead>
                                <tbody id="dvBodyDetail">
                                    <tr class="rowStyle">
                                         <td style="width: 30%">
                                            <select name="Account Name" id="ddlAccountName" class="chosen-select"></select></td>
                                        <td style="width: 49%">
                                            <input id="txtDescription" class="gridDetail-description"></td>
                                        <td style="width: 20%">
                                            <input id="txtDebit" class="gridDetail-amount"></td>
                                            <td style="width: 20%">
                                            <input id="txtCreidt" class="gridDetail-amount"></td>
                                             <td style="width: 1%">
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>
                                    </tr>
                                </tbody>
                            </table>
<%--                            <div class="col-md-12">
                                <div class="col-md-4"></div>
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div class="total">
                                        <label>
                                            TOTAL
                                        </label>
                                        <input type="text" id="txtCashBookTotal" readonly="" class="totalField" value="0.00" style="color: rgb(0, 0, 0);">
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                </div>








       <%--old work--%> 
<%--          <div class="row-fluid" id="divViewPanel">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <h3>
                        <label id="lblFormTitlee" runat="server"></label>
                    </h3>
                    <button class="btn" style="margin-right: 1%; float: right;" onclick="AddNew();return false;">Add New</button>
                    <a href="" id="btnPrint" class="btn btn-primary" style="display:none;float: right;margin-right: 3px;">Print Voucher</a>
                 </div>
                <div class="box-content" id="dvgrid">
                    <div id="DataTables_Table_2_length" class="dataTables_length">
                        <label id="lblResultEntries">
                        </label>
                    </div>
                    <div id="dvGridBody"></div>

                    <div class="pagination">
                        <div class="Pager"></div>

                    </div>
                </div>

            </div>
        </div>
    </div>
         <div class="row-fluid" id="divAddEditPanel" style="display: none;">
        <div class="span12">
            <div class="box box-bordered">
                <div class="box-title">
                    <h3>
                        <label id="Label12" runat="server">Add a New Voucher</label>
                    </h3>
                </div>
                <div class="box-content">
                    <div class="span12">
                        <div class="span4"></div>
                        <div class="span4"></div>
                        <div class="span4" style="text-align: right;">
                            <input type="submit" class="btn btn-primary" id="btnSave" value="Save" onclick="AddEditJournalVoucher(); return false;">
                            <input type="submit" class="btn btn-default" id="btnCancel" value="Close" onclick="RefreshModal(); return false;">
                        </div>
                    </div>
                         <div class="span12">
                       <div class="control-group">
                            <label for="textfield" class="control-label">Company</label>
                            <div class="controls">
                                <select name="aaa" id="ddlCompany" class="input-xlarge">
                                </select>
                            </div>
                        </div>
                             </div>
                    <div class="span12">
                         <div class="span4">
                            <div class="control-group">
                                <label for="textfield" class="control-label">Voucher #</label>
                                <div class="controls">
                                    <input type="text" placeholder="Auto" name="VoucherNo" id="txtVoucherNo" readonly="true" class="input-small">
                                </div>
                            </div>
                        </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Voucher Date</label>
                                <div class="controls">
                                     <input type="text" name="Voucher Date" id="txtVoucherDate" data-rule-required="true" class="input-xlarge datepick">
                                     
                                    <%--<input type='text' gldp-id="VoucherDate" id="Text2"/>--%>
<%--      </div>
                            </div>
                    </div>

                 <div class="span11">

                        <div class="control-group">
                            <label for="textfield" class="control-label">Remarks</label>
                            <div class="controls">
                                <input type="text" placeholder="Remarks" name="remarks" id="txtRemarks" class="input-xxlarge" style="width: 100%">
                            </div>
                        </div>
                    </div>

                    <div class="span12"></div>
                    <div class="span11">

                        <div class="control-group">
                            <input type="button" class="btn btn-primary" id="btnAdd" value="Add new line" onclick="AddDetailData();">

                            <table cellspacing="0" rules="all" border="1" id="gvwDetail" class="gridDetail">
                                <thead>
                                    <tr class="headerStyle">
                                        <th scope="col" data-hide="expand">Account Name</th>
                                        <th scope="col" data-hide="expand">Description</th>
                                        <th scope="col" data-hide="phone">Debit</th>
                                          <th scope="col" data-hide="phone">Credit</th>
                                        <th scope="col" data-hide="expand"></th>
                                    </tr>
                                </thead>
                                <tbody id="dvBodyDetail">
                                    <tr class="rowStyle">
                                        <td style="width: 30%">
                                            <select name="Account Name" id="ddlAccountName" class="gridDetail-accountname"></select></td>
                                        <td style="width: 49%">
                                            <input id="txtDescription" class="gridDetail-description"></td>
                                        <td style="width: 20%">
                                            <input id="txtDebit" class="gridDetail-amount"></td>
                                         <td style="width: 20%">
                                            <input id="txtCredit" class="gridDetail-amount"></td>
                                        <td style="width: 1%">
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="span12">
                        <div class="span4"></div>
                        <div class="span4"></div>
                      <%--  <div class="span4">
                            <div class="total">
                                <label>
                                    TOTAL
                                </label>
                                <input type="text" id="txtBankBookTotal" readonly="" class="totalField" value="0.00" style="color: rgb(0, 0, 0);">
                            </div>
                        </div>--%>
<%--  </div>

                </div>
            </div>

        </div>
    </div>--%>
        <input type="hidden" id="hidEdit" value="0" />
    <input type="hidden" id="hidDelete" value="0" />
    <input type="hidden" id="hidBankBookID" value="0" />
    <input type="hidden" id="hidFormType" runat="server"  />
</asp:Content>
