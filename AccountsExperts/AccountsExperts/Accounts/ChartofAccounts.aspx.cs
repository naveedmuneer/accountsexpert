﻿using AccountsExperts.Access_Layers.CommonDataaccess;
using AccountsExperts.Access_Layers.CommonDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;


namespace AccountsExperts
{
    public partial class ChartofAccounts : PageBase
    {
        DataObjects.ChartOfAccount objChartOfAccount = new DataObjects.ChartOfAccount();

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string fillGrid(int pageIndex, string chartType, int pageSize)
        {

            string query = "[usp_GetChartOfAccount]";
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.StoredProcedure;
            if (string.IsNullOrEmpty(chartType))
            {
                cmd.Parameters.AddWithValue("@ChartType", null);
            }
            else
            {
                cmd.Parameters.AddWithValue("@ChartType", chartType);
            }
            cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
            cmd.Parameters.AddWithValue("@PageSize", pageSize);
            cmd.Parameters.Add("@RecordCount", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
            return GetData(cmd, pageIndex, chartType, pageSize).GetXml();
        }

        private static DataSet GetData(SqlCommand cmd, int pageIndex, string chartType, int pageSize)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataSet ds = new DataSet())
                    {
                        DataTable dt = new DataTable();
                        sda.Fill(ds, "ChartOfAccount");
                                               
                        dt.TableName = "Pager";
                        
                        dt.Columns.Add("PageIndex");
                        dt.Columns.Add("PageSize");
                        dt.Columns.Add("RecordCount");
                        dt.Rows.Add();
                        dt.Rows[0]["PageIndex"] = pageIndex;
                        dt.Rows[0]["PageSize"] = pageSize;
                        dt.Rows[0]["RecordCount"] = cmd.Parameters["@RecordCount"].Value;
                        ds.Tables.Add(dt);
                        return ds;
                    }
                }
            }
        }

        [WebMethod]
        public static List<ListItem> fillAccountNature()
        {

            string query = "[usp_GetAccountNature]";
            string constr = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    List<ListItem> accountNature = new List<ListItem>();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            accountNature.Add(new ListItem
                            {
                                Value = sdr["AccountNatureID"].ToString(),
                                Text = sdr["AccountNatureName"].ToString()
                            });
                        }
                    }
                    con.Close();
                    return accountNature;
                }
            }
        }

        [WebMethod]
        public static string Insert(List<DataObjects.ChartOfAccount> ChartofAccounts)
        {
            //ChartOfAccount chartofAccounts = new ChartofAccounts();
            //chartofAccounts.
            try
            {
                DataObjects.ChartOfAccount objChartOfAccount = new DataObjects.ChartOfAccount();
                DataAccesslayer.ChartOfAccountDO objChartOfAccountDO = new DataAccesslayer.ChartOfAccountDO();

                objChartOfAccount.pk_ChartID = objChartOfAccountDO.GetMaxID() + 1;
                objChartOfAccount.ChartName = ChartofAccounts[0].ChartName;
                objChartOfAccount.fk_AccountNatureID = ChartofAccounts[0].fk_AccountNatureID;
                objChartOfAccount.GroupID = ChartofAccounts[0].GroupID;
                objChartOfAccount.Description = ChartofAccounts[0].Description;
                objChartOfAccount.Address = ChartofAccounts[0].Address;
                objChartOfAccount.Email = ChartofAccounts[0].Email;
                objChartOfAccount.Phone = ChartofAccounts[0].Phone;
                objChartOfAccount.NTNNO = ChartofAccounts[0].NTNNO;
                objChartOfAccount.GSTNO = ChartofAccounts[0].GSTNO;
                objChartOfAccount.ChartRefNo = (objChartOfAccountDO.GetMaxID() + 1).ToString();
                objChartOfAccount.Active = ChartofAccounts[0].Active.ToString();
                objChartOfAccount.Created_By = SessionHandler.Current.LoginName;



                bool result = objChartOfAccountDO.insertChartOfAccountData(objChartOfAccount);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        [WebMethod]
        public static List<ListItem> fillGroupName()
        {

            string query = "[usp_GetGroupDataFromChart]";
            string constr = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    List<ListItem> accountNature = new List<ListItem>();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            accountNature.Add(new ListItem
                            {
                                Value = sdr["ChartID"].ToString(),
                                Text = sdr["ChartName"].ToString()
                            });
                        }
                    }
                    con.Close();
                    return accountNature;
                }
            }
        }

        [WebMethod]
        public static string GetDataForUpdate(string chartID)
        {
            string query = "[usp_GetDataByChartIDForEdit]";
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ChartID", Convert.ToInt32(chartID));
            return GetXmlData(cmd).GetXml();
        }

        private static DataSet GetXmlData(SqlCommand cmd)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataSet ds = new DataSet())
                    {
                        sda.Fill(ds, "UpdateData");
                        return ds;
                    }
                }
            }
        }

        [WebMethod]
        public static string Update(List<DataObjects.ChartOfAccount> ChartofAccounts)
        {
            try
            {
                DataObjects.ChartOfAccount objChartOfAccount = new DataObjects.ChartOfAccount();
                DataAccesslayer.ChartOfAccountDO objChartOfAccountDO = new DataAccesslayer.ChartOfAccountDO();

                objChartOfAccount.pk_ChartID = ChartofAccounts[0].pk_ChartID;
                objChartOfAccount.ChartName = ChartofAccounts[0].ChartName;
                objChartOfAccount.fk_AccountNatureID = ChartofAccounts[0].fk_AccountNatureID;
                objChartOfAccount.GroupID = ChartofAccounts[0].GroupID;
                objChartOfAccount.Description = ChartofAccounts[0].Description;
                objChartOfAccount.Address = ChartofAccounts[0].Address;
                objChartOfAccount.Email = ChartofAccounts[0].Email;
                objChartOfAccount.Phone = ChartofAccounts[0].Phone;
                objChartOfAccount.NTNNO = ChartofAccounts[0].NTNNO;
                objChartOfAccount.GSTNO = ChartofAccounts[0].GSTNO;
                objChartOfAccount.ChartRefNo = ChartofAccounts[0].pk_ChartID.ToString();
                objChartOfAccount.Active = ChartofAccounts[0].Active.ToString();
                bool result = objChartOfAccountDO.updateChartOfAccountData(objChartOfAccount);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        [WebMethod]
        public static string DeleteRecord(string chartID)
        {
            try
            {
                DataAccesslayer.ChartOfAccountDO objChartOfAccountDO = new DataAccesslayer.ChartOfAccountDO();

                bool result = objChartOfAccountDO.deleteChartOfAccountData(Convert.ToInt32(chartID));
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
    }
}