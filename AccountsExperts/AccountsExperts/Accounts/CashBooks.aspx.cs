﻿using AccountsExperts.Access_Layers.CommonDataaccess;
using AccountsExperts.Access_Layers.CommonDataAccess;
using AccountsExperts.DataAccesslayer;
using AccountsExperts.DataObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace AccountsExperts
{
    public partial class CashBooks : PageBase
    {

        #region Constants

        //private const int ColumnWidth = 100;
        private const int PageSize = 10;


        #endregion

        #region MemberVariables
        // bool isChrome = false;

        #endregion

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["formType"]))
            {
                hidFormType.Value = Request["formType"];

            }
            if (hidFormType.Value.ToLower() == "cr")
            {
                lblFormTitle.InnerText = "Cash Receipt";
            }
            else
            {
                lblFormTitle.InnerText = "Cash Payment";
            }

        }
        #endregion

        #region Private Methods

        #endregion

        #region WebMethods

        [WebMethod]
        public static string FillGrid(int pageIndex, string formType, int pageSize, string transactionStartDate, string transactionEndDate, string searchVoucherNo, string searchVoucherDate, string searchVoucherBook, string searchChequeNo, string searchChequeDate)
        {
            System.Data.DataSet ds = CashBookDO.GetCashBookData(pageSize, pageIndex, formType, transactionStartDate, transactionEndDate, searchVoucherNo, searchVoucherDate, searchVoucherBook, searchChequeNo, searchChequeDate);
            return ds.GetXml();
        }
        [WebMethod]
        public static string BindAllDropDowns()
        {
            System.Data.DataSet ds = CashBookDO.BindAllDropDowns();
            return ds.GetXml();
        }

        [WebMethod]
        public static List<Company> BindCompany()
        {
            List<Company> lstCompany = new List<Company>();
            DataTable dt = new DataTable();
            dt = CashBookDO.PopulateCompany();
            foreach (DataRow dr in dt.Rows)
            {
                Company objCompany = new Company();
                objCompany.CompanyID = Convert.ToInt32(dr["pkCompany_ID"]);
                objCompany.CompanyName = Convert.ToString(dr["Company_Name"]);
                lstCompany.Add(objCompany);
            }
            return lstCompany;
        }

        [WebMethod]
        public static List<ChartOfAccount> BindVoucherBook()
        {
            List<ChartOfAccount> lstChart = new List<ChartOfAccount>();
            DataTable dt = new DataTable();
            dt = CashBookDO.PopulateCashVoucher();
            foreach (DataRow dr in dt.Rows)
            {
                ChartOfAccount objChart = new ChartOfAccount();
                objChart.pk_ChartID = Convert.ToInt32(dr["pk_ChartID"]);
                objChart.ChartName = Convert.ToString(dr["ChartName"]);
                lstChart.Add(objChart);
            }
            return lstChart;
        }

        [WebMethod]
        public static List<ChequeStatus> BindChequeStatus()
        {
            List<ChequeStatus> lstChequeStatus = new List<ChequeStatus>();
            DataTable dt = new DataTable();
            dt = CashBookDO.PopulateChequeStatus();
            foreach (DataRow dr in dt.Rows)
            {
                ChequeStatus objChequeStatus = new ChequeStatus();
                objChequeStatus.ChequeStatusId = Convert.ToInt32(dr["pkEnumerationValueId"]);
                objChequeStatus.ChequeStatusName = Convert.ToString(dr["Value"]);
                lstChequeStatus.Add(objChequeStatus);
            }
            return lstChequeStatus;
        }


        [WebMethod]
        public static List<ChartOfAccount> BindAccountName()
        {
            List<ChartOfAccount> lstCharts = new List<ChartOfAccount>();
            DataTable dt = new DataTable();
            dt = CashBookDO.PopulateAccountName();
            foreach (DataRow dr in dt.Rows)
            {
                ChartOfAccount objChartofAccount = new ChartOfAccount();
                objChartofAccount.pk_ChartID = Convert.ToInt32(dr["pk_ChartID"]);
                objChartofAccount.ChartName = Convert.ToString(dr["ChartName"]);
                lstCharts.Add(objChartofAccount);
            }
            return lstCharts;
        }

        [WebMethod(EnableSession = true)]
        public static string InsertCashBook(List<DataObjects.CashBook> CashBook, List<DataObjects.CashBookDetail> CashBookDetail)
        {
            try
            {
                DateTime chequeDate = Convert.ToDateTime("1/1/1900");

                DataObjects.CashBook objCashBook = new DataObjects.CashBook();
                objCashBook.InputType = CashBook[0].InputType;
                objCashBook.CompanyID = CashBook[0].CompanyID;
                objCashBook.TransactionDate =CashBook[0].TransactionDate;
                objCashBook.FkChartID = CashBook[0].FkChartID;
                objCashBook.ChequeNo = CashBook[0].ChequeNo;
                if (CashBook[0].ChequeDate.ToString()=="1/1/0001 12:00:00 AM") {
                    objCashBook.ChequeDate = chequeDate;
                }
                else{
                objCashBook.ChequeDate = CashBook[0].ChequeDate;
                }
                objCashBook.Remarks = CashBook[0].Remarks;
                if (!string.IsNullOrEmpty(SessionHandler.Current.LoginId))
                {
                    objCashBook.FkUserID = Convert.ToInt32(DataAccess.Decrypt((SessionHandler.Current.LoginId), true));
                }
                else
                {
                    return "redirect";
                }
                objCashBook.Created_By = SessionHandler.Current.LoginName;
                bool result = CashBookDO.InsertCashBook(objCashBook, CashBookDetail);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }

        }

       
        [WebMethod]
        public static string DeleteCashBook(string cashBookID)
        {
            try
            {
                bool result = CashBookDO.DeleteCashBookData(Convert.ToInt32(cashBookID));
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }


        [WebMethod]
        public static string GetDataForUpdate(int cashBookID)
        {
            System.Data.DataSet ds = CashBookDO.GetDataForUpdate(cashBookID);
            return ds.GetXml();
        
        }

        [WebMethod]
        public static string Update(List<DataObjects.CashBook> CashBook, List<DataObjects.CashBookDetail> CashBookDetail)
        {
            try
            {
                DateTime chequeDate = Convert.ToDateTime("1/1/1900");
                DataObjects.CashBook objCashBook = new DataObjects.CashBook();


                objCashBook.pk_CashBookID = CashBook[0].pk_CashBookID;
                objCashBook.CompanyID = CashBook[0].CompanyID;
                objCashBook.TransactionDate = CashBook[0].TransactionDate;
                objCashBook.FkChartID = CashBook[0].FkChartID;
                objCashBook.ChequeNo = CashBook[0].ChequeNo;
                if (CashBook[0].ChequeDate.ToString() == "1/1/0001 12:00:00 AM")
                {
                    objCashBook.ChequeDate = chequeDate;
                }
                else
                {
                    objCashBook.ChequeDate = CashBook[0].ChequeDate;
                }
                
                objCashBook.Remarks = CashBook[0].Remarks;
                objCashBook.Modified_By = SessionHandler.Current.LoginName;
                objCashBook.InputType = CashBook[0].InputType;

                if (!string.IsNullOrEmpty(SessionHandler.Current.LoginId))
                {
                    objCashBook.FkUserID = Convert.ToInt32(DataAccess.Decrypt((SessionHandler.Current.LoginId), true));
                }
                else
                {
                    return "redirect";
                }
                bool result = CashBookDO.UpdateCashBookData(objCashBook, CashBookDetail);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }


        [WebMethod]
        public static string GetLastInsertedID(string cashBookType)
        {
            try
            {
                int CashBookID = 0;
                CashBookID = CashBookDO.GetLastInsertedID(cashBookType);

                if (CashBookID > 0)
                    return CashBookID.ToString();
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        //[WebMethod]
        //public static string Update(List<DataObjects.CashBook> CashBook, List<DataObjects.CashBookDetail> CashBookDetail)
        //{
        //   try
        //    {
        //        DataObjects.CashBook objCashBook = new DataObjects.CashBook();
        //        DataAccesslayer.CashBookDO objCashBookDO = new DataAccesslayer.CashBookDO();

        //        objCashBook.pk_CashBookID = CashBook[0].pk_CashBookID;
        //        objCashBook.CompanyID = CashBook[0].CompanyID;
        //        objCashBook.TransactionDate = CashBook[0].TransactionDate;
        //        objCashBook.fk_AccountBookID = CashBook[0].fk_AccountBookID;
        //        objCashBook.ChequeNo = CashBook[0].ChequeNo;
        //        objCashBook.ChequeDate = CashBook[0].ChequeDate;
        //        objCashBook.ChequeStatus = CashBook[0].ChequeStatus;
        //        objCashBook.Remarks = CashBook[0].Remarks;
        //        objCashBook.Posted = CashBook[0].Posted;
        //        objCashBook.Active = CashBook[0].Active.ToString();
        //        bool result = objCashBookDO.UpdateMasterDetailCashBookData(objCashBook, CashBookDetail);
        //        if (result)
        //            return "true";
        //        else
        //        {
        //            return "false";
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        return ex.Message;
        //    }
        //}

      
        //private static DataSet GetData(SqlCommand cmd, int pageIndex)
        //{
        //    string strConnString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(strConnString))
        //    {
        //        using (SqlDataAdapter sda = new SqlDataAdapter())
        //        {
        //            cmd.Connection = con;
        //            sda.SelectCommand = cmd;
        //            using (DataSet ds = new DataSet())
        //            {
        //                sda.Fill(ds, "CashBook");
        //                DataTable dt = new DataTable("Pager");
        //                dt.Columns.Add("PageIndex");
        //                dt.Columns.Add("PageSize");
        //                dt.Columns.Add("RecordCount");
        //                dt.Rows.Add();
        //                dt.Rows[0]["PageIndex"] = pageIndex;
        //                dt.Rows[0]["PageSize"] = PageSize;
        //                dt.Rows[0]["RecordCount"] = cmd.Parameters["@RecordCount"].Value;
        //                ds.Tables.Add(dt);
        //                return ds;
        //            }
        //        }
        //    }
        //}

   

        //private static DataSet GetXmlData(SqlCommand cmd)
        //{
        //    string strConnString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(strConnString))
        //    {
        //        using (SqlDataAdapter sda = new SqlDataAdapter())
        //        {
        //            cmd.Connection = con;
        //            sda.SelectCommand = cmd;
        //            using (DataSet ds = new DataSet())
        //            {
        //                sda.Fill(ds, "UpdateData");
        //                return ds;
        //            }
        //        }
        //    }
        //}

        //[WebMethod]
        //public static string GetDataForUpdateDetail(string CashBookDetailID)
        //{
        //    string query = "[usp_GetDataByCashBookDetailIDForEdit]";
        //    SqlCommand cmd = new SqlCommand(query);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@CashBookDetailID", Convert.ToInt32(CashBookDetailID));
        //    return GetXmlDetailData(cmd).GetXml();
        //}

        //private static DataSet GetXmlDetailData(SqlCommand cmd)
        //{
        //    string strConnString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(strConnString))
        //    {
        //        using (SqlDataAdapter sda = new SqlDataAdapter())
        //        {
        //            cmd.Connection = con;
        //            sda.SelectCommand = cmd;
        //            using (DataSet ds = new DataSet())
        //            {
        //                sda.Fill(ds, "UpdateDataDetail");
        //                return ds;
        //            }
        //        }
        //    }
        //}


        //[WebMethod]
        //public static string GetLastInsertedID(string cashBookType)
        //{
        //    try
        //    {
        //        DataAccesslayer.CashBookDO objCashBookDO = new DataAccesslayer.CashBookDO();

        //        int CashBookID = 0;
        //        CashBookID = objCashBookDO.GetLastInsertedID(cashBookType);

        //        if (CashBookID>0)
        //            return CashBookID.ToString();
        //        else
        //        {
        //            return "false";
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        return ex.Message;
        //    }
        //}

        #endregion
    }
}