﻿using AccountsExperts.Access_Layers.CommonDataaccess;
using AccountsExperts.Access_Layers.CommonDataAccess;
using AccountsExperts.DataAccesslayer;
using AccountsExperts.DataObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AccountsExperts.Accounts
{
    public partial class JournalVoucher : PageBase
    {

        #region Constants

        //private const int ColumnWidth = 100;
        private const int PageSize = 10;


        #endregion

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region Private Methods

        #endregion

        #region WebMethods


        [WebMethod]
        public static List<ChartOfAccount> BindVoucherBook()
        {
            List<ChartOfAccount> lstChart = new List<ChartOfAccount>();
            DataTable dt = new DataTable();
            dt = CashBookDO.PopulateCashVoucher();
            foreach (DataRow dr in dt.Rows)
            {
                ChartOfAccount objChart = new ChartOfAccount();
                objChart.pk_ChartID = Convert.ToInt32(dr["pk_ChartID"]);
                objChart.ChartName = Convert.ToString(dr["ChartName"]);
                lstChart.Add(objChart);
            }
            return lstChart;
        }



        [WebMethod]
        public static string FillGrid(int pageIndex)
        {
            System.Data.DataSet ds = JournalVoucherDO.GetJournalVoucherData(PageSize, pageIndex);
            return ds.GetXml();
        }

        [WebMethod]
        public static List<ChartOfAccount> BindAccountName()
        {
            List<ChartOfAccount> lstCharts = new List<ChartOfAccount>();
            DataTable dt = new DataTable();
            dt = JournalVoucherDO.PopulateAccountName();
            foreach (DataRow dr in dt.Rows)
            {
                ChartOfAccount objChartofAccount = new ChartOfAccount();
                objChartofAccount.pk_ChartID = Convert.ToInt32(dr["pk_ChartID"]);
                objChartofAccount.ChartName = Convert.ToString(dr["ChartName"]);
                lstCharts.Add(objChartofAccount);
            }
            return lstCharts;
        }

        [WebMethod]
        public static List<Company> BindCompany()
        {
            List<Company> lstCompany = new List<Company>();
            DataTable dt = new DataTable();
            dt = JournalVoucherDO.PopulateCompany();
            foreach (DataRow dr in dt.Rows)
            {
                Company objCompany = new Company();
                objCompany.CompanyID = Convert.ToInt32(dr["pkCompany_ID"]);
                objCompany.CompanyName = Convert.ToString(dr["Company_Name"]);
                lstCompany.Add(objCompany);
            }
            return lstCompany;
        }

        [WebMethod(EnableSession = true)]
        public static string InsertJournalVoucher(List<DataObjects.JournalVoucherMaster> JournalVoucher, List<JournalVoucherDetail> JournalVoucherDetail)
        {
            try
            {

                DataObjects.JournalVoucherMaster objJournalVoucherMaster = new DataObjects.JournalVoucherMaster();
                //objJournalVoucherMaster.JournalVoucherRefNo = "JV" + "-" + "00000" + (JournalVoucherDO.GetMaxID() + 1).ToString();
                objJournalVoucherMaster.TransactionDate = JournalVoucher[0].TransactionDate;
                objJournalVoucherMaster.fkCompanyID = JournalVoucher[0].fkCompanyID;
                objJournalVoucherMaster.Remarks = JournalVoucher[0].Remarks;
                objJournalVoucherMaster.IsPosted = 1;
                objJournalVoucherMaster.Created_By = SessionHandler.Current.LoginName;

                if (!string.IsNullOrEmpty(SessionHandler.Current.LoginId))
                {
                    objJournalVoucherMaster.fkUserId = Convert.ToInt32(DataAccess.Decrypt((SessionHandler.Current.LoginId), true));
                }
                else
                {
                    return "redirect";
                }
                bool result = JournalVoucherDO.InsertJournalVoucherData(objJournalVoucherMaster, JournalVoucherDetail);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public static string DeleteJournalVoucher(string JournalVoucherID)
        {
            try
            {
                bool result = JournalVoucherDO.DeleteJournalVoucherData(Convert.ToInt32(JournalVoucherID));
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }



        #endregion

    }
}