﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true" CodeFile="ChartofAccounts.aspx.cs" Inherits="AccountsExperts.ChartofAccounts" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphBody" runat="Server">
    <script src="../JavaScripts/ChartofAccounts.js" type="text/javascript"></script>
    <style>
        .col-sm-2 {
            width: 50%;
            float: left;
            padding-bottom: 2px;
        }

        .col-sm-4 {
            width: 100%;
            float: left;
            padding-bottom: 2px;
        }

        .wizard {
            list-style-type: none !important;
            margin: 0 0 30px 0 !important;
            padding: 0 !important;
            background: #eee !important;
            width: 100%;
        }

            .wizard li {
                display: inline-block;
                cursor: pointer;
                width: 49%;
            }

            .wizard .active {
                background: #FFF !important;
                border: solid #eee 1px;
            }

                .wizard .active .single-step .title {
                    background: #368ee0 !important;
                    padding: 5px 5px 5px 5px;
                }

            .wizard li .single-step {
                padding: 15px 0 !important;
            }

            .wizard .steps-3 {
                list-style-type: none;
                margin: 0 0 30px 0;
                padding: 0;
                background: #eee;
            }

        .error {
            color: #b94a48;
            border-color: #b94a48 !important;
        }

        .control-group.error {
            border-color: #b94a48;
        }
    </style>
    <input type="hidden" id="hidEdit" value="0" />
    <input type="hidden" id="hidDelete" value="0" />
    <input type="hidden" id="hidAllPageSize" value="25" />
    <input type="hidden" id="hidChartType" value="" />
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <h3>Chart of Accounts
                    </h3>
                    <button class="btn" style="margin-right: 1%; float: right;" onclick="AddNew();return false;">Add Account</button>
                </div>
                <ul class="nav nav-tabs" style="padding-top: 24px;">
                    <li class="active"><a data-toggle="tab" href="#all" onclick="OnTabChange('')">All Accounts</a></li>
                    <li><a data-toggle="tab" href="#all" onclick="OnTabChange('Assets')">Assets</a></li>
                    <li><a data-toggle="tab" href="#all" onclick="OnTabChange('LIABILITIES')">Liabilities</a></li>
                    <li><a data-toggle="tab" href="#all" onclick="OnTabChange('Equity')">Capital</a></li>
                    <li><a data-toggle="tab" href="#all" onclick="OnTabChange('SALES')">Revenue</a></li>
                    <li><a data-toggle="tab" href="#all" onclick="OnTabChange('COST OF SALES')">Cost of Sales</a></li>
                    <li><a data-toggle="tab" href="#all" onclick="OnTabChange('Expenses')">Expenses</a></li>
                </ul>
                <div class="tab-content">
                    <div id="all" class="tab-pane fade in active">
                        <div class="box-content" id="dvgrid">
                            <div id="DataTables_Table_2_length" class="dataTables_length">
                                <div id="DataTables_Table_0_length" class="dataTables_length">
                                    <label id="lblResultEntries" style="float: left;">
                                    </label>
                                </div>
                                <div style="float: right; margin-bottom: 5px;" class="dataTables_filter" id="DataTables_Table_0_filter">
                                    <input style="float: right; margin-left: 3px;" class="btn" type="button" id="btnSearch" value="Search" />
                                    <input type="text" aria-controls="DataTables_Table_0" placeholder="Search here...">
                                </div>
                                <%-- <input type="text" id="txtSearchAll" style="float:right;"/>
                                <input type="button" id="btnSearchAll" style="float:right;" class="btn" value="Search"/>--%>
                            </div>
                            <div id="dvGridBody" style="padding-top: 4px"></div>

                            <div class="pagination">

                                <div style='float: Left;'>
                                    showing
                                    <select id='ddlPageSize' style='width: 57px;' onchange="SetPageSizeHiddenValue(1,'')">
                                        <option value='25'>25</option>
                                        <option value='50'>50</option>
                                        <option value='100'>100</option>
                                    </select>
                                    items per page
                                </div>
                                <div class="Pager" id="dvAllPaging">
                                </div>

                            </div>
                        </div>
                    </div>
                    <%--<div id="asset" class="tab-pane fade in">
                        <div class="box-content" id="Div2">
                            <div id="dvAsset" class="dataTables_length">
                                <label id="lblAssetResultEntries" style="float: left;">
                                </label>
                                <div style="float: right; margin-bottom: 5px;" class="dataTables_filter" id="Div3">
                                    <input style="float: right; margin-left: 3px;" class="btn" type="button" id="Button1" value="Search" />
                                    <input type="text" aria-controls="DataTables_Table_0" placeholder="Search here...">
                                </div>
                            </div>
                            <div id="dvAssetGridBody" style="padding-top: 4px"></div>

                            <div class="pagination">
                                <div style='float: Left;'>
                                    showing
                                    <select id='ddlAssetPageSize' style='width: 57px;' onchange="SetPageSizeHiddenValue(1,'Assets')">
                                        <option value='25'>25</option>
                                        <option value='50'>50</option>
                                        <option value='100'>100</option>
                                    </select>
                                    items per page
                                </div>
                                <div class="Pager" id="dvAssetPaging"></div>

                            </div>
                        </div>
                    </div>

                    <div id="equity" class="tab-pane fade in">
                        <div class="box-content" id="Div1">
                            <div id="Div5" class="dataTables_length">
                                <label id="lblEquityResultEntries" style="float: left;">
                                </label>
                                <div style="float: right; margin-bottom: 5px;" class="dataTables_filter" id="Div7">
                                    <input style="float: right; margin-left: 3px;" class="btn" type="button" id="Button3" value="Search" />
                                    <input type="text" aria-controls="DataTables_Table_0" placeholder="Search here...">
                                </div>
                            </div>
                            <div id="dvEquityGridBody" style="padding-top: 4px"></div>

                            <div class="pagination">
                                <div style='float: Left;'>
                                    showing
                                    <select id='ddlEquityPageSize' style='width: 57px;' onchange="SetPageSizeHiddenValue(1,'Equity')">
                                        <option value='25'>25</option>
                                        <option value='50'>50</option>
                                        <option value='100'>100</option>
                                    </select>
                                    items per page
                                </div>
                                <div class="Pager" id="dvEquityPaging"></div>

                            </div>
                        </div>
                    </div>
                    <div id="expenses" class="tab-pane fade in">
                        <div class="box-content" id="Div10">
                            <div id="Div11" class="dataTables_length">
                                <label id="lblExpenseResultEntries" style="float: left;">
                                </label>
                                <div style="float: right; margin-bottom: 5px;" class="dataTables_filter" id="Div8">
                                    <input style="float: right; margin-left: 3px;" class="btn" type="button" id="Button4" value="Search" />
                                    <input type="text" aria-controls="DataTables_Table_0" placeholder="Search here...">
                                </div>
                            </div>
                            <div id="dvExpenseGridBody"></div>

                            <div class="pagination">
                                <div style='float: Left;'>
                                    showing
                                    <select id='ddlExpensePageSize' style='width: 57px;' onchange="SetPageSizeHiddenValue(1,'Expense')">
                                        <option value='25'>25</option>
                                        <option value='50'>50</option>
                                        <option value='100'>100</option>
                                    </select>
                                    items per page
                                </div>
                                <div class="Pager" id="dvExpensePaging"></div>

                            </div>
                        </div>
                    </div>--%>


                </div>
            </div>
        </div>
    </div>



    <div id="AddNewModal" style="display: none; width: 500px;" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #eee;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="AddModalTitle">Add Account</h4>
                </div>
                <div class="modal-body">
                    <div class="row-fluid">
                         <div class="span12" style="display:none;">
                             </div>
                        <div class="span12">
                            <div class="span12">
                                <div class="form-group">
                                    <label for="textfield" class="control-label">Account Nature</label>
                                    <select name="aaa" id="ddlAccountNature" data-rule-required="true" style="width: 100%">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="span12">
                            <div class="span3">
                                <div class="form-group">
                                    <label for="textfield" class="control-label">Code</label>

                                    <input type="text" placeholder="Auto" name="Code" id="txtCode" disabled style="width:40%">
                                </div>
                            </div>
                        </div>

                        <div class="span12">
                                <div class="form-group">
                                    <label for="textfield" class="control-label">Account Name</label>

                                    <input type="text" placeholder="Account Name"  name="AccountName" id="txtAccountName" data-rule-required="true" style="width:97%">
                                </div>
                            </div>

                        <div class="span12">
                            <div class="form-group">
                                <label for="textfield" class="control-label">Sub Account Of</label>

                                <select name="Group Name" id="ddlGroup" data-rule-required="true" style="width:100%">
                                </select>

                            </div>
                        </div>

                        <div class="span12">
                            <div class="form-group">
                                <label for="textfield" class="control-label">Description</label>

                                <textarea style="width: 97%; height: 80px;" placeholder="Description" name="description" id="txtDescription" data-rule-required="true">
                                       </textarea>

                            </div>
                        </div>

                        <div class="span6">
                            <div class="form-group">
                                <label for="policy" class="control-label" style="width: 110%;">Active</label>

                                <label class="checkbox">
                                    <input type="checkbox" name="policy" value="true" id="chkActive" data-rule-required="true"></label>

                            </div>

                        </div>

                        <div class="span12">
                            <div class="span12">
                                <div class="form-group">
                                    <label for="textfield" class="control-label">Address</label>

                                    <input type="text" name="textfield" id="txtAddress" placeholder="Address" style="width:97%;">
                                </div>
                            </div>

                        </div>

                         

                        <div class="span12">
                            <div class="span6">
                                <div class="form-group">
                                    <label for="textfield" class="control-label">Phone</label>

                                    <input type="text" name="textfield" id="txtPhone" placeholder="Phone" style="width:94%;">
                                </div>
                            </div>
                            <div class="span6">
                                <div class="form-group">
                                    <label for="textfield" class="control-label">Fax</label>

                                    <input type="text" name="textfield" id="txtFax" placeholder="Address" style="width:94%;">
                                </div>
                            </div>
                        </div>

                        <div class="span12">
                                <div class="form-group">
                                    <label for="textfield" class="control-label">Email</label>

                                    <input type="text" name="textfield" data-rule-email="true" id="txtEmail" placeholder="Email" style="width:97%">
                                </div>
                            </div>

                        <div class="span12">
                            <div class="span6">
                                <div class="form-group">
                                    <label for="textfield" class="control-label">GST#</label>

                                    <input type="text" name="textfield" id="txtGst" placeholder="Phone" style="width:94%;">
                                </div>
                            </div>

                            <div class="span6">
                                <div class="form-group">
                                    <label for="textfield" class="control-label">NTN#</label>

                                    <input type="text" name="textfield" id="txtNtn" placeholder="Phone" style="width:94%;">
                                </div>
                            </div>
                        </div>

                    </div>



                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="refreshModal();">Close</button>
            <input type="submit" class="btn btn-primary" id="btnSave" value="Save" onclick="addEditChartOfAccount(); return false;">
        </div>
    </div>

    <%--<div id="AddNewModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="windowTitleLabel">
        <div class="modal-header">
            <h3>Add Account</h3>
        </div>
        <div class="modal-body">
            <div class="tabbable">
                <!-- Only required for left/right tabs -->

                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <div class="control-group">
                            <label for="textfield" class="control-label">Account Nature</label>
                            <div class="controls">
                                <select name="aaa" id="ddlAccountNature" data-rule-required="true">
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Code</label>
                            <div class="controls">
                                <input type="text" placeholder="Auto" name="Code" id="txtCode" disabled>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Voucher #</label>
                            <div class="controls">
                                <input type="text" placeholder="Auto" name="VoucherNo" id="txtVoucherNo" readonly="true">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Account Name</label>
                            <div class="controls">
                                <input type="text" placeholder="Account Name" name="AccountName" id="txtAccountName" data-rule-required="true">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Sub Account Of</label>
                            <div class="controls">
                                <select name="Group Name" id="ddlGroup" data-rule-required="true">
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Description</label>
                            <div class="controls">
                                <textarea style="width: 70%; height: 80px;" placeholder="Description" name="description" id="txtDescription" data-rule-required="true">
                                       </textarea>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="policy" class="control-label" >Active</label>
                            <div class="controls">
                                <label class="checkbox">
                                    <input type="checkbox" name="policy" value="true" id="Checkbox1" data-rule-required="true"></label>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Address</label>
                            <div class="controls">
                                <input type="text" name="textfield" id="txtAddress" placeholder="Address">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Phone</label>
                            <div class="controls">
                                <input type="text" name="textfield" id="txtPhone" placeholder="Phone">
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="textfield" class="control-label">Email</label>
                            <div class="controls">
                                <input type="text" name="textfield" data-rule-email="true" id="txtEmail" placeholder="Email">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Fax</label>
                            <div class="controls">
                                <input type="text" name="textfield" id="txtFax" placeholder="Address">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">GST#</label>
                            <div class="controls">
                                <input type="text" name="textfield" id="txtGst" placeholder="Phone">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">NTN#</label>
                            <div class="controls">
                                <input type="text" name="textfield" id="txtNtn" placeholder="Phone">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-primary" id="btnSave" value="Save" onclick="AddEditBankBook(); return false;">
           <button type="button" class="btn btn-default" id="Button1" data-dismiss="modal" onclick="refreshModal();">Close</button>
        </div>

    </div>--%>

    <script type="text/javascript">
        function WizardShowHide(dv) {
            if (dv.id === "firstStep") {
                $("#firstStep").attr("style", "display:none;");
                $("#secondStep").attr("style", "display:block;");
            }
            else {
                $("#firstStep").attr("style", "display:block;");
                $("#secondStep").attr("style", "display:none;");

            }
        }

    </script>
</asp:Content>

