﻿using AccountsExperts.Access_Layers.CommonDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AccountsExperts
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionHandler.Current.LoginId = null;
            Response.Redirect("Login.aspx");
        }
    }
}