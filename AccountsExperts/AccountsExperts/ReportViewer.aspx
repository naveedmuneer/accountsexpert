﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true" CodeBehind="~/ReportViewer.aspx.cs" Inherits="AccountsExperts.ReportViewer" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:content ID="Content3" ContentPlaceHolderID="cphBody" runat="Server">


   
         <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" Height="1000px"></rsweb:ReportViewer>
    
   <%--<asp:Button ID="btnShowReport" runat="server" OnClick="btnShowReport_Click" Visible="false"/>--%>
  
    <script>
        function ShowMessageBox(msg, messageType) {

            if (messageType === "error") {
                $("#divMessage").attr("class", "alert alert-danger alert-dismissable");
                $("#divMessage").attr("style", "display: block; height: 31px; border-radius: 6px;");
            }
            else if (messageType === "success") {
                $("#divMessage").attr("class", "alert alert-success alert-dismissable");
                $("#divMessage").attr("style", "display: block; height: 31px; border-radius: 6px;");
            }
            else {

                $("#divMessage").attr("class", "alert alert-success alert-dismissable");
                $("#divMessage").attr("style", "display: none; height: 31px; border-radius: 6px;");
            }

            $("#lblMsg").text(msg);

            $("#divMessage").delay(3200).fadeOut(300);
        }

    </script>

</asp:content>