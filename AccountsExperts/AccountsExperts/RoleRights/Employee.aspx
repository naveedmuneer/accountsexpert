﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster1.Master" AutoEventWireup="true" CodeBehind="Employee.aspx.cs" Inherits="AccountsExperts.Role_Rights.Employee" %>

<%--Css and Java script Sections--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" runat="server">

    <link href="../ThemeLayouts/css/CustomizeCss.css" rel="stylesheet" />
    <script src="../../JavaScripts/Employee.js" type="text/javascript"></script>

  



    <%--End Css and Java script Sections--%>
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <%--<h3>--%>
                        <label id="lblFormTitle" runat="server"></label>
                    </h3>
                    <button class="btn" style="margin-right: 1%; float: right;" onclick="AddNew();return false;" >Add New</button>
                </div>
                <div class="box-content" id="dvgrid">
                    <div id="DataTables_Table_2_length" class="dataTables_length">
                        <label id="lblResultEntries">
                        </label>
                    </div>
                    <div id="dvGridBody"></div>

                    <div class="pagination">
                        <div class="Pager"></div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="mdlAddNew" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="windowTitleLabel">
        <div class="modal-header">
            <h3>Add/Edit Employee</h3>
        </div>
        <div class="modal-body">
            <div class="tabbable">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <div class="control-group">
                            <label for="textfield" class="control-label">Employee Name</label>
                            <div class="controls">
                                <input type="text" placeholder="Name" name="Name" id="txtName">
                             </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">NIC</label>
                            <div class="controls">
                             <input type="text" placeholder="NIC" name="NIC" id="txtNIC">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Email</label>
                            <div class="controls">
                               <input type="text" placeholder="Email" name="Email" id="txtEmail">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Company Name</label>
                            <div class="controls">
                               <select name="aaa" id="ddlCompany">
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Department</label>
                            <div class="controls">
                                <select name="aaa" id="ddlDepartment">
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Designation</label>
                            <div class="controls">
                               <select name="aaa" id="ddlDesignation">
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Contact No</label>
                            <div class="controls">
                              <input type="text" placeholder="Contact" name="Contact" id="txtContact">
                                </select>

                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Secondary Contact</label>
                            <div class="controls">
                                <input type="text" placeholder="SecondaryContact" name="SecondaryContact" id="txtSecondaryContact">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="policy" class="control-label">Address</label>
                            <div class="controls">
                                <input type="text" placeholder="Address" name="SecondaryContact" id="txtAddress">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="policy" class="control-label">Permenant Address</label>
                            <div class="controls">
                                <input type="text" placeholder="PermenantAddress" name="PermenantAddress" id="txtPermenantAddress">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Is Active</label>
                            <div class="controls">
                              <label class="checkbox">
                                 <input type="checkbox" name="policy" value="true" id="chkActive" data-rule-required="true"></label>
                            </div>
                        </div>
                       <%-- <div class="control-group">
                            <label for="textfield" class="control-label">Created On</label>
                            <div class="controls">
                                 <input type="text" placeholder="CreatedOn" name="CreatedOn" id="txtCreatedOn">
                            </div>
                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-primary" id="btnSave" value="Save" onclick="AddEditEmployee(); return false;">
            <button type="submit" class="btn btn-default" id="btnCancel" data-dismiss="modal" onclick="refreshModal();">Close</button>
        </div>

    </div>
      <%--Hidden fields--%>
    <input type="hidden" id="hidEdit" value="0" />
    <input type="hidden" id="hidDelete" value="0" />
    <input type="hidden" id="hidFormType" runat="server" value="CR" />
    </asp:Content>