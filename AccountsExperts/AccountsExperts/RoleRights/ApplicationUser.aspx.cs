﻿using AccountsExperts.Access_Layers.CommonDataaccess;
using AccountsExperts.Access_Layers.CommonDataAccess;
using AccountsExperts.DataAccesslayer;
using AccountsExperts.DataObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AccountsExperts.RoleRights
{

    public partial class ApplicationUser : PageBase
    {
        #region Constant

        private const int PageSize = 10;

        #endregion

        #region Event Handler

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        #endregion

        #region WebMethods

        [WebMethod]
        public static string FillGrid(int pageIndex)
        {
            try
            {
                System.Data.DataSet ds = ApplicationUserDO.GetApplicationUserData(PageSize, pageIndex);
                return ds.GetXml();
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }

        [WebMethod]
        public static dynamic BindUser()
        {
            try
            {
                List<AccountsExperts.DataObjects.Employee> lstEmployee = new List<AccountsExperts.DataObjects.Employee>();
                DataTable dt = new DataTable();
                dt = ApplicationUserDO.PopulateUser();
                foreach (DataRow dr in dt.Rows)
                {
                    AccountsExperts.DataObjects.Employee objUser = new AccountsExperts.DataObjects.Employee();
                    objUser.pk_Employee_ID = Convert.ToInt32(dr["pk_Employee_ID"]);
                    objUser.Employee_Name = Convert.ToString(dr["Employee_Name"]);
                    lstEmployee.Add(objUser);
                }

                return lstEmployee;
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }

          }

        [WebMethod]
        public static string DeleteApplicationUser(string applicationUserID)
        {
            try
            {
                bool result = ApplicationUserDO.DeleteApplicationUserData(Convert.ToInt32(applicationUserID));
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }

        [WebMethod]
        public static string GetDataForUpdate(int applicationUserID)
        {
            try
            {
                System.Data.DataSet ds = ApplicationUserDO.GetDataForUpdate(applicationUserID);
                return ds.GetXml();
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }

        [WebMethod(EnableSession = true)]
        public static string InsertApplicationUser(List<DataObjects.ApplicationUser> applicationUser)
        {
            try
            {

                DataObjects.ApplicationUser objAppUser = new DataObjects.ApplicationUser();
                objAppUser.PkUserID = applicationUser[0].PkUserID;
                objAppUser.User_Name= applicationUser[0].User_Name;
                objAppUser.Password = DataAccess.Encrypt(Convert.ToString(applicationUser[0].Password),true);
                objAppUser.FkEmployee_ID = applicationUser[0].FkEmployee_ID;
                objAppUser.Is_Active = applicationUser[0].Is_Active;
                objAppUser.Created_By = SessionHandler.Current.LoginName;
                objAppUser.Created_On = applicationUser[0].Created_On;
                bool result = ApplicationUserDO.InsertApplicationUserData(objAppUser);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }

        }

        [WebMethod]
        public static string UpdateApplicationUser(List<DataObjects.ApplicationUser> ApplicationUser)
        {
            try
            {
                DataObjects.ApplicationUser objApplicationUser = new DataObjects.ApplicationUser();
                

                objApplicationUser.PkUserID = ApplicationUser[0].PkUserID;
                objApplicationUser.User_Name = ApplicationUser[0].User_Name;
                objApplicationUser.Password = DataAccess.Encrypt(ApplicationUser[0].Password,true);
                objApplicationUser.FkEmployee_ID = ApplicationUser[0].FkEmployee_ID;
                objApplicationUser.Is_Active = ApplicationUser[0].Is_Active;
                bool result = ApplicationUserDO.UpdateApplicationUser(objApplicationUser);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }

        #endregion
    }
}