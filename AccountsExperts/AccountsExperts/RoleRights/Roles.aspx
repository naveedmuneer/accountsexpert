﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster1.Master" AutoEventWireup="true" CodeBehind="Roles.aspx.cs" Inherits="AccountsExperts.RoleRights.Roles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" runat="server">

     <%--Css and Java script Sections--%>
    <link href="../ThemeLayouts/css/CustomizeCss.css" rel="stylesheet" />
    <script src="../JavaScripts/Roles.js"></script>
    <%--End Css and Java script Sections--%>

     <div class="page-title">

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="title-env">
                        <h1 class="title">Roles</h1>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="col-md-2" style="float: right;">
                        <button class="btn btn-blue" style="" onclick="AddNew();return false;">Add New</button>
                    </div>
                    <div class="col-md-1" style="float: right;">
                        <button class="btn btn-blue" style="margin-left: -18px;">
                            Post</button>
                    </div>
                    <div class="col-md-2" style="float: right;">
                        <button class="btn btn-blue" style="">Delete</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default" id="divViewPanel">
                <div class="panel-heading" style="display: none;">
                    <h3 class="panel-title">
                        <label id="lblFormTitle" runat="server"></label>
                    </h3>

                    <div class="panel-options" style="display: none;">
                        <a href="#">
                            <i class="linecons-cog"></i>
                        </a>

                        <a href="#" data-toggle="panel">
                            <span class="collapse-icon">&ndash;</span>
                            <span class="expand-icon">+</span>
                        </a>

                        <a href="#" data-toggle="reload">
                            <i class="fa-rotate-right"></i>
                        </a>

                        <a href="#" data-toggle="remove">&times;
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="example-3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 7px;">
                            <div class="col-md-6">
                                <div class="col-md-12">

                                    <label class="col-sm-3 control-label" style="padding-top: 11px;">
                                        Date Range</label>
                                    <%--<div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="daterange daterange-inline active" data-format="MMMM D, YYYY" data-start-date="September 19, 2014" data-end-date="October 3, 2014">
											<i class="fa-calendar"></i>
											<span>September 19, 2014 - October 3, 2014</span>
										</div>
                                           <%-- <input type="text" id="txtFromToDate" class="form-control daterange" data-format="YYYY-MM-DD" data-separator=",">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="linecons-calendar"></i></a>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <div class="col-sm-9">

                                        <div  id="divDateRange" class="daterange daterange-inline" data-format="DD-MMM-YYYY" data-start-date="" data-end-date="" >
                                            <i class="fa-calendar"></i>
                                            <span id="txtDateRange"></span>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-xs-12">
                                    <div class="dataTables_length" id="example-3_length" style="float: right;">
                                        <label>
                                            Show
                                        <select id="ddlPageSize" name="example-3_length" aria-controls="example-3" class="form-control input-sm" onchange="SetPageSizeHiddenValue(1,'')">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                            entries</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div data-pattern="priority-columns" data-focus-btn-icon="fa-asterisk" data-sticky-table-header="true" data-add-display-all-btn="true" data-add-focus-btn="true">
                            <div id="dvGridBody"></div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label id="lblResultEntries"></label>
                                </div>
                                <div class="col-xs-6">
                                    <div class="pagination">
                                        <div class="Pager"></div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
           
            
        </div>
    </div>


      <div id="mdlAddNew" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="windowTitleLabel">
        <div class="modal-header">
            <h3>Add/Edit Roles</h3>
        </div>
        <div class="modal-body">
            <div class="tabbable">
                
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        
                        
                        <div class="control-group">
                            <label for="textfield" class="control-label">Role</label>
                            <div class="controls">
                               <%-- <input type="text" placeholder="Enter Role" name="txtRole" id="txtRoleName" data-rule-required="true" >--%>
                            </div>
                        </div>
                    
                        <div class="control-group">
                            <label for="policy" class="control-label">Active</label>
                            <div class="controls">
                                <label class="checkbox">
                                   <%-- <input type="checkbox" name="policy"   id="chkActive" ></label>--%>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-primary" id="btnSave" value="Save" onclick="AddEditRoles(); return false;">
            <button type="submit" class="btn btn-default" id="btnCancel" data-dismiss="modal" onclick="refreshModal();">Close</button>
        </div>

    </div>
     <input type="hidden" id="hidEdit" value="0" />
    <input type="hidden" id="hidDelete" value="0" />

</asp:Content>


