﻿using AccountsExperts.Access_Layers.CommonDataaccess;
using AccountsExperts.Access_Layers.CommonDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AccountsExperts.DataAccesslayer;
using System.Web.Services;

namespace AccountsExperts.RoleRights
{
    public partial class UserMapping : PageBase
    {

        #region Constants

        //private const int ColumnWidth = 100;
        private const int PageSize = 10;


        #endregion

        #region Web Methods

        [System.Web.Services.WebMethod]
        public static string BindUsersDD()
        {
            try
            {
                UserMappingDO objDO = new UserMappingDO();

                System.Data.DataSet dt = new System.Data.DataSet();
                dt = objDO.GetActiveUsers();

                dt.Tables[0].TableName = "ddlUsers";

                return dt.GetXml();
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod]
        public static string BindRolesDD()
        {
            try
            {
                UserMappingDO objDO = new UserMappingDO();

                System.Data.DataSet ds = new System.Data.DataSet();
                ds = objDO.GetActiveRoles();

                ds.Tables[0].TableName = "ddlRoles";

                return ds.GetXml();
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }
       
        [WebMethod]
        public static string FillGrid(int pageIndex)
        {
            try
            {
                System.Data.DataSet ds = UserMappingDO.GetUserMappingData(PageSize, pageIndex);
                return ds.GetXml();
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }
        [WebMethod]
        public static string InsertUserMapping(string User, string Roles)
        {
            try
            {
                DataObjects.UserMapping objUserMapping = new DataObjects.UserMapping();

                objUserMapping.fkUser_ID = User;
                objUserMapping.fkRole_ID = Roles;
               


                bool result = UserMappingDO.InsertUserMapping(objUserMapping);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }

        }
        [WebMethod]
        public static string GetDataForUpdate(int UserID)
        {
            try
            {
                System.Data.DataSet ds = UserMappingDO.GetDataForUpdate(UserID);
                return ds.GetXml();
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }

        [WebMethod]
        public static string UpdateUserMapping(string User, string Roles)
        {
            try
            {
                DataObjects.UserMapping objUserMapping = new DataObjects.UserMapping();



                objUserMapping.fkUser_ID = User;
                objUserMapping.fkRole_ID = Roles;


                bool result = UserMappingDO.UpdateUserMappers(objUserMapping);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }

        }
        [WebMethod]
        public static string DeleteUserMapping(string UserID)
        {
            try
            {
                bool result = UserMappingDO.DeletUserMappers(Convert.ToInt32(UserID));
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion
    }

}