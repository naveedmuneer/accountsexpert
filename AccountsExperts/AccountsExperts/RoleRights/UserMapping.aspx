﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster1.Master" AutoEventWireup="true" CodeBehind="UserMapping.aspx.cs" Inherits="AccountsExperts.RoleRights.UserMapping" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" runat="server">
    <%--Css and Java script Sections--%>
    <link href="../ThemeLayouts/css/CustomizeCss.css" rel="stylesheet" />
    
    <script src="../JavaScripts/UserMapping.js"></script>
   <%-- <script src="../JavaScripts/jquery-1.10.2.min.js"></script>--%>
    <script src="../JavaScripts/utilities.js"></script>
    <script src="../JavaScripts/chosen.jquery.js"></script>
    <%--End Css and Java script Sections--%>
    <link href="../App_Themes/Theme1/SoftCrmTheme.css" rel="stylesheet" />
   
    
    <div id="div_UserDD">  </div>
    <div id="div_RoleDD">      </div>
     
                   
    <div class="modal-footer">
            <input type="submit" class="btn btn-primary" id="btnSave" value="Save" onclick="AddEditRoles(); return false;">
            <button type="submit" class="btn btn-default" id="btnCancel" data-dismiss="modal" onclick="refreshModal();">Close</button>
        </div>


       <div class="page-title">

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="title-env">
                        <h1 class="title">User Mapping</h1>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="col-md-2" style="float: right;">
                        <button class="btn btn-blue" style="" onclick="AddNew();return false;">Add New</button>
                    </div>
                    <div class="col-md-1" style="float: right;">
                        <button class="btn btn-blue" style="margin-left: -18px;">
                            Post</button>
                    </div>
                    <div class="col-md-2" style="float: right;">
                        <button class="btn btn-blue" style="">Delete</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default" id="divViewPanel">
                <div class="panel-heading" style="display: none;">
                    <h3 class="panel-title">
                        <label id="lblFormTitle" runat="server"></label>
                    </h3>

                    <div class="panel-options" style="display: none;">
                        <a href="#">
                            <i class="linecons-cog"></i>
                        </a>

                        <a href="#" data-toggle="panel">
                            <span class="collapse-icon">&ndash;</span>
                            <span class="expand-icon">+</span>
                        </a>

                        <a href="#" data-toggle="reload">
                            <i class="fa-rotate-right"></i>
                        </a>

                        <a href="#" data-toggle="remove">&times;
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="example-3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 7px;">
                            <div class="col-md-6">
                                <div class="col-md-12">

                                    <label class="col-sm-3 control-label" style="padding-top: 11px;">
                                        Date Range</label>
                                    <%--<div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="daterange daterange-inline active" data-format="MMMM D, YYYY" data-start-date="September 19, 2014" data-end-date="October 3, 2014">
											<i class="fa-calendar"></i>
											<span>September 19, 2014 - October 3, 2014</span>
										</div>
                                           <%-- <input type="text" id="txtFromToDate" class="form-control daterange" data-format="YYYY-MM-DD" data-separator=",">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="linecons-calendar"></i></a>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <div class="col-sm-9">

                                        <div  id="divDateRange" class="daterange daterange-inline" data-format="DD-MMM-YYYY" data-start-date="" data-end-date="" >
                                            <i class="fa-calendar"></i>
                                            <span id="txtDateRange"></span>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-xs-12">
                                    <div class="dataTables_length" id="example-3_length" style="float: right;">
                                        <label>
                                            Show
                                        <select id="ddlPageSize" name="example-3_length" aria-controls="example-3" class="form-control input-sm" onchange="SetPageSizeHiddenValue(1,'')">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                            entries</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div data-pattern="priority-columns" data-focus-btn-icon="fa-asterisk" data-sticky-table-header="true" data-add-display-all-btn="true" data-add-focus-btn="true">
                            <div id="dvGridBody"></div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label id="lblResultEntries"></label>
                                </div>
                                <div class="col-xs-6">
                                    <div class="pagination">
                                        <div class="Pager"></div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="panel panel-default" id="divAddEditPanel" style="display: none;">
                <div class="panel-heading" style="display: none;">
                    <h3 class="panel-title">
                        <label id="Label1" runat="server">Add Cash Receipt</label>
                    </h3>

                    <div class="panel-options">
                        <a href="#">
                            <i class="linecons-cog"></i>
                        </a>

                        <a href="#" data-toggle="panel">
                            <span class="collapse-icon">&ndash;</span>
                            <span class="expand-icon">+</span>
                        </a>

                        <a href="#" data-toggle="reload">
                            <i class="fa-rotate-right"></i>
                        </a>

                        <a href="#" data-toggle="remove">&times;
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div style="float: right;">
                                    <input type="submit" class="btn btn-blue" id="Submit1" value="Save" onclick="AddEditCashBook(); return false;">
                                    <input type="submit" class="btn  btn-gray" id="Submit2" value="Close" onclick="RefreshModal(); return false;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="col-xs-9 input-group">
                                <div class="form-group">
                                    <label class="control-label" for="field-1">Company</label>

                                    <select name="aaa" id="ddlCompany" class="form-control">
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="col-xs-9 input-group">
                                <div class="form-group">
                                    <label class="control-label" for="field-1">Voucher #</label>
                                    <input type="text" class="form-control" id="txtVoucherNo" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="field-1">Voucher Date</label>
                                <div class="col-xs-9 input-group">
                                    <input type="text" class="form-control datepicker" data-format="yyyy-mm-dd" id="txtVoucherDate">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="linecons-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-xs-9 input-group">
                                <div class="form-group">
                                    <label class="control-label" for="field-1">Voucher Book</label>
                                    <select id="ddlVoucheBook" data-msg-required="The DropDownRequiredDemo field is required."
                                        data-rule-required="true"
                                        name="DropDownRequiredDemo" class="form-control">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="col-xs-9 input-group">
                                <div class="form-group">
                                    <label class="control-label" for="field-1">Cheque #</label>
                                    <input type="text" class="form-control" id="txtChequeNo" name="firstName" data-msg-required="The Cheque Number field is required."
                                        data-rule-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-xs-9 input-group">
                                <div class="form-group">
                                    <label class="control-label" for="field-1">Cheque Date</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" data-format="yyyy-mm-dd" id="txtChequeDate" name="firstName">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="linecons-calendar"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="control-label" for="field-1">Remarks</label>
                                <input type="text" class="form-control" id="txtRemarks">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="button" class="btn btn-blue" id="btnAdd" value="Add new line" onclick="AddDetailData();">
                            <table cellspacing="0" rules="all" border="1" id="gvwDetail" class="table table-small-font table-bordered table-stripe">
                                <thead>
                                    <tr class="headerStyle">
                                        <th scope="col" data-hide="expand">Account Name</th>
                                        <th scope="col" data-hide="expand">Description</th>
                                        <th scope="col" data-hide="phone">Amount</th>
                                        <th scope="col" data-hide="expand"></th>
                                    </tr>
                                </thead>
                                <tbody id="dvBodyDetail">
                                    <tr class="rowStyle">
                                        <td style="width: 30%">
                                            <select name="Account Name" id="ddlAccountName" class="gridDetail-accountname"></select></td>
                                        <td style="width: 49%">
                                            <input id="txtDescription" class="gridDetail-description"></td>
                                        <td style="width: 20%">
                                            <input id="txtAmount" class="gridDetail-amount" onchange="GetTotal();"></td>
                                        <td style="width: 1%">
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      
     
    <%--   <div id="mdlAddNew" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="windowTitleLabel">
        <div class="modal-header">
            <h3>Add/Edit Roles</h3>
        </div>
        <div class="modal-body">
            <div class="tabbable">
                
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        
                        
                        <div class="control-group">
                            <label for="textfield" class="control-label">User</label>
                            <div class="controls">
                                <div id="div_UserDD">  </div>
                            </div>
                        </div>
                    
                        <div class="control-group">
                            <label for="policy" class="control-label">Roles</label>
                            <div class="controls">
                               
                                    <div id="div_RoleDD">      </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-primary" id="btnSave" value="Save" onclick="AddEditRoles(); return false;">
            <button type="submit" class="btn btn-default" id="btnCancel" data-dismiss="modal" onclick="refreshModal();">Close</button>
        </div>

    </div>--%>

        
     <input type="hidden" id="hidEdit" value="0" />
    <input type="hidden" id="hidDelete" value="0" />

</asp:Content>
