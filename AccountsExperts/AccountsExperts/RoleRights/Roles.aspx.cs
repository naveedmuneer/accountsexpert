﻿using AccountsExperts.Access_Layers.CommonDataaccess;
using AccountsExperts.Access_Layers.CommonDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using AccountsExperts.DataObjects;
using AccountsExperts.DataAccesslayer;

namespace AccountsExperts.RoleRights
{
    public partial class Roles : PageBase
    {
        #region Constants

        //private const int ColumnWidth = 100;
        private const int PageSize = 10;


        #endregion

        #region Web Methods

        [WebMethod]
        public static string InsertRole(List<DataObjects.Roles> lstRoles)
        {
            try
            {
                DataObjects.Roles objRoles = new DataObjects.Roles();

                objRoles.Role_Name = lstRoles[0].Role_Name;
                objRoles.Is_Active = lstRoles[0].Is_Active;
                objRoles.Created_By = SessionHandler.Current.LoginName;


                bool result = RolesDO.InsertRole(objRoles);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }
        [WebMethod]
        public static string UpdateRole(List<DataObjects.Roles> lstRoles)
        {
            try
            {
                DataObjects.Roles objRoles = new DataObjects.Roles();



                objRoles.pkRole_ID = lstRoles[0].pkRole_ID;
                objRoles.Role_Name = lstRoles[0].Role_Name;
                objRoles.Is_Active = lstRoles[0].Is_Active;


                bool result = RolesDO.UpdateRole(objRoles);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }

        }
        [WebMethod]
        public static string FillGrid(int pageIndex)
        {
            try
            {
                System.Data.DataSet ds = RolesDO.GetRolesData(PageSize, pageIndex);
                return ds.GetXml();
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }

        [WebMethod]
        public static string GetDataForUpdate(int RoleID)
        {
            try
            {
                System.Data.DataSet ds = RolesDO.GetDataForUpdate(RoleID);
                return ds.GetXml();
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }


        [WebMethod]
        public static string DeleteRoles(string RoleID)
        {
            try
            {
                bool result = RolesDO.DeletRole(Convert.ToInt32(RoleID));
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion
    }
}