﻿using AccountsExperts.Access_Layers.CommonDataaccess;
using AccountsExperts.Access_Layers.CommonDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using AccountsExperts.DataObjects;
using AccountsExperts.DataAccesslayer;
using System.Data;



namespace AccountsExperts.RoleRights
{
    public partial class Company : PageBase
    {
        #region Constants

        //private const int ColumnWidth = 100;
        private const int PageSize = 10;


        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region Web Methods

        [WebMethod]
        public static string FillGrid(int pageIndex)
        {
            try
            {
                System.Data.DataSet ds = AccountsExperts.DataAccesslayer.CompanyDO.GetCompanyData(PageSize, pageIndex);
                return ds.GetXml();
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
            }
       
        [WebMethod]
        public static string InsertCompany(List<DataObjects.Company> lstCompany)
        {
            try
            {
                DataObjects.Company objCompany = new DataObjects.Company();

                objCompany.CompanyName = lstCompany[0].CompanyName;
                objCompany.Description = lstCompany[0].Description;
                objCompany.Title = lstCompany[0].Title;
                objCompany.Business = lstCompany[0].Business;
                objCompany.Establishment_Date = lstCompany[0].Establishment_Date;
                objCompany.Expiry_Date = lstCompany[0].Expiry_Date;
                objCompany.Is_Active = lstCompany[0].Is_Active;
                objCompany.Created_By = SessionHandler.Current.LoginName;
                bool result = CompanyDO.InsertCompnay(objCompany);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }

        }
        [WebMethod]

        public static string GetDataForUpdate(int CompanyID)
        {
            try
            {
                System.Data.DataSet ds = CompanyDO.GetDataForUpdate(CompanyID);
                return ds.GetXml();
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
            }

        [WebMethod]
        public static string UpdateCompany(List<DataObjects.Company> lstCompany)
        {
            try
            {
                DataObjects.Company objCompany = new DataObjects.Company();

                objCompany.CompanyID = lstCompany[0].CompanyID;
                objCompany.CompanyName = lstCompany[0].CompanyName;
                objCompany.Description = lstCompany[0].Description;
                objCompany.Title = lstCompany[0].Title;
                objCompany.Business = lstCompany[0].Business;
                objCompany.Establishment_Date = lstCompany[0].Establishment_Date;
                objCompany.Expiry_Date = lstCompany[0].Expiry_Date;
                objCompany.Is_Active = lstCompany[0].Is_Active;
                objCompany.Created_By = lstCompany[0].Created_By;

                bool result = CompanyDO.UpdateCompany(objCompany);
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }

        }
        
        [WebMethod]
        public static string DeleteCompanies(string CompanyID)
        {
            try
            {
                bool result = CompanyDO.DeleteCompany(Convert.ToInt32(CompanyID));
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }

      

        #endregion
    }
}