﻿using AccountsExperts.Access_Layers.CommonDataaccess;
using AccountsExperts.Access_Layers.CommonDataAccess;
using AccountsExperts.DataAccesslayer;
using AccountsExperts.DataObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AccountsExperts.Role_Rights
{
    public partial class Employee : PageBase
    {
        #region Constants

        //private const int ColumnWidth = 100;
        private const int PageSize = 10;


        #endregion

        #region MemberVariables
        // bool isChrome = false;

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region WebMethods

        [System.Web.Services.WebMethod]
        public static string FillGrid(int pageIndex)
        {
            try
            {
                System.Data.DataSet ds = AccountsExperts.DataAccesslayer.EmployeeDO.GetEmployeeData(PageSize, pageIndex);
                return ds.GetXml();
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }

        [WebMethod]
        public static string DeleteEmployee(string employeeID)
        {
            try
            {
                bool result = EmployeeDO.DeleteEmployee(Convert.ToInt32(employeeID));
                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }

        [WebMethod]
        public static dynamic BindCompany()
        {
            try
            {
                List<Company> lstCompany = new List<Company>();
                DataTable dt = new DataTable();
                dt = EmployeeDO.PopulateCompany();
                foreach (DataRow dr in dt.Rows)
                {
                    Company objCompany = new Company();
                    objCompany.CompanyID = Convert.ToInt32(dr["pkCompany_ID"]);
                    objCompany.CompanyName = Convert.ToString(dr["Company_Name"]);
                    lstCompany.Add(objCompany);
                }
                return lstCompany;
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }

        }

        [WebMethod]
        public static dynamic BindDepartment()
        {
            try
            {
                List<Department> lstDepartment = new List<Department>();
                DataTable dt = new DataTable();
                dt = EmployeeDO.PopulateDepartment();
                foreach (DataRow dr in dt.Rows)
                {
                    Department objDepartment = new Department();
                    objDepartment.pkDepartment_ID = Convert.ToInt32(dr["pkDepartment_ID"]);
                    objDepartment.Department_Name = Convert.ToString(dr["Department_Name"]);
                    lstDepartment.Add(objDepartment);
                }
                return lstDepartment;
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }
        [WebMethod]
        public static dynamic  BindDesignation()
        {
            try
            {
                List<Designation> lstDesignation = new List<Designation>();
                DataTable dt = new DataTable();
                dt = EmployeeDO.PopulateDesignation();
                foreach (DataRow dr in dt.Rows)
                {
                    Designation objDesignation = new Designation();
                    objDesignation.pkDesignation_ID = Convert.ToInt32(dr["pkDesignation_ID"]);
                    objDesignation.Designation_Name = Convert.ToString(dr["Designation_Name"]);
                    lstDesignation.Add(objDesignation);
                }
                return lstDesignation;
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }
        [WebMethod(EnableSession = true)]
        public static string InsertEmployee(List<DataObjects.Employee> Employee)
        {
            try{
            DataObjects.Employee objEmployee = new DataObjects.Employee();
            objEmployee.Employee_Name = Employee[0].Employee_Name;
            objEmployee.NIC_No = Employee[0].NIC_No;
            objEmployee.fkCompany_ID = Employee[0].fkCompany_ID;
            objEmployee.fkDepartment_ID = Employee[0].fkDepartment_ID;
            objEmployee.fkDesignation_ID = Employee[0].fkDesignation_ID;
            objEmployee.Email = Employee[0].Email;
            objEmployee.Contact_No = Employee[0].Contact_No;
            objEmployee.Secondary_Contact_No = Employee[0].Secondary_Contact_No;
            objEmployee.Address = Employee[0].Address;
            objEmployee.Permenant_Address = Employee[0].Permenant_Address;
            objEmployee.Is_Active = Employee[0].Is_Active;
            objEmployee.Created_By = SessionHandler.Current.LoginName;
            bool result = EmployeeDO.InsertEmplpyee(objEmployee);

            if (result)
                return "true";
            else
            {
                return "false";
            }
          }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }
        [WebMethod(EnableSession = true)]
        public string UpdateEmployee(List<DataObjects.Employee> Employee)
        {
            try
            {
                DataObjects.Employee objEmployee = new DataObjects.Employee();
                objEmployee.pk_Employee_ID = Employee[0].pk_Employee_ID;
                objEmployee.Employee_Name = Employee[0].Employee_Name;
                objEmployee.NIC_No = Employee[0].NIC_No;
                objEmployee.fkCompany_ID = Employee[0].fkCompany_ID;
                objEmployee.fkDepartment_ID = Employee[0].fkDepartment_ID;
                objEmployee.fkDesignation_ID = Employee[0].fkDesignation_ID;
                objEmployee.Email = Employee[0].Email;
                objEmployee.Contact_No = Employee[0].Contact_No;
                objEmployee.Secondary_Contact_No = Employee[0].Secondary_Contact_No;
                objEmployee.Address = Employee[0].Address;
                objEmployee.Permenant_Address = Employee[0].Permenant_Address;
                objEmployee.Is_Active = Employee[0].Is_Active;
                objEmployee.Created_By = Employee[0].Created_By;
                bool result = EmployeeDO.UpdateEmplpyee(objEmployee);

                if (result)
                    return "true";
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
        }
        [WebMethod]
        public static string GetDataForUpdate(int employeeID)
        {
            try
            {
                System.Data.DataSet ds = EmployeeDO.GetDataForUpdate(employeeID);
                return ds.GetXml();
            }
            catch (Exception ex)
            {
                ExceptionLog.SendExeception(ex);
                return ex.Message;
            }
            
        }


        #endregion
    }
}