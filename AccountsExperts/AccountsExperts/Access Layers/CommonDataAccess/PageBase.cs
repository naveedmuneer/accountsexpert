﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace AccountsExperts.Access_Layers.CommonDataAccess
{
    public class PageBase : Page
    {
        protected override void OnLoad(EventArgs e)
        {
            if(SessionHandler.Current.LoginId == null)
            {
                Response.Redirect("../Logout.aspx");
            }
            base.OnLoad(e);
        }
        protected  void test(EventArgs e)
        {

        }
    }
}