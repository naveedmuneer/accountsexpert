﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Xml.Serialization;
using System.Xml;
using System.Transactions;

namespace AccountsExperts.Access_Layers.CommonDataaccess
{
    public class DataAccess
    {
        

        public static System.Data.DataSet GetDataset(string Spname)
        {

            string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
            SqlConnection SQLDatabaseConnection = new SqlConnection(conConnectionString);
            System.Data.DataSet  ds = new System.Data.DataSet();
            SqlDataAdapter da = new SqlDataAdapter(Spname, SQLDatabaseConnection);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            SQLDatabaseConnection.Open();
            da.Fill(ds);
            SQLDatabaseConnection.Close();
            return ds;
        }
        public static DataTable GetDatatable(string Spname)
        {

            string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
            SqlConnection SQLDatabaseConnection = new SqlConnection(conConnectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(Spname, SQLDatabaseConnection);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            SQLDatabaseConnection.Open();
            da.Fill(dt);
            SQLDatabaseConnection.Close();
            return dt;
        }

        public static DataTable GetDatatablebyParams(string StoredProcedure, params object[] Parameters)
        {
            var Results = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "exec " + StoredProcedure;
                        int i = 0;
                        char[] def;
                        string comma = ",";
                        foreach (var Param in Parameters)
                        {
                            var paramName = String.Format("@P{0}", i);

                            cmd.CommandText += comma + paramName;
                            if (paramName == "@P0")
                            {
                                int abc = (cmd.CommandText).IndexOf(',');
                                def = (cmd.CommandText).ToCharArray();
                                def[abc] = ' ';
                                cmd.CommandText = new string(def);
                            }

                            cmd.Parameters.AddWithValue(paramName, Param);
                            ++i;
                            comma = ",";
                        }

                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(Results);
                    }
                }
            }

            catch (Exception)
            {

            }

            return Results;
        }

        public static System.Data.DataSet GetDataSetByparams(string StoredProcedure, params object[] Parameters)
        {
            var Results = new System.Data.DataSet();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "exec " + StoredProcedure;
                        int i = 0;
                        char[] def;
                        string comma = ",";
                        foreach (var Param in Parameters)
                        {
                            var paramName = String.Format("@P{0}", i);

                            cmd.CommandText += comma + paramName;
                            if (paramName == "@P0")
                            {
                                int abc = (cmd.CommandText).IndexOf(',');
                                def = (cmd.CommandText).ToCharArray();
                                def[abc] = ' ';
                                cmd.CommandText = new string(def);
                            }

                            cmd.Parameters.AddWithValue(paramName, Param);
                            ++i;
                            comma = ",";
                        }

                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(Results);
                    }
                }
            }

            catch (Exception)
            {

            }

            return Results;
        }


        public static System.Data.DataSet GetDataSetForGrid(string storedProcedure, int pageSize, int pageIndex)
        {
            System.Data.DataSet Results = new System.Data.DataSet();
            string recordCount = "0";
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = storedProcedure;

                        cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                        cmd.Parameters.AddWithValue("@PageSize", pageSize);

                        SqlParameter output = new SqlParameter("@RecordCount", SqlDbType.Int);
                        output.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(output);
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(Results);
                        recordCount = output.Value.ToString();
                    }

                    dt.Columns.Add("PageIndex");
                    dt.Columns.Add("PageSize");
                    dt.Columns.Add("RecordCount");
                    dt.Rows.Add();
                    dt.Rows[0]["PageIndex"] = pageIndex;
                    dt.Rows[0]["PageSize"] = pageSize;
                    dt.Rows[0]["RecordCount"] = recordCount;
                    Results.Tables.Add(dt);
                }
            }

            catch (Exception)
            {

            }

            return Results;
        }

        public static bool SendEmail(String ToEmail, String Subject, string Message, string AttachmentPath, String CCEmail, String BCCEmail)
        {
            try
            {
                //Get Sender Email From Web.Config
                string Host = ConfigurationManager.AppSettings["Host"].ToString();
                string FromEmail = ConfigurationManager.AppSettings["FromMail"].ToString();
                string Password = ConfigurationManager.AppSettings["Password"].ToString();
                int Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);


                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(FromEmail);
                mailMessage.Subject = Subject;
                mailMessage.Body = Message;
                mailMessage.IsBodyHtml = true;

                //for Attachment
                if (AttachmentPath != "")
                {
                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(AttachmentPath);
                    mailMessage.Attachments.Add(attachment);
                }
                //Adding Multiple recipient email 
                string[] Multiple = ToEmail.Split(',');
                foreach (string MultiEmail in Multiple)
                {
                    mailMessage.To.Add(new MailAddress(MultiEmail));
                }

                if (CCEmail != "")
                {
                    string[] MultipleCCEmails = CCEmail.Split(',');
                    foreach (string MultiEmail in MultipleCCEmails)
                    {
                        mailMessage.CC.Add(new MailAddress(MultiEmail));
                    }
                }

                if (BCCEmail != "")
                {
                    string[] MultipleBCCEmails = BCCEmail.Split(',');
                    foreach (string MultiEmail in MultipleBCCEmails)
                    {
                        mailMessage.Bcc.Add(new MailAddress(MultiEmail));
                    }
                }

                SmtpClient smtp = new SmtpClient();
                smtp.Host = Host;


                //smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential();
                NetworkCred.UserName = mailMessage.From.Address;
                NetworkCred.Password = Password;
                //  smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = Port;
                smtp.Send(mailMessage);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            System.Configuration.AppSettingsReader settingsReader =
                                                new AppSettingsReader();
            // Get the key from config file

            string key = (string)settingsReader.GetValue("SecurityKey",typeof(String));
            //System.Windows.Forms.MessageBox.Show(key);
            //If hashing use get hashcode regards to your key
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice

                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)

            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string

            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            System.Configuration.AppSettingsReader settingsReader =
                                                new AppSettingsReader();
            //Get your key from config file to open the lock!
            string key = (string)settingsReader.GetValue("SecurityKey",
                                                         typeof(String));

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static void CreateXmlSerializer<T>(T serializableObject, out string xmlData)
        {
            try
            {
                Type ObjectType = typeof(T);
                XmlSerializer xmlserializer = new XmlSerializer(ObjectType);
                StringWriter sWriter = new StringWriter();
                XmlWriter xmlWriter = XmlWriter.Create(sWriter);
                xmlserializer.Serialize(xmlWriter, serializableObject);
                xmlData = sWriter.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred", ex);
            }
        }

        public static T CreateXmlDeserialize<T>(StringBuilder xmlData)
        {
            try
            {
                Type ObjectType = typeof(T);
                StringBuilder documentDataXml = xmlData;

                MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(documentDataXml.ToString()));

                XmlSerializer deserializer = new XmlSerializer(typeof(T));
                TextReader reader = new StreamReader(stream);
                object obj = deserializer.Deserialize(reader);
                T XmlData = (T)obj;
                reader.Close();

                return XmlData;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred", ex);
            }
        }
    }

}
 
