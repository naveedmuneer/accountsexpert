﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsExperts.Access_Layers.CommonDataAccess
{
    public class SessionHandler
    {
        // private constructor
        private SessionHandler()
        {
            //Property1 = "default value";
        }

        // Gets the current session.
        public static SessionHandler Current
        {
            get
            {
                SessionHandler session =
                  (SessionHandler)HttpContext.Current.Session["SessionId"];
                if (session == null)
                {
                    session = new SessionHandler();
                    HttpContext.Current.Session["SessionId"] = session;
                }
                return session;
            }
        }

        // **** add your session properties here, e.g like this:
  
        public string LoginId { get; set; }
        public string LoginName { get; set; }
    }
}