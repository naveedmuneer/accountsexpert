﻿
var isValid = true;
function validated(validationGroup) {
    $('[MyTag="required"]').each(function (index) {
        if ($(this).attr('validationG') == validationGroup) {
            if ($(this).val() === '') {
                $(this).attr('style', 'border-color:red');
                $(this).attr('title', 'this filed is required');
                // document.write(index + ': ' + $(this).val() + "<br>");
                // $(this).tooltip('open');
                isValid = false;
                return false;
            }
            else {
                $(this).attr('style', '');
                $(this).attr('title', '');
                $(this).attr('Class', '');
                return true;
            }
        }
        else {
            $(this).attr('style', '');
            $(this).attr('title', '');
            $(this).attr('Class', '');
            return true;
        }

    });
    if (isValid) {
        $('[EmailValidate="emailvalidate"]').each(function (index) {
            if ($(this).attr('validationG') == validationGroup) {
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                if (reg.test($(this).val()) == false || $(this).val()==='') {
                    $(this).attr('style', 'border-color:red');
                    $(this).attr('title', 'this filed is required');
                    // document.write(index + ': ' + $(this).val() + "<br>");
                    // $(this).tooltip('open');
                    return false;
                }
                else {
                    $(this).attr('style', '');
                    $(this).attr('title', '');
                    $(this).attr('Class', '');
                    return true;
                }
            }
            else {
                $(this).attr('style', '');
                $(this).attr('title', '');
                $(this).attr('Class', '');
                return true;
            }

        });

    }
}