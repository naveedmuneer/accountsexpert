﻿
//Constants
var FirstPage = 1;

//End ConstantsErrorlog


$(function () {

    FillGridView(FirstPage);
    $('#gvwList').footable();
    $('#txtEstablishment_Date').datepicker();
    $('#txtExpiry_Date').datepicker();
    


});


function AddNew() {

    $("#btnSave").val("Save");
    $("#mdlAddNew").attr("Style", "display:block");
    $("#mdlAddNew").modal('show');

}



function AddEditCompany() {


    if ($("#form1").valid()) {
        if ($("#chkActive").is(':checked'))
            $('#chkActive').val("true");  // checked
        else
            $('#chkActive').val("false");

        if ($("#btnSave").val() == "Save") {
            var jsonObjects = [
           {
               CompanyName: $('#txtCompanyName').val(),
               Description: $('#txtDescription').val(),
               Title: $('#txtTitle').val(),
               Business: $('#txtBusiness').val(),
               Establishment_Date: $('#txtEstablishment_Date').val(),
               Expiry_Date: $('#txtExpiry_Date').val(),
               Is_Active: $('#chkActive').val()
              
           }
            ];


            $.ajax({
                type: "POST",
                url: "Company.aspx/InsertCompany",
                data: '{lstCompany: ' + JSON.stringify(jsonObjects) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");

                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.ErrrUnspecified, "error");
                    }
                },
                error: function (response) {
                    ShowMessageBox(AexpMessages.AccountsMessages.Save, "error");
                    InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Company.aspx/InsertCompany')
                  
                }

            });

        }
        else {
            var jsonObjects = [
            {
                CompanyName: $('#txtCompanyName').val(),
                Description: $('#txtDescription').val(),
                Title: $('#txtTitle').val(),
                Business: $('#txtBusiness').val(),
                Establishment_Date: $('#txtEstablishment_Date').val(),
                Expiry_Date: $('#txtExpiry_Date').val(),
                Is_Active: $('#chkActive').val(),
                CompanyID: $("#hidEdit").val()
            }
            ];



            $.ajax({
                type: "POST",
                url: "Company.aspx/UpdateCompany",

                data: '{lstCompany: ' + JSON.stringify(jsonObjects) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Update, "success");
                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.ErrrUnspecified, "error");
                    }

                },
                failure: function (response) {
                    ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                },
                error: function (response) {
                    ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                    InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Company.aspx/UpdateCompany')

                }


            });
        }

        FillGridView(FirstPage);
        RefreshModal();
        $("#mdlAddNew").attr("Style", "display:none");
        $("#mdlAddNew").modal('hide');
    }
    else {
        ShowMessageBox(messages.errorCashBookDetailGridEmpty, "error");
    }

}





function FillGridView(pageIndex) {
    $.ajax({
        type: "POST",
        url: "Compan.aspx/FillGrid",
        data: '{pageIndex: ' + pageIndex + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: FillGridViewCompleted,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
        InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Company.aspx/FillGrid')
         
        }
    });

}

function FillGridViewCompleted(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var company = xml.find("Companies");

    $("#dvGridBody").empty();


    var content = '<table class="table table-striped table-bordered dataTable"\
    cellspacing="0" rules="all" border="1" id="gvwList"\
    style="border-collapse: collapse;">\
    <thead>\
        <tr class="headerStyle">\
            <th scope="col" data-hide="expand">Edit</th>\
            <th scope="col" data-hide="expand">Company <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">Description <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">Title <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">Business <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">Establishment_Date</th>\
            <th scope="col" data-hide="expand">Expiry_Date</th>\
            <th scope="col" data-hide="expand">Is_Active</th>\
            <th data-hide="phone" scope="col">Created On</th>\
            <th data-hide="phone" scope="col">Created By <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">Delete</th>\
           </tr>\
         <tr class="" id="trsearch">\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand"><input type="text" class="filter-textbox" size="4"></th>\
            <th data-hide="phone" scope="col"><input type="text" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"><input type="text" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"><input type="text" class="filter-textbox" size="4" ></th>\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand" ></th>\
            <th data-hide="phone" scope="col"><input type="text" class="filter-textbox" size="4" ></th>\
            <th scope="col" data-hide="expand"></th>\
           </tr>\
    </thead>\
    <tbody ></tbody></table';

    $("#dvGridBody").append(content);
    $.each(company, function () {
        var companies = $(this);
        var content = '<tr class="rowStyle">\
                                <td><input type="image" name="edit" id="' + companies.find("pkCompany_ID").text() + '" title="Edit" src="../Images/IconImage/icn-edit.png" onclick="GetDataForUpdate(' + companies.find("pkCompany_ID").text() + ');return false;" style="height:20px;width:20px;" class="phone footable-loaded"/></td>\
                                <td>' + companies.find("Company_Name").text() + '</td>\
                                <td>' + companies.find("Description").text() + '</td>\
                                <td>' + companies.find("Title").text() + '</td>\
                                <td>' + companies.find("Business").text() + '</td>\
                                <td>' + companies.find("Establishment_Date").text() + '</td>\
                                <td>' + companies.find("Expiry_Date").text() + '</td>\
                                <td>' + companies.find("Is_Active").text() + '</td>\
                                <td>' + companies.find("Created_On").text() + '</td>\
                                <td>' + companies.find("Created_By").text() + '</td>\
                                <td><input type="image" name="delete" id="' + companies.find("pkCompany_ID").text() + '" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="openDeleteModal(' + companies.find("pkCompany_ID").text() + ',DeleteCompany);return false;" style="height:20px;width:20px;" class="phone footable-loaded"/></td>\
                                </tr>'


        $("#dvGridBody table> tbody").append(content);
    });

    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        LabelName: "lblResultEntries",
        RecordCount: parseInt(pager.find("RecordCount").text())
    });

    $('#gvwList').footable();

};

function GetDataForUpdate(CompanyID) {
    $("#hidEdit").val(CompanyID);
    $.ajax({
        type: "POST",
        url: "Company.aspx/GetDataForUpdate",
        data: '{CompanyID: ' + CompanyID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            var updateDate = xml.find("UpdateData");

            $.each(updateDate, function () {
                var company = $(this);

                $('#txtCompanyName').val(company.find("Company_Name").text());
                $('#txtDescription').val(company.find("Description").text());
                $('#txtTitle').val(company.find("Title").text());
                $('#txtBusiness').val(company.find("Business").text());
                $('#txtEstablishment_Date').val(company.find("Establishment_Date").text());
                $('#txtExpiry_Date').val(company.find("Expiry_Date").text());


                if (company.find("Is_Active").text() == "true") {
                    $('#chkActive').prop('checked', true);
                }
                else {
                    $('#chkActive').prop('checked', false);
                }


            });
            $("#mdlAddNew").attr("Style", "display:block");
            $("#mdlAddNew").modal('show');
            $("#btnSave").val("Update");

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Company.aspx/GetDataForUpdate')
        }
    });


}



function DeleteCompany(CompanyID) {
    $("#hidDelete").val(CompanyID);
    $.ajax({
        type: "POST",
        url: "Company.aspx/DeleteCompanies",
        data: '{CompanyID: ' + CompanyID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            closeDeleteModal();
            if (response.d === "true") {
                ShowMessageBox(AexpMessages.AccountsMessages.Delete, "success");
            }
            else {
                ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");
            }
            FillGridView(FirstPage);
        },
        failure: function (response) {
            ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");

        },
        error: function (response) {
            ShowMessageBox(response.d, "error");
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Company.aspx/DeleteCompanies')

        }
    });


}
$(".Pager .page").live("click", function () {
    FillGridView(parseInt($(this).attr('page')));

});

