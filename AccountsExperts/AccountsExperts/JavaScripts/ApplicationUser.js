﻿//Constants
var FirstPage = 1;

//End Constants

//Events
$(function () {
     
    FillGridView(FirstPage);
    BindEmployeeName();
    $('#gvwList').footable();
 
});


function FillGridView(pageIndex) {
    $.ajax({
        type: "POST",
        url: "ApplicationUser.aspx/FillGrid",
        data: '{pageIndex: ' + pageIndex + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: FillGridViewCompleted,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'ApplicationUser.aspx/FillGrid')
        }
    });

}

function FillGridViewCompleted(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var ApplicationUser = xml.find("ApplicationUser");

    $("#dvGridBody").empty();


    var content = '<table class="table table-striped table-bordered dataTable"\
    cellspacing="0" rules="all" border="1" id="gvwList"\
    style="border-collapse: collapse;">\
    <thead>\
         <tr class="headerStyle">\
           <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand" >User Name <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
           <th scope="col" data-hide="expand" >Password</th>\
            <th scope="col" data-hide="expand" >Employee Name <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand" >Is Active</th>\
            <th scope="col" data-hide="expand" ></th>\
           </tr>\
            <tr class="" id="trsearch">\
            <th scope="col" data-hide="expand" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th scope="col" data-hide="expand" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand"></th>\
           </tr>\
    </thead>\
    <tbody ></tbody></table';

    $("#dvGridBody").append(content);
    $.each(ApplicationUser, function () {
        var User = $(this);
        var content = '<tr class="rowStyle">\
                                <td><input type="image" name="edit" id="' + User.find("pkUserID").text() + '" title="Edit" src="../Images/IconImage/icn-edit.png" onclick="GetDataForUpdate(' + User.find("pkUserID").text() + ');return false;" style="height:20px;width:20px;" class="phone footable-loaded"/></td>\
                                <td>' + User.find("UserName").text() + '</td>\
                                <td>' + User.find("Password").text() + '</td>\
                                <td>' + User.find("EmployeeName").text() + '</td>\
                                <td>' + User.find("IsActive").text() + '</td>\
                                <td><input type="image" name="delete" id="' + User.find("pkUserID").text() + '" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="openDeleteModal(' + User.find("pkUserID").text() + ',DeleteApplicationUser);return false;" style="height:17px;width:17px;" class="phone footable-loaded"/></td>\
                                </tr>'

        $("#dvGridBody table> tbody").append(content);
    });

    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        LabelName: "lblResultEntries",
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
    $('#gvwList').footable();

};

function BindEmployeeName() {
    $.ajax({
        type: "POST",
        url: "ApplicationUser.aspx/BindUser",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlCompany = $("[id*=ddlEmployeeName]");
            ddlCompany.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                ddlCompany.append($("<option></option>").val(this['pk_Employee_ID']).html(this['Employee_Name']));
            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'ApplicationUser.aspx/BindUser')
        }
    });


}


function AddNew() {

    $("#btnSave").val("Save");
    $("#mdlAddNew").attr("Style", "display:block");
    $("#mdlAddNew").modal('show');
}

function DeleteApplicationUser(applicationUserID) {
    $("#hidDelete").val(applicationUserID);
    $.ajax({
        type: "POST",
        url: "ApplicationUser.aspx/DeleteApplicationUser",
        data: '{applicationUserID: ' + applicationUserID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            closeDeleteModal();
            if (response.d === "true") {
                ShowMessageBox(AexpMessages.AccountsMessages.Delete, "success");
            }
            else {
                ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");
            }
            FillGridView(FirstPage);
        },
        failure: function (response) {
            ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");

        },
        error: function (response) {
            ShowMessageBox(response.d, "error");
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'ApplicationUser.aspx/DeleteApplicationUser')
        }
    });


}

$(".Pager .page").live("click", function () {
    FillGridView(parseInt($(this).attr('page')));

});

function AddEditApplicationUser() {

        var jsonObjects = [
            {
                pkUserID: $('#hidEdit').val(),
                User_Name: $('#txtUserName').val(),
                Password: $('#txtPassword').val(),
                fkEmployee_ID:$('#ddlEmployeeName').val(),
                Is_Active: $('#chkActive').val()

            }
        ];
        if ($("#btnSave").val() === "Save") {

            $.ajax({
                type: "POST",
                url: "ApplicationUser.aspx/InsertApplicationUser",
                data: '{applicationUser: ' + JSON.stringify(jsonObjects) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");
                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                    }
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.Save, "error");
                    InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'ApplicationUser.aspx/InsertApplicationUser')
                }

            });

        }
        else {


            $.ajax({
                type: "POST",
                url: "ApplicationUser.aspx/UpdateApplicationUser",
                data: '{ApplicationUser: ' + JSON.stringify(jsonObjects) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(messages.update, "success");
                    }
                    else {
                        ShowMessageBox(messages.errorUpdate, "error");
                    }

                },
                failure: function (response) {
                    ShowMessageBox(messages.errorUpdate, "error");
                },
                error: function (result) {
                    ShowMessageBox(messages.errorUpdate, "error");
                    InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'ApplicationUser.aspx/UpdateApplicationUser')
                }

            });
        }

        FillGridView(FirstPage);
        RefreshModal();
        $("#mdlAddNew").attr("Style", "display:none");
        $("#mdlAddNew").modal('hide');
    }
    




function GetDataForUpdate(applicationUserID) {
    $("#hidEdit").val(applicationUserID);
    $.ajax({
        type: "POST",
        url: "ApplicationUser.aspx/GetDataForUpdate",
        data: '{applicationUserID: ' + applicationUserID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            var updateDate = xml.find("UpdateData");

            $.each(updateDate, function () {
                var User = $(this);
                $('#ddlEmployeeName').val(User.find("fkEmployee_ID").text());
                $('#txtUserName').val(User.find("User_Name").text());
                $('#txtPassword').val(User.find("Password").text());
                if (User.find("Active").text() === "true") {
                    $('#chkActive').prop('checked', true);
                }
                else {
                    $('#chkActive').prop('checked', true);
                }
              
            });
            $("#mdlAddNew").attr("Style", "display:block");
            $("#mdlAddNew").modal('show');
            $("#btnSave").val("Update");

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'ApplicationUser.aspx/GetDataForUpdate')
        }
    });
    }

//Events Ends

function RefreshModal() {

    var validator = $("#form1").validate();
    validator.resetForm();
    $(".control-group").attr("class", "control-group");
    $("#dvBodyDetail").empty();
}
//End Private Methods

