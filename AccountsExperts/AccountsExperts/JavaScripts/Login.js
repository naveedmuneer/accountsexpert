﻿

//Events
function FormValidation() {
         var username = document.getElementById('txtUserName').value;
         var password = document.getElementById('txtPassword').value;
         if (username == "" && password == "") {
             document.getElementById('txtUserName').style.borderColor = "red";
             document.getElementById('txtPassword').style.borderColor = "red";
             return false;
         } else if (username != "" && password == "") {

             document.getElementById('txtPassword').style.borderColor = "red";
             document.getElementById('txtUserName').style.borderColor = "#A0A0A0";
             return false;
         }
         else if (username == "" && password != "") {

             document.getElementById('txtUserName').style.borderColor = "red";
             document.getElementById('txtPassword').style.borderColor = "#A0A0A0";
             return false;
         }
         else {
             return true;
         }
        
     }
//Events Ends
