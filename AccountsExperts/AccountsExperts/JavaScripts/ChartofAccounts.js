﻿
$(function () {

    GetChartOfAccountForAll(1, $("#hidAllPageSize").val(), $("#hidChartType").val());

    fillAccountNature();
    fillGroupName();

    //$("#AddNewModal").attr("Style", "display:none");

});
$("#dvAllPaging .page").live("click", function () {
    GetChartOfAccountForAll(parseInt($(this).attr('page')), $("#hidAllPageSize").val(), $("#hidChartType").val());

});


function GetChartOfAccountForAll(pageIndex, pageSize, chartType) {
    $.ajax({
        type: "POST",
        url: "ChartOfAccounts.aspx/fillGrid",
        data: '{pageIndex: ' + pageIndex + ',chartType: "' + chartType + '",pageSize:' + pageSize + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: OnSuccess,
        failure: function (response) {
            ShowMessageBox(messages.errorFillGrid, "error");;
        },
        error: function (response) {
            ShowMessageBox(messages.errorFillGrid, "error");;
        }
    });

}

function OnSuccess(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var chartAccount = xml.find("ChartOfAccount");

    $("#dvGridBody").empty();

    var content = '<table class="footable"\
    cellspacing="0" rules="all" border="1" id="gvwList"\
    style="border-collapse: collapse;">\
    <thead>\
        <tr class="headerStyle">\
<th scope="col" data-hide="expand"></th>\
            <th scope="col" data-hide="expand" >Account Number</th>\
            <th scope="col" data-hide="expand" >Account Name</th>\
            <th scope="col" data-hide="expand">Account Nature</th>\
           <th scope="col" data-hide="expand"></th>\
        </tr>\
    </thead>\
    <tbody ></tbody></table';

    $("#dvGridBody").append(content);
    $.each(chartAccount, function () {
        var account = $(this);
        var content = '<tr class="rowStyle">\
                            <td><input type="image" name="edit" id="' + account.find("pk_ChartID").text() + '" title="Edit" src="../Images/IconImage/icn-edit.png" onclick="GetDataForUpdate(' + account.find("ChartID").text() + ');return false;" style="height:20px;width:20px;" class="phone footable-loaded"/></td>\
                                <td>' + account.find("ChartRefNo").text() + '</td>\
                                 <td><span style="Color:#048fc2;">' + account.find("ChartName").text() + '</span><br/>' + account.find("Description").text() + '</td>\
                                <td>' + account.find("AccountNature").text() + '</td>\
                                <td><input type="image" name="delete" id="' + account.find("pk_ChartID").text() + '" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="openDeleteModal(' + account.find("ChartID").text() + ',deleteChartOdAccount);return false;" style="height:20px;width:20px;" class="phone footable-loaded"/></td>\
                            </tr>'


        $("#dvGridBody table> tbody").append(content);
    });

    var pager = xml.find("Pager");
    $("#dvAllPaging").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        LabelName: "lblResultEntries",
        RecordCount: parseInt(pager.find("RecordCount").text())
    });

    $('#gvwList').footable();

};

$("#tabs").tabs();
//-- Modal has finished being hidden
$('#AddNewModal').on('hidden.bs.modal', function (e) {
    //$(this).find('form')[0].reset();
});

function AddNew() {

    $("#btnSave").val("Save");
    $("#AddNewModal").attr("Style", "display:block");
    $("#AddNewModal").modal('show');
    $("#AddModalTitle").text('Add Account');
}


function addEditChartOfAccount() {
    if ($("#form1").valid()) {
        var jsonObjects = [
            {
                pk_ChartID: $('#hidEdit').val(), ChartRefNo: $('#txtAccountName').val(), ChartName: $('#txtAccountName').val(), Description: $('#txtDescription').val(), GroupID: Number($('#ddlGroup').val()),
                fk_AccountNatureID: Number($('#ddlAccountNature').val()), AccountType: Number($('#ddlAccountType').val()), Note: $('#txtNote').val(),
                Address: $('#txtAddress').val(), Phone: $('#txtPhone').val(), Fax: $('#txtFax').val(),
                Email: $('#txtEmail').val(), NTNNO: $('#txtNtn').val(), GSTNO: $('#txtGst').val(),
                Active: $('#chkActive').val()
            }
        ];
        if ($("#btnSave").val() === "Save") {
            $.ajax({
                type: "POST",
                url: "ChartofAccounts.aspx/Insert",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                data: JSON.stringify({ ChartofAccounts: jsonObjects }),
                success: function (result) {
                    ShowMessageBox(messages.save, "success");
                },
                error: function (result) {
                    ShowMessageBox(messages.errorSave, "error");
                }

            });

        }
        else {
            $.ajax({
                type: "POST",
                url: "ChartofAccounts.aspx/Update",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                data: JSON.stringify({ ChartofAccounts: jsonObjects }),
                success: function (result) {
                    ShowMessageBox(messages.update, "success");
                },
                error: function (result) {
                    ShowMessageBox(messages.errorUpdate, "error");
                }

            });
        }
        refreshModal();
        GetChartOfAccountForAll(1, $("#hidAllPageSize").val(), $("#hidChartType").val());
        $("#AddNewModal").attr("Style", "display:none");
        $("#AddNewModal").modal('hide');
    }

}

function fillAccountNature() {

    $.ajax({
        type: "POST",
        url: "ChartOfAccounts.aspx/fillAccountNature",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlAccountNature = $("[id*=ddlAccountNature]");
            ddlAccountNature.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                ddlAccountNature.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        }
    });


}

function fillGroupName() {

    $.ajax({
        type: "POST",
        url: "ChartOfAccounts.aspx/fillGroupName",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlGroup = $("[id*=ddlGroup]");
            ddlGroup.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                ddlGroup.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        }
    });


}

function GetDataForUpdate(chartID) {
    $("#hidEdit").val(chartID);
    $.ajax({
        type: "POST",
        url: "ChartOfAccounts.aspx/getDataForUpdate",
        data: '{chartID: ' + chartID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            var updateDate = xml.find("UpdateData");

            $.each(updateDate, function () {
                var account = $(this);
                $('#txtCode').val(account.find("ChartRefNo").text());
                $('#txtAccountName').val(account.find("ChartName").text());
                $('#txtDescription').val(account.find("Description").text());
                $('#ddlGroup').val(account.find("GroupID").text());
                $('#ddlAccountNature').val(account.find("fk_AccountNatureID").text());
                $('#ddlAccountType').val(account.find("AccountType").text());
                $('#txtNote').val(account.find("Note").text());
                $('#txtAddress').val(account.find("Address").text());
                $('#txtPhone').val(account.find("Phone").text());
                $('#txtFax').val(account.find("Fax").text());
                $('#txtEmail').val(account.find("Email").text());
                $('#txtNtn').val(account.find("NTNNO").text());
                $('#txtGst').val(account.find("GSTNO").text());
                if (account.find("Active").text() === "true") {
                    $('#chkActive').prop('checked', true);
                }
                else {
                    $('#chkActive').prop('checked', true);
                }

            });
            $("#AddNewModal").attr("Style", "display:block");
            $("#AddNewModal").modal('show');
            $("#btnSave").val("Update");
            $("#AddModalTitle").text("Edit Account");
        },
        failure: function (response) {
            ShowMessageBox(messages.errorEdit, "error");
        },
        error: function (response) {
            ShowMessageBox(messages.errorEdit, "error");
        }
    });


}

function refreshModal() {
    //location.reload();
    //$("#form1").clearValidation();
    var validator = $("#form1").validate();
    validator.resetForm();
    $(".control-group").attr("class", "control-group");
}

function deleteChartOdAccount(chartID) {
    $("#hidDelete").val(chartID);
    $.ajax({
        type: "POST",
        url: "ChartOfAccounts.aspx/DeleteRecord",
        data: '{chartID: ' + chartID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            closeDeleteModal();
            if (response.d === "true") {
                ShowMessageBox(messages.delete, "success");
            }
            else {
                ShowMessageBox(messages.errorDelete, "error");
            }

        },
        failure: function (response) {
            ShowMessageBox(messages.errorDelete, "error");

        },
        error: function (response) {
            ShowMessageBox(response.d, "error");
        }
    });


}


function SetPageSizeHiddenValue(pageSize, type) {

    $("#hidAllPageSize").val($("#ddlPageSize").val());
    GetChartOfAccountForAll(1, $("#hidAllPageSize").val(), $("#hidChartType").val());

}

function OnTabChange(type) {
    $("#hidChartType").val(type);
    GetChartOfAccountForAll(1, $("#hidAllPageSize").val(), $("#hidChartType").val());
}