﻿//Constants
var FirstPage = 1;
var SourceTypeX = '';
var UserXML = '';
var ROLEXML = '';
//End Constants



$(function () {

  
    BindUserDropDown();
    BindRoleDropDown();

    FillGridView(FirstPage);
    $('#gvwList').footable();

});
function BindUserDropDown() {

    jQuery.ajax({

        url: 'UserMapping.aspx/BindUsersDD', type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function OnSuccess_LeadSourceType(response) {

            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            // var SourceType = xml.find("ddlLeadSourceType");
            UserXML = xml.find("ddlUsers");

            var LeadSourceType = CreateDropDown(UserXML, "ddlUsers", "Choose Lead Source Type", "350px", "User_Name", "pkUser_ID", "false");

            $('#div_UserDD').append(LeadSourceType).promise().done(function () {
                DdlChoosen();
            });

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'UserMapping.aspx/BindUsersDD')
        }
    });
}

function BindRoleDropDown() {

    jQuery.ajax({

        url: 'UserMapping.aspx/BindRolesDD', type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function OnSuccess_LeadSourceType(response) {

            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            // var SourceType = xml.find("ddlLeadSourceType");
            ROLEXML = xml.find("ddlRoles");

            var LeadSourceType = CreateMultiSelectDropDown(ROLEXML, "ddlRoles", "Choose Lead Source Type", "350px", "Role_Name", "pkRole_ID", "false");

            $('#div_RoleDD').append(LeadSourceType).promise().done(function () {
                DdlChoosen();
            });

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'UserMapping.aspx/BindRolesDD')
        }
    });
}


function FillGridView(pageIndex) {
    $.ajax({
        type: "POST",
        url: "UserMapping.aspx/FillGrid",
        data: '{pageIndex: ' + pageIndex + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: FillGridViewCompleted,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'UserMapping.aspx/FillGrid')
        }
    });

}

function FillGridViewCompleted(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var role = xml.find("UserRoleMappings");

    $("#dvGridBody").empty();

    var content = '<table class="table table-striped table-bordered dataTable"\
    cellspacing="0" rules="all" border="1" id="gvwList"\
    style="border-collapse: collapse;">\
    <thead>\
        <tr class="headerStyle">\
            <th scope="col" data-hide="expand">Edit</th>\
            <th scope="col" data-hide="expand">User <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">Roles <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">Delete</th>\
           </tr>\
         <tr class="" id="trsearch">\
             <th scope="col" data-hide="expand" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th scope="col" data-hide="expand"></th>\
           </tr>\
    </thead>\
    <tbody ></tbody></table';

    $("#dvGridBody").append(content);
    $.each(role, function () {
        var roles = $(this);
        var content = '<tr class="rowStyle">\
                                <td><input type="image" name="edit" id="' + roles.find("UserID").text() + '" title="Edit" src="../Images/IconImage/icn-edit.png" onclick="GetDataForUpdate(' + roles.find("UserID").text() + ');return false;" style="height:17px;width:17px;" class="phone footable-loaded"/></td>\
                                <td>' + roles.find("User_Name").text() + '</td>\
                                <td>' + roles.find("Roles").text() + '</td>\
                                <td><input type="image" name="delete" id="' + roles.find("UserID").text() + '" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="openDeleteModal(' + roles.find("UserID").text() + ',DeleteMapping);return false;" style="height:17px;width:17px;" class="phone footable-loaded"/></td>\
                                </tr>'

        $("#dvGridBody table> tbody").append(content);
    });

    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        LabelName: "lblResultEntries",
        RecordCount: parseInt(pager.find("RecordCount").text())
    });

    $('#gvwList').footable();

};

function AddNew() {

    $("#btnSave").val("Save");
    $("#mdlAddNew").attr("Style", "display:block");
    $("#mdlAddNew").modal('show');

}
function RefreshModal() {

    //var validator = $("#form1").validate();
    //validator.resetForm();
    //$(".control-group").attr("class", "control-group");
    //$("#dvBodyDetail").empty();
}



function AddEditRoles() {


    if ($("#form1").valid()) {
       

        var User = $("#ddlUsers").val();
        var Roles = $("#ddlRoles").val();
        if (User ==  "")
        {
            alert("User is mandatory");
            return false;
           
        }
        if (Roles == null)
        {
            alert("Role is mandatory");
            return false;
            
        }


        if ($("#btnSave").val() == "Save")
        {
          
          
            $.ajax({
                type: "POST",
                url: "UserMapping.aspx/InsertUserMapping",
                data: '{User: ' + User + ',Roles:"' + Roles + '"}',
              
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");

                    }
                    else {
                        ShowMessageBox("Duplicate row", "error");
                    }
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                    InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'UserMapping.aspx/InsertUserMapping')
                }

            });

        }
        else
        {
            

            $.ajax({
                type: "POST",
                url: "UserMapping.aspx/UpdateUserMapping",
                 data: '{User: ' + User + ',Roles:"' + Roles + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");
                    }
                    else {
                        ShowMessageBox("Duplicate row", "error");
                    }

                },
                failure: function (response) {
                    ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                    InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'No URL')
                }

            });
        }

        FillGridView(FirstPage);
        RefreshModal();
        $("#mdlAddNew").attr("Style", "display:none");
        $("#mdlAddNew").modal('hide');
    }
    else {
        ShowMessageBox(messages.errorCashBookDetailGridEmpty, "error");
    }

}

function GetDataForUpdate(UserID) {
    $("#hidEdit").val(UserID);
    
    $.ajax({
        type: "POST",
        url: "UserMapping.aspx/GetDataForUpdate",
        data: '{UserID: ' + UserID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
           

            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            var updateDate = xml.find("UpdateData");

            SelectDropDownValuesForUpdate(updateDate.find("UserID").text(), updateDate.find("RoleIDs").text());



            //$("#mdlAddNew").attr("Style", "display:block");
            //$("#mdlAddNew").modal('show');

            $("#ddlUsers").attr('disabled', true);
            //$("#ddlUsers").prop("disabled", true);
            $("#btnSave").val("Update");
         

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'No URL')
        }
    });


}


function SelectDropDownValuesForUpdate(UserID, RoleIDS) {

   
    var UserDD = CreateDropDown(UserXML, "ddlUsers", "Choose Lead Source", "350px", "User_Name", "pkUser_ID", "false", UserID);
   

    $('#div_UserDD').empty();
    $('#div_UserDD').append(UserDD).promise().done(function () {
        DdlChoosen();
    });

   
    var RolesDD = CreateMultiSelectDropDown(ROLEXML, "ddlRoles", "Choose Lead Source", "350px", "Role_Name", "pkRole_ID", "false", RoleIDS);
   

    $('#div_RoleDD').empty();
    $('#div_RoleDD').append(RolesDD).promise().done(function () {
        DdlChoosen();
    });

}

function DeleteMapping(UserID) {
    $("#hidDelete").val(UserID);
    $.ajax({
        type: "POST",
        url: "UserMapping.aspx/DeleteUserMapping",
        data: '{UserID: ' + UserID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            closeDeleteModal();
            if (response.d === "true") {
                ShowMessageBox(AexpMessages.AccountsMessages.Delete, "success");
            }
            else {
                ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");
            }
            FillGridView(FirstPage);
        },
        failure: function (response) {
            ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");

        },
        error: function (response) {
            ShowMessageBox(response.d, "error");
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'No URL')
        }
    });


}


$(".Pager .page").live("click", function () {
    FillGridView(parseInt($(this).attr('page')));

});


