﻿

function DdlChoosen() {
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-width': { width: "95%" },
        search_contains: true
    }
    for (var selector in config) {
        $(selector).chosen(
            config[selector]);
    }

}


function CreateDropDown(_XMLData, _DdlId, _DdlText, _DdlWidth, _DdlTextField, _DdlIdField, _Disabled, _Selected) {

    _Disabled = _Disabled.toUpperCase();
    var Disabled = '';
    if (_Disabled == "TRUE") {
        Disabled = 'disabled';
    }

    var DropDown = "<select id='" + _DdlId + "' data-placeholder='Select' class='chosen-select' style='width: " + _DdlWidth + ";' tabindex='2' " + Disabled + " >";
    DropDown += " <option value=''></option>";
    $(_XMLData).each(function () {

        if (typeof _Selected === 'undefined') {
            DropDown += "<option value='" + $(this).find(_DdlIdField).text() + "'> " + $(this).find(_DdlTextField).text() + "</option>";
        } else {
            if ($(this).find(_DdlIdField).text() == _Selected) {
                DropDown += "<option value='" + $(this).find(_DdlIdField).text() + "' selected> " + $(this).find(_DdlTextField).text() + "</option>";
            } else {
                DropDown += "<option value='" + $(this).find(_DdlIdField).text() + "'> " + $(this).find(_DdlTextField).text() + "</option>";
            }
        }
    });
    DropDown += "</select>";

    return DropDown;
}

function CreateMultiSelectDropDown(_XMLData, _DdlId, _DdlText, _DdlWidth, _DdlTextField, _DdlIdField, _Disabled, _Selected) {


    _Disabled = _Disabled.toUpperCase();
    var Disabled = '';
    if (_Disabled == "TRUE") {
        Disabled = 'disabled';
    }

    var DropDown = "<select id='" + _DdlId + "' data-placeholder='Select' class='chosen-select' style='width: " + _DdlWidth + ";' tabindex='2' " + Disabled + " Multiple >";
    DropDown += " <option value=''></option>";
    _XMLData.each(function () {

        if (typeof _Selected === 'undefined') {
            DropDown += "<option value='" + $(this).find(_DdlIdField).text() + "'> " + $(this).find(_DdlTextField).text() + "</option>";
        }
        else {




            if (Utilities_check(_Selected, $(this).find(_DdlIdField).text())) {

                DropDown += "<option value='" + $(this).find(_DdlIdField).text() + "' selected> " + $(this).find(_DdlTextField).text() + "</option>";
            }
            else {

                DropDown += "<option value='" + $(this).find(_DdlIdField).text() + "'> " + $(this).find(_DdlTextField).text() + "</option>";

            }





        }
    });
    DropDown += "</select>";


    return DropDown;
}



function Utilities_check(_Selected, currentid) {

    var list = _Selected.split(',');
    var result;
    for (var i = 0; i < list.length; i++) {


        if ($.trim(list[i]) == $.trim(currentid)) {


            result = true;
            //return result;
            break;
        }
        else {
            result = false;
            //return result;
        }


    }
    return result;


}

function InsertIntoErrorlog(exceptionMessage, exceptionType, exceptionSource, exceptionUrl) {
    $.ajax({
        type: "POST",
        url: "../ErrorLog.asmx/LogError",
        cache: false,
        contentType: "application/json; charset=utf-8",
       
        data: JSON.stringify({ exceptionMessage: exceptionMessage, exceptionType: exceptionType, exceptionSource: exceptionSource, exceptionUrl: exceptionUrl }),
       
        dataType: "json",

        success: function (response) {
            //alert("utility 1 alert");
        },

        failure: function (response) {
          //alert("utility 2 alert");
        },

        error: function (response) {
         //alert("utility 3 alert");

        }
    });



}



