﻿//Constants
var FirstPage = 1;
var INDEX_OF_CHARTID = 1;
var INDEX_OF_DESCRIPTION = 2;
var INDEX_OF_DEBIT = 3;
var INDEX_OF_CREDIT = 4;
var INDEX_OF_JOURNALVOUCHERID = 1;

//End Constants

//Events
$(function () {
    FillGridView(FirstPage);
    $('#gvwList').footable();
 
});

function FillGridView(pageIndex) {
    $.ajax({
        type: "POST",
        url: "JournalVoucher.aspx/FillGrid",
        data: '{pageIndex: ' + pageIndex + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: FillGridViewCompleted,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });

}



function FillGridViewCompleted(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var JournalVoucher = xml.find("JournalVoucher");

    $("#dvGridBody").empty();


    var content = '<table class="table table-striped table-bordered dataTable"\
    cellspacing="0" rules="all" border="1" id="gvwList"\
    style="border-collapse: collapse;">\
    <thead>\
          <tr class="headerStyle">\
            <th scope="col" data-hide="expand"><input type="checkbox" id="chkCheckAll" onclick="CheckAll();"/></th>\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand">JournalVoucher Ref #<input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">Transaction Date <input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Amount <input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Remarks <input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand"></th>\
            </tr>\
            <tr class="" id="trsearch">\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand" ><input type="image" src="../Images/IconImage/icn-search.png" style="height:20px" onclick="SearchGrid();"></th>\
            <th data-hide="phone" scope="col"><input type="text" class="filter-textbox" size="4"></th>\
            <th data-hide="phone" scope="col"><input type="text" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"><input type="text" class="filter-textbox" size="4" ></th>\
            <th scope="col" data-hide="expand"></th>\
            <th scope="col" data-hide="expand"></th>\
             </tr>\
</thead>\
    <tbody ></tbody></table';

    $("#dvGridBody").append(content);
    $.each(JournalVoucher, function () {
        var account = $(this);
        var content = '<tr class="rowStyle">\
                                <td style="width:5%;"><input type="hidden" id="hdnJournalVoucherkID" value=' + account.find("pk_JournalVoucherID").text() + '><input type="checkbox" id="chkCheck" ></td>\
                                <td><input type="image" name="edit" id="' + account.find("pk_JournalVoucherID").text() + '" title="Edit" src="../Images/IconImage/icn-edit.png" onclick="GetDataForUpdate(' + account.find("pk_JournalVoucherID").text() + ');return false;" style="height:20px;width:20px;" class="phone footable-loaded"/></td>\
                                <td>' + account.find("JournalVoucherRefNo").text() + '</td>\
                                <td>' + account.find("TransactionDate").text() + '</td>\
                                <td>' + account.find("Amount").text() + '</td>\
                                <td>' + account.find("Description").text() + '</td>\
                                <td style="width:5%;"><a href="../ReportViewer.aspx?ID=' + account.find("pk_JournalVoucherID").text() + '&ReportType=' + $("#cphBody_hidFormType").val() + '"><img src="../Images/IconImage/icn-print.png" style="height25px;width:25px"></a></td>\
                                 </tr>'

        $("#dvGridBody table> tbody").append(content);
    });

    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        LabelName: "lblResultEntries",
        RecordCount: parseInt(pager.find("RecordCount").text())
    });

    $('#gvwList').footable();

};

function AddNew() {

    BindAccountName('ddlAccountName');
    BindVoucher();
    BindCompany();
    $("#btnSave").val("Save");
    $("#divViewPanel").attr("Style", "display:none;");
    $("#divAddEditPanel").attr("Style", "display:block");
}

function BindAccountName(ddlAccountID) {
    $.ajax({
        type: "POST",
        url: "JournalVoucher.aspx/BindAccountName",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (r) {
            var ddlAccountName = $("#" + ddlAccountID);
            ddlAccountName.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                //  ddlVoucheBook.empty();
                ddlAccountName.append($("<option></option>").val(this['pk_ChartID']).html(this['ChartName']));

            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
        }

    });
}

function BindCompany() {
    $.ajax({
        type: "POST",
        url: "JournalVoucher.aspx/BindCompany",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlCompany = $("[id*=ddlCompany]");
            ddlCompany.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                ddlCompany.append($("<option></option>").val(this['CompanyID']).html(this['CompanyName']));
            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
        }
    });


}

function AddEditJournalVoucher() {

    if ($("#form1").valid() && $('#gvwDetail >tbody >tr').length > 0) {


        var jsonObjects = [
            {
               pk_JournalVoucherID: $('#hidEdit').val(),
               TransactionDate: $('#txtVoucherDate').val(),
               fkCompanyID: $('#ddlCompany').val(),
               Remarks: $('#txtRemarks').val(),
             }
        ];
        if ($("#btnSave").val() === "Save") {
            var JournalVoucherDetail = new Array();
            $('[id*=gvwDetail]').find('tbody>tr:has(td)').each(function () {
                var JournalVoucherDetails = {};
                JournalVoucherDetails.fk_ChartID = $(this).find("td:nth-child(" + INDEX_OF_CHARTID + ")").find('select').val();
                JournalVoucherDetails.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val();
                JournalVoucherDetails.Debit = $(this).find("td:nth-child(" + INDEX_OF_DEBIT + ")").find('input').val();
                JournalVoucherDetails.Credit = $(this).find("td:nth-child(" + INDEX_OF_CREDIT + ")").find('input').val();
                JournalVoucherDetail.push(JournalVoucherDetails);
            });
            $.ajax({
                type: "POST",
                url: "JournalVoucher.aspx/InsertJournalVoucher",
                 data: '{JournalVoucher: ' + JSON.stringify(jsonObjects) + ', JournalVoucherDetail: ' + JSON.stringify(JournalVoucherDetail) + '}',
                //data : '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");
                    }
                    else if (result.d === "redirect") {
                        var url = document.location.toString().split('/');
                        var redirectLink = url[0] + "//" + url[2] + '/' + 'Login.aspx';
                        window.location = redirectLink;
                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                    }
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                }

            });
            $("#btnPrint").attr("style", "display:block;float: right;margin-right: 3px;");
            
        }



        else {

            var JournalVoucherDetails = new Array();
            $('[id*=gvwDetail]').find('tbody>tr:has(td)').each(function () {
                var JournalVoucherDetail = {};
                JournalVoucherDetail.Fk_ChartID = $(this).find("td:nth-child(" + INDEX_OF_CHARTID + ")").find('select').val();
                JournalVoucherDetail.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val();
                JournalVoucherDetail.Amount = $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val();
                JournalVoucherDetails.push(JournalVoucherDetail);
            });

            $.ajax({
                type: "POST",
                url: "JournalVoucher.aspx/Update",
                data: '{JournalVoucher: ' + JSON.stringify(jsonObjects) + ', JournalVoucherDetail: ' + JSON.stringify(JournalVoucherDetails) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");
                    }
                    else if (result.d === "redirect") {
                        var url = document.location.toString().split('/');
                        var redirectLink = url[0] + "//" + url[2] + '/' + 'Login.aspx';
                        window.location = redirectLink;
                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                    }

                },
                failure: function (response) {
                    ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                }

            });
            $("#btnPrint").attr("style", "display:block;float: right;margin-right: 3px;");
            GetLastInsertedID($("#cphBody_hidFormType").val(), "update");
        }

        FillGridView(FirstPage);
        RefreshModal();
        $("#divViewPanel").attr("Style", "display:block;");
        $("#divAddEditPanel").attr("Style", "display:none");

    }
    else {
        ShowMessageBox(AexpMessages.AccountsMessages.ErrorBankBookDetailGridEmpty, "error");
    }
}

function AddDetailData() {
    if ($("#form1").valid()) {

        var ddlAccountID = 'ddlAccountName' + Math.random().toString(36).substr(2, 9);
        var txtDescription = 'txtDescription' + Math.random().toString(36).substr(2, 9);
        var txtDebit = 'txtDebit' + Math.random().toString(36).substr(2, 9);
        var txtCredit = 'txtcCredit' + Math.random().toString(36).substr(2, 9);


        var content = '<tr>\
                                        <td style="width: 30%">\
                                            <select id="' + ddlAccountID + '" class="gridDetail-accountname"></select></td>\
                                        <td style="width: 49%">\
                                            <input id="' + txtDescription + '" class="gridDetail-description"></td>\
                                        <td style="width: 20%">\
                                        <input id="' + txtDebit + '" data-rule-number="true" class="gridDetail-amount" ></td>\
                                        <td style="width: 20%">\
                                            <input id="' + txtCredit + '" data-rule-number="true" class="gridDetail-amount" ></td>\
                                        <td style="width: 1%">\
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>\
                                        <tr>'

        $("#dvBodyDetail").append(content);
        BindAccountName(ddlAccountID);

    }
}

//function DeleteJournalVoucher(JournalVoucherID) {
//    $("#hidDelete").val(JournalVoucherID);
//    $.ajax({
//        type: "POST",
//        url: "JournalVoucher.aspx/DeleteJournalVoucher",
//        data: '{JournalVoucherID: ' + JournalVoucherID + '}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            closeDeleteModal();
//            if (response.d === "true") {
//                ShowMessageBox(AexpMessages.AccountsMessages.Delete, "success");
//            }
//            else {
//                ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");
//            }
//            FillGridView(FirstPage);
//        },
//        failure: function (response) {
//            ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");

//        },
//        error: function (response) {
//            ShowMessageBox(response.d, "error");
//        }
//    });


//}

//$(".Pager .page").live("click", function () {
//    FillGridView(parseInt($(this).attr('page')));

//});

//function DeleteDetailGridData(element) {
//    element.parentNode.parentNode.remove();
//}

function RefreshModal() {

    //var validator = $("#form1").validate();
    //validator.resetForm();
    //$(".control-group").attr("class", "control-group");
    //$("#dvBodyDetail").empty();
    $("#divViewPanel").attr("Style", "display:block;");
    $("#divAddEditPanel").attr("Style", "display:none");
    $("#btnSave").val("Save");

    $("#txtVoucherNo").val("");
    $("#txtVoucherDate").val("");
   
    $("#txtChequeDate").val("");
    $("#txtRemarks").val("");
    $("#dvBodyDetail").empty();

    var content = '<tr>\
                        <td style="width: 30%">\
                        <select name="Account Name" id="ddlAccountName" class="gridDetail-accountname"></select></td>\
                        <td style="width: 49%">\
                                <input id="txtDescription" class="gridDetail-description"></td>\
                        <td style="width: 20%">\
                            <input id="txtDebit" class="gridDetail-amount"></td>\
                        <td style="width: 20%">\
                        <input id="txtCredit" class="gridDetail-amount"></td>\
                        <td style="width: 1%">\
                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>\
        <tr>'
$("#dvBodyDetail").append(content);

    BindCompany();
    BindVoucher();
    BindAccountName('ddlAccountName');
}

function EditData() {
    var GridData = new Array();
    $('[id*=gvwList]').find('tbody>tr:has(td)').each(function () {
        var GridRowData = {};
        GridRowData.Fk_ChartID = $(this).find("td:nth-child(" + INDEX_OF_CHARTID + ")").find('select').val();
        GridRowData.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val();
        GridRowData.Amount = $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val();
        GridRowData.push(BankBookDetail);
    });
}


    




function BindVoucher() {
    $.ajax({
        type: "POST",
        url: "JournalVoucher.aspx/BindVoucherBook",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlVoucheBook = $("[id*=ddlVoucheBook]");
            ddlVoucheBook.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                //  ddlVoucheBook.empty();
                ddlVoucheBook.append($("<option></option>").val(this['pk_ChartID']).html(this['ChartName']));

            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
        }

    });
}




function DeleteJournalVoucher(JournalVoucherID) {
    var isDelete = false;
    $('[id*=gvwList]').find('tbody>tr:has(td)').each(function () {
        if ($(this).find('input:checkbox').prop('checked') === true) {
            var jsid = $(this).find("td:nth-child(" + INDEX_OF_JOURNALVOUCHERID + ")").find('input').val();
            //    CashBookDetails.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val();
            //   CashBookDetails.Amount = $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val();



            $.ajax({
                type: "POST",
                url: "JournalVoucher.aspx/DeleteJournalVoucher",
                data: '{JournalVoucherID: ' + jsid + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    closeDeleteModal();
                    if (response.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Delete, "success");
                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");
                    }
                    FillGridView(FirstPage, $("#ddlPageSize").val(), '', '', '', '', '', '', '');
                },
                failure: function (response) {
                    ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");

                },
                error: function (response) {
                    ShowMessageBox(response.d, "error");
                }

            });
            isDelete = true;
        }

    });

    if (!isDelete) {
        ShowMessageBox("Please select row for delete", "error");
    }
}

