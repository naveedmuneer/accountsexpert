﻿//Constants
var FirstPage = 1;
var INDEX_OF_CHARTID = 1;
var INDEX_OF_DESCRIPTION = 2;
var INDEX_OF_AMOUNT = 3;

//End Constants

//Events
$(function () {
    BindCompany();
    BindVoucher();
    BindAccountName('ddlAccountName');
    FillGridView(FirstPage);
    $('#gvwList').footable();
    $("#txtVoucherDate").datepicker();
    $("#txtChequeDate").datepicker();
    //$("#AddNewModal").attr("Style", "display:none");
    var myDatePicker = $('#txtVoucherDate').glDatePicker(
   {
       cssName: 'default',
       dowNames: ['Su', 'M', 'Tu', 'W', 'T', 'F', 'Sa'],
       borderSize: 2,
   }).glDatePicker(true);
    myDatePicker.render();

});

function FillGridView(pageIndex) {
    $.ajax({
        type: "POST",
        url: "CashBooks.aspx/FillGrid",
        data: '{pageIndex: ' + pageIndex + ',formType:"' + $("#cphBody_hidFormType").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: FillGridViewCompleted,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });

}

function FillGridViewCompleted(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var chartAccount = xml.find("CashBook");

    $("#dvGridBody").empty();


    var content = '<table class="footable"\
    cellspacing="0" rules="all" border="1" id="gvwList"\
    style="border-collapse: collapse;">\
    <thead>\
        <tr class="headerStyle">\
            <th scope="col" data-hide="expand"></th>\
            <th scope="col" data-hide="expand">Report</th>\
            <th scope="col" data-hide="expand">Voucher No</th>\
            <th scope="col" data-hide="phone">Voucher Date</th>\
            <th data-hide="phone" scope="col">Voucher Book</th>\
            <th data-hide="phone" scope="col">Cheque No</th>\
            <th data-hide="phone" scope="col">Cheque Date</th>\
            <th data-hide="phone" scope="col">Amount</th>\
            <th data-hide="phone" scope="col">Remarks</th>\
            <th scope="col" data-hide="expand"></th>\
           </tr>\
    </thead>\
    <tbody ></tbody></table';

    $("#dvGridBody").append(content);
    $.each(chartAccount, function () {
        var account = $(this);
        var content = '<tr class="rowStyle">\
                                <td><input type="image" name="edit" id="' + account.find("pk_CashBookID").text() + '" title="Edit" src="../Images/IconImage/icn-edit.png" onclick="GetDataForUpdate(' + account.find("pk_CashBookID").text() + ');return false;" style="height:20px;width:20px;" class="phone footable-loaded"/></td>\
                                <td><a href="../ReportViewer.aspx?ID=' + account.find("pk_CashBookID").text() + '&ReportType=' + $("#cphBody_hidFormType").val() + '">Print Voucher</a></td>\
                                <td>' + account.find("CashBookRefNo").text() + '</td>\
                                <td>' + account.find("TransactionDate").text() + '</td>\
                                <td>' + account.find("VoucherBook").text() + '</td>\
                                <td>' + account.find("ChequeNo").text() + '</td>\
                                <td>' + account.find("ChequeDate").text() + '</td>\
                                <td>' + account.find("Amount").text() + '</td>\
                                <td>' + account.find("Remarks").text() + '</td>\
                                <td><input type="image" name="delete" id="' + account.find("pk_CashBookID").text() + '" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="openDeleteModal(' + account.find("pk_CashBookID").text() + ',DeleteCashBook);return false;" style="height:20px;width:20px;" class="phone footable-loaded"/></td>\
                                </tr>'

        $("#dvGridBody table> tbody").append(content);
    });

    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });

    $('#gvwList').footable();

};
function BindCompany() {
    $.ajax({
        type: "POST",
        url: "CashBooks.aspx/BindCompany",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlCompany = $("[id*=ddlCompany]");
            ddlCompany.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                ddlCompany.append($("<option></option>").val(this['PkCompanyID']).html(this['CompanyName']));
            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
        }
    });


}


function BindVoucher() {
    $.ajax({
        type: "POST",
        url: "CashBooks.aspx/BindVoucherBook",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlVoucheBook = $("[id*=ddlVoucheBook]");
            ddlVoucheBook.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                //  ddlVoucheBook.empty();
                ddlVoucheBook.append($("<option></option>").val(this['pk_ChartID']).html(this['ChartName']));

            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
        }

    });
}




function AddEditCashBook() {


    if ($("#form1").valid() && $('#gvwDetail >tbody >tr').length > 0) {

        var jsonObjects = [
           {
               pk_CashBookID: $('#hidEdit').val(),
               InputType: $('#cphBody_hidFormType').val(),
               CompanyID: $('#ddlCompany').val(),
               CashBookRefNo: $('#txtVoucherNo').val(),
               TransactionDate: $('#txtVoucherDate').val(),
               FkChartID: Number($('#ddlVoucheBook').val()),
               ChequeNo: Number($('#txtChequeNo').val()),
               ChequeDate: $('#txtChequeDate').val(),
               Remarks: $('#txtRemarks').val(),
               Created_By: 'admin'
           }
        ];
        if ($("#btnSave").val() === "Save") {

            var CashBookDetail = new Array();
            $('[id*=gvwDetail]').find('tbody>tr:has(td)').each(function () {
                var CashBookDetails = {};
                CashBookDetails.Fk_ChartID = $(this).find("td:nth-child(" + INDEX_OF_CHARTID + ")").find('select').val();
                CashBookDetails.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val();
                CashBookDetails.Amount = $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val();
                CashBookDetail.push(CashBookDetails);


            });
            $.ajax({
                type: "POST",
                url: "CashBooks.aspx/InsertCashBook",
                data: '{CashBook: ' + JSON.stringify(jsonObjects) + ', CashBookDetail: ' + JSON.stringify(CashBookDetail) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");
                    }
                    else if (result.d === "redirect") {
                        var url = document.location.toString().split('/');
                        var redirectLink = url[0] + "//" + url[2] + '/' + 'Login.aspx';
                        window.location = redirectLink;
                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                    }
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.Save, "error");
                }

            });
            $("#btnPrint").attr("style", "display:block;float: right;margin-right: 3px;");
            GetLastInsertedID($("#cphBody_hidFormType").val(), "insert");
        }
        else {

            var CashBookDetail = new Array();
            $('[id*=gvwDetail]').find('tbody>tr:has(td)').each(function () {
                var CashBookDetails = {};
                CashBookDetails.Fk_ChartID = $(this).find("td:nth-child(" + INDEX_OF_CHARTID + ")").find('select').val();
                CashBookDetails.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val();
                CashBookDetails.Amount = $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val();
                CashBookDetail.push(CashBookDetails);


            });

            $.ajax({
                type: "POST",
                url: "CashBooks.aspx/Update",
                data: '{CashBook: ' + JSON.stringify(jsonObjects) + ', CashBookDetail: ' + JSON.stringify(CashBookDetail) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Update, "success");
                    }
                    else if (result.d === "redirect") {
                        var url = document.location.toString().split('/');
                        var redirectLink = url[0] + "//" + url[2] + '/' + 'Login.aspx';
                        window.location = redirectLink;
                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                    }

                },
                failure: function (response) {
                    ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                }

            });
            $("#btnPrint").attr("style", "display:block;float: right;margin-right: 3px;");
            GetLastInsertedID($("#cphBody_hidFormType").val(), "update");
        }

        FillGridView(FirstPage);
        RefreshModal();
        $("#divViewPanel").attr("Style", "display:block;");
        $("#divAddEditPanel").attr("Style", "display:none");


    }
    else {
        ShowMessageBox(messages.errorCashBookDetailGridEmpty, "error");
    }
}
$(".Pager .page").live("click", function () {
    FillGridView(parseInt($(this).attr('page')));

});



function AddNew() {

    $("#btnSave").val("Save");
    $("#divViewPanel").attr("Style", "display:none;");
    $("#divAddEditPanel").attr("Style", "display:block");

}


function GetDataForUpdate(cashBookID) {
    $("#hidEdit").val(cashBookID);
    $.ajax({
        type: "POST",
        url: "CashBooks.aspx/GetDataForUpdate",
        data: '{cashBookID: ' + cashBookID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            var updateDate = xml.find("UpdateData");
            $("#dvBodyDetail").empty()
            $.each(updateDate, function () {
                var account = $(this);
                $('#ddlCompany').val(account.find("fkCompanyID").text());
                $('#ddlVoucheBook').val(account.find("VoucherBook").text());
                $('#txtVoucherNo').val(account.find("CashBookRefNo").text());
                $('#txtVoucherDate').val(account.find("VoucherDate").text());
                $('#txtChequeNo').val(account.find("ChequeNo").text());
                $('#txtChequeDate').val(account.find("ChequeDates").text());
                $('#txtRemarks').val(account.find("Remarks").text());

                var ddlAccountID = 'ddlAccountName' + Math.random().toString(36).substr(2, 9);
                var txtDescription = 'txtDescription' + Math.random().toString(36).substr(2, 9);
                var txtAmount = 'txtAmount' + Math.random().toString(36).substr(2, 9);

                var content = '<tr>\
                                        <td style="width: 30%">\
                                            <select id="' + ddlAccountID + '" class="gridDetail-accountname"></select></td>\
                                        <td style="width: 49%">\
                                            <input id="' + txtDescription + '" class="gridDetail-description"></td>\
                                        <td style="width: 20%">\
                                            <input id="' + txtAmount + '" data-rule-number="true"  class="gridDetail-amount"  onchange="GetTotal();"></td>\
                                        <td style="width: 1%">\
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>\
                            <tr>'

                $("#dvBodyDetail").append(content);
                BindAccountName(ddlAccountID);
                $("#" + ddlAccountID).val(account.find("AccountID").text());
                $("#" + txtDescription).val(account.find("Description").text());
                $("#" + txtAmount).val(account.find("Amount").text());
            });
            $("#divViewPanel").attr("Style", "display:none;");
            $("#divAddEditPanel").attr("Style", "display:block");

            $("#btnSave").val("Update");
            GetTotal();
        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });


}

//function GetDataForUpdateDetailGrid(cashBookDetailID) {
//    $("#hidEdit").val(cashBookDetailID);
//    $.ajax({
//        type: "POST",
//        url: "ChartOfAccounts.aspx/GetDataForUpdateDetail",
//        data: '{CashBookDetailID: ' + cashBookDetailID + '}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {

//            var xmlDoc = $.parseXML(response.d);
//            var xml = $(xmlDoc);
//            var updateDate = xml.find("UpdateDetailData");

//            $.each(updateDate, function () {
//                var account = $(this);
//                $('#ddlAccountName').val(account.find("fk_ChartID").text());
//                $('#txtDescription').val(account.find("Description").text());
//                $('#txtAmount').val(account.find("Amount").text());

//            });
//            //$("#AddNewModal").attr("Style", "display:block");
//            //$("#AddNewModal").modal('show');
//            $("#dvAddEdit").attr("style", "display:block;");
//            $("#dvView").attr("style", "display:none;");
//            $("#btnSave").val("Update");

//        },
//        failure: function (response) {
//            alert(response.d);
//        },
//        error: function (response) {
//            alert(response.d);
//        }
//    });
//}


function AddDetailData() {
    if ($("#form1").valid()) {

        var ddlAccountID = 'ddlAccountName' + Math.random().toString(36).substr(2, 9);
        var txtDescription = 'txtDescription' + Math.random().toString(36).substr(2, 9);
        var txtAmount = 'txtAmount' + Math.random().toString(36).substr(2, 9);

        var content = '<tr>\
                                        <td style="width: 30%">\
                                            <select id="' + ddlAccountID + '" class="gridDetail-accountname"></select></td>\
                                        <td style="width: 49%">\
                                            <input id="' + txtDescription + '" class="gridDetail-description""></td>\
                                        <td style="width: 20%">\
                                            <input id="' + txtAmount + '" data-rule-number="true" class="gridDetail-amount"  onchange="GetTotal();"></td>\
                                        <td style="width: 1%">\
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>\
                            <tr>'

        $("#dvBodyDetail").append(content);
        BindAccountName(ddlAccountID);

    }

}

function RefreshModal() {

    //var validator = $("#form1").validate();
    //validator.resetForm();
    //$(".control-group").attr("class", "control-group");
    //$("#dvBodyDetail").empty();
    $("#divViewPanel").attr("Style", "display:block;");
    $("#divAddEditPanel").attr("Style", "display:none");
    $("#btnSave").val("Save");

    $("#txtVoucherNo").val("");
    $("#txtVoucherDate").val("");
    $("#txtChequeNo").val("");
    $("#txtChequeDate").val("");
    $("#txtRemarks").val("");
    $("#dvBodyDetail").empty();

    var content = '<tr>\
                                        <td style="width: 30%">\
                                            <select id="ddlAccountName" class="gridDetail-accountname"></select></td>\
                                        <td style="width: 49%">\
                                            <input id="txtDescription" class="gridDetail-description"></td>\
                                        <td style="width: 20%">\
                                            <input id="txtAmount" data-rule-number="true" class="gridDetail-amount"  onchange="GetTotal();"></td>\
                                        <td style="width: 1%">\
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>\
                            <tr>'
    $("#dvBodyDetail").append(content);

    BindCompany();
    BindVoucher();
    BindAccountName('ddlAccountName');
}

function DeleteDetailGridData(element) {
    element.parentNode.parentNode.remove();
}

function DeleteCashBook(cashBookID) {
    $("#hidDelete").val(cashBookID);
    $.ajax({
        type: "POST",
        url: "CashBooks.aspx/DeleteCashBook",
        data: '{cashBookID: ' + cashBookID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            closeDeleteModal();
            if (response.d === "true") {
                ShowMessageBox(AexpMessages.AccountsMessages.Delete, "success");
            }
            else {
                ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");
            }
            FillGridView(FirstPage);
        },
        failure: function (response) {
            ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");

        },
        error: function (response) {
            ShowMessageBox(response.d, "error");
        }
    });


}



function BindAccountName(ddlAccountID) {
    $.ajax({
        type: "POST",
        url: "BankBook.aspx/BindAccountName",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (r) {
            var ddlAccountName = $("#" + ddlAccountID);
            ddlAccountName.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                //  ddlVoucheBook.empty();
                ddlAccountName.append($("<option></option>").val(this['pk_ChartID']).html(this['ChartName']));

            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
        }

    });
}
function CallReport() {
    document.getElementById("btnPrint").href = "../ReportViewer.aspx?ID=" + $("#hidCashBookID").val() + "&ReportType=" + $('#cphBody_hidFormType').val();
}

function GetLastInsertedID(cashBookType, transactionType) {
    $.ajax({
        type: "POST",
        url: "CashBooks.aspx/GetLastInsertedID",
        data: '{cashBookType:"' + cashBookType + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (transactionType === "insert")
                $("#hidCashBookID").val(response.d);
            else
                $("#hidCashBookID").val($("#hidEdit").val());

            CallReport();

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}

function GetTotal() {
    $("#txtCashBookTotal").attr('value', "0.00");
    $('[id*=gvwDetail]').find('tbody>tr:has(td)').each(function () {
        $("#txtCashBookTotal").attr('value', (parseFloat($("#txtCashBookTotal").attr('value')) + parseFloat($(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val())));
    });
}
