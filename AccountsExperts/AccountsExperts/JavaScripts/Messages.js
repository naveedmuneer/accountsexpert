﻿/*******************************************************************************************************************
Class       : Messages.js
Who created  : Naveed Muneer
When Created : 
Description  :
********************************************************************************************************************
MODIFICATION HISTORY:-
********************************************************************************************************************
Who(SNo)		Date          Description
********************************************************************************************************************

********************************************************************************************************************/

var AexpMessages = new Messages();

function Messages()
{
    this.AccountsMessages = new AccountsMessages();
    //this.AccountsMessages = new AccountsMessages();
    
}

function AccountsMessages()
{
    this.Save = "Data Saved Successfully.";
    this.Delete = "Data Deleted Successfully.";
    this.Update = "Data Updated Successfully.";
    this.ErrrUnspecified = "An unspecified error occurs.Please Contact your Administrator";
    this.ErrorSave = "Error in save data.";
    this.ErrorDelete = "Error in delete data.";
    this.ErrorUpdate = "Error in update data.";
    this.ErrorCashBookDetailGridEmpty = "Please fill information.";
    this.ErrorBankBookDetailGridEmpty = "Please fill information.";
}

//var messages = {
    
//        "save": "Data Saved Successfully",
//        "delete": "Data Deleted Successfully",
//        "update": "Data Updated Successfully",
//        "errorSave": "Error in save data",
//        "errorDelete": "Error in delete data",
//        "errorUpdate": "Error in update data",
//        "errorCashBookDetailGridEmpty": "Please fill other information",

    
//}