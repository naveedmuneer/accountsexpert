﻿//Constants
var FirstPage = 1;
var INDEX_OF_CHARTID = 1;
var INDEX_OF_DESCRIPTION = 3;
var INDEX_OF_AMOUNT = 4;

//End Constants


//Events
$(function () {

    BindDepartment();
    BindDesignation();
    BindCompany();
    FillGridView(FirstPage);
    $('#gvwList').footable();
});


function AddNew() {

    $("#btnSave").val("Save");
    $("#mdlAddNew").attr("Style", "display:block");
    $("#mdlAddNew").modal('show');
}

function FillGridView(pageIndex) {
    $.ajax({
        type: "POST",
        url: "Employee.aspx/FillGrid",
        data: '{pageIndex: ' + pageIndex + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: FillGridViewCompleted,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Employee.aspx/FillGrid')
        }
    });

}

function FillGridViewCompleted(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var employeeData = xml.find("EmployeeData");

    $("#dvGridBody").empty();

    var content = '<table class="table table-striped table-bordered dataTable"\
    cellspacing="0" rules="all" border="1" id="gvwList"\
    style="border-collapse: collapse;">\
    <thead>\
         <tr class="headerStyle">\
            <th scope="col" data-hide="expand">Edit</th>\
            <th scope="col" data-hide="expand">Name <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">NIC <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="phone">Email <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Company Name <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Department <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Designation <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Contact <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Secondary Contact <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Address <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Permenant Address <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Is Active</th>\
            <th data-hide="phone" scope="col">Created On</th>\
            <th scope="col" data-hide="expand">Delete</th>\
           </tr>\
            <tr class="" id="trsearch">\
            <th scope="col" data-hide="expand" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th scope="col" data-hide="expand" ></th>\
             <th scope="col" data-hide="expand" ></th>\
             <th scope="col" data-hide="expand" ></th>\
 </tr>\
    </thead>\
    <tbody ></tbody></table';


    $("#dvGridBody").append(content);
    $.each(employeeData, function () {
        var account = $(this);
        var content = '<tr class="rowStyle">\
                                <td><input type="image" name="edit" id="' + account.find("pk_Employee_ID").text() + '" title="Edit" src="../Images/IconImage/icn-edit.png" onclick="GetDataForUpdate(' + account.find("pk_Employee_ID").text() + ');return false;" style="height:20px;width:20px;" class="phone footable-loaded"/></td>\
                                <td>' + account.find("Employee_Name").text() + '</td>\
                                <td>' + account.find("NIC_No").text() + '</td>\
                                <td>' + account.find("Email").text() + '</td>\
                                <td>' + account.find("Company_Name").text() + '</td>\
                                <td>' + account.find("Department_Name").text() + '</td>\
                                <td>' + account.find("Designation_Name").text() + '</td>\
                                <td>' + account.find("Contact_No").text() + '</td>\
                                <td>' + account.find("Secondary_Contact_No").text() + '</td>\
                                <td>' + account.find("Address").text() + '</td>\
                                <td>' + account.find("Permenant_Address").text() + '</td>\
                                <td>' + account.find("Is_Active").text() + '</td>\
                                <td>' + account.find("Created_On").text() + '</td>\
                                <td><input type="image" name="delete" id="' + account.find("pk_Employee_ID").text() + '" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="openDeleteModal(' + account.find("pk_Employee_ID").text() + ',DeleteEmployee);return false;" style="height:20px;width:20px;" class="phone footable-loaded"/></td>\
                                </tr>'

        $("#dvGridBody table> tbody").append(content);
    });

    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });

    $('#gvwList').footable();

};

$(".Pager .page").live("click", function () {
    FillGridView(parseInt($(this).attr('page')));

});

function DeleteEmployee(employeeID) {
    $("#hidDelete").val(employeeID);
    $.ajax({
        type: "POST",
        url: "Employee.aspx/DeleteEmployee",
        data: '{employeeID: ' + employeeID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            closeDeleteModal();
            if (response.d === "true") {
                ShowMessageBox(AexpMessages.AccountsMessages.Delete, "success");
            }
            else {
                ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");
            }
            FillGridView(FirstPage);
        },
        failure: function (response) {
            ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");

        },
        error: function (response) {
            ShowMessageBox(response.d, "error");
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Employee.aspx/DeleteEmployee')
        }
    });


}

function BindDepartment() {
    $.ajax({
        type: "POST",
        url: "Employee.aspx/BindDepartment",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlDepartment = $("[id*=ddlDepartment]");
            ddlDepartment.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                ddlDepartment.append($("<option></option>").val(this['pkDepartment_ID']).html(this['Department_Name']));
            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Employee.aspx/BindDepartment')
        }
    });


}

function BindDesignation() {
    $.ajax({
        type: "POST",
        url: "Employee.aspx/BindDesignation",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlDesignation = $("[id*=ddlDesignation]");
            ddlDesignation.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                ddlDesignation.append($("<option></option>").val(this['pkDesignation_ID']).html(this['Designation_Name']));
            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Employee.aspx/BindDesignation')
        }
    });


}


function BindCompany() {
    $.ajax({
        type: "POST",
        url: "Employee.aspx/BindCompany",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlCompany = $("[id*=ddlCompany]");
            ddlCompany.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                ddlCompany.append($("<option></option>").val(this['CompanyID']).html(this['CompanyName']));
            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Employee.aspx/BindCompany')
        }
    });


}

function AddEditEmployee() {
    if ($("#form1").valid()) {
        var jsonObjects = [
                    {
                        pk_Employee_ID: $('#hidEdit').val(),
                        Employee_Name: $('#txtName').val(),
                        NIC_No: $('#txtNIC').val(),
                        fkCompany_ID: $('#ddlCompany').val(),
                        fkDepartment_ID: $('#ddlDepartment').val(),
                        fkDesignation_ID: $('#ddlDesignation').val(),
                        Email: $('#txtEmail').val(),
                        Contact_No: $('#txtContact').val(),
                        Secondary_Contact_No: $('#txtSecondaryContact').val(),
                        Address: $('#txtAddress').val(),
                        Permenant_Address: $('#txtPermenantAddress').val(),
                        Is_Active: $('#chkActive').val()
                       
                    }
        ];
        if ($("#btnSave").val() === "Save") {

            $.ajax({
                type: "POST",
                url: "Employee.aspx/InsertEmployee",
                data: '{Employee: ' + JSON.stringify(jsonObjects) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");
                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                    }
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.Save, "error");
                    InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Employee.aspx/InsertEmployee')
                }

            });

        }
        else {

            $.ajax({
                type: "POST",
                url: "Employee.aspx/UpdateEmployee",
                data: '{Employee: ' + JSON.stringify(jsonObjects) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Update, "success");
                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                    }

                },
                failure: function (response) {
                    ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                    InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Employee.aspx/UpdateEmployee')
                }


            });
        }
        FillGridView(FirstPage);
        RefreshModal();
        $("#mdlAddNew").attr("Style", "display:none");
        $("#mdlAddNew").modal('hide');

    }
}

function GetDataForUpdate(employeeID) {
    $("#hidEdit").val(employeeID);
    $.ajax({
        type: "POST",
        url: "Employee.aspx/GetDataForUpdate",
        data: '{employeeID: ' + employeeID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            var updateDate = xml.find("UpdateData");

            $.each(updateDate, function () {
                var account = $(this);

                $('#txtName').val(account.find("Employee_Name").text());
                $('#txtNIC').val(account.find("NIC_No").text());

                $('#ddlCompany').val(account.find("fkCompany_ID").text());
                $('#ddlDepartment').val(account.find("fkDepartment_ID").text());
                $('#ddlDesignation').val(account.find("fkDesignation_ID").text());
                $('#txtEmail').val(account.find("Email").text());
                $('#txtContact').val(account.find("Contact_No").text());
                $('#txtSecondaryContact').val(account.find("Secondary_Contact_No").text());
                $('#txtAddress').val(account.find("Address").text());
                $('#txtPermenantAddress').val(account.find("Permenant_Address").text());
                if (account.find("Is_Active").text() === "true") {
                    $('#chkActive').prop('checked', true);
                }
                else {
                    $('#chkActive').prop('checked', true);
                }
            });
            $("#mdlAddNew").attr("Style", "display:block");
            $("#mdlAddNew").modal('show');
            $("#btnSave").val("Update");

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Employee.aspx/GetDataForUpdate')
        }
    });


}

//Events Ends

//#Region Private Methods

function RefreshModal() {

    var validator = $("#form1").validate();
    validator.resetForm();
    $(".control-group").attr("class", "control-group");
    $("#dvBodyDetail").empty();
}
//End Private Methods