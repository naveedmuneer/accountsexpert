﻿
//Constants
var FirstPage = 1;

//End Constants


$(function () {
    
    FillGridView(FirstPage);
    $('#gvwList').footable();
   // $("#txtVoucherDate").datepicker();
   //$("#txtChequeDate").datepicker();
    

});


function AddNew() {

    $("#btnSave").val("Save");
   // $("#mdlAddNew").attr("Style", "display:block");
    $("#mdlAddNew").modal('show');
    
}

function RefreshModal() {

    //var validator = $("#form1").validate();
    //validator.resetForm();
    //$(".control-group").attr("class", "control-group");
    //$("#dvBodyDetail").empty();
}

function AddEditRoles() {


    if ($("#form1").valid() ) 
    {
        if ($("#chkActive").is(':checked'))
            $('#chkActive').val("true");  // checked
        else
            $('#chkActive').val("false");
       
        if ($("#btnSave").val() == "Save") {
            var jsonObjects = [
           {
               Role_Name: $('#txtRoleName').val(),
               Is_Active: $('#chkActive').val()
                
           }
            ];
            

            $.ajax({
                type: "POST",
                url: "Roles.aspx/InsertRole",
                data: '{lstRoles: ' + JSON.stringify(jsonObjects) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");
                        
                    }
                    else {
                        ShowMessageBox("Duplicate row", "error");
                    }
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                    InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Roles.aspx/InsertRole')
                }

            });

        }
        else
        {
            var jsonObjects = [
            {
                Role_Name: $('#txtRoleName').val(),
                Is_Active: $('#chkActive').val(),
                pkRole_ID: $("#hidEdit").val()
            }
            ];

            

            $.ajax({
                type: "POST",
                url: "Roles.aspx/UpdateRole",
               
                data: '{lstRoles: ' + JSON.stringify(jsonObjects) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");
                    }
                    else {
                        ShowMessageBox("Duplicate row", "error");
                    }

                },
                failure: function (response) {
                    ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                    InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Roles.aspx/UpdateRole')
                }

            });
        }

        FillGridView(FirstPage);
        RefreshModal();
        $("#mdlAddNew").attr("Style", "display:none");
        $("#mdlAddNew").modal('hide');
    }
    else {
        ShowMessageBox(messages.errorCashBookDetailGridEmpty, "error");
    }

}

function FillGridView(pageIndex) {
    $.ajax({
        type: "POST",
        url: "Roles.aspx/FillGrid",
        data: '{pageIndex: ' + pageIndex + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: FillGridViewCompleted,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Roles.aspx/FillGrid')
        }
    });

}

function FillGridViewCompleted(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var role = xml.find("Roles");

    $("#dvGridBody").empty();

    var content = '<table class="table table-striped table-bordered dataTable"\
    cellspacing="0" rules="all" border="1" id="gvwList"\
    style="border-collapse: collapse;">\
    <thead>\
        <tr class="headerStyle">\
            <th scope="col" data-hide="expand">Edit</th>\
            <th scope="col" data-hide="expand">Role <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">Active</th>\
            <th data-hide="phone" scope="col">Created On</th>\
            <th data-hide="phone" scope="col">Created By <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">Delete</th>\
           </tr>\
            <tr class="" id="trsearch">\
             <th scope="col" data-hide="expand" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th scope="col" data-hide="expand" ></th>\
             <th scope="col" data-hide="expand" ></th>\
             <th data-hide="phone,tablet" scope="col"><input type="text" class="filter-textbox" size="5" ></th>\
             <th scope="col" data-hide="expand"></th>\
           </tr>\
    </thead>\
    <tbody ></tbody></table';

    $("#dvGridBody").append(content);
    $.each(role, function () {
        var roles = $(this);
        var content = '<tr class="rowStyle">\
                                <td><input type="image" name="edit" id="' + roles.find("pkRole_ID").text() + '" title="Edit" src="../Images/IconImage/icn-edit.png" onclick="GetDataForUpdate(' + roles.find("pkRole_ID").text() + ');return false;" style="height:17px;width:17px;" class="phone footable-loaded"/></td>\
                                <td>' + roles.find("Role_Name").text() + '</td>\
                                <td>' + roles.find("Is_Active").text() + '</td>\
                                <td>' + roles.find("Created_On").text() + '</td>\
                                <td>' + roles.find("Created_By").text() + '</td>\
                                <td><input type="image" name="delete" id="' + roles.find("pkRole_ID").text() + '" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="openDeleteModal(' + roles.find("pkRole_ID").text() + ',DeleteRole);return false;" style="height:17px;width:17px;" class="phone footable-loaded"/></td>\
                                </tr>'

        $("#dvGridBody table> tbody").append(content);
    });

    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        LabelName: "lblResultEntries",
        RecordCount: parseInt(pager.find("RecordCount").text())
    });

    $('#gvwList').footable();

};

function GetDataForUpdate(RoleID) {
    $("#hidEdit").val(RoleID);
    $.ajax({
        type: "POST",
        url: "Roles.aspx/GetDataForUpdate",
        data: '{RoleID: ' + RoleID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            var updateDate = xml.find("UpdateData");

            $.each(updateDate, function () {
                var role = $(this);
            
                $('#txtRoleName').val(role.find("Role_Name").text());
                
                
                if (role.find("Is_Active").text() == "true") {
                    $('#chkActive').prop('checked', true);
                }
                else {
                    $('#chkActive').prop('checked', false);
                }

                
            });
            $("#mdlAddNew").attr("Style", "display:block");
            $("#mdlAddNew").modal('show');
            $("#btnSave").val("Update");

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Roles.aspx/GetDataForUpdate')
        }
    });


}

function DeleteRole(RoleID) {
    $("#hidDelete").val(RoleID);
    $.ajax({
        type: "POST",
        url: "Roles.aspx/DeleteRoles",
        data: '{RoleID: ' + RoleID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            closeDeleteModal();
            if (response.d === "true") {
                ShowMessageBox(AexpMessages.AccountsMessages.Delete, "success");
            }
            else {
                ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");
            }
            FillGridView(FirstPage);
        },
        failure: function (response) {
            ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");

        },
        error: function (response) {
            ShowMessageBox(response.d, "error");
            InsertIntoErrorlog('Status: ' + response.status + " " + response.responseText, 'JavaScript', 'No Source', 'Roles.aspx/DeleteRoles')
        }
    });


}
$(".Pager .page").live("click", function () {
    FillGridView(parseInt($(this).attr('page')));

});