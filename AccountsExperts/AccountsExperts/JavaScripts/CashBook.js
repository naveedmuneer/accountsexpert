﻿//Constants
var FirstPage = 1;
var INDEX_OF_CASHBOOKID = 1;
var INDEX_OF_CHARTID = 1;
var INDEX_OF_DESCRIPTION = 2;
var INDEX_OF_AMOUNT = 3;
var FormType;
var ddlCompanyData='';
var ddlVoucherBookData='';
var ddlAccountData='';
//End Constants

//Events
$(function () {
    $(".chosen-select").chosen({ width: "100%" });
    FillGridView(FirstPage, $("#ddlPageSize").val(), '', '', '', '', '', '', '');
    $('#gvwList').footable();
    //$("#txtVoucherDate").datepicker();
    //$("#txtChequeDate").datepicker();
    //$("#AddNewModal").attr("Style", "display:none");
    ToDayDate();
    $('.applyBtn btn btn-small btn-sm btn-success').after("<a href='javascript:my_function()'> Click Me </a>");

    // $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" }) //for all select having class .chosen-select
});

function ToDayDate() {
    $("#divDateRange").attr("data-start-date", moment().format('DD-MMM-YYYY'));
    $("#divDateRange").attr("data-end-date", moment().format('DD-MMM-YYYY'));
    $("#txtDateRange").text(moment().format('DD-MMM-YYYY') + " | " + moment().format('DD-MMM-YYYY'));
}
//Call for geting data from cashbook tables and show in grid
function FillGridView(pageIndex, pageSize, startDate, endDate, searchVoucherNo, searchVoucherDate, searchVoucherBook, searchChequeNo, searchChequeDate) {
    $.ajax({
        type: "POST",
        url: "CashBooks.aspx/FillGrid",

        data: '{pageIndex: ' + pageIndex + ',formType:"' + $("#cphBody_hidFormType").val() + '",pageSize:' + pageSize + ',transactionStartDate:"' + startDate + '",transactionEndDate:"' + endDate + '",searchVoucherNo:"' + searchVoucherNo + '",searchVoucherDate:"' + searchVoucherDate + '",searchVoucherBook:"' + searchVoucherBook + '",searchChequeNo:"' + searchChequeNo + '",searchChequeDate:"' + searchChequeDate + '"}',



        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: FillGridViewCompleted,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });

}
//Get data from webmethod and make grid and bind data
function FillGridViewCompleted(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var chartAccount = xml.find("CashBook");

    $("#dvGridBody").empty();


    var content = '<table class="table table-striped table-bordered dataTable"\
    cellspacing="0" rules="all" border="1" id="gvwList"\
    style="border-collapse: collapse;font-size:11px">\
    <thead>\
         <tr class="headerStyle" style="height: 32px;">\
            <th scope="col" data-hide="expand"><input type="checkbox" id="chkCheckAll" onclick="CheckAll();"/></th>\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand" >Voucher #<input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">Date<input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Book<input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Cheque #<input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Date<input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col" style="text-align: right;">Amount <input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand"></th>\
            </tr>\
            <tr class="" id="trsearch">\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand" ><input type="image" src="../Images/IconImage/icn-search.png" style="height:20px" onclick="SearchGrid();"></th>\
            <th data-hide="phone" scope="col"><input type="text" id="txtSearchVoucherNo" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"><input type="text" id="txtSearchVoucherDate" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"><input type="text" id="txtSearchVoucherBook" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"><input type="text" id="txtSearchChequeNo" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"><input type="text" id="txtSearchChequeDate" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"></th>\
                <th scope="col" data-hide="expand"></th>\
           </tr>\
</thead>\
    <tbody ></tbody></table';

    $("#dvGridBody").append(content);
    $.each(chartAccount, function () {
        var account = $(this);
        var content = '<tr class="rowStyle">\
                                <td style="width:5%;"><input type="hidden" id="hdnCashBookID" value=' + account.find("pk_CashBookID").text() + '><input type="checkbox" id="chkCheck" ></td>\
                                <td style="width:5%;"><input type="image" name="edit" id="' + account.find("pk_CashBookID").text() + '" title="Edit" src="../Images/IconImage/icn-edit.png" onclick="GetDataForUpdate(' + account.find("pk_CashBookID").text() + ');return false;" style="height:25px;width:25px;" class="phone footable-loaded"/></td>\
                                <td>' + account.find("CashBookRefNo").text() + '</td>\
                                <td>' + account.find("TransactionDate").text() + '</td>\
                                <td>' + account.find("VoucherBook").text() + '</td>\
                                <td>' + account.find("ChequeNo").text() + '</td>\
                                <td>' + account.find("ChequeDate").text() + '</td>\
                                <td><label style="float:right;">' + account.find("Amount").text() + '</label></td>\
                                <td style="width:5%;"><a href="../ReportViewer.aspx?ID=' + account.find("pk_CashBookID").text() + '&ReportType=' + $("#cphBody_hidFormType").val() + '"><img src="../Images/IconImage/icn-print.png" style="height25px;width:25px"></a></td>\
                                </tr>'

        $("#dvGridBody table> tbody").append(content);
    });

    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        LabelName: "lblResultEntries",
        RecordCount: parseInt(pager.find("RecordCount").text())
    });

    $('#gvwList').footable();

};

//for AddEdit record
function AddEditCashBook() {


    if ($("#form1").valid() && $('#gvwDetail >tbody >tr').length > 0) {
        // if ($('#gvwDetail >tbody >tr').length > 0) {
        var isError = false;
        var jsonObjects = [
           {
               pk_CashBookID: $('#hidEdit').val(),
               InputType: $('#cphBody_hidFormType').val(),
               CompanyID: $("#ddlCompany").val(),
               CashBookRefNo: $('#txtVoucherNo').val(),
               TransactionDate: $('#txtVoucherDate').val(),
               FkChartID: Number($('#ddlVoucheBook').val()),
               ChequeNo: Number($('#txtChequeNo').val()),
               ChequeDate: $('#txtChequeDate').val(),
               Remarks: $('#txtRemarks').val()

           }
        ];
        if ($("#btnSave").val() === "Save") {

           
            var CashBookDetail = new Array();
            $('[id*=gvwDetail]').find('tbody>tr:has(td)').each(function () {
                var isChartID = false;
                var isDescription = false;
                var isAmount = false;

                var CashBookDetails = {};
                if ($(this).find("td:nth-child(" + INDEX_OF_CHARTID + ")").find('select').val() != '') {
                    CashBookDetails.Fk_ChartID = $(this).find("td:nth-child(" + INDEX_OF_CHARTID + ")").find('select').val();
                    isChartID = true;
                }
                if (CashBookDetails.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val() != '') {
                    CashBookDetails.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val();
                    isDescription = true;
                }
                if ($(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val() != '') {
                    CashBookDetails.Amount = $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val();
                    isAmount = true;
                }
                if (isChartID && isDescription && isAmount) {
                    CashBookDetail.push(CashBookDetails);
                }
                else {
                    if (!isChartID && !isAmount && !isDescription) { }
                    else {
                        if (!isChartID) {
                            ShowMessageBox('Please select Account Name', "error");
                            isError=true;
                        }
                        else if (!isDescription) {
                            $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').attr('style', 'border: 1px solid;Border-Color: #b94a48 !important;');
                            ShowMessageBox('Description should not be empty', "error");
                            isError = true;
                            
                        }
                        else if (!isAmount) {
                            $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').attr('style', 'border: 1px solid;Border-Color: #b94a48 !important;width: 100%;');
                            ShowMessageBox('Amount should not be empty', "error");
                            isError = true;
                        }
                    }
                }


            });

            if (CashBookDetail.length <= 0) {
                ShowMessageBox(AexpMessages.AccountsMessages.ErrorCashBookDetailGridEmpty, "error");
                isError = true;
            }

            if (!isError) {
                $.ajax({
                    type: "POST",
                    url: "CashBooks.aspx/InsertCashBook",
                    data: '{CashBook: ' + JSON.stringify(jsonObjects) + ', CashBookDetail: ' + JSON.stringify(CashBookDetail) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        if (result.d === "true") {
                            ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");
                        }
                        else if (result.d === "redirect") {
                            var url = document.location.toString().split('/');
                            var redirectLink = url[0] + "//" + url[2] + '/' + 'Login.aspx';
                            window.location = redirectLink;
                        }
                        else {
                            ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                        }
                    },
                    error: function (result) {
                        ShowMessageBox(result.responseText, "error");
                    }

                });
                $("#btnPrint").attr("style", "display:block;float: right;margin-right: 3px;");
               
            }
           
        }
        else {

            var CashBookDetail = new Array();
            $('[id*=gvwDetail]').find('tbody>tr:has(td)').each(function () {
                var isChartID = false;
                var isDescription = false;
                var isAmount = false;

                var CashBookDetails = {};
                if ($(this).find("td:nth-child(" + INDEX_OF_CHARTID + ")").find('select').val() != '') {
                    CashBookDetails.Fk_ChartID = $(this).find("td:nth-child(" + INDEX_OF_CHARTID + ")").find('select').val();
                    isChartID = true;
                }
                if (CashBookDetails.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val() != '') {
                    CashBookDetails.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val();
                    isDescription = true;
                }
                if ($(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val() != '') {
                    CashBookDetails.Amount = $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val();
                    isAmount = true;
                }
                if (isChartID && isDescription && isAmount) {
                    CashBookDetail.push(CashBookDetails);
                }
                else {
                    if (!isChartID && !isAmount && !isDescription) { }
                    else {
                        if (!isChartID) {
                            ShowMessageBox('Please select Account Name', "error");
                            isError = true;
                        }
                        else if (!isDescription) {
                            $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').attr('style', 'border: 1px solid;Border-Color: #b94a48 !important;');
                            ShowMessageBox('Description should not be empty', "error");
                            isError = true;

                        }
                        else if (!isAmount) {
                            $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').attr('style', 'border: 1px solid;Border-Color: #b94a48 !important;width: 100%;');
                            ShowMessageBox('Amount should not be empty', "error");
                            isError = true;
                        }
                    }
                }


            });

            if (CashBookDetail.length <= 0) {
                ShowMessageBox(AexpMessages.AccountsMessages.ErrorCashBookDetailGridEmpty, "error");
                isError = true;
            }

            if (!isError) {
                $.ajax({
                    type: "POST",
                    url: "CashBooks.aspx/Update",
                    data: '{CashBook: ' + JSON.stringify(jsonObjects) + ', CashBookDetail: ' + JSON.stringify(CashBookDetail) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        if (result.d === "true") {
                            ShowMessageBox(AexpMessages.AccountsMessages.Update, "success");
                        }
                        else if (result.d === "redirect") {
                            var url = document.location.toString().split('/');
                            var redirectLink = url[0] + "//" + url[2] + '/' + 'Login.aspx';
                            window.location = redirectLink;
                        }
                        else {
                            ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                        }

                    },
                    failure: function (response) {
                        ShowMessageBox(result.responseText, "error");
                    },
                    error: function (result) {
                        ShowMessageBox(result.responseText, "error");
                    }

                });
                $("#btnPrint").attr("style", "display:block;float: right;margin-right: 3px;");
              
            }
        }
        if (!isError) {
            FillGridView(FirstPage, $("#ddlPageSize").val(), '', '', '', '', '', '', '');
            RefreshModal();

            $("#divViewPanel").attr("Style", "display:block;");
            $("#divAddEditPanel").attr("Style", "display:none");
        }


    }
    else {
        ShowMessageBox(AexpMessages.AccountsMessages.ErrorCashBookDetailGridEmpty, "error");
    }
}

//For paging
$(".Pager .page").live("click", function () {
    if ($("#txtDateRange").val() != '') {
        var date = $("#txtDateRange").text().split('|');
        var startDate = date[0];
        var endDate = date[1];
        FillGridView(parseInt($(this).attr('page')), $("#ddlPageSize").val(), startDate, endDate, '', '', '', '', '');
    }
    else {
        FillGridView(parseInt($(this).attr('page')), $("#ddlPageSize").val(), '', '', '', '', '', '', '');
    }

});


//For open the Add Panel
function AddNew() {
    $('.chosen-select').chosen('destroy');
    $("#dvBodyDetail").empty();
    for (var i = 0; i < 5; i++) {

        var content = '<tr><td style="width: 30%">\
                                            <select id="ddlAccountName'+i+'" class="chosen-select"></select></td>\
                                        <td style="width: 49%">\
                                            <input id="txtDescription" class="gridDetail-description"></td>\
                                        <td style="width: 20%">\
                                            <input id="txtAmount" data-rule-number="true"  class="gridDetail-amount"  onchange="GetTotal();" style="width:100%"></td>\
                                        <td style="width: 1%">\
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>\
                            </tr>'
        $("#dvBodyDetail").append(content);
    }
    

    BindAllDropDowns('', '', '');
    $("#btnSave").val("Save");
    $("#divViewPanel").attr("Style", "display:none;");
    $("#divAddEditPanel").attr("Style", "display:block");
    $("#btnAddNew").attr("Style", "display:none;");
    $("#btnDelete").attr("Style", "display:none;");
    $("#btnPost").attr("Style", "display:none;");

}

//on click of edit button fires this method 
function GetDataForUpdate(cashBookID) {
    $("#btnAddNew").attr("Style", "display:none;");
    $("#btnDelete").attr("Style", "display:none;");
    $("#btnPost").attr("Style", "display:none;");

    $("#hidEdit").val(cashBookID);
    $.ajax({
        type: "POST",
        url: "CashBooks.aspx/GetDataForUpdate",
        data: '{cashBookID: ' + cashBookID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {


            var dict = [];


            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            var updateDate = xml.find("UpdateData");
            $("#dvBodyDetail").empty()
            var selectedCompanyID = "";
            var seletedVoucherBookID = "";
            var selectedAccountIDs = "";
            
            $.each(updateDate, function () {

                var account = $(this);
                var ddlAccountID = 'ddlAccountName' + Math.random().toString(36).substr(2, 9);
              
                dict.push({
                    key: ddlAccountID,
                    value: account.find("AccountID").text()
                });
               
                $('#txtVoucherNo').val(account.find("CashBookRefNo").text());
                $('#txtVoucherDate').val(account.find("VoucherDate").text());
                $('#txtChequeNo').val(account.find("ChequeNo").text());
                $('#txtChequeDate').val(account.find("ChequeDates").text());
                $('#txtRemarks').val(account.find("Remarks").text());
                selectedCompanyID = account.find("fkCompanyID").text();
                seletedVoucherBookID = account.find("VoucherBook").text();
                
                var txtDescription = 'txtDescription' + Math.random().toString(36).substr(2, 9);
                var txtAmount = 'txtAmount' + Math.random().toString(36).substr(2, 9);

                var content='<tr><td style="width: 30%">\
                                            <select id="' + ddlAccountID + '" class="chosen-select"></select></td>\
                                        <td style="width: 49%">\
                                            <input id="' + txtDescription + '" class="gridDetail-description"></td>\
                                        <td style="width: 20%">\
                                            <input id="' + txtAmount + '" data-rule-number="true"  class="gridDetail-amount"  onchange="GetTotal();" style="width:100%"></td>\
                                        <td style="width: 1%">\
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>\
                            </tr>'

               
                $("#dvBodyDetail").append(content);
                selectedAccountIDs = ddlAccountID + ",";
                $("#" + ddlAccountID).val(account.find("AccountID").text());
                $("#" + txtDescription).val(account.find("Description").text());
                $("#" + txtAmount).val(account.find("Amount").text());
            });

           
            BindAllDropDowns(selectedCompanyID, seletedVoucherBookID, dict);
            $("#divViewPanel").attr("Style", "display:none;");
            $("#divAddEditPanel").attr("Style", "display:block");

            $("#btnSave").val("Update");
            GetTotal();

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });

}

//for Adding row in detail grid
function AddDetailData() {
    if ($("#form1").valid()) {

        var ddlAccountID = 'ddlAccountName' + Math.random().toString(36).substr(2, 9);
        var txtDescription = 'txtDescription' + Math.random().toString(36).substr(2, 9);
        var txtAmount = 'txtAmount' + Math.random().toString(36).substr(2, 9);

        var content = '<tr>\
                                        <td style="width: 30%">\
                                            <select id="' + ddlAccountID + '" class="chosen-select" style="width:100%;"></select></td>\
                                        <td style="width: 49%">\
                                            <input id="' + txtDescription + '" class="gridDetail-description""></td>\
                                        <td style="width: 20%">\
                                            <input id="' + txtAmount + '" data-rule-number="true" class="gridDetail-amount"  onchange="GetTotal();" style="width:100%"></td>\
                                        <td style="width: 1%">\
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>\
                            </tr>'

        $("#dvBodyDetail").append(content);

        BindAccountName(ddlAccountID);
        $('.chosen-select').chosen('destroy');
        $(".chosen-select").chosen({ width: "100%" });
    }

}

//For refresh the page
function RefreshModal() {

    //var validator = $("#form1").validate();
    //validator.resetForm();
    //$(".control-group").attr("class", "control-group");
    //$("#dvBodyDetail").empty();
    $("#divViewPanel").attr("Style", "display:block;");
    $("#divAddEditPanel").attr("Style", "display:none");
    $("#btnSave").val("Save");

    $("#txtVoucherNo").val("");
    $("#txtVoucherDate").val("");
    $("#txtChequeNo").val("");
    $("#txtChequeDate").val("");
    $("#txtRemarks").val("");
    $("#dvBodyDetail").empty();
    $("#txtCashBookTotal").val("0.00");
    var content = '<tr>\
                                        <td style="width: 30%">\
                                            <select id="ddlAccountName" class="chosen-select"></select></td>\
                                        <td style="width: 49%">\
                                            <input id="txtDescription" class="gridDetail-description"></td>\
                                        <td style="width: 20%">\
                                            <input id="txtAmount" data-rule-number="true" class="gridDetail-amount"  onchange="GetTotal();" style="width:100%"></td>\
                                        <td style="width: 1%">\
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>\
                            <tr>'
    $("#dvBodyDetail").append(content);
    BindAllDropDowns('', '', '');

    $("#btnAddNew").attr("Style", "display:block;");
    $("#btnDelete").attr("Style", "display:block;");
    $("#btnPost").attr("Style", "display:block;");
    //For close the modal od close confirmation
    ShowCloseConfirmation('', '');
}

//Delete Detail data grid
function DeleteDetailGridData(element) {
    if ($("#gvwDetail tbody>tr").length > 1) {
        element.parentNode.parentNode.remove();
        GetTotal();
    }
}

//Delete data from cashbook
function DeleteCashBook(cashBookID) {
    var isDelete = false;
    $('[id*=gvwList]').find('tbody>tr:has(td)').each(function () {
        if ($(this).find('input:checkbox').prop('checked') === true) {
            var cashBookID = $(this).find("td:nth-child(" + INDEX_OF_CASHBOOKID + ")").find('input').val();
            //    CashBookDetails.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val();
            //   CashBookDetails.Amount = $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val();


            $.ajax({
                type: "POST",
                url: "CashBooks.aspx/DeleteCashBook",
                data: '{cashBookID: ' + cashBookID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    closeDeleteModal();
                    if (response.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Delete, "success");
                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");
                    }
                    FillGridView(FirstPage, $("#ddlPageSize").val(), '', '', '', '', '', '', '');
                },
                failure: function (response) {
                    ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");

                },
                error: function (response) {
                    ShowMessageBox(response.d, "error");
                }

            });
            isDelete = true;
        }

    });

    if (!isDelete) {
        ShowMessageBox("Please select row for delete", "error");
    }
}

//for Bind the account name when click on add new linw for adding one row in detail data
function BindAccountName(ddlAccountID) {
    var output = [];
    output.push('<option selected="selected" value=""></option>');

    $.each(ddlAccountData, function () {
        var account1 = $(this);
        output.push('<option value="' + account1.find('pk_ChartID').text() + '">' + account1.find('ChartName').text() + '</option>');
        
    });
    $("#" + ddlAccountID).html(output.join(''));
}
//For Showing report
function CallReport() {
    //  document.getElementById("btnPrint").href = "../ReportViewer.aspx?ID=" + $("#hidCashBookID").val() + "&ReportType=" + $('#cphBody_hidFormType').val();
}


//For calculating total in detail grid
function GetTotal() {
    $("#txtCashBookTotal").prop('value', "0.00");
    $('[id*=gvwDetail]').find('tbody>tr:has(td)').each(function () {
        if ($(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val() != '') {
            $("#txtCashBookTotal").prop('value', (parseFloat($("#txtCashBookTotal").prop('value')) + parseFloat($(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val())));
        }
        if ($("#txtCashBookTotal").val() === 'NaN') {
            $("#txtCashBookTotal").val("0.00");
        }
    });
}

//For changing the number of diplay rows
function SetPageSizeHiddenValue(pageSize, type) {

    ApplySearching();

}

//for Show the number of row selected from page size
function ApplySearching() {
    if ($("#txtDateRange").val() != '') {

        var date = $("#txtDateRange").val().split(' | ');
        var startDate = date[0];
        var endDate = date[1];
        FillGridView(FirstPage, $("#ddlPageSize").val(), startDate, endDate, '', '', '', '', '');
    }
    else {
        FillGridView(FirstPage, $("#ddlPageSize").val(), '', '', '', '', '', '', '');
    }
}

//for checking the checkbox
function CheckAll() {

    if ($("#chkCheckAll").prop('checked')) {
        $('#gvwList tbody tr td input[type="checkbox"]').each(function () {
            $(this).prop('checked', true);
        });
    }
    else {
        $('#gvwList tbody tr td input[type="checkbox"]').each(function () {
            $(this).prop('checked', false);
        });
    }
    //$('#chkCheck').attr('checked', 'checked');


}

//For searching from grid using date range
function SearchGrid() {
    FillGridView(FirstPage, $("#ddlPageSize").val(), '', '', $("#txtSearchVoucherNo").val(), $("#txtSearchVoucherDate").val(), $("#txtSearchVoucherBook").val(), $("#txtSearchChequeNo").val(), $("#txtSearchChequeDate").val());
}


//For call and fill the variable for the all dropdowns
function BindAllDropDowns(companyID, voucherBookID, accountID) {
    if (ddlCompanyData === '' && ddlVoucherBookData === '' && ddlAccountData === '') {
        $.ajax({
            type: "POST",
            url: "CashBooks.aspx/BindAllDropDowns",
            data: '{}',
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                FillDropDown(companyID, voucherBookID, accountID,r);
            },
            failure: function (response) {
                ShowMessageBox(response.responseText, "error");
            },
            error: function (response) {
                ShowMessageBox(response.responseText, "error");
            }

        });

    }
    else {
        // $('.chosen-select').chosen('destroy');
        $(".chosen-select").chosen({ width: "100%" });
        BindDropDown(companyID, voucherBookID, accountID);
    }
}

//fill All dropdown
function FillDropDown(companyID, voucherBookID, accountID,data) {
    //FillCompany

    if (companyID === '') {
        var ddlCompany = $("[id*=ddlCompany]");
        //ddlCompany.empty().append('<option selected="selected" value=""></option>');
        var xmlDoc = $.parseXML(data.d);
        var xml = $(xmlDoc);
        ddlCompanyData = xml.find("Company");
        $.each(ddlCompanyData, function () {
            var account = $(this);
            ddlCompany.append($("<option></option>").val(account.find('pkCompany_ID').text()).html(account.find('Company_Name').text()));
        });
    }
    else {
        var ddlCompany = $("[id*=ddlCompany]");
        //  ddlCompany.empty().append('<option selected="selected" value=""></option>');
        var xmlDoc = $.parseXML(data.d);
        var xml = $(xmlDoc);
        ddlCompanyData = xml.find("Company");
        $.each(ddlCompanyData, function () {
            var account = $(this);
            ddlCompany.append($("<option></option>").val(account.find('pkCompany_ID').text()).html(account.find('Company_Name').text()));

            $('#ddlCompany').val(companyID);
        })
    }

    $('.chosen-select').chosen('destroy');
    $(".chosen-select").chosen({ width: "100%" });

    var ddlAccountName = "";
    if (accountID === '') {
        ddlAccountName = $("[id*=ddlAccountName]");
       
        ddlAccountData = xml.find("AccountName");
        var output = [];
        output.push('<option selected="selected" value=""></option>');

        $.each(ddlAccountData, function () {
            var account1 = $(this);
            output.push('<option value="' + account1.find('pk_ChartID').text() + '">' + account1.find('ChartName').text() + '</option>');
        });
        $("#ddlAccountName0").html(output.join(''));
        $("#ddlAccountName1").html(output.join(''));
        $("#ddlAccountName2").html(output.join(''));
        $("#ddlAccountName3").html(output.join(''));
        $("#ddlAccountName4").html(output.join(''));
    }
    else {
        ddlAccountData = xml.find("AccountName");
        for (var i = 0; i < accountID.length; i++) {
            // accountID.split(',')
            ddlAccountName = $("#" + accountID[i].key);
            ddlAccountName.empty().append('<option selected="selected" value=""></option>');
            var output = [];
            $.each(ddlAccountData, function () {
                var account1 = $(this);
                output.push('<option value="' + account1.find('pk_ChartID').text() + '">' + account1.find('ChartName').text() + '</option>');
            });
            $("#" + ddlAccountName.attr('id')).html(output.join(''));
            $("#" + ddlAccountName.attr('id')).val(accountID[i].value);
        }

        //
    }
    $('.chosen-select').chosen('destroy');
    $(".chosen-select").chosen({ width: "100%" });

    if (voucherBookID === '') {
        var ddlVoucheBook = $("[id*=ddlVoucheBook]");
        //ddlVoucheBook.empty().append('<option selected="selected" value=""></option>');
        ddlVoucherBookData = xml.find("VoucherBook");
        $.each(ddlVoucherBookData, function () {
            var account2 = $(this);
            ddlVoucheBook.append($("<option></option>").val(account2.find('pk_ChartID').text()).html(account2.find('ChartName').text()));
        });
    }
    else {
        var ddlVoucheBook = $("[id*=ddlVoucheBook]");
        ddlVoucheBook.empty().append('<option selected="selected" value=""></option>');
        ddlVoucherBookData = xml.find("VoucherBook");
        $.each(ddlVoucherBookData, function () {
            var account2 = $(this);
            ddlVoucheBook.append($("<option></option>").val(account2.find('pk_ChartID').text()).html(account2.find('ChartName').text()));
        });


        $('#ddlVoucheBook').val(voucherBookID);
    }
    $('.chosen-select').chosen('destroy');
    $(".chosen-select").chosen({ width: "100%" });
}

// for selecting the values from dropdown
function BindDropDown(companyID, voucherBookID, accountID) {
    if (companyID != '') {
        $('#ddlCompany').val(companyID);
    }

    if (voucherBookID != '') {
        $('#ddlVoucheBook').val(voucherBookID);
    }

    if (accountID != '') {
        for (var i = 0; i < accountID.length; i++) {
            // accountID.split(',')
            ddlAccountName = $("#" + accountID[i].key);
            ddlAccountName.empty().append('<option selected="selected" value=""></option>');
            var output = [];
            $.each(ddlAccountData, function () {
                var account1 = $(this);
                output.push('<option value="' + account1.find('pk_ChartID').text() + '">' + account1.find('ChartName').text() + '</option>');
                //  $("#" + ddlAccountName.attr('id')).append($("<option></option>").val(account1.find('pk_ChartID').text()).html(account1.find('ChartName').text()));
            });
            $("#" + ddlAccountName.attr('id')).html(output.join(''));
            $("#" + ddlAccountName.attr('id')).val(accountID[i].value);
        }
    }
    else {
        var output = [];
        output.push('<option selected="selected" value=""></option>');
       
        $.each(ddlAccountData, function () {
            var account1 = $(this);
            output.push('<option value="' + account1.find('pk_ChartID').text() + '">' + account1.find('ChartName').text() + '</option>');
            //  $("#" + ddlAccountName.attr('id')).append($("<option></option>").val(account1.find('pk_ChartID').text()).html(account1.find('ChartName').text()));
        });
        $("#ddlAccountName0").html(output.join(''));
        $("#ddlAccountName1").html(output.join(''));
        $("#ddlAccountName2").html(output.join(''));
        $("#ddlAccountName3").html(output.join(''));
        $("#ddlAccountName4").html(output.join(''));
    }


    $('.chosen-select').chosen('destroy');
    $(".chosen-select").chosen({ width: "100%" });

}


function OnCloseClick() {

    var isFill = false;
    $('[id*=gvwDetail]').find('tbody>tr:has(td)').each(function () {
        if ($(this).find("td:nth-child(" + INDEX_OF_CHARTID + ")").find('select').val() != '') {
            isFill = true;
            return;
        }
        if ($(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val() != '') {
            isFill = true;
            return;
        }
        if ($(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val() != '') {
            isFill = true;
            return;
        }
    });

    if ($("#txtVoucherDate").val() != "" || $("#txtChequeNo").val() != "" || $("#txtChequeDate").val() != "" || $("#txtRemarks").val() != "" || isFill) {
        ShowCloseConfirmation("RefreshModal", "show");
    }
    else {
        RefreshModal();
    }
    
}

