﻿

//Constants
var INDEX_OF_BANKBOOKID = 1;
var FirstPage = 1;
var INDEX_OF_CHARTID = 1;
var INDEX_OF_DESCRIPTION = 2;
var INDEX_OF_AMOUNT = 3;
var FormType;


//End Constants

//Events

$(function () {

    BindCompany();
    BindVoucher();
    getUrlVars()["FormType"];
    // BindChequeStatus();
    BindAccountName('ddlAccountName');
    FillGridView(FirstPage);
    $('#gvwList').footable();

    //  $("#txtVoucherDate").datepicker();
    //$("#txtVoucherDate").datepicker({
    //    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    //    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    //    changeYear: true,
    //    changeMonth: true,
    //});
    //$('input[id*="txtVoucherDate"]').daterangepicker({
    //    singleDatePicker: true,
    //    showDropdowns: true
    //});
    // $('#datetimepicker1').datetimepicker();
    //   $("#txtChequeDate").datepicker();
    //$("#AddNewModal").attr("Style", "display:none");


    //var myDatePicker = $('#txtVoucherDate').glDatePicker(
    //{
    //    cssName: 'default',
    //dowNames: ['Su', 'M', 'Tu', 'W', 'T', 'F', 'Sa'],
    //borderSize: 2,
    //hideOnClick: true,
    //}).glDatePicker(true);
    //myDatePicker.render();
    // myDatePicker.show();

    //$("#txtChequeDate").datepicker({

    //});

});
function PostBankBookEnteries() {
  
    var bankBookID = "";
    $('[id*=gvwList]').find('tbody>tr:has(td)').each(function () {
        if ($(this).find('input:checkbox').prop('checked') === true) {
            bankBookID += $(this).find("td:nth-child(" + INDEX_OF_BANKBOOKID + ")").find('input').val() + ",";
            //    CashBookDetails.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val();
            //   CashBookDetails.Amount = $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val();
        }
    });

    bankBookID = bankBookID.substring(0, bankBookID.length - 1);

    if (bankBookID != '') {
        $.ajax({
            type: "POST",
            url: "BankBook.aspx/PostedEnteries",
            data: '{BankBookIDs:"' + bankBookID + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: OnSuccess,
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);
            }
        });

    }

   }

function OnSuccess(response) {
}


function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    FormType = (vars[hash[0]]);
    return (vars[hash[0]]);
}

function FillGridView(pageIndex) {
    $.ajax({
        type: "POST",
        url: "BankBook.aspx/FillGrid",
        data: '{pageIndex: ' + pageIndex + ',formType:"' + $('#cphBody_hidFormType').val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: FillGridViewCompleted,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });

}

function FillGridViewCompleted(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var chartAccount = xml.find("BankBook");

    $("#dvGridBody").empty();


    var content = '<table class="table table-striped table-bordered dataTable"\
    cellspacing="0" rules="all" border="1" id="gvwList"\
    style="border-collapse: collapse;">\
    <thead>\
         <tr class="headerStyle">\
            <th scope="col" data-hide="expand"><input type="checkbox" id="chkCheckAll" onclick="CheckAll();"/></th>\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand">Report</th>\
            <th scope="col" data-hide="expand" >Voucher No <input type="image" src="../Images/IconImage/icn-sorticondown.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand">Voucher Date <input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Voucher Book <input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Cheque No <input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Cheque Date <input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Amount <input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th data-hide="phone" scope="col">Remarks <input type="image" src="../Images/IconImage/icn-sorticonup.png" style="height:10px"></th>\
            <th scope="col" data-hide="expand"></th>\
            </tr>\
            <tr class="" id="trsearch">\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand" ></th>\
            <th scope="col" data-hide="expand"><input type="text" class="filter-textbox" size="4"></th>\
            <th data-hide="phone" scope="col"><input type="text" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"><input type="text" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"><input type="text" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"><input type="text" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"><input type="text" class="filter-textbox" size="4" ></th>\
            <th data-hide="phone" scope="col"></th>\
            <th scope="col" data-hide="expand"></th>\
           </tr>\
</thead>\
    <tbody ></tbody></table';

    $("#dvGridBody").append(content);
    $.each(chartAccount, function () {
        var account = $(this);
        var content = '<tr class="rowStyle">\
                                <td style="width:5%;"><input type="hidden" id="hdnBankBookID" value=' + account.find("pk_BankBookId").text() + '><input type="checkbox" name="item" id="chkCheck" value=' + account.find("pk_BankBookId").text() + ' ></td>\
                                <td><input type="image" name="edit" id="' + account.find("pk_BankBookId").text() + '" title="Edit" src="../Images/IconImage/icn-edit.png" onclick="GetDataForUpdate(' + account.find("pk_BankBookId").text() + ');return false;" style="height:20px;width:20px;" class="phone footable-loaded"/></td>\
                                <td><a href="../ReportViewer.aspx?ID=' + account.find("pk_BankBookId").text() + '&ReportType=' + FormType + '">Print Voucher</a></td>\
                                <td>' + account.find("BankBookRefNo").text() + '</td>\
                                <td>' + account.find("TransactionDate").text() + '</td>\
                                <td>' + account.find("VoucherBook").text() + '</td>\
                                <td>' + account.find("ChequeNo").text() + '</td>\
                                <td>' + account.find("ChequeDate").text() + '</td>\
                                <td>' + account.find("Amount").text() + '</td>\
                                <td>' + account.find("Remarks").text() + '</td>\
                                <td><input type="image" name="delete" id="' + account.find("pk_BankBookId").text() + '" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="openDeleteModal(' + account.find("pk_BankBookId").text() + ',DeleteBankBook);return false;" style="height:20px;width:20px;" class="phone footable-loaded"/></td>\
                                </tr>'

        $("#dvGridBody table> tbody").append(content);
    });

    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        LabelName: "lblResultEntries",
        RecordCount: parseInt(pager.find("RecordCount").text())
    });

    $('#gvwList').footable();

};

function AddNew() {

    $("#btnSave").val("Save");
    $("#divViewPanel").attr("Style", "display:none;");
    $("#divAddEditPanel").attr("Style", "display:block");
}

function WizardShowHide(dv) {
    if (dv.id === "firstStep") {
        $("#firstStep").attr("style", "display:block;");
        $("#secondStep").attr("style", "display:none;");
    }
    else {
        $("#firstStep").attr("style", "display:none;");
        $("#secondStep").attr("style", "display:block;");

    }
}

function BindCompany() {
    $.ajax({
        type: "POST",
        url: "BankBook.aspx/BindCompany",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlCompany = $("[id*=ddlCompany]");
            ddlCompany.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                ddlCompany.append($("<option></option>").val(this['CompanyID']).html(this['CompanyName']));
            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
        }
    });


}

function BindVoucher() {
    $.ajax({
        type: "POST",
        url: "BankBook.aspx/BindVoucherBook",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlVoucheBook = $("[id*=ddlVoucheBook]");
            ddlVoucheBook.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                //  ddlVoucheBook.empty();
                ddlVoucheBook.append($("<option></option>").val(this['pk_ChartID']).html(this['ChartName']));

            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
        }

    });
}

function BindChequeStatus() {
    $.ajax({
        type: "POST",
        url: "BankBook.aspx/BindChequeStatus",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlChequestats = $("[id*=ddlChequeStatus]");
            ddlChequestats.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                //  ddlVoucheBook.empty();
                ddlChequestats.append($("<option></option>").val(this['ChequeStatusId']).html(this['ChequeStatusName']));

            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
        }

    });
}

function BindAccountName(ddlAccountID) {
    $.ajax({
        type: "POST",
        url: "BankBook.aspx/BindAccountName",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (r) {
            var ddlAccountName = $("#" + ddlAccountID);
            ddlAccountName.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(r.d, function () {
                //  ddlVoucheBook.empty();
                ddlAccountName.append($("<option></option>").val(this['pk_ChartID']).html(this['ChartName']));

            });
        },
        failure: function (response) {
            ShowMessageBox(response.responseText, "error");
        },
        error: function (response) {
            ShowMessageBox(response.responseText, "error");
        }

    });
}

function AddDetailData() {
    if ($("#form1").valid()) {

        var ddlAccountID = 'ddlAccountName' + Math.random().toString(36).substr(2, 9);
        var txtDescription = 'txtDescription' + Math.random().toString(36).substr(2, 9);
        var txtAmount = 'txtAmount' + Math.random().toString(36).substr(2, 9);

        var content = '<tr>\
                                        <td style="width: 30%">\
                                            <select id="' + ddlAccountID + '" class="gridDetail-accountname"></select></td>\
                                        <td style="width: 49%">\
                                            <input id="' + txtDescription + '" class="gridDetail-description"></td>\
                                        <td style="width: 20%">\
                                            <input id="' + txtAmount + '" data-rule-number="true" class="gridDetail-amount" onchange="GetTotal();"></td>\
                                        <td style="width: 1%">\
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>\
                            <tr>'

        $("#dvBodyDetail").append(content);
        BindAccountName(ddlAccountID);

    }

}

function AddEditBankBook() {


    if ($("#form1").valid() && $('#gvwDetail >tbody >tr').length > 0) {


        var jsonObjects = [
            {
                pk_BankBookID: $('#hidEdit').val(),
                InputType: $('#cphBody_hidFormType').val(),
                CompanyID: $('#ddlCompany').val(),
                BankBookRefNo: $('#txtVoucherNo').val(),
                TransactionDate: $('#txtVoucherDate').val(),
                FkChartID: Number($('#ddlVoucheBook').val()),
                ChequeNo: Number($('#txtChequeNo').val()),
                ChequeDate: $('#txtChequeDate').val(),
                Remarks: $('#txtRemarks').val(),

            }
        ];
        if ($("#btnSave").val() === "Save") {

            var BankBookDetails = new Array();
            $('[id*=gvwDetail]').find('tbody>tr:has(td)').each(function () {
                var BankBookDetail = {};
                BankBookDetail.Fk_ChartID = $(this).find("td:nth-child(" + INDEX_OF_CHARTID + ")").find('select').val();
                BankBookDetail.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val();
                BankBookDetail.Amount = $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val();
                BankBookDetails.push(BankBookDetail);
            });


            $.ajax({
                type: "POST",
                url: "BankBook.aspx/InsertBankBook",
                data: '{BankBook: ' + JSON.stringify(jsonObjects) + ', BankBookDetail: ' + JSON.stringify(BankBookDetails) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");
                    }
                    else if (result.d === "redirect") {
                        var url = document.location.toString().split('/');
                        var redirectLink = url[0] + "//" + url[2] + '/' + 'Login.aspx';
                        window.location = redirectLink;
                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.ErrorSave, "error");
                    }
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.Save, "error");
                }

            });
            $("#btnPrint").attr("style", "display:block;float: right;margin-right: 3px;");
            GetLastInsertedID($("#cphBody_hidFormType").val(), "insert");
        }
        else {

            var BankBookDetails = new Array();
            $('[id*=gvwDetail]').find('tbody>tr:has(td)').each(function () {
                var BankBookDetail = {};
                BankBookDetail.Fk_ChartID = $(this).find("td:nth-child(" + INDEX_OF_CHARTID + ")").find('select').val();
                BankBookDetail.Description = $(this).find("td:nth-child(" + INDEX_OF_DESCRIPTION + ")").find('input').val();
                BankBookDetail.Amount = $(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val();
                BankBookDetails.push(BankBookDetail);
            });

            $.ajax({
                type: "POST",
                url: "BankBook.aspx/Update",
                data: '{BankBook: ' + JSON.stringify(jsonObjects) + ', BankBookDetail: ' + JSON.stringify(BankBookDetails) + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d === "true") {
                        ShowMessageBox(AexpMessages.AccountsMessages.Save, "success");
                    }
                    else if (result.d === "redirect") {
                        var url = document.location.toString().split('/');
                        var redirectLink = url[0] + "//" + url[2] + '/' + 'Login.aspx';
                        window.location = redirectLink;
                    }
                    else {
                        ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                    }

                },
                failure: function (response) {
                    ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                },
                error: function (result) {
                    ShowMessageBox(AexpMessages.AccountsMessages.errorUpdate, "error");
                }

            });
            $("#btnPrint").attr("style", "display:block;float: right;margin-right: 3px;");
            GetLastInsertedID($("#cphBody_hidFormType").val(), "update");
        }

        FillGridView(FirstPage);
        RefreshModal();
        $("#divViewPanel").attr("Style", "display:block;");
        $("#divAddEditPanel").attr("Style", "display:none");

    }
    else {
        ShowMessageBox(AexpMessages.AccountsMessages.ErrorBankBookDetailGridEmpty, "error");
    }

}

function DeleteBankBook(bankBookID) {
    $("#hidDelete").val(bankBookID);
    $.ajax({
        type: "POST",
        url: "BankBook.aspx/DeleteBankBook",
        data: '{bankBookID: ' + bankBookID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            closeDeleteModal();
            if (response.d === "true") {
                ShowMessageBox(AexpMessages.AccountsMessages.Delete, "success");
            }
            else {
                ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");
            }
            FillGridView(FirstPage);
        },
        failure: function (response) {
            ShowMessageBox(AexpMessages.AccountsMessages.ErrorDelete, "error");

        },
        error: function (response) {
            ShowMessageBox(response.d, "error");
        }
    });


}
$(".Pager .page").live("click", function () {
    FillGridView(parseInt($(this).attr('page')));

});

function GetDataForUpdate(bashBookID) {
    $("#hidEdit").val(bashBookID);
    $.ajax({
        type: "POST",
        url: "BankBook.aspx/GetDataForUpdate",
        data: '{bankBookID: ' + bashBookID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            var updateDate = xml.find("UpdateData");
            $("#dvBodyDetail").empty();
            $.each(updateDate, function () {
                var account = $(this);
                $('#ddlCompany').val(account.find("fkCompanyID").text());
                $('#ddlVoucheBook').val(account.find("VoucherBookID").text());
                $('#txtVoucherNo').val(account.find("BankBookRefNo").text());
                $('#txtVoucherDate').val(account.find("VoucherDate").text());
                $('#txtChequeNo').val(account.find("ChequeNo").text());
                $('#txtChequeDate').val(account.find("ChequeDates").text());
                $('#txtRemarks').val(account.find("Remarks").text());


                var ddlAccountID = 'ddlAccountName' + Math.random().toString(36).substr(2, 9);
                var txtDescription = 'txtDescription' + Math.random().toString(36).substr(2, 9);
                var txtAmount = 'txtAmount' + Math.random().toString(36).substr(2, 9);

                var content = '<tr>\
                                        <td style="width: 30%">\
                                            <select id="' + ddlAccountID + '" class="gridDetail-accountname"></select></td>\
                                        <td style="width: 49%">\
                                            <input id="' + txtDescription + '" class="gridDetail-description"></td>\
                                        <td style="width: 20%">\
                                            <input id="' + txtAmount + '" data-rule-number="true"  class="gridDetail-amount" onchange="GetTotal();"></td>\
                                        <td style="width: 1%">\
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>\
                            <tr>'

                $("#dvBodyDetail").append(content);
                BindAccountName(ddlAccountID);
                $("#" + ddlAccountID).val(account.find("AccountID").text());
                $("#" + txtDescription).val(account.find("Description").text());
                $("#" + txtAmount).val(account.find("Amount").text());

            });
            $("#divViewPanel").attr("Style", "display:none;");
            $("#divAddEditPanel").attr("Style", "display:block");
            $("#btnSave").val("Update");
            GetTotal();
        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });


}

function GetLastInsertedID(bankBookType, transactionType) {
    $.ajax({
        type: "POST",
        url: "BankBook.aspx/GetLastInsertedID",
        data: '{bankBookType:"' + bankBookType + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (transactionType === "insert")
                $("#hidBankBookID").val(response.d);
            else
                $("#hidBankBookID").val($("#hidEdit").val());

            CallReport();

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
//Events Ends

//#Region Private Methods

function RefreshModal() {


    $("#divViewPanel").attr("Style", "display:block;");
    $("#divAddEditPanel").attr("Style", "display:none");
    $("#btnSave").val("Save");
    $("#txtVoucherNo").val("");
    $("#txtVoucherDate").val("");
    $("#txtChequeNo").val("");
    $("#txtChequeDate").val("");
    $("#txtRemarks").val("");
    $("#dvBodyDetail").empty();

    var content = '<tr>\
                                        <td style="width: 30%">\
                                            <select id="ddlAccountName" class="gridDetail-accountname"></select></td>\
                                        <td style="width: 49%">\
                                            <input id="txtDescription" class="gridDetail-description"></td>\
                                        <td style="width: 20%">\
                                            <input id="txtAmount" data-rule-number="true" class="gridDetail-amount" onchange="GetTotal();"></td>\
                                        <td style="width: 1%">\
                                            <input type="image" name="delete" title="Delete" src="../Images/IconImage/icn-delete.png" onclick="DeleteDetailGridData(this); return false;" style="height: 20px; width: 20px;" class="phone footable-loaded" /></td>\
                            <tr>'
    $("#dvBodyDetail").append(content);

    BindCompany();
    BindVoucher();
    BindAccountName('ddlAccountName');
}

function DeleteDetailGridData(element) {
    element.parentNode.parentNode.remove();
}

function CallReport() {
    document.getElementById("btnPrint").href = "../ReportViewer.aspx?ID=" + $("#hidBankBookID").val() + "&ReportType=" + $('#cphBody_hidFormType').val();
}

function GetTotal() {
    $("#txtBankBookTotal").attr('value', "0.00");
    $('[id*=gvwDetail]').find('tbody>tr:has(td)').each(function () {
        $("#txtBankBookTotal").attr('value', (parseFloat($("#txtBankBookTotal").attr('value')) + parseFloat($(this).find("td:nth-child(" + INDEX_OF_AMOUNT + ")").find('input').val())));
    });
}

function CheckAll() {

    if ($("#chkCheckAll").prop('checked')) {
        $('#gvwList tbody tr td input[type="checkbox"]').each(function () {
            $(this).prop('checked', true);
        });
    }
    else {
        $('#gvwList tbody tr td input[type="checkbox"]').each(function () {
            $(this).prop('checked', false);
        });
    }
    //$('#chkCheck').attr('checked', 'checked');


}

//End Private Methods
