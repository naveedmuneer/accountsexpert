﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsExperts.DataObjects
{
    public class UserMapping
    {
        public Int32 pkUserRoleMap_ID { get; set; }
        public string fkRole_ID { get; set; }
        public string fkUser_ID { get; set; }
    }
}