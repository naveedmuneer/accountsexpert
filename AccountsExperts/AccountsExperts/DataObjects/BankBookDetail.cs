﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace AccountsExperts.DataObjects
{
    [Serializable()]
    public class BankBookDetail
    {
        [XmlAttribute("PkBankBookDetailID")]
        public Int32 PkBankBookDetailID { get; set; }
        [XmlAttribute("FkBankBookID")]
        public Int32 FkBankBookID { get; set; }
        [XmlAttribute("Fk_ChartID")]
        public Int32 Fk_ChartID { get; set; }
        [XmlAttribute("Description")]
        public string Description { get; set; }
        [XmlAttribute("Amount")]
        public Decimal Amount { get; set; }


    }

}