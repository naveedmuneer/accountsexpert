﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsExperts.DataObjects
{
    public class Employee
    {
        private Int32 _pkEmployeeID;
        private string _employee_Name;
        private string _nic_No;
        private Int32 _fkCompany_ID;
        private Int32 _fkDepartment_ID;
        private Int32 _fkDesignation_ID;
        private string _email;
        private string  _contact_No;
        private string _secondary_Contact_No;
        private string _address;
        private string _permenant_Address;
        private bool _isActive;
        private DateTime _createdOn;
        private string _createdBy;
        private string _machineCreated;
        private DateTime _modifiedOn;
        private string _modifiedBy;
        private string _machineModified;

        public Int32 pk_Employee_ID
        {
            get { return _pkEmployeeID; }
            set { _pkEmployeeID = value; }
        }
        public string Employee_Name
        {
            get { return _employee_Name; }
            set { _employee_Name = value; }
        }

        public string NIC_No
        {
            get { return _nic_No; }
            set { _nic_No = value; }
        }
        
        public Int32 fkCompany_ID
        {
            get { return _fkCompany_ID; }
            set { _fkCompany_ID = value; }
        }
        public Int32 fkDepartment_ID
        {
            get { return _fkDepartment_ID; }
            set { _fkDepartment_ID = value; }
        }
        public Int32 fkDesignation_ID
        {
            get { return _fkDesignation_ID; }
            set { _fkDesignation_ID = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string Contact_No
        {
            get { return _contact_No; }
            set { _contact_No = value; }
        }
        public string Secondary_Contact_No
        {
            get { return _secondary_Contact_No; }
            set { _secondary_Contact_No = value; }
        }
       public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string Permenant_Address
        {
            get { return _permenant_Address; }
            set { _permenant_Address = value; }
        }

        public bool Is_Active
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public DateTime Created_On
        {
            get { return _createdOn; }
            set { _createdOn = value; }
        }

        public string Created_By
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public string Machine_Created
        {
            get { return _machineCreated; }
            set { _machineCreated = value; }
        }

        public DateTime Modified_On
        {
            get { return _modifiedOn; }
            set { _modifiedOn = value; }
        }

        public string Modified_By
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }

        public string Machine_Modified
        {
            get { return _machineModified; }
            set { _machineModified = value; }
        }

    }
}