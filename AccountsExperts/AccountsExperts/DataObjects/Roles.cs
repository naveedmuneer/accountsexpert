﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsExperts.DataObjects
{
    public class Roles
    {
        public Int32 pkRole_ID { get; set; }
        public string Role_Name { get; set; }
        public string Is_Active { get; set; }
        public string Created_By { get; set; }
    }
}