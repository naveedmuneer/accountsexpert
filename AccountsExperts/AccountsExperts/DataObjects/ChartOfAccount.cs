﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsExperts.DataObjects
{
    public class ChartOfAccount
    {
        private Int32 _pkChartID;
        private string _chartRefNo;
        private string _chartName;

        private string _description;
        private string _groupID;

        private Int32 _fkAccountNatureID;
        private string _note;

        private string _address;
        private string _phone;
        private string _fax;

        private string _email;
        private string _ntnNo;
        private string _gstNo;

        private string _active;
        private DateTime _createdOn;
        private string _createdBy;

        private string _machineCreated;
        private DateTime _modifiedOn;
        private string _modifiedBy;

        private string _machineModified;
             

        public Int32 pk_ChartID
        {
            get { return _pkChartID; }
            set { _pkChartID = value; }
        }

        public string ChartRefNo
        {
            get { return _chartRefNo; }
            set { _chartRefNo = value; }
        }

        public string ChartName
        {
            get { return _chartName; }
            set { _chartName = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string GroupID
        {
            get { return _groupID; }
            set { _groupID = value; }
        }

        public Int32 fk_AccountNatureID
        {
            get { return _fkAccountNatureID; }
            set { _fkAccountNatureID = value; }
        }
        
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string NTNNO
        {
            get { return _ntnNo; }
            set { _ntnNo = value; }
        }

        public string GSTNO
        {
            get { return _gstNo; }
            set { _gstNo = value; }
        }

        public string Active
        {
            get { return _active; }
            set { _active = value; }
        }

         public DateTime Created_On
        {
            get { return _createdOn; }
            set { _createdOn = value; }
        }

         public string Created_By
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

         public string Machine_Created
        {
            get { return _machineCreated; }
            set { _machineCreated = value; }
        }

         public DateTime Modified_On
        {
            get { return _modifiedOn; }
            set { _modifiedOn = value; }
        }

         public string Modified_By
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }

         public string Machine_Modified
        {
            get { return _machineModified; }
            set { _machineModified = value; }
        }


    }
}