﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace AccountsExperts.DataObjects
{

    [Serializable()]
    public class CashBookDetail
{
        [XmlAttribute("pk_CashBookDetailID")]
        public Int32 pk_CashBookDetailID { get; set; }
        [XmlAttribute("fk_CashBookID")]
        public Int32 fk_CashBookID { get; set; }
        [XmlAttribute("fk_ChartID")]
        public Int32 fk_ChartID { get; set; }
        [XmlAttribute("Description")]
        public string Description { get; set; }
        [XmlAttribute("Amount")]
        public decimal Amount { get; set; }
       

      
    }
}