﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsExperts.DataObjects
{
    public class Designation
    {
        private Int32 _pkDesignationtID;
        private string _designation_Name;
        private bool _isActive;
        private DateTime _createdOn;
        private string _createdBy;
        private string _machineCreated;
        private DateTime _modifiedOn;
        private string _modifiedBy;
        private string _machineModified;


        public Int32 pkDesignation_ID
        {
            get { return _pkDesignationtID; }
            set { _pkDesignationtID = value; }
        }
        public string Designation_Name
        {
            get { return _designation_Name; }
            set { _designation_Name = value; }
        }
        public bool Is_Active
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public DateTime Created_On
        {
            get { return _createdOn; }
            set { _createdOn = value; }
        }

        public string Created_By
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public string Machine_Created
        {
            get { return _machineCreated; }
            set { _machineCreated = value; }
        }

        public DateTime Modified_On
        {
            get { return _modifiedOn; }
            set { _modifiedOn = value; }
        }

        public string Modified_By
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }

        public string Machine_Modified
        {
            get { return _machineModified; }
            set { _machineModified = value; }
        }    
    }
}