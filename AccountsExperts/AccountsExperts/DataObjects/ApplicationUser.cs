﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsExperts.DataObjects
{
    public class ApplicationUser
    {
        private Int32 _pkUserID;
        private string _user_Name;
        private string _password;
        private Int32 _fkEmployee_ID;
        private bool _isActive;
        private DateTime _createdOn;
        private string _createdBy;
        private string _machineCreated;
        private DateTime _modifiedOn;
     

        public Int32 PkUserID
        {
            get { return _pkUserID; }
            set { _pkUserID = value; }
        }

        public string User_Name
        {
            get { return _user_Name; }
            set { _user_Name = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        public Int32 FkEmployee_ID
        {
            get { return _fkEmployee_ID; }
            set { _fkEmployee_ID = value; }
        }
        public bool Is_Active
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public DateTime Created_On
        {
            get { return _createdOn; }
            set { _createdOn = value; }
        }

        public string Created_By
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public string Machine_Created
        {
            get { return _machineCreated; }
            set { _machineCreated = value; }
        }

        public DateTime Modified_On
        {
            get { return _modifiedOn; }
            set { _modifiedOn = value; }
        }

    }
}