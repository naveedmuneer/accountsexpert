﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace AccountsExperts.DataObjects
{
    [Serializable()]
    public class JournalVoucherDetail
    {
        //public Int32 pk_JournalVoucherDetailID { get; set; }
        //public Int32 fk_JournalVoucherID { get; set; }
        //public Int32 fk_ChartID { get; set; }
        //public Decimal Debit { get; set; }
        //public Decimal Credit { get; set; }
        //public string Description { get; set; }
        //public Int32 SeqNo { get; set; }
        //public Int32 fk_DepartmentId { get; set; }
        //public DateTime Created_On { get; set; }
        //public string Created_By { get; set; }
        //public string Machine_Created { get; set; }
        //public DateTime Modified_On { get; set; }



        [XmlAttribute("pk_JournalVoucherDetailID")]
        public Int32 pk_JournalVoucherDetailID { get; set; }
        [XmlAttribute("fk_JournalVoucherID")]
        public Int32 fk_JournalVoucherID { get; set; }
        [XmlAttribute("fk_ChartID")]
        public Int32 fk_ChartID { get; set; }
        [XmlAttribute("Debit")]
        public decimal Debit { get; set; }
        [XmlAttribute("Credit")]
        public decimal Credit { get; set; }
        [XmlAttribute("Description")]
        public string Description { get; set; }
        [XmlAttribute("SeqNo")]
        public Int32 SeqNo { get; set; }
        [XmlAttribute("fk_DepartmentId")]
        public Int32 fk_DepartmentId { get; set; }
        [XmlAttribute("Created_On")]
        public DateTime Created_On { get; set; }
        //[XmlAttribute("Created_By")]
        //public string Created_By { get; set; }
        [XmlAttribute("Machine_Created")]
        public string Machine_Created { get; set; }
        [XmlAttribute("Modified_On")]
        public DateTime Modified_On { get; set; }
    }
}