﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace AccountsExperts.DataObjects
{
  
    [Serializable()]
    public class BankBook
    {
        [XmlAttribute("pk_BankBookID")]
        public Int32 pk_BankBookID { get; set; }
        [XmlAttribute("InputType")]
        public string InputType { get; set; }
        [XmlAttribute("CompanyID")]
        public Int32 CompanyID { get; set; }
        [XmlAttribute("BankBookRefNo")]
        public string BankBookRefNo { get; set; }
        [XmlAttribute("TransactionDate")]
        public DateTime TransactionDate { get; set; }
        [XmlAttribute("FkChartID")]
        public Int32 FkChartID { get; set; }
        [XmlAttribute("ChequeNo")]
        public string ChequeNo { get; set; }
        [XmlAttribute("ChequeDate")]
        public DateTime ChequeDate { get; set; }
        [XmlAttribute("EnmChequeStatusID")]
        public Int32 EnmChequeStatusID { get; set; }
        [XmlAttribute("Remarks")]
        public string Remarks { get; set; }
        [XmlAttribute("Posted")]
        public string Posted { get; set; }
        [XmlAttribute("FkUserID")]
        public Int32 FkUserID { get; set; }
        [XmlAttribute("Active")]
        public string Active { get; set; }
        [XmlAttribute("Created_On")]
        public DateTime Created_On { get; set; }
        [XmlAttribute("Created_By")]
        public string Created_By { get; set; }
        [XmlAttribute("Machine_Created")]
        public string Machine_Created { get; set; }
        [XmlAttribute("Modified_On")]
        public DateTime Modified_On { get; set; }
        [XmlAttribute("Modified_By")]
        public string Modified_By { get; set; }
        [XmlAttribute("Machine_Modified")]
        public string Machine_Modified { get; set; }

     
    }
}