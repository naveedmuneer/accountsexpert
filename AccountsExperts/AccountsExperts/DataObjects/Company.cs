﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsExperts.DataObjects
{
    public class Company
    {
        public Int32 CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string Business { get; set; }
        public DateTime Establishment_Date { get; set; }
        public DateTime Expiry_Date { get; set; }
        public string Is_Active { get; set; }
        public string Created_By { get; set; }

    }
}