﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace AccountsExperts.DataObjects
{
    [Serializable()]
    public class JournalVoucherMaster
    {

        [XmlAttribute("pk_JournalVoucherID")]
        public Int32 pk_JournalVoucherID { get; set; }
        [XmlAttribute("JournalVoucherRefNo")]
        public string JournalVoucherRefNo { get; set; }
        [XmlAttribute("TransactionDate")]
        public DateTime TransactionDate { get; set; }
        [XmlAttribute("fkCompanyID")]
        public Int32 fkCompanyID { get; set; }
        [XmlAttribute("Remarks")]
        public string Remarks { get; set; }
        [XmlAttribute("IsPosted")]
        public Int32 IsPosted { get; set; }
        [XmlAttribute("fkUserId")]
        public Int32 fkUserId { get; set; }
        [XmlAttribute("Created_On")]
        public DateTime Created_On { get; set; }
        [XmlAttribute("Created_By")]
        public string Created_By { get; set; }
        [XmlAttribute("Machine_Created")]
        public string Machine_Created { get; set; }
        [XmlAttribute("Modified_On")]
        public DateTime Modified_On { get; set; }
    }
}