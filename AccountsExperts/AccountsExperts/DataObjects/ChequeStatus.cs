﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountsExperts.DataObjects
{
    public class ChequeStatus
    {
        private Int32 _chequeStatusId;
        private string _chequeStatusName;

        public Int32 ChequeStatusId
        {
            get { return _chequeStatusId; }
            set { _chequeStatusId = value; }
        }

        public string ChequeStatusName
        {
            get { return _chequeStatusName; }
            set { _chequeStatusName = value; }
        }
    }
}