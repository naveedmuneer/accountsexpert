﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using AccountsExperts.Access_Layers.CommonDataaccess;
using System.Data;
using System.Transactions;
using AccountsExperts.DataObjects;

namespace AccountsExperts.DataAccesslayer
{
    public class UserMappingDO
    {
        #region Constants

        private static string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;

        #endregion

        #region Public Methods

        public System.Data.DataSet GetActiveUsers()
        {
            System.Data.DataSet ds = new System.Data.DataSet();
            try
            {

                ds = DataAccess.GetDataset("usp_GetActiveUsers");
                if (ds != null)
                {
                    return ds;

                }
                else
                {
                    return ds;

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public System.Data.DataSet GetActiveRoles()
        {
            System.Data.DataSet ds = new System.Data.DataSet();
            try
            {

                ds = DataAccess.GetDataset("usp_GetActiveRoles");
                if (ds != null)
                {
                    return ds;

                }
                else
                {
                    return ds;

                }


            }
            catch (Exception ex)
            {
                throw ex;

            }


        }

        public static System.Data.DataSet GetUserMappingData(int pageSize, int pageIndex)
        {
            try
            {
                System.Data.DataSet ds = DataAccess.GetDataSetForGrid("usp_GetUserRoleMappings", pageSize, pageIndex);
                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "UserRoleMappings";
                    ds.Tables[1].TableName = "Pager";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        public static bool InsertUserMapping(UserMapping objUserMapping)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    bool flag = false;

                    flag = InsertUserMappings(objUserMapping);


                    if (flag)
                    {
                        ts.Complete();
                        return true;
                    }
                    else
                    {
                        ts.Dispose();
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

        }
        public static bool UpdateUserMappers(UserMapping objUserMapping)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    bool flag = false;

                    flag = UpdateUserMappings(objUserMapping);


                    if (flag)
                    {
                        ts.Complete();
                        return true;
                    }
                    else
                    {
                        ts.Dispose();
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

        }

        public static bool DeletUserMappers(int UserID)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    bool Flag = false;
                    Flag = DeletUserMappings(UserID);

                    if (Flag)
                    {
                        ts.Complete();
                        return true;
                    }
                    else
                    {
                        ts.Dispose();
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;

            }


        }

        public static System.Data.DataSet GetDataForUpdate(int UserID)
        {
            try
            {
                System.Data.DataSet ds = DataAccess.GetDataSetByparams("usp_GetUserRoleMappingsDataForEdit", UserID);
                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "UpdateData";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        #endregion


        #region Private Methods

        private static bool InsertUserMappings(UserMapping objUserMapping)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_InsertUserRoleMapping", cn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@fkUser_ID", SqlDbType.Int, 50).Value = objUserMapping.fkUser_ID;
                    cmd.Parameters.Add("@fkRole_ID", SqlDbType.VarChar, 50).Value = objUserMapping.fkRole_ID;
                    


                    bool result = (bool)cmd.ExecuteScalar();
                    if (result)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }


                // return true;
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }

        }
        
        private static bool UpdateUserMappings(UserMapping objUserMapping)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_UpdateUserRoleMapping", cn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@fkUser_ID", SqlDbType.Int, 50).Value = objUserMapping.fkUser_ID;
                    cmd.Parameters.Add("@fkRole_ID", SqlDbType.VarChar, 50).Value = objUserMapping.fkRole_ID;


                    bool result = (bool)cmd.ExecuteScalar();
                    if (result)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }


                // return true;
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }

        private static bool DeletUserMappings(int UserID)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_DeleteUserRoleMapping", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@fkUser_ID", SqlDbType.Int, 50).Value = UserID;
                    bool result = (bool)cmd.ExecuteScalar();
                    if (result)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #endregion
    }
}