﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using context = System.Web.HttpContext;

namespace AccountsExperts.DataAccesslayer
{
    public static class ExceptionLoggingDO
    {

        #region Constants

        private static string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
        private static String exepurl;
      //  private static String exepJSurl;

        #endregion
        #region Public Methods
        public static void InsertExcepToDB(Exception exdb)
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                exepurl = context.Current.Request.Url.ToString();
                SqlCommand com = new SqlCommand("usp_InsertExceptionLoggingData", cn);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ExceptionMsg", exdb.Message.ToString());
                com.Parameters.AddWithValue("@ExceptionType", exdb.GetType().Name.ToString());
                com.Parameters.AddWithValue("@ExceptionURL", exepurl);
                com.Parameters.AddWithValue("@ExceptionSource", exdb.StackTrace.Substring(exdb.StackTrace.IndexOf("at ") + 3, exdb.StackTrace.IndexOf("at ", exdb.StackTrace.IndexOf("at ") + 1) - 6));
                com.ExecuteNonQuery();
            }
        }

       
        public static void InsertJSExcepToDB(string exceptionMessage,string exceptionType ,string exceptionSource,string exceptionUrl)
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                
                SqlCommand com = new SqlCommand("usp_InsertExceptionLoggingData", cn);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ExceptionMsg", exceptionMessage);
                com.Parameters.AddWithValue("@ExceptionType", exceptionType);
                com.Parameters.AddWithValue("@ExceptionURL", exceptionUrl);
                com.Parameters.AddWithValue("@ExceptionSource", exceptionSource);
                com.ExecuteNonQuery();
            }
        }
        #endregion

    }
}


