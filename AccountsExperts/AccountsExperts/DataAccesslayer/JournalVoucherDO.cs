﻿using AccountsExperts.Access_Layers.CommonDataaccess;
using AccountsExperts.Access_Layers.CommonDataAccess;
using AccountsExperts.DataObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web;

namespace AccountsExperts.DataAccesslayer
{
    public static class JournalVoucherDO
    {
        #region Constants
        private static string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
        #endregion

        #region Public Methods


        public static System.Data.DataSet GetJournalVoucherData(int pageSize, int pageIndex)
        {
            System.Data.DataSet ds = DataAccess.GetDataSetForGrid("usp_GetJounalVoucher", pageSize, pageIndex);
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "JournalVoucher";
                ds.Tables[1].TableName = "Pager";
            }
            return ds;
        }

        public static DataTable PopulateAccountName()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                DataTable dt = new DataTable();
                dt = DataAccess.GetDatatable("usp_GetDataFromChart");
                cn.Close();
                return dt;
            }
        }

        public static DataTable PopulateCompany()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                DataTable dt = new DataTable();
                dt = DataAccess.GetDatatable("usp_GetAllCompany");
                cn.Close();
                return dt;
            }
        }

        public static Int32 GetMaxID()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_GetJournalVoucherMaxID", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                int ID = 0;
                ID = (int)cmd.ExecuteScalar();
                return ID;

            }
        }

        public static bool InsertJournalVoucherData(DataObjects.JournalVoucherMaster objJournalVoucher, List<DataObjects.JournalVoucherDetail> objJournalVoucherDetail)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {

                    JournalVoucherDetail obj = new JournalVoucherDetail();
                    string xmlJournalVoucher = string.Empty;
                    string xmlJournalVoucherDetail = string.Empty;
                    DataAccess.CreateXmlSerializer(objJournalVoucher, out xmlJournalVoucher);
                    DataAccess.CreateXmlSerializer(objJournalVoucherDetail, out xmlJournalVoucherDetail);
                    using (SqlConnection cn = new SqlConnection(conConnectionString))
                    {
                        cn.Open();
                        SqlCommand cmdInsert = new SqlCommand("usp_InsertJournalVoucherData", cn);
                        cmdInsert.CommandType = CommandType.StoredProcedure;
                        cmdInsert.Parameters.Add("@xmlJournalVoucher", SqlDbType.Xml).Value = xmlJournalVoucher;
                        cmdInsert.Parameters.Add("@xmlJournalVoucherDetail", SqlDbType.Xml).Value = xmlJournalVoucherDetail;
                        int rowsAffected = cmdInsert.ExecuteNonQuery();
                        if (rowsAffected > 1)
                        {
                            ts.Complete();
                            return true;
                        }
                        else
                        {
                            ts.Dispose();
                            return false;
                        }

                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool DeleteJournalVoucherData(int JournalVoucherID)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    using (SqlConnection cn = new SqlConnection(conConnectionString))
                    {
                        cn.Open();
                        SqlCommand cmd = new SqlCommand("usp_DeleteJournalVoucherData", cn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@JournalVoucherID", SqlDbType.Int, 50).Value = JournalVoucherID;

                        bool result = (bool)cmd.ExecuteScalar();
                        if (result)
                        {
                            ts.Complete();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                catch
                {
                    ts.Dispose();
                    return false;
                }
            }
        }

        #endregion

    }
}