﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using AccountsExperts.Access_Layers.CommonDataaccess;
using System.Data;
using System.Transactions;
using AccountsExperts.DataObjects;

namespace AccountsExperts.DataAccesslayer
{
    public static class ApplicationUserDO
    {
        #region Constants
        private static string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
        #endregion

        #region Public Methods

        public static System.Data.DataSet GetApplicationUserData(int pageSize, int pageIndex)
        {
            try
            {
                System.Data.DataSet ds = DataAccess.GetDataSetForGrid("usp_GetApplicationUserData", pageSize, pageIndex);
                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "ApplicationUser";
                    ds.Tables[1].TableName = "Pager";
                }
                return ds;
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }


        public static DataTable PopulateUser()
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    DataTable dt = new DataTable();
                    dt = DataAccess.GetDatatable("usp_GetAllUser");
                    cn.Close();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }

        public static bool InsertApplicationUserData(DataObjects.ApplicationUser objApplicationUser)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmdInsert = new SqlCommand("usp_InsertApplicationUser", cn);
                    cmdInsert.CommandType = CommandType.StoredProcedure;
                    cmdInsert.Parameters.Add("@pk_UserID", SqlDbType.Int, 50).Value =objApplicationUser.PkUserID;
                    cmdInsert.Parameters.Add("@UserName", SqlDbType.VarChar, 100).Value = objApplicationUser.User_Name;
                    cmdInsert.Parameters.Add("@Password", SqlDbType.VarChar, 100).Value = objApplicationUser.Password;
                    cmdInsert.Parameters.Add("@fkEmployeeID", SqlDbType.Int, 20).Value = objApplicationUser.FkEmployee_ID;
                    cmdInsert.Parameters.Add("@Is_Active", SqlDbType.Decimal, 50).Value = objApplicationUser.Is_Active;
                    cmdInsert.Parameters.Add("@createdBy", SqlDbType.VarChar, 100).Value = objApplicationUser.Created_By;
                    cmdInsert.Parameters.Add("@CreatedOn", SqlDbType.DateTime, 50).Value = DateTime.Now;

                    bool result = (bool)cmdInsert.ExecuteScalar();
                    if (result)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }

        public static bool DeleteApplicationUserData(int applicationUserID)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_DeleteApplicationUser", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ApplicationUserID", SqlDbType.Int, 50).Value = applicationUserID;
                    bool result = (bool)cmd.ExecuteScalar();
                    if (result)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }

        public static System.Data.DataSet GetDataForUpdate(int applicationUserID)
        {
            try
            {
                System.Data.DataSet ds = DataAccess.GetDataSetByparams("usp_GetDataByApplicationUserForEdit", applicationUserID);
                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "UpdateData";
                }
                return ds;
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }

        public static bool UpdateApplicationUser(DataObjects.ApplicationUser objApplicationUser)
        {
            try
            {

                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_UpdateApplicationUser", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pk_UserID", SqlDbType.Int, 50).Value = objApplicationUser.PkUserID;
                    cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 100).Value = objApplicationUser.User_Name;
                    cmd.Parameters.Add("@Password", SqlDbType.VarChar, 100).Value = objApplicationUser.Password;
                    cmd.Parameters.Add("@fkEmployeeID", SqlDbType.Int, 20).Value = objApplicationUser.FkEmployee_ID;
                    cmd.Parameters.Add("@Is_Active", SqlDbType.Decimal, 50).Value = objApplicationUser.Is_Active;
                    cmd.Parameters.Add("@ModifiedBy", SqlDbType.VarChar, 50).Value = "Admin";
                    cmd.Parameters.Add("@ModifiedOn", SqlDbType.DateTime, 50).Value = DateTime.Now;


                    int result = (int)cmd.ExecuteNonQuery();
                    if (result == -1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }


                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }



        #endregion
    }
}