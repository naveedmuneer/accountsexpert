﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using AccountsExperts.Access_Layers.CommonDataaccess;
using System.Data;
using System.Transactions;
using AccountsExperts.DataObjects;
using System.Xml.Serialization;
using System.Xml;
using System.Security.Cryptography;
namespace AccountsExperts.DataAccesslayer
{
    public static class BankBookDO
    {
        #region Constants
        private static string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
        #endregion

        #region Public Methods

        public static System.Data.DataSet GetBankBookData(int pageSize, int pageIndex, string formType)
        {
            System.Data.DataSet ds = GetDataSetForBankBook("usp_GetBankBook", pageSize, pageIndex, formType);
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "BankBook";
                ds.Tables[1].TableName = "Pager";
            }
            return ds;
        }

        public static DataTable PopulateCompany()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                DataTable dt = new DataTable();
                dt = DataAccess.GetDatatable("usp_GetAllCompany");
                cn.Close();
                return dt;
            }
        }

        public static DataTable PopulateBankVoucher()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                DataTable dt = new DataTable();
                dt = DataAccess.GetDatatable("usp_GetBankVoucher");
                cn.Close();
                return dt;
            }
        }

        public static DataTable PopulateChequeStatus()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                DataTable dt = new DataTable();
                dt = DataAccess.GetDatatable("usp_GetChequeStatus");
                cn.Close();
                return dt;
            }
        }

        public static DataTable PopulateAccountName()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                DataTable dt = new DataTable();
                dt = DataAccess.GetDatatable("usp_GetDataFromChart");
                cn.Close();
                return dt;
            }
        }

        public static bool InsertBankBookData(DataObjects.BankBook objBankBook, List<DataObjects.BankBookDetail> objBankBookDetail)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    BankBookDetail obj = new BankBookDetail();
                    var myObjectArray = (from item in objBankBookDetail select item as object).ToArray();
                    string xmlBankBook = string.Empty;
                    string xmlBankBookDetail = string.Empty;
                    DataAccess.CreateXmlSerializer(objBankBook, out xmlBankBook);
                    DataAccess.CreateXmlSerializer(objBankBookDetail, out xmlBankBookDetail);
                    using (SqlConnection cn = new SqlConnection(conConnectionString))
                    {
                        cn.Open();
                        SqlCommand cmdInsert = new SqlCommand("usp_InsertBankBookData", cn);
                        cmdInsert.CommandType = CommandType.StoredProcedure;
                        cmdInsert.Parameters.Add("@xmlBankBook", SqlDbType.Xml).Value = xmlBankBook;
                        cmdInsert.Parameters.Add("@xmlBankBookDetail", SqlDbType.Xml).Value = xmlBankBookDetail;
                        int rowsAffected = cmdInsert.ExecuteNonQuery();
                        if (rowsAffected > 1)
                        {
                            ts.Complete();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                catch
                {
                    ts.Dispose();
                    return false;
                }
            }
        }


        public static bool InsertBankBookPostedEntries(String BankBookIDs)
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                SqlCommand cmdInsert = new SqlCommand("usp_InsertBankBookPostedEnteries", cn);
                cmdInsert.CommandType = CommandType.StoredProcedure;
                cmdInsert.Parameters.Add("@BankBookIDs", SqlDbType.VarChar).Value = BankBookIDs;
                int rowsAffected = cmdInsert.ExecuteNonQuery();
                if (rowsAffected > 0)
                {
                    
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


       public static bool DeleteBankBookData(int bankBookID)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    using (SqlConnection cn = new SqlConnection(conConnectionString))
                    {
                        cn.Open();
                        SqlCommand cmd = new SqlCommand("usp_DeleteBankBookData", cn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@BankBookID", SqlDbType.Int, 50).Value = bankBookID;

                        bool result = (bool)cmd.ExecuteScalar();
                        if (result)
                        {
                            ts.Complete();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                catch
                {
                    ts.Dispose();
                    return false;
                }
            }
        }

        public static System.Data.DataSet GetDataForUpdate(int bankBookID)
        {
            System.Data.DataSet ds = DataAccess.GetDataSetByparams("usp_GetDataByBankBookForEdit", bankBookID);
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "UpdateData";

            }
            return ds;
        }

        public static bool UpdateBankBookData(DataObjects.BankBook objBankBook, List<DataObjects.BankBookDetail> BankBookDetail)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    BankBookDetail obj = new BankBookDetail();

                    string xmlBankBookMaster = string.Empty;
                    string xmlBanakBookDetail = string.Empty;
                    DataAccess.CreateXmlSerializer(objBankBook, out xmlBankBookMaster);
                    DataAccess.CreateXmlSerializer(BankBookDetail, out xmlBanakBookDetail);
                    using (SqlConnection cn = new SqlConnection(conConnectionString))
                    {
                        cn.Open();
                        SqlCommand cmdInsert = new SqlCommand("usp_UpdateBankBookData", cn);
                        cmdInsert.CommandType = CommandType.StoredProcedure;
                        cmdInsert.Parameters.Add("@xmlBankBookMaster", SqlDbType.Xml).Value = xmlBankBookMaster;
                        cmdInsert.Parameters.Add("@xmlBanakBookDetail", SqlDbType.Xml).Value = xmlBanakBookDetail;
                        int rowsAffected = cmdInsert.ExecuteNonQuery();
                        if (rowsAffected > 1)
                        {
                            ts.Complete();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                catch
                {
                    ts.Dispose();
                    return false;
                }
            }
        }

        public static Int32 GetLastInsertedID(string bankBookType)
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_GetBankBookLastInsertedID", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@InputType", SqlDbType.VarChar, 50).Value = bankBookType;
                int ID = 0;
                ID = (int)cmd.ExecuteScalar();
                return ID;

            }
        }

        #endregion

        #region Private Methods

        private static System.Data.DataSet GetDataSetForBankBook(string storedProcedure, int pageSize, int pageIndex, string formType)
        {
            System.Data.DataSet Results = new System.Data.DataSet();
            string recordCount = "0";
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = storedProcedure;

                        cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                        cmd.Parameters.AddWithValue("@PageSize", pageSize);
                        cmd.Parameters.AddWithValue("@BankBookType", formType);


                        SqlParameter output = new SqlParameter("@RecordCount", SqlDbType.Int);
                        output.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(output);
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(Results);
                        recordCount = output.Value.ToString();
                    }

                    dt.Columns.Add("PageIndex");
                    dt.Columns.Add("PageSize");
                    dt.Columns.Add("RecordCount");
                    dt.Rows.Add();
                    dt.Rows[0]["PageIndex"] = pageIndex;
                    dt.Rows[0]["PageSize"] = pageSize;
                    dt.Rows[0]["RecordCount"] = recordCount;
                    Results.Tables.Add(dt);
                }
            }

            catch (Exception)
            {

            }

            return Results;
        }

        #endregion




    }
}