﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using AccountsExperts.Access_Layers.CommonDataaccess;
using System.Data;
using System.Transactions;
using AccountsExperts.DataObjects;




namespace AccountsExperts.DataAccesslayer
{
    public static class EmployeeDO
    {
        #region Constants
        private static string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
        #endregion

        #region Public Methods

        public static System.Data.DataSet GetEmployeeData(int pageSize, int pageIndex)
        {
            try
            {
                System.Data.DataSet ds = DataAccess.GetDataSetForGrid("usp_GetEmployeeData", pageSize, pageIndex);
                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "EmployeeData";
                    ds.Tables[1].TableName = "Pager";
                }
                return ds;
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }

        public static bool DeleteEmployee(int employeeID)
        {
            try
            {
                int result = 0;
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_DeleteEmployee", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@EmplpoyeeID", SqlDbType.Int, 50).Value = employeeID;
                    result = cmd.ExecuteNonQuery();
                }
                if (result != 0)
                {
                    return true;
                }
                else
                    return false;

            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }

        public static DataTable PopulateCompany()
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    DataTable dt = new DataTable();
                    dt = DataAccess.GetDatatable("usp_GetAllCompany");
                    cn.Close();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }

        public static DataTable PopulateDepartment()
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    DataTable dt = new DataTable();
                    dt = DataAccess.GetDatatable("usp_GetAllDepartment");
                    cn.Close();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }

        public static DataTable PopulateDesignation()
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    DataTable dt = new DataTable();
                    dt = DataAccess.GetDatatable("usp_GetAllDesignation");
                    cn.Close();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }

        public static bool InsertEmplpyee(DataObjects.Employee objEmployee)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_InsertEmplpyee", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@EmployeeName", SqlDbType.VarChar, 50).Value = objEmployee.Employee_Name;
                    cmd.Parameters.Add("@NIC", SqlDbType.VarChar, 50).Value = objEmployee.NIC_No;
                    cmd.Parameters.Add("@CompanyID", SqlDbType.Int, 50).Value = objEmployee.fkCompany_ID;
                    cmd.Parameters.Add("@DepartmentID", SqlDbType.Int, 100).Value = objEmployee.fkDepartment_ID;
                    cmd.Parameters.Add("@DesignationID", SqlDbType.Int, 500).Value = objEmployee.fkDesignation_ID;
                    cmd.Parameters.Add("@Email", SqlDbType.VarChar, 50).Value = objEmployee.Email;
                    cmd.Parameters.Add("@ContactNo", SqlDbType.VarChar, 100).Value = objEmployee.Contact_No;
                    cmd.Parameters.Add("@SecondaryConatctNo", SqlDbType.VarChar, 500).Value = objEmployee.Secondary_Contact_No;
                    cmd.Parameters.Add("@Address", SqlDbType.VarChar, 50).Value = objEmployee.Address;
                    cmd.Parameters.Add("@PermenantAddress", SqlDbType.VarChar, 100).Value = objEmployee.Permenant_Address;
                    cmd.Parameters.Add("@Active", SqlDbType.Bit, 1).Value = Convert.ToBoolean(objEmployee.Is_Active);
                    cmd.Parameters.Add("@CreatedOn", SqlDbType.DateTime, 100).Value = DateTime.Now;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.VarChar, 50).Value = objEmployee.Created_By;

                    int result = cmd.ExecuteNonQuery();
                    if (result != 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }


                // return true;
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }

        public static bool UpdateEmplpyee(DataObjects.Employee objEmployee)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_UpdateEmployee", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@EmployeeID", SqlDbType.Int, 50).Value = objEmployee.pk_Employee_ID;
                    cmd.Parameters.Add("@EmployeeName", SqlDbType.VarChar, 50).Value = objEmployee.Employee_Name;
                    cmd.Parameters.Add("@NIC", SqlDbType.VarChar, 50).Value = objEmployee.NIC_No;
                    cmd.Parameters.Add("@CompanyID", SqlDbType.Int, 50).Value = objEmployee.fkCompany_ID;
                    cmd.Parameters.Add("@DepartmentID", SqlDbType.Int, 100).Value = objEmployee.fkDepartment_ID;
                    cmd.Parameters.Add("@DesignationID", SqlDbType.Int, 500).Value = objEmployee.fkDesignation_ID;
                    cmd.Parameters.Add("@Email", SqlDbType.VarChar, 50).Value = objEmployee.Email;
                    cmd.Parameters.Add("@ContactNo", SqlDbType.VarChar, 100).Value = objEmployee.Contact_No;
                    cmd.Parameters.Add("@SecondaryConatctNo", SqlDbType.VarChar, 500).Value = objEmployee.Secondary_Contact_No;
                    cmd.Parameters.Add("@Address", SqlDbType.VarChar, 50).Value = objEmployee.Address;
                    cmd.Parameters.Add("@PermenantAddress", SqlDbType.VarChar, 100).Value = objEmployee.Permenant_Address;
                    cmd.Parameters.Add("@Active", SqlDbType.Bit, 1).Value = Convert.ToBoolean(objEmployee.Is_Active);
                    cmd.Parameters.Add("@ModifiedOn", SqlDbType.DateTime, 200).Value = DateTime.Now;
                    cmd.Parameters.Add("@ModifiedBy", SqlDbType.VarChar, 50).Value = objEmployee.Created_By;





                    int result = (int)cmd.ExecuteNonQuery();
                    if (result != 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }


            // return true;
        }

        public static System.Data.DataSet GetDataForUpdate(int employeeID)
        {
            try
            {
                System.Data.DataSet ds = DataAccess.GetDataSetByparams("usp_GetDatabByEmployeeForEdit", employeeID);
                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "UpdateData";
                }
                return ds;
        
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }

        
        #endregion

        }
    }
