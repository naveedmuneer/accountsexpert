﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using AccountsExperts.DataObjects;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using AccountsExperts.Access_Layers.CommonDataaccess;


namespace AccountsExperts.DataAccesslayer
{
    public class CompanyDO
    {
        #region Constants

        private static string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;


        #endregion

        #region Public Methods
        public static bool InsertCompnay(Company objCompany)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    bool flag = false;

                    flag = InsertCompanies(objCompany);


                    if (flag)
                    {
                        ts.Complete();
                        return true;
                    }
                    else
                    {
                        ts.Dispose();
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }

        }

        public static bool UpdateCompany(AccountsExperts.DataObjects.Company objCompany)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    bool flag = false;

                    flag = UpdateCompanies(objCompany);


                    if (flag)
                    {
                        ts.Complete();
                        return true;
                    }
                    else
                    {
                        ts.Dispose();
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
                //  return false;
            }

        }

        public static bool DeleteCompany(int CompanyID)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    bool Flag = false;
                    Flag = DeleteCompanies(CompanyID);

                    if (Flag)
                    {
                        ts.Complete();
                        return true;
                    }
                    else
                    {
                        ts.Dispose();
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
                //  return false;
            }


        }

        public static System.Data.DataSet GetCompanyData(int pageSize, int pageIndex)
        {
            try
            {
                System.Data.DataSet ds = DataAccess.GetDataSetForGrid("usp_GetCompany", pageSize, pageIndex);
                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "Companies";
                    ds.Tables[1].TableName = "Pager";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
                //  return false;
            }

        }



        public static System.Data.DataSet GetDataForUpdate(int bankBookID)
        {
            try
            {

                System.Data.DataSet ds = DataAccess.GetDataSetByparams("usp_GetCompanyDataForEdit", bankBookID);
                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].TableName = "UpdateData";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
                //  return false;
            }

        }

        #endregion

        #region Private Methods
        private static bool InsertCompanies(Company objCompany)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_InsertCompany", cn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@Company_Name", SqlDbType.VarChar, 50).Value = objCompany.CompanyName;
                    cmd.Parameters.Add("@Description", SqlDbType.VarChar, 50).Value = objCompany.Description;
                    cmd.Parameters.Add("@Title", SqlDbType.VarChar, 50).Value = objCompany.Title;
                    cmd.Parameters.Add("@Business", SqlDbType.VarChar, 50).Value = objCompany.Business;
                    cmd.Parameters.Add("@Establishment_Date", SqlDbType.DateTime, 500).Value = objCompany.Establishment_Date;
                    cmd.Parameters.Add("@Expiry_Date", SqlDbType.DateTime, 500).Value = objCompany.Expiry_Date;
                    cmd.Parameters.Add("@Is_Active", SqlDbType.Bit, 50).Value = objCompany.Is_Active;
                   cmd.Parameters.Add("@CreatedBy", SqlDbType.VarChar, 50).Value = objCompany.Created_By;


                    bool result = (bool)cmd.ExecuteScalar();
                    if (result)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }


                // return true;
            }
            catch (Exception ex)
            {
                throw ex;
                // return false;
            }

        }

        private static bool UpdateCompanies(Company objCompany)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_UpdateCompany", cn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@pkCompany_ID", SqlDbType.Int, 50).Value = objCompany.CompanyID;
                    cmd.Parameters.Add("@Company_Name", SqlDbType.VarChar, 50).Value = objCompany.CompanyName;
                    cmd.Parameters.Add("@Description", SqlDbType.VarChar, 50).Value = objCompany.Description;
                    cmd.Parameters.Add("@Title", SqlDbType.VarChar, 50).Value = objCompany.Title;
                    cmd.Parameters.Add("@Business", SqlDbType.VarChar, 50).Value = objCompany.Business;
                    cmd.Parameters.Add("@Establishment_Date", SqlDbType.Date, 500).Value = objCompany.Establishment_Date;
                    cmd.Parameters.Add("@Expiry_Date", SqlDbType.Date, 500).Value = objCompany.Expiry_Date;
                    cmd.Parameters.Add("@Is_Active", SqlDbType.Bit, 50).Value = objCompany.Is_Active;


                    bool result = (bool)cmd.ExecuteScalar();
                    if (result)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }


                // return true;
            }
            catch (Exception ex)
            {
                throw ex;
                //   return false;
            }

        }

        private static bool DeleteCompanies(int CompanyID)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_DeleteCompany", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@CompanyID", SqlDbType.Int, 50).Value = CompanyID;
                    bool result = (bool)cmd.ExecuteScalar();
                    if (result)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //  return false;
            }

        }

        #endregion

    }


}