﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AccountsExperts.DataAccesslayer
{
    public class ChartOfAccountDO
    {

        string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;

        public bool insertChartOfAccountData(DataObjects.ChartOfAccount objChartOfAccount)
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_InsertChartOfAccount", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ChartID", SqlDbType.VarChar, 50).Value = objChartOfAccount.pk_ChartID;
                cmd.Parameters.Add("@ChartRefNo", SqlDbType.VarChar, 50).Value = objChartOfAccount.ChartRefNo;
                cmd.Parameters.Add("@ChartName", SqlDbType.VarChar, 50).Value = objChartOfAccount.ChartName;
                cmd.Parameters.Add("@Description", SqlDbType.VarChar, 50).Value = objChartOfAccount.Description;
                cmd.Parameters.Add("@GroupID", SqlDbType.VarChar, 500).Value = objChartOfAccount.GroupID;
                cmd.Parameters.Add("@AccountNatureID", SqlDbType.VarChar, 50).Value = objChartOfAccount.fk_AccountNatureID;
                cmd.Parameters.Add("@Note", SqlDbType.VarChar, 100).Value = objChartOfAccount.Note;
                cmd.Parameters.Add("@Address", SqlDbType.VarChar, 1000).Value = objChartOfAccount.Address;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar, 100).Value = objChartOfAccount.Phone;
                cmd.Parameters.Add("@Fax", SqlDbType.VarChar, 100).Value = objChartOfAccount.Fax;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar, 100).Value = objChartOfAccount.Email;
                cmd.Parameters.Add("@NTNNO", SqlDbType.VarChar).Value = objChartOfAccount.NTNNO;
                cmd.Parameters.Add("@GSTNO", SqlDbType.VarChar, 1).Value = objChartOfAccount.GSTNO;
                cmd.Parameters.Add("@Active", SqlDbType.Bit, 1).Value = Convert.ToBoolean(objChartOfAccount.Active);
                cmd.Parameters.Add("@CreatedBy", SqlDbType.VarChar, 50).Value = objChartOfAccount.Created_By;
                cmd.Parameters.Add("@CreatedOn", SqlDbType.DateTime, 50).Value = DateTime.Now;


                bool result = (bool)cmd.ExecuteScalar();
                if (result)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public Int32 GetMaxID()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_GetChartOfAccountMaxID", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                int ID=0;
                ID=(int)cmd.ExecuteScalar();
                return ID;

            }
        }

        public bool updateChartOfAccountData(DataObjects.ChartOfAccount objChartOfAccount)
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_UpdateChartOfAccount", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ChartID", SqlDbType.VarChar, 50).Value = objChartOfAccount.pk_ChartID;
                cmd.Parameters.Add("@ChartRefNo", SqlDbType.VarChar, 50).Value = objChartOfAccount.ChartRefNo;
                cmd.Parameters.Add("@ChartName", SqlDbType.VarChar, 50).Value = objChartOfAccount.ChartName;
                cmd.Parameters.Add("@Description", SqlDbType.VarChar, 50).Value = objChartOfAccount.Description;
                cmd.Parameters.Add("@GroupID", SqlDbType.VarChar, 500).Value = objChartOfAccount.GroupID;
                cmd.Parameters.Add("@AccountNatureID", SqlDbType.VarChar, 50).Value = objChartOfAccount.fk_AccountNatureID;
                cmd.Parameters.Add("@Note", SqlDbType.VarChar, 100).Value = objChartOfAccount.Note;
                cmd.Parameters.Add("@Address", SqlDbType.VarChar, 1000).Value = objChartOfAccount.Address;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar, 100).Value = objChartOfAccount.Phone;
                cmd.Parameters.Add("@Fax", SqlDbType.VarChar, 100).Value = objChartOfAccount.Fax;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar, 100).Value = objChartOfAccount.Email;
                cmd.Parameters.Add("@NTNNO", SqlDbType.VarChar).Value = objChartOfAccount.NTNNO;
                cmd.Parameters.Add("@GSTNO", SqlDbType.VarChar, 1).Value = objChartOfAccount.GSTNO;
                cmd.Parameters.Add("@Active", SqlDbType.Bit, 1).Value = Convert.ToBoolean(objChartOfAccount.Active);
                cmd.Parameters.Add("@ModifiedBy", SqlDbType.VarChar, 50).Value = "Admin";
                cmd.Parameters.Add("@ModifiedOn", SqlDbType.DateTime, 50).Value = DateTime.Now;


                int result = (int)cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public bool deleteChartOfAccountData(int chartID)
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_DeleteChartOfAccount", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ChartID", SqlDbType.Int, 50).Value = chartID;

                bool result = (bool)cmd.ExecuteScalar();
                if (result)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
    }
}