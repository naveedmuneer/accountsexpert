﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using AccountsExperts.DataObjects;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using AccountsExperts.Access_Layers.CommonDataaccess;

namespace AccountsExperts.DataAccesslayer
{
    public static class RolesDO
    {
        #region Constants

        private static string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;

        #endregion


        #region Public Methods
        public static bool InsertRole(Roles objRoles)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    bool flag = false;

                    flag = InsertRoles(objRoles);


                    if (flag)
                    {
                        ts.Complete();
                        return true;
                    }
                    else
                    {
                        ts.Dispose();
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }

        }

        public static bool UpdateRole(Roles objRoles)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    bool flag = false;

                    flag = UpdateRoles(objRoles);


                    if (flag)
                    {
                        ts.Complete();
                        return true;
                    }
                    else
                    {
                        ts.Dispose();
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }

        }
        public static System.Data.DataSet GetRolesData(int pageSize, int pageIndex)
        {
            try{
            System.Data.DataSet ds = DataAccess.GetDataSetForGrid("usp_GetRoles", pageSize, pageIndex);
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "Roles";
                ds.Tables[1].TableName = "Pager";
            }
            return ds;
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }
        public static System.Data.DataSet GetDataForUpdate(int bankBookID)
        {
         try {
            System.Data.DataSet ds = DataAccess.GetDataSetByparams("usp_GetRoleDataForEdit", bankBookID);
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "UpdateData";
            }
            return ds;
         }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }
        public static bool DeletRole(int RoleID)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    bool Flag = false;
                    Flag = DeleteRoles(RoleID);

                    if (Flag)
                    {
                        ts.Complete();
                        return true;
                    }
                    else
                    {
                        ts.Dispose();
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }


        }

        #endregion


        #region private methods
        private static bool InsertRoles(Roles objRole)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_InsertRole", cn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@Role_Name", SqlDbType.VarChar, 50).Value = objRole.Role_Name;
                    cmd.Parameters.Add("@Is_Active", SqlDbType.Bit, 50).Value = objRole.Is_Active;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.VarChar, 50).Value = objRole.Created_By;
                   

                    bool result = (bool)cmd.ExecuteScalar();
                    if (result)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }


                // return true;
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }

        }
        private static bool UpdateRoles(Roles objRole)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_UpdateRole", cn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@pkRole_ID", SqlDbType.Int, 50).Value = objRole.pkRole_ID;
                    cmd.Parameters.Add("@Role_Name", SqlDbType.VarChar, 50).Value = objRole.Role_Name;
                    cmd.Parameters.Add("@Is_Active", SqlDbType.VarChar, 50).Value = objRole.Is_Active;


                    bool result = (bool)cmd.ExecuteScalar();
                    if (result)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }


                // return true;
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }

        }
        private static bool DeleteRoles(int RoleID)
        {
            try
            {
                //usp_DeleteCashBookDetail
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_DeleteRole", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@RoleID", SqlDbType.Int, 50).Value = RoleID;
                    bool result = (bool)cmd.ExecuteScalar();
                    if (result)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                //return false;
                throw ex;
            }
        }
        

        #endregion
    }
}