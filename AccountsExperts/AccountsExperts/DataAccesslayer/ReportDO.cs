﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace AccountsExperts.DataAccesslayer
{
    public static class ReportDO
    {
        private static string strConnString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;

        //Get CashBookReceipt and CashBookPayment Report
        public static DataSet GetCashBookReport(int CashBookID)
        {

            using (SqlConnection con = new SqlConnection(strConnString))
            {
                string query = "usp_GetCashBookReportData";
                SqlCommand cmd = new SqlCommand(query);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CashBookID", CashBookID);

                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    da.SelectCommand = cmd;
                    using (DataSet ds = new DataSet())
                    {
                        da.Fill(ds, "CashBook");
                        return ds;
                    }
                }
            }

        }

        public static DataSet GetBankBookReport(int bankBookID)
        {

            using (SqlConnection con = new SqlConnection(strConnString))
            {
                string query = "usp_GetBankBookReportData";
                SqlCommand cmd = new SqlCommand(query);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BankBookID", bankBookID);

                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    da.SelectCommand = cmd;
                    using (DataSet ds = new DataSet())
                    {
                        da.Fill(ds, "BankBook");
                        return ds;
                    }
                }
            }

        }
    }
}