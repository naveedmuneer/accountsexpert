﻿using AccountsExperts.DataObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AccountsExperts.DataAccesslayer
{
    public static class LoginDO
    {

        #region Constants
        private static string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
        #endregion

        #region Public Methods

        public static bool ValidateUser(ApplicationUser objUser)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_Validateuser", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@strName", SqlDbType.VarChar, 50).Value = objUser.User_Name;
                    cmd.Parameters.Add("@strPassword", SqlDbType.VarChar, 50).Value = objUser.Password;
                    bool status = (bool)cmd.ExecuteScalar();
                    return status;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //lll
        }
        public static int GetSessionId(ApplicationUser objUser)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(conConnectionString))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("usp_GetSessionId", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@strName", SqlDbType.VarChar, 50).Value = objUser.User_Name;
                    int SessionId = (int)cmd.ExecuteScalar();
                    return SessionId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        #endregion

        #region Private Methods

        #endregion
    }
}