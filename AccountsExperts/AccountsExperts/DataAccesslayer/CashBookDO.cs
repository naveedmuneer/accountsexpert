﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using AccountsExperts.Access_Layers.CommonDataaccess;
using System.Data;
using System.Transactions;
using AccountsExperts.DataObjects;
using System.Xml.Serialization;
using System.Xml;
using System.Security.Cryptography;
using AccountsExperts.Access_Layers.CommonDataaccess;


namespace AccountsExperts.DataAccesslayer
{
    public static class CashBookDO
    {
        #region Constants
        private static string conConnectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
        #endregion

        #region Public Methods

        public static System.Data.DataSet GetDataForUpdate(int cashBookID)
        {
            System.Data.DataSet ds = DataAccess.GetDataSetByparams("usp_GetDataByCashBookForEdit", cashBookID);
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "UpdateData";
            }
            return ds;
        }


        public static bool InsertCashBook(DataObjects.CashBook objCashBook, List<DataObjects.CashBookDetail> objCashBookDetail)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    CashBookDetail obj = new CashBookDetail();
                    var myObjectArray = (from item in objCashBookDetail select item as object).ToArray();
                    string xmlCashBook = string.Empty;
                    string xmlCashBookDetail = string.Empty;
                    DataAccess.CreateXmlSerializer(objCashBook, out xmlCashBook);
                    DataAccess.CreateXmlSerializer(objCashBookDetail, out xmlCashBookDetail);
                    using (SqlConnection cn = new SqlConnection(conConnectionString))
                    {
                        cn.Open();
                        SqlCommand cmdInsert = new SqlCommand("usp_InsertCashBook", cn);
                        cmdInsert.CommandType = CommandType.StoredProcedure;
                        cmdInsert.Parameters.Add("@xmlCashBook", SqlDbType.Xml).Value = xmlCashBook;
                        cmdInsert.Parameters.Add("@xmlCashBookDetail", SqlDbType.Xml).Value = xmlCashBookDetail;
                        int rowsAffected = cmdInsert.ExecuteNonQuery();
                        if (rowsAffected > 1)
                        {
                            ts.Complete();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                catch
                {
                    ts.Dispose();
                    return false;
                }
            }
        }



        public static bool DeleteCashBookData(int CashBookID)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    using (SqlConnection cn = new SqlConnection(conConnectionString))
                    {
                        cn.Open();
                        SqlCommand cmd = new SqlCommand("usp_DeleteCashBookData", cn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@CashBookID", SqlDbType.Int, 50).Value = CashBookID;

                        bool result = (bool)cmd.ExecuteScalar();
                        if (result)
                        {
                            ts.Complete();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                catch
                {
                    ts.Dispose();
                    return false;
                }
            }
        }

        public static bool UpdateCashBookData(DataObjects.CashBook objCashBook, List<DataObjects.CashBookDetail> CashBookDetail)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    CashBookDetail obj = new CashBookDetail();

                    string xmlCashBookMaster = string.Empty;
                    string xmlCashBookDetail = string.Empty;
                    DataAccess.CreateXmlSerializer(objCashBook, out xmlCashBookMaster);
                    DataAccess.CreateXmlSerializer(CashBookDetail, out xmlCashBookDetail);
                    using (SqlConnection cn = new SqlConnection(conConnectionString))
                    {
                        cn.Open();
                        SqlCommand cmdInsert = new SqlCommand("usp_UpdateCashBookData", cn);
                        cmdInsert.CommandType = CommandType.StoredProcedure;
                        cmdInsert.Parameters.Add("@xmlCashBookMaster", SqlDbType.Xml).Value = xmlCashBookMaster;
                        cmdInsert.Parameters.Add("@xmlCashBookDetail", SqlDbType.Xml).Value = xmlCashBookDetail;
                        int rowsAffected = cmdInsert.ExecuteNonQuery();
                        if (rowsAffected > 1)
                        {
                            ts.Complete();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                catch
                {
                    ts.Dispose();
                    return false;
                }
            }
        }


 public static Int32 GetLastInsertedID(string cashBookType)
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_GetCashBookLastInsertedID", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@InputType", SqlDbType.VarChar, 50).Value = cashBookType;
                int ID = 0;
                ID = (int)cmd.ExecuteScalar();
                return ID;

            }
        }

        public static DataTable PopulateCompany()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                DataTable dt = new DataTable();
                dt = DataAccess.GetDatatable("usp_GetAllCompany");
                cn.Close();
                return dt;
            }
        }

        public static System.Data.DataSet GetCashBookData(int pageSize, int pageIndex, string formType, string transactionStartDate, string transactionEndDate, string searchVoucherNo, string searchVoucherDate, string searchVoucherBook, string searchChequeNo, string searchChequeDate)
        {
            System.Data.DataSet ds = GetDataSetForCashBook("usp_GetCashBook", pageSize, pageIndex, formType, transactionStartDate, transactionEndDate, searchVoucherNo, searchVoucherDate, searchVoucherBook, searchChequeNo, searchChequeDate);
            if (ds.Tables.Count > 0)
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "CashBook";
                ds.Tables[1].TableName = "Pager";
            }
            return ds;
        }


        public static System.Data.DataSet BindAllDropDowns()
        {
            System.Data.DataSet ds = DataAccess.GetDataset("usp_BindAllDropDownsOfCashBook");
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "Company";
                ds.Tables[1].TableName = "VoucherBook";
                ds.Tables[2].TableName = "AccountName";
            }
            return ds;
        }

        public static DataTable PopulateCashVoucher()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                DataTable dt = new DataTable();
                dt = DataAccess.GetDatatable("usp_GetBankVoucher");
                cn.Close();
                return dt;
            }
        }

        public static DataTable PopulateChequeStatus()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                DataTable dt = new DataTable();
                dt = DataAccess.GetDatatable("usp_GetChequeStatus");
                cn.Close();
                return dt;
            }
        }

        public static DataTable PopulateAccountName()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                DataTable dt = new DataTable();
                dt = DataAccess.GetDatatable("usp_GetDataFromChart");
                cn.Close();
                return dt;
            }
        }

        public static Int32 GetMaxID()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_GetCashBookMaxID", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                int ID = 0;
                ID = (int)cmd.ExecuteScalar();
                return ID;

            }
        }

        #endregion

        #region Private Methods

                

        private static Int32 GetCashBookDetailMaxID()
        {
            using (SqlConnection cn = new SqlConnection(conConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_GetCashBookDetailMaxID", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                int ID = 0;
                ID = (int)cmd.ExecuteScalar();
                return ID;

            }
        }

        private static System.Data.DataSet GetDataSetForCashBook(string storedProcedure, int pageSize, int pageIndex, string formType, string transactionStartDate,
          string transactionEndDate, string searchVoucherNo, string searchVoucherDate, string searchVoucherBook, string searchChequeNo, string searchChequeDate)
        {
            System.Data.DataSet Results = new System.Data.DataSet();
            string recordCount = "0";
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = storedProcedure;

                        cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                        cmd.Parameters.AddWithValue("@PageSize", pageSize);
                        cmd.Parameters.AddWithValue("@CashBookType", formType);
                        if (!string.IsNullOrEmpty(transactionStartDate))
                            cmd.Parameters.AddWithValue("@TransactionStartDate", Convert.ToDateTime(transactionStartDate).ToString("yyyy/MM/dd"));
                        else
                            cmd.Parameters.AddWithValue("@TransactionStartDate", null);

                        if (!string.IsNullOrEmpty(transactionEndDate))
                            cmd.Parameters.AddWithValue("@TransactionENDDate", Convert.ToDateTime(transactionEndDate).ToString("yyyy/MM/dd"));
                        else
                            cmd.Parameters.AddWithValue("@TransactionENDDate", null);

                        if (!string.IsNullOrEmpty(searchVoucherNo))
                            cmd.Parameters.AddWithValue("@SearchCashBookRefNo", searchVoucherNo);
                        else
                            cmd.Parameters.AddWithValue("@SearchCashBookRefNo", null);

                        if (!string.IsNullOrEmpty(searchVoucherDate))
                            cmd.Parameters.AddWithValue("@SearchTransactionDate", Convert.ToDateTime(searchVoucherDate).ToString("yyyy/MM/dd"));
                        else
                            cmd.Parameters.AddWithValue("@SearchTransactionDate", null);

                        if (!string.IsNullOrEmpty(searchVoucherBook))
                            cmd.Parameters.AddWithValue("@SearchVoucherBook", searchVoucherBook);
                        else
                            cmd.Parameters.AddWithValue("@SearchVoucherBook", null);

                        if (!string.IsNullOrEmpty(searchChequeNo))
                            cmd.Parameters.AddWithValue("@SearchChequeNo", searchChequeNo);
                        else
                            cmd.Parameters.AddWithValue("@SearchChequeNo", null);

                        if (!string.IsNullOrEmpty(searchChequeDate))
                            cmd.Parameters.AddWithValue("@SearchChequeDate", Convert.ToDateTime(searchChequeDate).ToString("yyyy/MM/dd"));
                        else
                            cmd.Parameters.AddWithValue("@SearchChequeDate", null);


                        SqlParameter output = new SqlParameter("@RecordCount", SqlDbType.Int);
                        output.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(output);
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(Results);
                        recordCount = output.Value.ToString();
                    }

                    dt.Columns.Add("PageIndex");
                    dt.Columns.Add("PageSize");
                    dt.Columns.Add("RecordCount");
                    dt.Rows.Add();
                    dt.Rows[0]["PageIndex"] = pageIndex;
                    dt.Rows[0]["PageSize"] = pageSize;
                    dt.Rows[0]["RecordCount"] = recordCount;
                    Results.Tables.Add(dt);
                }
            }

            catch (Exception)
            {

            }

            return Results;
        }

    
        #endregion


    }
}