﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Data.SqlClient;
using AccountsExperts.Access_Layers.CommonDataaccess;
using AccountsExperts.Access_Layers.DataAccesslayer;
using AccountsExperts.Access_Layers.CommonDataAccess;
using System.IO;


namespace AccountsExperts
{
    public partial class MainMaster1 : System.Web.UI.MasterPage
    {
        MainMasterDO objMainMaster1 = new MainMasterDO();

        protected void Page_Load(object sender, EventArgs e)
        {


            

            if (!IsPostBack)
            {
                GetMenuData();
                Session["ReturnUrl"] = Request.Url.AbsolutePath;
                string formpath = Path.GetFileName(Request.PhysicalPath);

                if (formpath != "frmForm" && (formpath != "frmRole"))
                {
                    var username = SessionHandler.Current.LoginName;
                    //lblUserLoggedIn.Text = "Welcome " + username;
                   // lblUserNameTopBar.Text = "Welcome " + username;
                    lblUserNameTopNav.Text = username + " ... !";

                   


                }
            }
        }
        private void GetMenuData()
        {
            DataTable dt = objMainMaster1.GetMenusData();
            DataView view = new DataView(dt);
            view.RowFilter = "ParentNode = 0";
            view.Sort = "tab_Seq ASC ";
            litMenu.Text += "<ul id='main-menu' class='main-menu'>";
            foreach (DataRowView row in view)
            {
                DataView viewSubItem = new DataView(dt);
                viewSubItem.RowFilter = "ParentNode=" + row["pk_MenuID"];
                if (viewSubItem.Count > 0)
                {
                    litMenu.Text += "<li><a href=''><i class='linecons-cog'></i><span class='title' >" + row["NodeName"].ToString() + "</span><span class='title'></span></a>";
                }
                else
                {
                    litMenu.Text += "<li><a href='" + row["NodeUrl"].ToString() + "'><i class='linecons-cloud'></i><span class='title'>" + row["NodeName"].ToString() + "</span></a>";
                }

                MenuItem menuItem = new MenuItem(row["NodeName"].ToString(), row["pk_MenuID"].ToString());
                menuItem.NavigateUrl = row["NodeUrl"].ToString();

                AddChildItems(dt, menuItem);
            }
            litMenu.Text += "</li>";
            litMenu.Text += "</ul>";
        }
        private void AddChildItems(DataTable table, MenuItem menuItem)
        {
            DataView viewItem = new DataView(table);
            viewItem.RowFilter = "ParentNode=" + menuItem.Value;
            viewItem.Sort = "Seq ASC ";
            if (viewItem.Count > 0)
            {
                litMenu.Text += "<ul class='main-menu'>";
                foreach (DataRowView childView in viewItem)
                {
                    DataView viewSubItem = new DataView(table);
                    viewSubItem.RowFilter = "ParentNode=" + childView["pk_MenuID"];
                    if (viewSubItem.Count > 0)
                    {
                        litMenu.Text += "<li><a>" + childView["NodeName"].ToString() + "</a>";
                    }
                    else
                    {
                        litMenu.Text += "<li ><a href='" + childView["NodeUrl"].ToString() + "' ><i  class='fa fa-files-o'></i><span class='title' ></span>" + childView["NodeName"].ToString() + "</a>";
                    }

                    MenuItem childItem = new MenuItem(childView["NodeName"].ToString(), childView["pk_MenuID"].ToString());
                    childItem.NavigateUrl = childView["NodeUrl"].ToString();
                    menuItem.ChildItems.Add(childItem);
                   // AddSubChildItems(table, childItem);
                    litMenu.Text += "</li>";
                }
                litMenu.Text += "</ul>";
            }
        }
        private void AddSubChildItems(DataTable table, MenuItem menuItem)
        {
            DataView viewItem = new DataView(table);
            viewItem.RowFilter = "ParentNode=" + menuItem.Value;
            litMenu.Text += "<ul>";
            foreach (DataRowView childView in viewItem)
            {
                litMenu.Text += "<li><a href='" + childView["NodeUrl"].ToString() + "'>" + childView["NodeName"].ToString() + "</a>";
                MenuItem childItem = new MenuItem(childView["NodeName"].ToString(), childView["menu_id"].ToString());
                childItem.NavigateUrl = childView["NodeUrl"].ToString();
                menuItem.ChildItems.Add(childItem);
                litMenu.Text += "</li>";
            }
            litMenu.Text += "</ul>";
        }
    }
}