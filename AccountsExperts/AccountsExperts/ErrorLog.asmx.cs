﻿using AccountsExperts.DataAccesslayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace AccountsExperts
{
    /// <summary>
    /// Summary description for ErrorLog
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    [System.Web.Script.Services.ScriptService]
    public class ErrorLog : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public void LogError(string exceptionMessage, string exceptionType, string exceptionSource, string exceptionUrl)
        {

            ExceptionLoggingDO.InsertJSExcepToDB(exceptionMessage, exceptionType, exceptionSource, exceptionUrl);

        }

    }
}
