﻿using AccountsExperts.Access_Layers.CommonDataaccess;
using AccountsExperts.Access_Layers.CommonDataAccess;
using AccountsExperts.DataAccesslayer;
using AccountsExperts.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AccountsExperts
{
    public partial class Login : System.Web.UI.Page
    {




        #region Constants
        ApplicationUser objApplicationUser = new ApplicationUser();
        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
        }



        protected void btnlogin_Click(object sender, EventArgs e)
        {


            SetuserProperties();
            bool status = LoginDO.ValidateUser(objApplicationUser);

            if (status == true)
            {
                SessionHandler.Current.LoginId = DataAccess.Encrypt(Convert.ToString(LoginDO.GetSessionId(objApplicationUser)), true);
                SessionHandler.Current.LoginName = objApplicationUser.User_Name;
                
            }
            if (status == false)
            {
                lblstatus.Text = "The username or password provided is incorrect.";
            }
            else
                Response.Redirect("DashBoard/DashBoard.aspx");  
        }

        #endregion

        #region Private Methods

        private void SetuserProperties()
        {
            objApplicationUser.User_Name = txtUserName.Text;
            objApplicationUser.Password = DataAccess.Encrypt(txtPassword.Text,true);
        }
        #endregion
    }
}