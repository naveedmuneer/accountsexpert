﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestValidation.aspx.cs" Inherits="AccountsExperts.TestValidation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  
    <meta charset="UTF-8">
<title>Example of Bootstrap 3 Tooltip Methods</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".show-tooltip").click(function () {
            $(".myTooltip").tooltip('show');
        });
        $(".hide-tooltip").click(function () {
            $(".myTooltip").tooltip('hide');
        });
        $(".toggle-tooltip").click(function () {
            $(".myTooltip").tooltip('toggle');
        });
        $(".destroy-tooltip").click(function () {
            $(".myTooltip").tooltip('destroy');
        });
    });
</script>
<style type="text/css">
	.bs-example{
    	margin: 60px;
    }
</style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="JavaScripts/CustomValidation.js"></script>
</head>
<body>
   
        <select id="city" MyTag="required" name="city" validationG="vaid" onchange="validated('vaid');" data-toggle="tooltip">
            <option value="">Select Your City</option>
            <option value="mumbai">Mumbai</option>
            <option value="pune">Pune</option>
            <option value="nashik">Nashik</option>
        </select>
    <input id="txtCheck" validationG="vaid" MyTag="required" onchange="validated('vaid');" data-toggle="tooltip"/>
        </br>
		<input type="button" value="Validate" id="validateButton" name="validateButton" onclick="validated('vaid');" />
    <input type="button" value="Validate" id="Button1" name="validateButton" onclick="validated('vaide');" />
</body>
</html>
