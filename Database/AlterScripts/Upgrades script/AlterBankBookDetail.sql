
IF EXISTS (SELECT data_type FROM Information_Schema.Columns WHERE Table_Name = 'BankBookDetail'
      AND Column_Name = 'Amount' AND data_type = 'decimal' )
BEGIN
Alter Table BankBookDetail
Alter Column Amount decimal(18,2)
END