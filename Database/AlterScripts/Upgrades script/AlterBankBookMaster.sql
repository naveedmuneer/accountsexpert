IF EXISTS (SELECT data_type FROM Information_Schema.Columns WHERE Table_Name = 'BankBookMaster'
      AND Column_Name = 'Remarks' AND data_type = 'text' )
BEGIN
ALTER TABLE [dbo].BankBookMaster
ALTER COLUMN Remarks VARCHAR(MAX)NOT NULL
END