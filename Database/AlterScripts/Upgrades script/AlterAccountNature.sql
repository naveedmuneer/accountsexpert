if not exists (select
                     column_name
               from
                     INFORMATION_SCHEMA.columns
               where
                     table_name  = 'AccountNature'
                     and column_name = 'Header1')
Alter table AccountNature add Header1 Varchar(100)

if not exists (select
                     column_name
               from
                     INFORMATION_SCHEMA.columns
               where
                     table_name  = 'AccountNature'
                     and column_name = 'Header2')
Alter table AccountNature add Header2 Varchar(100)



Alter table charts
Drop constraint fk_Charts_AccountNature

Delete From AccountNature

INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (1, N'1', N'<Undefined Account>', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (2, N'2', N'Cash', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'ASSETS', N'CURRENT ASSETS')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (3, N'3', N'Bank', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'ASSETS', N'CURRENT ASSETS')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (4, N'4', N'Inventory', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'ASSETS', N'CURRENT ASSETS')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (5, N'5', N'Accounts Receivable', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'ASSETS', N'CURRENT ASSETS')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (6, N'6', N'Current Assets', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'ASSETS', N'CURRENT ASSETS')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (7, N'7', N'Other Current Assets', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'ASSETS', N'CURRENT ASSETS')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (8, N'8', N'Fixed Assets', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'ASSETS', N'FIXED ASSETS')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (9, N'9', N'Accumulated Depreciation', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'ASSETS', N'FIXED ASSETS')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (10, N'10', N'Long Term Deposits', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'ASSETS', N'OTHER ASSETS')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (11, N'11', N'Accounts Payable', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'EQUITY', N'CURRENT LIABILITIES')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (12, N'12', N'Current Liabilities', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'EQUITY', N'CURRENT LIABILITIES')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (13, N'13', N'Long Term Liabilities', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'EQUITY', N'LONG TERM LIABILITIES')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (14, N'14', N'Capital', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'EQUITY', N'CAPITAL')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (15, N'15', N'Drawing', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'EQUITY', N'CAPITAL')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (16, N'16', N'Retained Earning', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'EQUITY', N'CAPITAL')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (17, N'17', N'Income', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'SALES', N'SALES')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (18, N'18', N'Cost of Sales', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'COST OF SALES', N'COST OF SALES')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (19, N'19', N'Expenses', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'EXPENSES', N'EXPENSES')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (20, N'20', N'Purchases', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'COST OF SALES', N'COST OF SALES')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (21, N'21', N'Purchases Return', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'COST OF SALES', N'COST OF SALES')
INSERT [dbo].[AccountNature] ([pk_AccountNatureID], [AccountNatureRefNo], [AccountNatureName], [Active], [Created_On], [Created_By], [Machine_Created], [Modified_On], [Modified_By], [Machine_Modified], [Header1], [Header2]) VALUES (23, N'23', N'Other Income', 1, CAST(0x0000A622000E2924 AS DateTime), N'Admim', NULL, NULL, NULL, NULL, N'OTHER INCOME', N'OTHER INCOME')

Alter table Charts
Add Constraint fk_Charts_AccountNature FOREIGN KEY([fk_AccountNatureID]) REFERENCES AccountNature ([pk_AccountNatureID])

