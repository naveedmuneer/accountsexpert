@echo off

rem ---------------------------------------------------------------------------------
rem Created by:  Danish Farooque Janjua
rem Created on:  12th April 2007
rem Description: Merge all files specified in the given output file.
rem ---------------------------------------------------------------------------------

if "%1"=="/?" goto HelpSec
if NOT "%1"=="-d" if NOT "%1"=="-D" if NOT "%1"=="-s" if NOT "%1"=="-S" goto ErrorSec
if "%1"=="-d" set incSubDr=0
if "%1"=="-D" set incSubDr=0
if "%1"=="-s" set incSubDr=1
if "%1"=="-S" set incSubDr=1
if NOT "%3"=="-i" if NOT "%3"=="-I" goto ErrorSec
if "%4"=="" goto ErrorSec
if NOT "%5"=="-o" if NOT "%5"=="-O" goto ErrorSec
if "%6"=="" goto ErrorSec
if NOT "%7"=="" if NOT "%7"=="-f" if NOT "%7"=="-F" goto ErrorSec
if "%7"=="-f" if "%8"=="" goto ErrorSec

rem copy list of all subfolders in FolderList.txt
if %incSubDr%==1 dir %2 /ad /b /s > FolderList.txt

rem create a temporary file to hold "set folder="
echo e 100 "set folder="> temp.txt
echo rcx>> temp.txt
echo b>> temp.txt
echo n setFold.txt>> temp.txt
echo w>> temp.txt
echo q>> temp.txt

rem debug < temp.txt > nul

del temp.txt > nul

echo 6th param = %6

echo %2\%4

rem Merge all files exist in specified directory
for %%i IN (%2\%4) DO type "%%i" %8 >> %6

if %incSubDr%==0 goto CleanUP

rem Loop through each sub directory
:LOOP
    echo In loop!
    dir FolderList.txt | find "FolderList.txt" | find " 0 " > nul
    if not errorlevel 1 goto CleanUP
    copy setFold.txt + FolderList.txt temp.txt > nul
    type temp.txt | find /v "set folder=" > FolderList.txt
    type temp.txt | find "set folder=" > Folder.bat
    call Folder.bat

    for %%i IN ("%folder%\%4") DO type "%%i" %8 >> %6

goto LOOP

:ErrorSec
echo The syntax of the command is incorrect.
echo.
echo      Merge ^<-[d,s]=directoryname^> 
echo               ^<-i=filename^> 
echo               ^<-o=[drive:][Path]filename^>
echo               [-f^<[drive:][Path]filename^>]
echo.
echo see "Merge /?" for details
echo.
goto end

:HelpSec
echo.
echo      Merge all files specified in the given output file.
echo      Merge ^<-[d,s]=directoryname^> 
echo               ^<-i=filename^> 
echo               ^<-o=[drive:][Path]filename^>
echo               [-f^<[drive:][Path]filename^>]
echo.
echo      -d      process files only in the direcory specified
echo                   e.g. -s="E:\ProjectName\Database" 
echo.
echo      -s      process files in directory (include sub directories) 
echo                   e.g. -s="E:\ProjectName\Database"
echo.
echo      [drive:][path]filename
echo           Specifies drive, directory, and/or file(s) to merge.
echo           Wildcards supported. e.g. *.sql (without quotation)
echo.
echo      -i      file/file list to be merged e.g. -i=*.sql (without quotation)
echo.
echo      -o      output file e.g. -o="E:\StoredProcedures.sql" (with quotation)
echo               do not create output file under the directory specified with -d/-s
echo               if you must need to create under same directory then
echo               do not choose same extension as specified with -i
echo               if output file already exists then scripts will be appended in it
echo.
echo      -f      footer file to append after each file merged
echo                   e.g. -f="E:\ProjectName\footer.txt" (with quotation)
echo.
echo      all values for switches are relative to the "Start in" directory.
echo.
goto end

:CleanUP
rem del all temporary files
if exist temp.txt del temp.txt > nul
if exist setFold.txt del setFold.txt > nul
if exist FolderList.txt del FolderList.txt > nul
if exist Folder.bat del Folder.bat > nul

:end
