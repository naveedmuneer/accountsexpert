USE [AccountsExperts]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetDateFormat_DDMMMYYYY]    Script Date: 6/26/2016 5:55:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetDateFormat_DDMMMYYYY]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetDateFormat_DDMMMYYYY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetDateFormat_DDMMMYYYY]    Script Date: 6/26/2016 5:55:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetDateFormat_DDMMMYYYY]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'Create function [dbo].[fn_GetDateFormat_DDMMMYYYY](@datetime DateTime)
Returns nvarchar(15)
As
Begin
 return (SELECT REPLACE(CONVERT(VARCHAR(11), @datetime, 106), '' '', ''-''));
End

--select dbo.fn_GetDateFormat_DDMMMYYYY( GetDate())
' 
END

GO
