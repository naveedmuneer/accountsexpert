
Create  FUNCTION [dbo].[fn_ReverseChartOfAccountName] (
@ip VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
BEGIN
DECLARE @op VARCHAR(MAX)
SET @op = null
DECLARE @Lenght INT
WHILE LEN(@ip) > 0
BEGIN
IF CHARINDEX('::', @ip) > 0
BEGIN
if @op is null
SET @op = SUBSTRING(@ip,0,CHARINDEX('::', @ip))
else
SET @op = SUBSTRING(@ip,0,CHARINDEX('::', @ip)) + '::' + @op

SET @ip = LTRIM(RTRIM(SUBSTRING(@ip,CHARINDEX('::', @ip) + 2,LEN(@ip))))
END
ELSE
BEGIN
SET @op = @ip + '::' + @op
SET @ip = ''
END
END
return @op
END