
Create FUNCTION [dbo].[getChartAccountParent](@childId INT)
RETURNS VARCHAR(MAX)
AS
BEGIN
    DECLARE @parentName VARCHAR(MAX) = NULL
 DECLARE @parent INT = NULL
    DECLARE @parentKey INT = null
    SET @parentName = (SELECT cast(ChartName as varchar(MAX))  FROM charts WHERE pk_ChartID = @childId)
	SET @parentKey = (SELECT GroupID  FROM charts WHERE pk_ChartID = @childId)

    WHILE(@parentKey IS NOT NULL)
    begin
		if @parent is null
			SET @parent = @parentKey;
		else
			SET @parent = (SELECT cast(GroupID as varchar(MAX))FROM charts WHERE pk_ChartID = @parent)
	
		SET @parentName = @parentName +'::'+(SELECT cast(ChartName as varchar(MAX)) FROM charts WHERE pk_ChartID = @parent)
		SET @parentKey = (SELECT GroupID FROM charts WHERE pk_ChartID = @parent)
    END
   return @parentName
END