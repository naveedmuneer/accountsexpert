If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'Split'
       )
 Drop function [Split]
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : Split
Author      : Moiz Saleem
Create date : 11-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
-- Select * from dbo.Split ('55|65|85|105','|')
Create FUNCTION [dbo].[Split]
(
    @ListOfValues varchar(max),
    @ValueSeparator varchar(10)
)
RETURNS @ListOfValuesInRows TABLE
(
    Value varchar(max)
)
AS
BEGIN

    IF Len(@ListOfValues) = 0
        RETURN

    if @ValueSeparator <> ' '
    Begin
        WHILE CHARINDEX(@ValueSeparator, @ListOfValues) > 0
        BEGIN

            INSERT INTO @ListOfValuesInRows
            SELECT LTRIM(RTRIM(SUBSTRING(@ListOfValues, 1, CHARINDEX(@ValueSeparator, @ListOfValues)-1)))

            SET @ListOfValues = SubString(@ListOfValues, CharIndex(@ValueSeparator, @ListOfValues)+Len(@ValueSeparator), Len(@ListOfValues))

        END

        INSERT INTO @ListOfValuesInRows
        SELECT LTRIM(RTRIM(@ListOfValues))
    End
    Else
    BEGIN
        DECLARE @xml XML;
        SET @xml = N'<t>' + REPLACE(@ListOfValues, @ValueSeparator, '</t><t>') + '</t>';
        INSERT INTO @ListOfValuesInRows (Value)
        SELECT LTRIM(RTRIM(r.value( '.', 'varchar(MAX)' ))) AS item
        FROM @xml.nodes( '/t' ) AS records( r )

    END

RETURN

END