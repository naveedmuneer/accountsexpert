CREATE TABLE dbo.Enumeration
( 
  	[pkEnumerationId]  [int] NOT NULL,
	[Name] varchar(50) NOT NULL,
	[Description] varchar(100) NOT NULL,
	PRIMARY KEY(pkEnumerationId),
);


