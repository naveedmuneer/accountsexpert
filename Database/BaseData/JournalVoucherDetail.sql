CREATE TABLE dbo.JournalVoucherDetail
(
   [pk_JournalVoucherDetailID] [int] IDENTITY(1,1)  NOT NULL,
   [fk_JournalVoucherID] [int] NOT NULL,
   [fk_ChartID] [int] NOT NULL,
   [Debit] Decimal(18,3)NULL,
   [Credit] Decimal(18,3)NULL,
   [Description] Varchar(MAX) NULL,
   [SeqNo] [int]  NULL,
   [fk_DepartmentId] [int] NULL,
   [Created_On] [datetime] NOT NULL,
   --[Created_By] [varchar](50) NOT NULL,
   [Machine_Created] [varchar](90) NULL,
   [Modified_On] [datetime] NULL,
   [Modified_By] [varchar](50) NULL,
   [Machine_Modified] [varchar](90) NULL
   
   PRIMARY KEY(
	[pk_JournalVoucherDetailID]
)
Constraint FK_JournalVoucherDetail_GeneralVoucherMaster FOREIGN KEY([fk_JournalVoucherID]) REFERENCES dbo.JournalVoucherMaster([pk_JournalVoucherID]),
Constraint FK_JournalVoucherDetail_Charts FOREIGN KEY([fk_ChartID]) REFERENCES dbo.Charts(pk_ChartID),
Constraint FK_JournalVoucherDetail_Department FOREIGN KEY([fk_DepartmentId]) REFERENCES dbo.Department(pkDepartment_ID),

)


