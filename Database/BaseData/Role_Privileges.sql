CREATE TABLE dbo.Role_Privileges
( 
    [pkRolePrevilagesID] [int] IDENTITY(1,1) NOT NULL,
	[fkRole_ID] [int] NOT NULL,
	[fkForm_ID] [int] NOT NULL,
	[enmActionId] int NOT NULL, 
	[Created_On] [datetime] NULL,
	[Created_By] [varchar](50) NULL,
	[Machine_Created] [varchar](50) NULL,
	PRIMARY KEY(pkRolePrevilagesID),
	CONSTRAINT FK_Role_Privileges_Role FOREIGN KEY([fkRole_ID])REFERENCES Role(pkRole_ID),
	CONSTRAINT FK_Role_Privileges_Form FOREIGN KEY([fkForm_ID])REFERENCES Application_Form(pk_Form_ID),
);


