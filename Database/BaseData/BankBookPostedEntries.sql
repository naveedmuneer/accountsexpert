

CREATE TABLE dbo.BankBookPostedEntries
(
	[pk_BankBookPostedEntriesId] [int] Identity(1,1)  NOT NULL,
	[Inputtype] varchar(10)  NOT NULL, 
	[BankBookRefNo] [varchar](50) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[ChartID] [int]  NOT NULL,
	[ChartName] varchar(100) NOT NULL,
	[Debit] Decimal(18,2) NULL,
	[Credit] Decimal(18,2) NULL

	
PRIMARY KEY(
	[pk_BankBookPostedEntriesId]
)

--Constraint FK_BankBookMaster_ApplicationUser FOREIGN KEY([fkUserId]) REFERENCES dbo.ApplicationUser([pkUser_ID]),
--Constraint fk_BankBookMaster_Charts FOREIGN KEY([fk_ChartID]) REFERENCES dbo.Charts ([pk_ChartID])
)




