CREATE TABLE dbo.Company
( 
   	[pkCompany_ID] [int] IDENTITY(1,1) NOT NULL,
	[Company_Name] [varchar](80) NOT NULL,
	[Description] [varchar](80) NULL,
	[Title] [varchar](50) NULL,
	[Business] [varchar](50) NULL,
	[Establishment_Date] [datetime] NULL,
	[Expiry_Date] [datetime] NULL,
	[Is_Active] [bit] default((0)),
	[Created_On] [datetime] NOT NULL,
	[Created_By] [varchar](50) NOT NULL,
	[Machine_Created] [varchar](90) NOT NULL,
	[Modified_On] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Machine_Modified] [varchar](90) NULL,
	PRIMARY KEY(pkCompany_ID),
);


	
