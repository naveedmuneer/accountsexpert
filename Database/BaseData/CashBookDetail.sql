

CREATE TABLE [CashBookDetail](
	pk_CashBookDetailID [int] Identity(1,1) NOT NULL,
	[fk_CashBookID] [int] NOT NULL,
	[fk_ChartID] [int] NOT NULL,
	[Description] [varchar](250) NULL,
	[Amount] Decimal(18,5) NULL
	
PRIMARY KEY(
	pk_CashBookDetailID
)
Constraint FK_CashBookDetail_CashBookMaster FOREIGN KEY([fk_CashBookID]) REFERENCES dbo.CashBookMaster ([pk_CashBookID]),
Constraint FK_CashBookDetail_Charts FOREIGN KEY([fk_ChartID]) REFERENCES dbo.Charts ([pk_ChartID])
) 



