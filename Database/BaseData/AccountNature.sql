
CREATE TABLE dbo.AccountNature
(
	[pk_AccountNatureID] INT NOT NULL,
	[AccountNatureRefNo] Varchar(50) NOT NULL,
	[AccountNatureName] Varchar(250) NOT NULL,
	[Active] Bit NOT NULL Default(0),
	[Header1] Varchar(50),
	[Header2] Varchar(50),
	[Created_On] [datetime] NOT NULL,
	[Created_By] [varchar](50) NOT NULL,
	[Machine_Created] [varchar](90) NULL,
	[Modified_On] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Machine_Modified] [varchar](90) NULL
	
PRIMARY KEY(
	[pk_AccountNatureID]
)
)

