CREATE TABLE dbo.ExceptionLogging
(
	[pk_ExceptionLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[ExceptionMsg] [nvarchar](max)  NULL,
	[ExceptionType] [varchar](100) NULL,
	[ExceptionSource] [nvarchar](max) NULL,
	[ExceptionURL] [varchar](100) NULL,
	[Logdate] [datetime] NULL,
	
PRIMARY KEY(
	[pk_ExceptionLogID]
)
)





