CREATE TABLE dbo.Role
( 
  	[pkRole_ID] [int] NOT NULL,
	[Role_Name] [varchar](50) NOT NULL,
	[Is_Active] [bit] default((0)),
	[Created_On] [datetime] NOT NULL,
	[Created_By] [varchar](50) NOT NULL,
	[Machine_Created] [varchar](90)  NULL,
	[Modified_On] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Machine_Modified] [varchar](90) NULL,
	PRIMARY KEY(pkRole_ID),
);


