
CREATE TABLE [BankBookDetail](
    pk_BankBookDetailID  [int] Identity(1,1) NOT NULL,
	[fk_BankBookId] [int] NOT NULL,
	[fk_ChartID] [int] NOT NULL,
	[Description] [varchar](250) NULL,
	[Amount] Decimal(18,2) NULL
	
PRIMARY KEY(
	pk_BankBookDetailID
)
Constraint fk_BankBookDetail_BankBookMaster FOREIGN KEY([fk_BankBookId]) REFERENCES dbo.BankBookMaster([pk_BankBookId]),
Constraint fk_BankBookDetail_Charts FOREIGN KEY([fk_ChartID]) REFERENCES dbo.Charts ([pk_ChartID])
) 

