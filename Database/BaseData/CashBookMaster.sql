

CREATE TABLE dbo.CashBookMaster
(
	[pk_CashBookID] [int] NOT NULL,
	[InputType] [varchar](5) NULL,
	[fkCompanyID] [int] NULL,
	[CashBookRefNo] [varchar](10) NULL,
	[TransactionDate] [datetime] NULL,
	[fk_ChartID] [int] NOT NULL,
	[ChequeNo] [varchar](25) NULL,
	[ChequeDate] [datetime] NULL,
	[enmChequestatus] int NULL,
	[Remarks] [varchar](MAX) NULL,
	[Posted] [tinyint] NULL,
	[fkUserId] [int] NULL,
	[Active] Bit NOT NULL Default(0),
	[Created_On] [datetime] NOT NULL,
	[Created_By] [varchar](50) NOT NULL,
	[Machine_Created] [varchar](90) NULL,
	[Modified_On] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Machine_Modified] [varchar](90) NULL
	
PRIMARY KEY(
	[pk_CashBookID]
)
Constraint FK_CashBookMaster_ApplicationUser FOREIGN KEY([fkUserId]) REFERENCES dbo.ApplicationUser([pkUser_ID]),
Constraint FK_CashBookMaster_Charts FOREIGN KEY([fk_ChartID]) REFERENCES dbo.Charts ([pk_ChartID])
)
