CREATE TABLE dbo.EnumerationValue
( 
  	[pkEnumerationValueId]  [int] NOT NULL,
    [fkEnumerationId] [int] NOT NULL,
	[Value] varchar(50) NOT NULL,
	[Description] varchar(100) NOT NULL,
	PRIMARY KEY(pkEnumerationValueId),
	CONSTRAINT FK_EnumerationValue_Enumeration FOREIGN KEY([fkEnumerationId])REFERENCES Enumeration(pkEnumerationId),
);

