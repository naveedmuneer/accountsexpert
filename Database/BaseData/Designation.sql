CREATE TABLE dbo.Designation
( 
    [pkDesignation_ID] [int] IDENTITY(1,1) NOT NULL,
	[Designation_Name] [varchar](60) NULL,
	[Is_Active] [bit] default((0)),
	[Created_On] [datetime] NOT NULL,
	[Created_By] [varchar](50)NOT NULL,
	[Machine_Created] [varchar](90) NULL,
	[Modified_On] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Machine_Modified] [varchar](90) NULL,
	PRIMARY KEY(pkDesignation_ID),
);
