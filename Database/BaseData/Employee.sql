CREATE TABLE dbo.Employee
( 
    [pk_Employee_ID] [int] IDENTITY(1,1) NOT NULL,
	[Employee_Name] [varchar](50) NOT NULL,
	[NIC_No] [varchar](20) NULL,
	[fkCompany_ID] [int] NULL,
	[fkDepartment_ID] [int] NULL,
	[fkDesignation_ID] [int] NULL,
	[Email] [varchar](50) NULL,
	[Contact_No] [varchar](50) NULL,
	[Secondary_Contact_No] [varchar](50) NULL,
	[Address] [varchar](90) NULL,
	[Permenant_Address] [varchar](90) NULL,
	[Is_Active] [bit] default((0)),
	[Created_On] [datetime] NOT NULL,
	[Created_By] [varchar](50) NOT NULL,
	[Machine_Created] [varchar](50) NULL,
	[Modified_On] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Machine_Modified] [varchar](50) NULL,
	PRIMARY KEY(pk_Employee_ID),
	
	CONSTRAINT FK_Employee_Company FOREIGN KEY([fkCompany_ID])REFERENCES company(pkCompany_ID),
	CONSTRAINT FK_Employee_Department FOREIGN KEY([fkDepartment_ID])REFERENCES Department(pkDepartment_ID),
	CONSTRAINT FK_Employee_Designation FOREIGN KEY([fkDesignation_ID])REFERENCES Designation(pkDesignation_ID),
);


