CREATE TABLE dbo.JournalVoucherMaster
(
    [pk_JournalVoucherID] [int] NOT NULL,
    [JournalVoucherRefNo] [varchar](10) NULL,
    [TransactionDate] [datetime] NULL,
    [fkCompanyID] [int] NULL,
    [Remarks] Varchar(MAX) NULL,
    [IsPosted] [tinyint] NULL,
    [fkUserId] [int] NULL,
	[Created_On] [datetime] NOT NULL,
	[Created_By] [varchar](50) NOT NULL,
	[Machine_Created] [varchar](90) NULL,
	[Modified_On] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Machine_Modified] [varchar](90) NULL
	
    

   PRIMARY KEY(
	[pk_JournalVoucherID]
)

	CONSTRAINT FK_JournalVoucherMaster_Company FOREIGN KEY([fkCompanyID])REFERENCES Company(pkCompany_ID),
	CONSTRAINT FK_JournalVoucherMaster_ApplicationUser FOREIGN KEY([fkUserId])REFERENCES ApplicationUser([pkUser_ID]),
);


