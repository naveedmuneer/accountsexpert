CREATE TABLE dbo.UserRoleMapping
( 
  	[pkUserRoleMap_ID]  [int] IDENTITY(1,1) NOT NULL,
	[fkRole_ID] [int] NOT NULL,
	[fkUser_ID] [int] NOT NULL,
	 PRIMARY KEY(pkUserRoleMap_ID),
	 CONSTRAINT FK_UserRoleMapping_Role FOREIGN KEY([fkRole_ID])REFERENCES Role(pkRole_ID),
	 CONSTRAINT FK_UserRoleMapping_User FOREIGN KEY([fkUser_ID])REFERENCES ApplicationUser(pkUser_ID),
	 
);

