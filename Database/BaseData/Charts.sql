

CREATE TABLE dbo.Charts
(
	[pk_ChartID] INT NOT NULL,
	[ChartRefNo] Varchar(50) NOT NULL,
	[ChartName] Varchar(250) NOT NULL,
	Description Varchar(MAX) NULL,
	GroupID Varchar(50) NULL,
	fk_AccountNatureID INT NOT NULL,
	AccountType Varchar(50) NOT NULL,
	Note Varchar(250) NULL,
	Address Varchar(500) NULL,
	Phone Varchar(50) NULL,
	Fax Varchar(50) NULL,
	Email Varchar(50) NULL,
	NTNNO Varchar(50) NULL,
	GSTNO Varchar(50) NULL,
	[Active] Bit NOT NULL Default(0),
	[Created_On] [datetime] NOT NULL,
	[Created_By] [varchar](50) NOT NULL,
	[Machine_Created] [varchar](90) NULL,
	[Modified_On] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Machine_Modified] [varchar](90) NULL
	
PRIMARY KEY(
	[pk_ChartID]
)
Constraint fk_Charts_AccountNature FOREIGN KEY([fk_AccountNatureID]) REFERENCES AccountNature ([pk_AccountNatureID])
)
