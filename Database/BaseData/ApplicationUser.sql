CREATE TABLE dbo.ApplicationUser
( 
  	[pkUser_ID]  [int] IDENTITY(1,1) NOT NULL,
  	[User_Name] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[fkEmployee_ID] [int] NOT NULL,
	[Is_Active] [bit]Default((0)),
	[Created_On] [datetime] NOT NULL,
	[Created_By] [varchar](50) NOT NULL,
	[Machine_Created] [varchar](50)NULL,
	[Modified_On] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Machine_Modified] [varchar](50) NULL,
	PRIMARY KEY(pkUser_ID),
	CONSTRAINT FK_ApplicationUser_Employee FOREIGN KEY([fkEmployee_ID])REFERENCES Employee(pk_Employee_ID),
);


