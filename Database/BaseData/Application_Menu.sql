CREATE TABLE dbo.Application_Menu
( 
    [pk_MenuID] [int] NOT NULL,
	[NodeName] [varchar](50) NOT NULL,
	[NodeUrl] [varchar](50) NOT NULL,
	[ParentNode] [int] NOT NULL,
	[fk_Form_ID] [int] NULL,
	[Seq] [int] NULL,
	[tab_Seq] [int] NULL,
	[Is_Active] [bit] NOT NULL Default((0)),
	[Created_By] [varchar](35) NULL,
	[Created_On] [datetime] NULL,

    PRIMARY KEY(pk_MenuID),
	CONSTRAINT FK_Application_Menu_Application_Form FOREIGN KEY([fk_Form_ID])REFERENCES Application_Form(pk_Form_ID),
);


