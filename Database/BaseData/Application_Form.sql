CREATE TABLE dbo.Application_Form
(
    [pk_Form_ID] INT NOT NULL,
	[Form_Name] [varchar](50) NOT NULL,
    [Form_Description] [varchar](50) NOT NULL,
    [fk_Module_ID] [int] NOT NULL,
	[Is_Active] [bit] NOT NULL Default((0)),
	[Created_On] [datetime] NOT NULL,
	[Created_By] [varchar](50) NOT NULL,
	[Machine_Created] [varchar](90) NULL,
	[Modified_On] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Machine_Modified] [varchar](90) NULL,

    PRIMARY KEY(pk_Form_ID),
	CONSTRAINT FK_Application_Form_Application_Module FOREIGN KEY([fk_Module_ID])REFERENCES Application_Module(pk_Module_ID),
);


