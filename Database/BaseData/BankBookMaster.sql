

CREATE TABLE dbo.BankBookMaster
(
	[pk_BankBookId] [int]  NOT NULL,
	[InputType] [varchar](5) NULL,
	[fkCompanyID] [int] NULL,
	[BankBookRefNo] [varchar](50) NULL,
	[TransactionDate] [datetime] NULL,
	[fk_ChartID] [int]  NOT NULL,
	[ChequeNo] [varchar](25) NULL,
	[ChequeDate] [datetime] NULL,
	[enmChequestatus] int NULL,
	[Remarks] Varchar(MAX) NULL,
	[Posted] [tinyint] NULL,
	[fkUserId] [int] NULL,
	[Active] Bit NOT NULL Default(0),
	[Created_On] [datetime] NOT NULL,
	[Created_By] [varchar](50) NOT NULL,
	[Machine_Created] [varchar](90) NULL,
	[Modified_On] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Machine_Modified] [varchar](90) NULL
	
PRIMARY KEY(
	[pk_BankBookId]
)

Constraint FK_BankBookMaster_ApplicationUser FOREIGN KEY([fkUserId]) REFERENCES dbo.ApplicationUser([pkUser_ID]),
Constraint fk_BankBookMaster_Charts FOREIGN KEY([fk_ChartID]) REFERENCES dbo.Charts ([pk_ChartID])
)




