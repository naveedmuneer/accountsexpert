CREATE TABLE dbo.Application_Module
( 
  	[pk_Module_ID] [int] NOT NULL,
	[Module_Name] [varchar](50) NOT NULL,
	[Is_Active] [bit] NOT NULL Default((0)),
	[Created_On] [datetime] NOT NULL,
	[Created_By] [varchar](50) NOT NULL,
	[Machine_Created] [varchar](90) NULL,
	[Modified_On] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Machine_Modified] [varchar](90) NULL,

    PRIMARY KEY(pk_Module_ID),
);
