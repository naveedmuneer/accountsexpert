	
	If Exists (Select *
	   From   Information_Schema.Routines
	   Where  Specific_Schema = N'dbo'
	   And	   Specific_Name = N'usp_UpdateBankBookData')
	   Drop Procedure dbo.usp_UpdateBankBookData

	GO
/********************************************************************************************
Module       : usp_UpdateBankBookData
Created By   : Abdul Wahab
CreatedOn   : July 29, 2016
Description  : 

*******************************Modification History*******************************************
*********************************************************************************************/
CREATE PROC [dbo].usp_UpdateBankBookData

(	@xmlBankBookMaster xml,
	@xmlBanakBookDetail xml
)
	AS
	BEGIN
	-- DECLARE @status BIT;
	--SET NOCOUNT ON;
	  --BEGIN
    UPDATE BankBookMaster 
	SET 
	InputType = i.InputType, 
	fkCompanyID = i.CompanyId,
	TransactionDate = i.TrasactionDate,
	fk_ChartID = i.FkchartId, 
	ChequeNo = i.ChequeNo,
	ChequeDate = i.ChequeDate, 
	Remarks = i.Remarks,
	fkUserId = i.FkuserId, 
	Active = i.Active,
	Modified_On=i.ModifiedOn,
	Modified_By=i.ModifiedBy

	FROM
	 (
  		SELECT 
		XmlDataTable.BankBook.value('@InputType[1]','varchar(5)') as InputType,
		XmlDataTable.BankBook.value('@CompanyID[1]','int') as CompanyId,
		XmlDataTable.BankBook.value('@TransactionDate','Datetime')as TrasactionDate ,
		XmlDataTable.BankBook.value('@FkChartID[1]','int')as FkchartId,
		XmlDataTable.BankBook.value('@ChequeNo[1]','varchar(100)') as ChequeNo,
		XmlDataTable.BankBook.value('@ChequeDate[1]','Datetime')as ChequeDate,
		XmlDataTable.BankBook.value('@Remarks[1]','varchar(max)')as Remarks,
		XmlDataTable.BankBook.value('@FkUserID[1]','int')as FkuserId,
		1 as Active,
		GetDate() as ModifiedOn,
		XmlDataTable.BankBook.value('@Modified_By[1]','varchar(max)')as ModifiedBy 
		
FROM @xmlBankBookMaster.nodes('.//BankBook') as XmlDataTable(BankBook) 
	) as i

WHERE pk_BankBookID = (SELECT XmlDataTable.BankBook.value('@pk_BankBookID[1]','int') 
from @xmlBankBookMaster.nodes('.//BankBook') as XmlDataTable(BankBook))
  



Delete from BankBookDetail 
WHERE fk_BankBookId=(SELECT XmlDataTable.BankBook.value('@pk_BankBookID[1]','int') 
from @xmlBankBookMaster.nodes('.//BankBook') as XmlDataTable(BankBook)) 

     INSERT INTO BankBookDetail 
	 (	
	    fk_BankBookId,
		fk_ChartID,	
		[Description],
		Amount
	)

	Select	
			(
			SELECT
			XmlDataTable.BankBook.value('@pk_BankBookID[1]','int') from @xmlBankBookMaster.nodes('.//BankBook') as XmlDataTable(BankBook)),
			XmlUpdateDataTable.Detail.value('@Fk_ChartID[1]','int'),
			XmlUpdateDataTable.Detail.value('@Description[1]','varchar(max)'),
			XmlUpdateDataTable.Detail.value('@Amount[1]','Decimal(18,5)')

	FROM @xmlBanakBookDetail.nodes('//BankBookDetail') as XmlUpdateDataTable(Detail)
 
END 
--END

GO
