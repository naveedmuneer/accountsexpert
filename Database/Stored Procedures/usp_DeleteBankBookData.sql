If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteBankBookData'
       )
 Drop Procedure usp_DeleteBankBookData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteBankBookData
Author      : Abdul Wahab
Create date : 08-09-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteBankBookData
(
	@BankBookID		INT
)
AS
BEGIN
DECLARE @status BIT;

	IF EXISTS(SELECT 1 FROM BankBookDetail WHERE fk_BankBookId=@BankBookID)

	BEGIN
 	
	Delete FROM BankBookDetail WHERE fk_BankBookId=@BankBookID
    
	Delete FROM BankBookMaster WHERE pk_BankBookId=@BankBookID

	SET @status = 1;
    SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END

	GO
