
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetAllAccountsBook'
       )
 Drop Procedure usp_GetAllAccountsBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetAllAccountsBook
Author      : Muhammad Ammar
Create date : 24-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetAllAccountsBook

AS
BEGIN
 SET NOCOUNT ON;

 Select * from AccountsBook WHERE Is_Active=1
End
GO

