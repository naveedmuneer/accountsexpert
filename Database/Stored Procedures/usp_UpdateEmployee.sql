If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateEmployee'
       )
 Drop Procedure usp_UpdateEmployee
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_UpdateEmployee
Author      : Muhammad Waleed
Create date : 3 July 2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_UpdateEmployee
(   
    @EmployeeID INT,
	@EmployeeName Varchar(50),
	@NIC Varchar(50),
	@CompanyID INT,
	@DepartmentID INT,
	@DesignationID INT,
	@Email Varchar(50),
	@ContactNo Varchar(50) ,
	@SecondaryConatctNo Varchar(50),
	@Address varchar(100),
	@PermenantAddress varchar(100),
	@Active	BIT,
	@ModifiedOn	DateTime,
	@ModifiedBy	Varchar(50)
	

)
AS
BEGIN
SET NOCOUNT ON;
Update Employee 
SET
Employee_Name = @EmployeeName,
NIC_No = @NIC,
fkCompany_ID = @CompanyID,
fkDepartment_ID = @DepartmentID,
fkDesignation_ID = @DesignationID,
Email = @Email,
Contact_No = @ContactNo,
Secondary_Contact_No = @SecondaryConatctNo,
Address = @Address,
Permenant_Address = @PermenantAddress,
Is_Active = @Active,
Modified_On = @ModifiedOn,
Modified_By = @ModifiedBy
WHERE @EmployeeID = pk_Employee_ID


END
GO
