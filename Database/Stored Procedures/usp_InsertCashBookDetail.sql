
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertCashBookDetail'
       )
 Drop Procedure usp_InsertCashBookDetail
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_InsertCashBookDetail
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertCashBookDetail
(
	@CashBookDetailID INT,
	@fkCashBookID INT,
	@fkChartID INT,
	@Description varchar(max),
	@Amount Decimal(18,5)
	

)
AS
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF NOT EXISTS(SELECT 1 FROM CashBookDetail WHERE pk_CashBookDetailID=@CashBookDetailID)
	BEGIN

 INSERT INTO CashBookDetail 
 (
	pk_CashBookDetailID,
	fk_CashBookID,
	fk_ChartID,
	[Description],
	Amount
)
values
(
	@CashBookDetailID,
	@fkCashBookID,
	@fkChartID,
	@Description,
	@Amount
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;

GO



GO

 