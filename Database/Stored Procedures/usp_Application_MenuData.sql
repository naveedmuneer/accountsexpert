
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_Application_MenuData'
       )
	Drop Procedure usp_Application_MenuData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_Application_MenuData
Author      : Naveed Muneer		
Create date	: 5-13-2016
Description : Get MenuData
**********************************************************************************
Who				Date			Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_Application_MenuData
AS
BEGIN
SET NOCOUNT ON;
SELECT *
FROM Application_Menu  
End
GO

