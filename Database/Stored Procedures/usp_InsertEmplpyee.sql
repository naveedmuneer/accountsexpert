
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertEmplpyee'
       )
 Drop Procedure usp_InsertEmplpyee
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_InsertEmplpyee
Author      : Muhammad Waleed
Create date : 2 July 2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertEmplpyee
(
	
	@EmployeeName Varchar(50),
	@NIC Varchar(50),
	@CompanyID INT,
	@DepartmentID INT,
	@DesignationID INT,
	@Email Varchar(50),
	@ContactNo Varchar(50) ,
	@SecondaryConatctNo Varchar(50),
	@Address varchar(100),
	@PermenantAddress varchar(100),
	@Active	BIT,
	@CreatedOn	dateTime,
	@CreatedBy	Varchar(50)
)
AS
Begin
	SET NOCOUNT ON;

	

 INSERT INTO Employee 
 (
	Employee_Name,
	NIC_No,
	fkCompany_ID,
	fkDepartment_ID,
	fkDesignation_ID,
	Email,
	Contact_No,
	Secondary_Contact_No,
	Address,
	Permenant_Address,
	Is_Active,
	Created_On,
	Created_By
)
values
(
	@EmployeeName,
	@NIC, 
	@CompanyID ,
	@DepartmentID, 
	@DesignationID, 
	@Email ,
	@ContactNo,
	@SecondaryConatctNo, 
	@Address ,
	@PermenantAddress, 
	@Active,		
	@CreatedOn,	
	@CreatedBy	
)
End

GO





