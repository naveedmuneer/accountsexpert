If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetJournalVoucherMaxID'
       )
 Drop Procedure usp_GetJournalVoucherMaxID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetJournalVoucherMaxID
Author      : Naveed Muneer
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetJournalVoucherMaxID

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

Select ISNULL(MAX(pk_JournalVoucherID),0) from JournalVoucherMaster

END

GO

