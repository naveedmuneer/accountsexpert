If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteChartOfAccount'
       )
 Drop Procedure usp_DeleteChartOfAccount
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteChartOfAccount
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteChartOfAccount
(
	@ChartID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM Charts WHERE pk_ChartID=@ChartID And (Select Count(*) from Charts Where GroupID=@ChartID) = 0)

	BEGIN
 	
	Delete FROM Charts WHERE pk_ChartID=@ChartID And (Select Count(*) from Charts Where GroupID=@ChartID) = 0

	SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;	
END

GO

