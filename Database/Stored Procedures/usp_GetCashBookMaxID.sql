If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCashBookMaxID'
       )
 Drop Procedure usp_GetCashBookMaxID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCashBookMaxID
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetCashBookMaxID

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

Select ISNULL(MAX(pk_CashBookID),0) from CashBookMaster

END

GO

