
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteCompany'
       )
 Drop Procedure usp_DeleteCompany
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     usp_DeleteCompany
Author      : Abdul wahab
Create date : 14-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/


Create PROCEDURE [dbo].[usp_DeleteCompany]
(
	@CompanyID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT pkCompany_ID FROM Company WHERE pkCompany_ID=@CompanyID)

	BEGIN
 	
	Delete FROM Company WHERE pkCompany_ID=@CompanyID

	SET @status = 1;
    SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END


GO
