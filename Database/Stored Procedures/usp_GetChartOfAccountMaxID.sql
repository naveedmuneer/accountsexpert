
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetChartOfAccountMaxID'
       )
 Drop Procedure usp_GetChartOfAccountMaxID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetChartOfAccountMaxID
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetChartOfAccountMaxID

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

Select MAX(ISNULL(pk_ChartID,0)) from Charts

END

GO

