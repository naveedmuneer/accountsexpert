
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetSessionId'
       )
	Drop Procedure usp_GetSessionId
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_GetSessionId
Author      : Naveed Muneer
Create date	: 21/6/2016
Description : Get Session Id
**********************************************************************************
Who				Date			Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetSessionId
(
@strName as varchar(20)
)
AS
BEGIN

SET NOCOUNT ON;
Select pkUser_ID from ApplicationUser
where User_Name=@strName
end
GO




