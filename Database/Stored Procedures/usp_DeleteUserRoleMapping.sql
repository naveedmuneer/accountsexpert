
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteUserRoleMapping'
       )
 Drop Procedure [usp_DeleteUserRoleMapping]
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_DeleteUserRoleMapping
Author      : Moiz Saleem
Create date : 11-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create  PROCEDURE [dbo].[usp_DeleteUserRoleMapping]
(
	@fkUser_ID	INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT fkUser_ID FROM UserRoleMapping WHERE fkUser_ID=@fkUser_ID)

	BEGIN
 	
	Delete FROM  UserRoleMapping where fkUser_ID=@fkUser_ID

	SET @status = 1;
   -- SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END
    GO

