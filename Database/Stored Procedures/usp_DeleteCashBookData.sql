If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteCashBookData'
       )
 Drop Procedure usp_DeleteCashBookData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteCashBookData
Author      : Waleed
Create date : 08-09-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteCashBookData
(
	@CashBookID	 INT
)
AS
BEGIN
DECLARE @status BIT;

	IF EXISTS(SELECT 1 FROM CashBookDetail WHERE fk_CashBookId=@CashBookID)

	BEGIN
 	
	Delete FROM CashBookDetail WHERE fk_CashBookId=@CashBookID
    
	Delete FROM CashBookMaster WHERE pk_CashBookId=@CashBookID

	SET @status = 1;
    SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END

	GO
