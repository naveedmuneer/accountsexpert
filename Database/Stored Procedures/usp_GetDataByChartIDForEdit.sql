If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDataByChartIDForEdit'
       )
 Drop Procedure usp_GetDataByChartIDForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetDataByChartIDForEdit
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDataByChartIDForEdit
(
	@ChartID INT 
)
AS
BEGIN
 SET NOCOUNT ON;

 Select * From Charts WHERE pk_ChartID=@ChartID
 
End
GO

--Select * From charts
--Select * from accountNature