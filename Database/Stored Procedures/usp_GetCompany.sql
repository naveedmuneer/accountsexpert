
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCompany'
       )
 Drop Procedure usp_GetCompany
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     [[usp_GetCompany]]
Author      : abdul wahab
Create date : 14-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/

Create PROCEDURE [dbo].[usp_GetCompany]
(
    @PageIndex INT = 1,
	@PageSize INT = 10,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT ROW_NUMBER() OVER
      (
      ORDER BY pkCompany_ID ASC
      )
      AS RowNumber,
	    pkCompany_ID 
		,[Company_Name]
		,[Description]
		,[Title],
		[Business],
		 dbo.fn_GetDateFormat_DDMMMYYYY(Establishment_Date) as Establishment_Date,
		 dbo.fn_GetDateFormat_DDMMMYYYY(Expiry_Date) as Expiry_Date, 
		[Is_Active] , 
		
		dbo.fn_GetDateFormat_DDMMMYYYY([Created_On]) as Created_On ,[Created_By]
	  into #Results
	  from Company 
	  
     

      SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1

	  
     
      DROP TABLE #Results				
End
GO
