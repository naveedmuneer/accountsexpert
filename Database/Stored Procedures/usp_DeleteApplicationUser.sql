If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteApplicationUser'
       )
 Drop Procedure usp_DeleteApplicationUser
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_DeleteApplicationUser
Author      : Muneem Ahmed
Create date : 27-06-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteApplicationUser
(
	@ApplicationUserID	INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM ApplicationUser where pkUser_ID=@ApplicationUserID)

	BEGIN
 	
	Delete FROM ApplicationUser WHERE pkUser_ID=@ApplicationUserID

	SET @status = 1;
    SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END

	GO

