If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetBankBook'
       )
 Drop Procedure usp_GetBankBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetBankBook
Author  : Naveed Muneer
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetBankBook
(
	@PageIndex INT = 1,
	@PageSize INT = 10,
	@BankBookType varchar(10)=null,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT ROW_NUMBER() OVER
      (
      ORDER BY pk_BankBookId ASC
      )
      AS RowNumber
	  ,pk_BankBookId
	  ,Company.Company_Name
	  ,BMaster.BankBookRefNo
	  ,dbo.fn_GetDateFormat_DDMMMYYYY(TransactionDate) as TransactionDate 
	  ,ChartMaster.ChartName as VoucherBook
	   ,SUM(BDetail.Amount) AS Amount
	  ,BMaster.ChequeNo
	   ,dbo.fn_GetDateFormat_DDMMMYYYY(ChequeDate) as ChequeDate 
	  ,BMaster.Remarks
	  into #Results
	  from BankBookMaster AS BMaster
	  INNER JOIN BankBookDetail AS BDetail ON BMaster.pk_BankBookId=BDetail.fk_BankBookId
	  INNER JOIN Charts as ChartMaster ON  ChartMaster.pk_ChartID=BMaster.fk_ChartID
	  INNER JOIN Charts as ChartDetail ON  ChartDetail.pk_ChartID=BDetail.fk_ChartID
	  Left JOIN Company ON  Company.pkCompany_ID=BMaster.fkCompanyID
	  Where BMaster.InputType=@BankBookType
	  GROUP BY pk_BankBookId,Company_Name,BankBookRefNo,TransactionDate,ChartMaster.ChartName,ChequeNo,ChequeDate,Remarks

      SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
      DROP TABLE #Results
End
GO


