If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateBankBook'
       )
 Drop Procedure usp_UpdateBankBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_UpdateBankBook
Author      : Muhammad Ammar
Create date : 25-06-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_UpdateBankBook
(
	@BankBookID INT,
	@CompanyID INT,
	@TransactionDate Datetime,
	@fkChartID INT,
	@ChequeNo varchar(100),
	@ChequeDate Datetime,
	@Remarks varchar(max),
	@UserID INT,
	@ModifiedOn	dateTime,
	@ModifiedBy	Varchar(50)
	

)
AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM BankBookMaster WHERE pk_BankBookID = @BankBookID)
	BEGIN
 Update BankBookMaster 
SET

	fkCompanyID=@CompanyID,
	TransactionDate=@TransactionDate,
	fk_ChartID=@fkChartID,
	ChequeNo=@ChequeNo,
	ChequeDate=@ChequeDate,
	Remarks=@Remarks,
	fkUserId=@UserID,
	Modified_On=@ModifiedOn,
	Modified_By=@ModifiedBy
	WHERE pk_BankBookID = @BankBookID

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;
END
GO
