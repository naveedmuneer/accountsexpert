If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteCashBookMaster'
       )
 Drop Procedure usp_DeleteCashBookMaster
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteCashBookMaster
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteCashBookMaster
(
	@CashBookID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM CashBookMaster WHERE pk_CashBookID=@CashBookID)

	BEGIN
 	
	Delete FROM CashBookMaster WHERE pk_CashBookID=@CashBookID

	SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;	
END

GO

