
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetChequeStatus'
       )
 Drop Procedure usp_GetChequeStatus
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetChequeStatus
Author      : Naveed Muneer
Create date : 24-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetChequeStatus

AS
BEGIN
 SET NOCOUNT ON;
 
Select * from EnumerationValue where fkEnumerationId= 1  

End
GO

