If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDataByBankBookForEdit'
       )
 Drop Procedure usp_GetDataByBankBookForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetDataByBankBookForEdit
Author      : Muhammad Naveed
Create date : 05-25-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDataByBankBookForEdit
(
	@BankBookID INT 
)
AS	
BEGIN
 SET NOCOUNT ON;
 Select 
 BMaster.fkCompanyID,
 BMaster.BankBookRefNo,
 BMaster.TransactionDate,
 BMaster.ChequeNo,
 dbo.fn_GetDateFormat_DDMMMYYYY(ChequeDate) as ChequeDates, 
 dbo.fn_GetDateFormat_DDMMMYYYY(TransactionDate) as VoucherDate,
 BMaster.Remarks,
 BDetail.Description,
 BDetail.Amount,
 ChartMaster.pk_ChartID as VoucherBookID ,
 ChartDetail.ChartName as AccountsName,
 BDetail.fk_ChartID As AccountID
 from BankBookMaster AS BMaster
 INNER JOIN BankBookDetail AS BDetail ON BMaster.pk_BankBookId=BDetail.fk_BankBookId
 LEFT JOIN Charts as ChartDetail ON  ChartDetail.pk_ChartID=  BDetail.fk_ChartID
 INNER JOIN Charts as ChartMaster ON  ChartMaster.pk_ChartID= BMaster.fk_ChartID
 INNER JOIN Company ON  Company.pkCompany_ID=BMaster.fkCompanyID
 WHERE pk_BankBookID=@BankBookID
 End
 GO


