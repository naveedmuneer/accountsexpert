
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetBankVoucher'
       )
 Drop Procedure usp_GetBankVoucher
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetBankVoucher
Author      : Naveed Muneer
Create date : 24-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetBankVoucher

AS
BEGIN
 SET NOCOUNT ON;
 
Select * from dbo.Charts where fk_AccountNatureID = 3 AND GroupID IS NULL

End
GO

