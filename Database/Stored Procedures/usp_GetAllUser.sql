If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetAllUser'
       )
 Drop Procedure usp_GetAllUser
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_GetAllUser
Author      : Muneem Ahmed
Create date : 26-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetAllUser

AS
BEGIN
 SET NOCOUNT ON;

 Select * from Employee WHERE Is_Active=1
End
GO

