
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetGroupDataFromChart'
       )
 Drop Procedure usp_GetGroupDataFromChart
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetGroupDataFromChart
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetGroupDataFromChart

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

 Select pk_ChartID AS ChartID,ChartName from charts


END

GO

