
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertUserRoleMapping'
       )
 Drop Procedure [usp_InsertUserRoleMapping]
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_InsertUserRoleMapping
Author      : Moiz Saleem
Create date : 11-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create PROCEDURE [dbo].[usp_InsertUserRoleMapping]
(
	@fkUser_ID int,
	@fkRole_ID Varchar(max)
	
	
	

)
AS
	DECLARE @status BIT;
	SET NOCOUNT ON;



	IF NOT EXISTS(SELECT  fkUser_ID FROM UserRoleMapping WHERE fkUser_ID=@fkUser_ID)
	BEGIN

Insert INTO UserRoleMapping(fkRole_ID,fkUser_ID) Select cast(Value as int),@fkUser_ID from dbo.Split(@fkRole_ID,',') 


SET @status = 1;
--SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;
GO


