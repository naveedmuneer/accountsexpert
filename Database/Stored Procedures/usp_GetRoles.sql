If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetRoles'
       )
 Drop Procedure usp_GetRoles
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     [usp_GetRoles]
Author      : Muhamad Moiz
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/


-- Exec [usp_GetBankBook] 1,10,5
Create PROCEDURE [dbo].[usp_GetRoles]
(
	@PageIndex INT = 1,
	@PageSize INT = 10,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT ROW_NUMBER() OVER
      (
      ORDER BY pkRole_ID ASC
      )
      AS RowNumber,
	    pkRole_ID ,[Role_Name],[Is_Active] , dbo.fn_GetDateFormat_DDMMMYYYY([Created_On]) as Created_On ,[Created_By]
	  into #Results
	  from Role 
	  
     

      SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1

	  
     
      DROP TABLE #Results
End

GO
