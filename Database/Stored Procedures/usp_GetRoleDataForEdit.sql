
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetRoleDataForEdit'
       )
 Drop Procedure usp_GetRoleDataForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetRoleDataForEdit
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create PROCEDURE [dbo].[usp_GetRoleDataForEdit]
(
	@RoleID INT 
)
AS	
BEGIN
 SET NOCOUNT ON;
 select *  from Role
 WHERE pkRole_ID=@RoleID
 End
 
Go
