If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateRole'
       )
 Drop Procedure usp_UpdateRole
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     [usp_UpdateRole]
Author      : Muhamad Moiz
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/

 create PROCEDURE [dbo].[usp_UpdateRole]
(
	@pkRole_ID int,
	@Role_Name Varchar(50),
	@Is_Active BIT
	
	

)
AS
	DECLARE @status BIT;
	SET NOCOUNT ON;


	IF  EXISTS(SELECT  pkRole_ID FROM Role WHERE pkRole_ID=@pkRole_ID)
BEGIN
	IF  EXISTS(SELECT  Role_Name FROM Role WHERE pkRole_ID!=@pkRole_ID and Role_Name=@Role_Name )
BEGIN
SET @status = 0;

END
ELSE

BEGIN
		
		Update [Role]  set Role_Name=@Role_Name,Is_Active=@Is_Active,Modified_On=getdate() WHERE pkRole_ID=@pkRole_ID
		SET @status = 1;
		end
		
		
END

	ELSE

BEGIN

	SET @status = 0;

END



SELECT @status;


GO
