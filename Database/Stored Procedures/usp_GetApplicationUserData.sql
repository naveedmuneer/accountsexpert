
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetApplicationUserData'
       )
 Drop Procedure usp_GetApplicationUserData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetApplicationUserData
Author  : Muneem Ahmed
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetApplicationUserData
(
	@PageIndex INT = 1,
	@PageSize INT = 10,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT ROW_NUMBER() OVER
      (
      ORDER BY pkuser_id ASC
      )
      AS RowNumber
	  ,pkuser_id as pkUserID
	  ,ApplicationUser.User_Name as UserName
	  ,ApplicationUser.Password as Password
	  ,Employee.Employee_Name as EmployeeName
	  ,ApplicationUser.Is_Active as IsActive
	  into #Results
	  from ApplicationUser AS ApplicationUser
	  INNER JOIN Employee AS Employee ON applicationUser.fkEmployee_ID=employee.pk_Employee_ID
	  
      SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
      DROP TABLE #Results
End
GO




