
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateCompany'
       )
 Drop Procedure usp_UpdateCompany
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     [usp_UpdateCompany]
Author      : abdul wahab
Create date : 14-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/

Create PROCEDURE [dbo].[usp_UpdateCompany]
(
	@pkCompany_ID int,
	@Company_Name Varchar(50),
	@Description varchar(50),
	@Title varchar(50),
	@Business varchar(50),
	@Establishment_Date Date,
	@Expiry_Date Date,
	@Is_Active BIT
)
AS
	DECLARE @status BIT;
	SET NOCOUNT ON;


	IF  EXISTS(SELECT  pkCompany_ID FROM Company WHERE pkCompany_ID=@pkCompany_ID)
    BEGIN
	IF  EXISTS(SELECT  Company_Name FROM Company WHERE pkCompany_ID!=@pkCompany_ID and Company_Name=@Company_Name )
    BEGIN
	
    SET @status = 0;

    END
    ELSE

    BEGIN
		
	Update [Company]  set
	Company_Name=@Company_Name,
	Description=@Description,
	Title=@Title,
	Business=@Business,
	Establishment_Date=@Establishment_Date,
	Expiry_Date=@Expiry_Date,
	Is_Active=@Is_Active,
	Modified_On=getdate() 
	WHERE pkCompany_ID=@pkCompany_ID
	SET @status = 1;
	end
		
		
	END

	ELSE

	BEGIN

	SET @status = 0;

	END



	SELECT @status;


GO
