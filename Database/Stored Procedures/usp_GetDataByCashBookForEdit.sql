If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDataByCashBookForEdit'
       )
 Drop Procedure usp_GetDataByCashBookForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetDataByCashBookForEdit
Author      : Muhammad Ammar
Create date : 05-25-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDataByCashBookForEdit
(
	@CashBookID INT 
)
AS	
BEGIN
 SET NOCOUNT ON;
 Select 
 CMaster.fkCompanyID,
 CMaster.CashBookRefNo,
 CMaster.TransactionDate,
 CMaster.ChequeNo,
 dbo.fn_GetDateFormat_DDMMMYYYY(ChequeDate) as ChequeDates ,
 dbo.fn_GetDateFormat_DDMMMYYYY(TransactionDate) as VoucherDate ,
 CMaster.Remarks,
 CDetail.Description,
 CDetail.Amount,
 ChartMaster.pk_ChartID as VoucherBook ,
 CDetail.fk_ChartID As AccountID,
 ChartMaster.ChartName as AccountsName
 from CashBookMaster AS CMaster
 INNER JOIN CashBookDetail AS CDetail on CMaster.pk_CashBookID=CDetail.fk_CashBookID
 LEFT JOIN Charts as ChartDetail ON  ChartDetail.pk_ChartID=  CDetail.fk_ChartID
 INNER JOIN Charts as ChartMaster ON  ChartMaster.pk_ChartID= CMaster.fk_ChartID
 INNER JOIN Company ON  Company.pkCompany_ID=CMaster.fkCompanyID
 WHERE pk_CashBookID=@CashBookID
 End
 GO
