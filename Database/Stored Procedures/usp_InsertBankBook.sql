

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertBankBook'
       )
 Drop Procedure usp_InsertBankBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     usp_InsertBankBook
Author      : Naveed Muneer
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertBankBook
(
	@BankBookID INT,
	@InputType Varchar(200),
	@CompanyID INT,
	@BankBookRefNo Varchar(100),
	@TransactionDate Datetime,
	@fkChartID INT,
	@ChequeNo varchar(100),
	@ChequeDate Datetime,
	@Remarks varchar(max),
	@fkUserID INT,
	@CreatedOn	dateTime,
	@CreatedBy	Varchar(50)
	

)
AS
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF NOT EXISTS(SELECT 1 FROM BankBookMaster WHERE pk_BankBookId=@BankBookID)
	BEGIN

 INSERT INTO BankBookMaster 
 (
	pk_BankBookId,
	InputType,
	fkCompanyID,
	BankBookRefNo,
	TransactionDate,
	fk_ChartID,
	ChequeNo,
	ChequeDate,
	Remarks,
	fkUserId,
	Created_On,
	Created_By
)
values
(
	@BankBookID,
	@InputType,
	@CompanyID,
	@BankBookRefNo,
	@TransactionDate,
	@fkChartID,
	@ChequeNo,
	@ChequeDate,
	@Remarks,
	@fkUserID,
	@CreatedOn,
	@CreatedBy
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;

GO



GO

