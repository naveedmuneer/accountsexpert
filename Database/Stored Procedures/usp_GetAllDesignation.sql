If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetAllDesignation'
       )
 Drop Procedure usp_GetAllDesignation
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetAllDesignation
Author      : Muhammad Waleed
Create date : 2 july 2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetAllDesignation

AS
BEGIN
 SET NOCOUNT ON;

 Select * from Designation WHERE Is_Active=1
End
GO

