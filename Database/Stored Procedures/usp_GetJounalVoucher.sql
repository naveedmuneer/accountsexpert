
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetJounalVoucher'
       )
 Drop Procedure usp_GetJounalVoucher
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetJounalVoucher
Author      : Muhammad Naveed
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetJounalVoucher
(
@PageIndex INT = 1,
@PageSize INT = 10,
@RecordCount INT OUTPUT
)


AS
BEGIN

SELECT ROW_NUMBER() OVER
(
ORDER BY pk_JournalVoucherID ASC
)
AS RowNumber,
pk_JournalVoucherID,
JournalVoucherRefNo,
dbo.fn_GetDateFormat_DDMMMYYYY(TransactionDate) as TransactionDate ,
Remarks as Description,
Sum(GVD.Debit) as Amount
into #Results
from 
JournalVoucherMaster GVM inner join JournalVoucherDetail GVD
on GVM.pk_JournalVoucherID = fk_JournalVoucherID
group by pk_JournalVoucherID,JournalVoucherRefNo,TransactionDate,Remarks

SELECT @RecordCount = COUNT(*)
FROM #Results
   
SELECT * FROM #Results
WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1

DROP TABLE #Results
END
GO


