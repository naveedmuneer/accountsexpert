If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertCashBook'
       )
 Drop Procedure usp_InsertCashBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_InsertCashBook
Author      : Waleed Saleem
Create date : 11-8-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertCashBook
    
    @xmlCashBook xml,
	@xmlCashBookDetail xml
	
	
AS
Begin

Declare @rvcCashBookMasterID as int 
Declare @CashBookRefNo as varchar(50)
Declare @Inputtype as varchar(5)
Select @Inputtype = XmlDataTable.CashBook.value('@InputType[1]','Varchar(20)') FROM @xmlCashBook.nodes('.//CashBook') as XmlDataTable(CashBook)
Select @CashBookRefNo = @Inputtype + '-0000' + cast(ISNULL(MAX(pk_CashBookID),0) + 1 as varchar(50)) from CashBookMaster	
Declare @CashBookId as int
Select @CashBookId=cast(ISNULL(MAX(pk_CashBookID),0) +1  as int)   from CashBookMaster



	

 INSERT INTO CashBookMaster 
 (
	pk_CashBookID,
	InputType,
	fkCompanyID,
	CashBookRefNo,
	TransactionDate,
	fk_ChartID,
	ChequeNo,
	ChequeDate,
	Remarks,
	fkUserId,
	Active,
	Created_On,
	Created_By
)
 Select	
	        @CashBookId,
			XmlDataTable.CashBook.value('@InputType[1]','Varchar(20)'),
			XmlDataTable.CashBook.value('@CompanyID[1]','int'),
			@CashBookRefNo,
			XmlDataTable.CashBook.value('@TransactionDate[1]','Datetime'),
			XmlDataTable.CashBook.value('@FkChartID[1]','int'),
			XmlDataTable.CashBook.value('@ChequeNo[1]','varchar(100)'),
			XmlDataTable.CashBook.value('@ChequeDate[1]','Datetime'),
			XmlDataTable.CashBook.value('@Remarks[1]','varchar(max)'),
			XmlDataTable.CashBook.value('@fkUserID[1]','int'),
			1,
			GetDate(),
			XmlDataTable.CashBook.value('@Created_By[1]','varchar(50)')
	
					
	FROM @xmlCashBook.nodes('.//CashBook') as XmlDataTable(CashBook)

 INSERT INTO CashBookDetail 
 (

	fk_CashBookId,
	fk_ChartID,
	[Description],
	Amount
)
    Select	
		@CashBookId,
		XmlDataTable.CashBookDetail.value('@fk_ChartID[1]','int'),
		XmlDataTable.CashBookDetail.value('@Description[1]','varchar(max)'),
		XmlDataTable.CashBookDetail.value('@Amount[1]','Decimal(18,5)')
        FROM @xmlCashBookDetail.nodes('//CashBookDetail') as XmlDataTable(CashBookDetail)
	 END

GO
