If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetChartOfAccount'
       )
 Drop Procedure usp_GetChartOfAccount
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetChartOfAccount
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetChartOfAccount
(
	@ChartType varchar(50)=null,
	@PageIndex INT = 1,
	@PageSize INT = 10,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT ROW_NUMBER() OVER
      (
            ORDER BY pk_ChartID ASC
      )AS RowNumber
	  ,pk_ChartID AS [ChartID]
	  ,ChartRefNo
	  ,chart.Description
	  ,dbo.fn_ReverseChartOfAccountName(dbo.getChartAccountParent(pk_ChartID)) As ChartName
	  ,AC.AccountNatureName As AccountNature
	  ,chart.Active As [Status]
	  ,dbo.getChartOfAccountLevel(pk_ChartID) As [Level] 
	  INTO #Results
	  from charts chart
	  LEFT JOIN AccountNature AC On chart.fk_AccountNatureID=Ac.pk_AccountNatureID
		WHERE ((@ChartType is null) or (AC.Header1 = @ChartType))
	

 SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
      DROP TABLE #Results
End
GO

--Select * From charts
--Select * from accountNature