
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCompanyDataForEdit'
       )
 Drop Procedure usp_GetCompanyDataForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCompanyDataForEdit
Author      : abdul wahab
Create date : 14-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create PROCEDURE [dbo].[usp_GetCompanyDataForEdit]
(
	@CompanyID INT 
)
AS	
BEGIN
 SET NOCOUNT ON;
 select *  from Company
 WHERE pkCompany_ID=@CompanyID
 End
 
Go
