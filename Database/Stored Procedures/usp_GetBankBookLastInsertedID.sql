If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetBankBookLastInsertedID'
       )
 Drop Procedure usp_GetBankBookLastInsertedID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetBankBookLastInsertedID
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetBankBookLastInsertedID
(
	@InputType varchar(50)
)
AS
BEGIN
 SET NOCOUNT ON;

 Select Max(pk_BankBookID) As BankBookID From BankBookMaster where InputType=@InputType
 
End
GO
