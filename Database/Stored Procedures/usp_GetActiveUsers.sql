If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetActiveUsers'
       )
 Drop Procedure [usp_GetActiveUsers]
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetActiveUsers
Author      : Moiz Saleem
Create date : 11-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create PROCEDURE [dbo].[usp_GetActiveUsers]
AS	

BEGIN

SET NOCOUNT ON;
select  pkUser_ID ,[User_Name]  from ApplicationUser WHERE  Is_Active='true'

End
GO