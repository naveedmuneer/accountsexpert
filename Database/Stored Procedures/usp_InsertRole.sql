If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertRole'
       )
 Drop Procedure usp_InsertRole
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     [usp_InsertRole]
Author      : Muhamad Moiz
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/

create PROCEDURE [dbo].[usp_InsertRole]
(
	
	@Role_Name Varchar(50),
	@Is_Active BIT,
	@CreatedBy	Varchar(50)
	

)
AS
	DECLARE @status BIT;
	SET NOCOUNT ON;


	IF NOT EXISTS(SELECT  Role_Name FROM Role WHERE Role_Name=@Role_Name)
	BEGIN

 INSERT INTO Role 
 (
	
	Role_Name,
	Is_Active,
	Created_On,
	Created_By
)
values
(
	@Role_Name,
	@Is_Active,
	getdate(),
	@CreatedBy
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;


GO
