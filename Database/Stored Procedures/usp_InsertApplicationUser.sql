
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertApplicationUser'
       )
 Drop Procedure usp_InsertApplicationUser
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_InsertApplicationUser
Author      : Muneem Ahmed
Create date : 26-06-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertApplicationUser
(
	@pk_UserID INT,
	@UserName varchar(100),
	@Password Varchar(100),
	@fkEmployeeID Int,
	@Is_Active Bit,
	@createdBy varchar(100),
	@createdOn date
	)
	

AS
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF NOT EXISTS(SELECT 1 FROM ApplicationUser WHERE pkUser_ID=@pk_UserID)
	BEGIN
 INSERT INTO ApplicationUser 
 (
	
	User_Name,
	Password,
	fkEmployee_ID,
	Is_Active,
	Created_On,
	Created_By
)
values
(
	
	
	@UserName,
	@Password,
	@fkEmployeeID,
	@Is_Active,
	@createdOn,
	@createdBy

)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;

Go
Go