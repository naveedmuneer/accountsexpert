If Exists (Select *
   From   Information_Schema.Routines
   Where  Specific_Schema = N'dbo'
   And	   Specific_Name = N'usp_InsertJournalVoucherData')
	Drop Procedure dbo.usp_InsertJournalVoucherData

GO


/********************************************************************************************
Module       : usp_InsertJournalVoucherData
Created By   : Abdul Wahab
CreatedOn    : Aug-25-2016
Description  : 

*******************************Modification History*******************************************
*********************************************************************************************/
CREATE PROC [dbo].usp_InsertJournalVoucherData
(
    @xmlJournalVoucher xml,
	@xmlJournalVoucherDetail xml
)
AS
BEGIN

Declare @JournalVoucherRefNo as Varchar(100)
Select @JournalVoucherRefNo ='JV'  + '-00000' + cast(ISNULL(MAX(pk_JournalVoucherID),0) + 1 as varchar(50)) from JournalVoucherMaster	


Declare @Jvid as int 
Select @Jvid = cast(ISNULL(MAX(pk_JournalVoucherID),0) +1  as int)   from JournalVoucherMaster


INSERT INTO JournalVoucherMaster (  

	pk_JournalVoucherID,
	JournalVoucherRefNo,
	TransactionDate,
	fkCompanyID,
	Remarks,
	fkUserId,
	Created_On,
	Created_By,
	IsPosted

	)
	select 

			@Jvid,
			@JournalVoucherRefNo,
			xmlJournalVoucher.JournalVoucher.value('@TransactionDate[1]','Datetime'),
			xmlJournalVoucher.JournalVoucher.value('@fkCompanyID[1]','int'),
			xmlJournalVoucher.JournalVoucher.value('@Remarks[1]','varchar(100)'),
			xmlJournalVoucher.JournalVoucher.value('@fkUserId[1]','int'),
			GETDATE(),
			xmlJournalVoucher.JournalVoucher.value('@Created_By[1]','varchar(50)'),
			1
	
					
	FROM @xmlJournalVoucher.nodes('.//JournalVoucherMaster') as xmlJournalVoucher(JournalVoucher)

INSERT INTO JournalVoucherDetail  
(  
        fk_JournalVoucherID,
		fk_ChartID,
		Debit,
		Credit,
		Description,
		SeqNo,
		Created_On
	   
)
select 

	
		@Jvid,
		XmlDataTable.JournalVoucherDetail.value('@fk_ChartID[1]','int'),
		XmlDataTable.JournalVoucherDetail.value('@Debit[1]','Decimal'),
		XmlDataTable.JournalVoucherDetail.value('@Credit[1]','Decimal'),
		XmlDataTable.JournalVoucherDetail.value('@Description[1]','varchar(MAX)'),
		XmlDataTable.JournalVoucherDetail.value('@SeqNo[1]','int'),
		GetDate()

	 FROM @xmlJournalVoucherDetail.nodes('//JournalVoucherDetail') as XmlDataTable(JournalVoucherDetail)


	 END





















