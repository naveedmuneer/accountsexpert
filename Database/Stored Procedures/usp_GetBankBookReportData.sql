If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetBankBookReportData'
       )
 Drop Procedure usp_GetBankBookReportData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCashBook
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetBankBookReportData
(
	@BankBookID int
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT 
  pk_BankBookId,
  Company.Company_Name,
  BMaster.BankBookRefNo As BankBookRefNo,
  BDetail.Description ,
  BMaster.ChequeDate,
  BMaster.ChequeNo,
Cast(CONVERT(date,BMaster.TransactionDate) AS varchar(12)) AS TransactionDate,
Charts.ChartName,
ChartMaster.ChartName As BookName ,
BDetail.Amount,
BMaster.ChequeNo,
Cast(CONVERT(date,BMaster.ChequeDate)AS varchar(12)) AS ChequeDate ,
BMaster.Remarks 
from BankBookMaster AS BMaster 
INNER JOIN BankBookDetail AS BDetail ON BMaster.pk_BankBookID=BDetail.fk_BankBookId 
INNER JOIN Charts ON  Charts.pk_ChartID=BDetail.fk_ChartID 
Left JOIN Company ON  Company.pkCompany_ID=BMaster.fkCompanyID 
INNER JOIN Charts  AS ChartMaster ON ChartMaster.pk_ChartID=BMaster.fk_ChartID
WHERE pk_BankBookId=@BankBookID
End
GO
