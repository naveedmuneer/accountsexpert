If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteEmployee'
       )
 Drop Procedure usp_DeleteEmployee
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteEmployee
Author      : Waleed Saleem
Create date : 30-6-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteEmployee
(
	@EmplpoyeeID		INT
)
AS
BEGIN
DELETE FROM Employee WHERE pk_Employee_ID = @EmplpoyeeID

END

    GO

