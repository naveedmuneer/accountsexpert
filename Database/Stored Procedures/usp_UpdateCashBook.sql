If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateCashBook'
       )
 Drop Procedure usp_UpdateCashBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_UpdateCashBook
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_UpdateCashBook
(
	@CashBookID INT,
	@CompanyID INT,
	@TransactionDate Datetime,
	@fkAccountBookID INT,
	@ChequeNo varchar(100),
	@ChequeDate Datetime,
	@Remarks varchar(max),
	@UserID INT,
	@ModifiedOn	dateTime,
	@ModifiedBy	Varchar(50)
	

)
AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM CashBookMaster WHERE pk_CashBookID = @CashBookID)
	BEGIN
 Update CashBookMaster 
SET

	fkCompanyID=@CompanyID,
	TransactionDate=@TransactionDate,
	fk_ChartID=@fkAccountBookID,
	ChequeNo=@ChequeNo,
	ChequeDate=@ChequeDate,
	Remarks=@Remarks,
	fkUserId=@UserID,
	Modified_On=@ModifiedOn,
	Modified_By=@ModifiedBy
	WHERE pk_CashBookID=@CashBookID

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;
END
GO
