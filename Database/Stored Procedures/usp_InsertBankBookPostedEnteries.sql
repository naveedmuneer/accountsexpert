	
	If Exists (Select *
	   From   Information_Schema.Routines
	   Where  Specific_Schema = N'dbo'
	   And	   Specific_Name = N'usp_InsertBankBookPostedEnteries')
	   Drop Procedure dbo.usp_InsertBankBookPostedEnteries

	GO
/********************************************************************************************
Module       : usp_InsertBankBookPostedEnteries
Created By   : Waleed Saleem
CreatedOn   : 30 -8 -2106
Description  : 

*******************************Modification History*******************************************
*********************************************************************************************/
CREATE PROC [dbo].usp_InsertBankBookPostedEnteries

(	
@BankBookIDs VARCHAR(MAX)
)
	AS
	BEGIN
	
declare @sql1 nvarchar(Max)


Set @sql1=
'Insert into BankBookPostedEntries
Select
BBM.InputType,
BBM.BankBookRefNo,
BBM.TransactionDate, 
BBM.fk_ChartID ,
C.ChartName Chartname, 
Case when BBM.InputType = ''BP'' then 0 else BBD.Amount  end as Debit,

Case when BBM.InputType = ''BR'' then 0 else BBD.Amount end as Credit

from BankBookDetail BBD
inner join BankBookMaster BBM on BBM.pk_BankBookId = BBD.fk_BankBookId
inner join Charts C  on C.pk_ChartID = BBM.fk_ChartID
where BBM.pk_BankBookId In ('+@BankBookIDs+')
union 
Select 
BBM.InputType,
BBM.BankBookRefNo,
BBM.TransactionDate, 
BBD.fk_ChartID ,
ch.ChartName chartname,
Case when BBM.InputType = ''BP'' then BBD.Amount  else 0   end as Debit,
Case when BBM.InputType = ''BR'' then BBD.Amount else 0 end as Credit

from BankBookDetail BBD
inner join BankBookMaster BBM on BBM.pk_BankBookId = BBD.fk_BankBookId
inner join charts ch on ch.pk_ChartID = BBD.fk_ChartID
where BBM.pk_BankBookId In ('+@BankBookIDs+')'
exec sp_executesql @sql1
END 


GO





