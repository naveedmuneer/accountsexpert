If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteRole'
       )
 Drop Procedure usp_DeleteRole
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     usp_DeleteRole
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/


create PROCEDURE [dbo].[usp_DeleteRole]
(
	@RoleID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT pkRole_ID FROM Role WHERE pkRole_ID=@RoleID)

	BEGIN
 	
	Delete FROM Role WHERE pkRole_ID=@RoleID

	SET @status = 1;
    SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END


GO
