If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCashBook'
       )
 Drop Procedure usp_GetCashBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCashBook
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetCashBook
(
@PageIndex INT = 1,

	@PageSize INT = 10,

	@CashBookType varchar(10)=null,

	@TransactionStartDate varchar(100)=null,

	@TransactionENDDate varchar(100)=null,

	@SearchCashBookRefNo varchar(200)=null,

@SearchTransactionDate varchar(200)=null,

@SearchVoucherBook varchar(200)=null,

@SearchChequeNo varchar(200)=null,

@SearchChequeDate varchar(200)=null,

	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT ROW_NUMBER() OVER
      (
            ORDER BY pk_CashBookID ASC
      )
     AS RowNumber
	  ,pk_CashBookID
	  ,CMaster.CashBookRefNo
	  ,dbo.fn_GetDateFormat_DDMMMYYYY(TransactionDate) as TransactionDate 
	  
	  ,ChartMaster.ChartName as VoucherBook
	   ,ROUND(Cast(Sum(CDetail.Amount)As float),2) AS Amount
	  ,CMaster.ChequeNo
	  ,dbo.fn_GetDateFormat_DDMMMYYYY(ChequeDate) as ChequeDate 
	  ,CMaster.Remarks
	  into #Results
	  from CashBookMaster AS CMaster
	  LEFT JOIN CashBookDetail AS CDetail ON CMaster.pk_CashBookID=CDetail.fk_CashBookID
	  INNER JOIN Charts as ChartMaster ON  ChartMaster.pk_ChartID=CMaster.fk_ChartID
	  INNER JOIN Charts as ChartDetail ON  ChartDetail.pk_ChartID=CDetail.fk_ChartID
	  Left JOIN Company ON  Company.pkCompany_ID=CMaster.fkCompanyID
       Where CMaster.InputType=@CashBookType And (CMaster.TransactionDate >= @TransactionStartDate OR @TransactionStartDate IS NULL) 

	  And (CMaster.TransactionDate <= @TransactionEndDate OR @TransactionEndDate IS NULL)

	   And  (CashBookRefNo LIKE '%'+@SearchCashBookRefNo+'%' OR @SearchCashBookRefNo IS NULL)

	  And (TransactionDate LIKE '%'+@SearchTransactionDate+'%' OR @SearchTransactionDate IS NULL)

	  And (ChartMaster.ChartName LIKE '%'+@SearchVoucherBook+'%' OR @SearchVoucherBook IS NULL)

	  And (ChequeNo LIKE '%'+@SearchChequeNo+'%' OR @SearchChequeNo IS NULL)

	  And (ChequeDate LIKE '%'+@SearchChequeDate+'%' OR @SearchChequeDate IS NULL)

	  GROUP BY pk_CashBookID,Company_Name,CashBookRefNo,TransactionDate,ChartMaster.ChartName,ChequeNo,ChequeDate,Remarks

 SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
      DROP TABLE #Results
End
GO
