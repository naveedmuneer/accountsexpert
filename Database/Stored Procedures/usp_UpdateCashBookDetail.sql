If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateCashBookDetail'
       )
 Drop Procedure usp_UpdateCashBookDetail
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_UpdateCashBookDetail
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_UpdateCashBookDetail
(
	@CashBookDetailID INT,
	@fkCashBookID INT,
	@fkChartID INT,
	@Description varchar(max),
	@Amount Decimal(18,5)
)
AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM CashBookDetail WHERE pk_CashBookDetailID = @CashBookDetailID)
	BEGIN
 Update CashBookDetail 
SET

	fk_CashBookID=@fkCashBookID,
	fk_ChartID=@fkChartID,
	[Description]=@Description,
	Amount=@Amount
	WHERE pk_CashBookDetailID=@CashBookDetailID

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;
END
GO
