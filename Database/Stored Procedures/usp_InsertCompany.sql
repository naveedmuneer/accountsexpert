
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertCompany'
       )
 Drop Procedure usp_InsertCompany
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     [usp_InsertCompany]
Author      : abdul wahab
Create date : 14-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/

Create PROCEDURE [dbo].[usp_InsertCompany]
(
	
	@Company_Name Varchar(50),
	@Description varchar(50),
	@Title varchar(50),
	@Business varchar(50),
	@Establishment_Date Date,
	@Expiry_Date Date,
	@Is_Active BIT,
	@CreatedBy	Varchar(50)
	

)
AS
	DECLARE @status BIT;
	SET NOCOUNT ON;


	IF NOT EXISTS(SELECT  Company_Name FROM Company WHERE Company_Name=@Company_Name)
	BEGIN

 INSERT INTO Company 
 (
	
	Company_Name ,
	Description,
	Title,
	Business,
	Establishment_Date,
	Expiry_Date,
	Is_Active,
	Created_On,
	Created_By,
	Machine_Created
)
values
(
	@Company_Name,
	@Description,
	@Title,
	@Business,
	@Establishment_Date,
	@Expiry_Date,
	@Is_Active,
	getdate(),
	@CreatedBy,
	getdate()
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;


Go