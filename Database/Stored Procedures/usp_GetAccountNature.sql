If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetAccountNature'
       )
 Drop Procedure usp_GetAccountNature
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetAccountNature
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetAccountNature

AS
BEGIN
 SET NOCOUNT ON;

 Select pk_AccountNatureID AS AccountNatureID,AccountNatureName from AccountNature WHERE pk_AccountNatureID!=1
End
GO

