	
	If Exists (Select *
	   From   Information_Schema.Routines
	   Where  Specific_Schema = N'dbo'
	   And	   Specific_Name = N'usp_UpdateCashBookData')
	   Drop Procedure dbo.usp_UpdateCashBookData

	GO
/********************************************************************************************
Module       : usp_UpdateCashBookData
Created By   : Waleed Saleem
CreatedOn   : 12 August 2015
Description  : 

*******************************Modification History*******************************************
*********************************************************************************************/
CREATE PROC [dbo].usp_UpdateCashBookData

(	@xmlCashBookMaster xml,
	@xmlCashBookDetail xml
)
	AS
	BEGIN
	-- DECLARE @status BIT;
	--SET NOCOUNT ON;
	  --BEGIN
    UPDATE CashBookMaster 
	SET 
	InputType = i.InputType, 
	fkCompanyID = i.CompanyId,
	TransactionDate = i.TrasactionDate,
	fk_ChartID = i.FkchartId, 
	ChequeNo = i.ChequeNo,
	ChequeDate = i.ChequeDate, 
	Remarks = i.Remarks,
	fkUserId = i.FkuserId, 
	Active = i.Active,
	Modified_On=i.ModifiedOn,
	Modified_By=i.ModifiedBy

	FROM
	 (
  		SELECT 
		XmlDataTable.CashBook.value('@InputType[1]','varchar(5)') as InputType,
		XmlDataTable.CashBook.value('@CompanyID[1]','int') as CompanyId,
		XmlDataTable.CashBook.value('@TransactionDate','Datetime')as TrasactionDate ,
		XmlDataTable.CashBook.value('@FkChartID[1]','int')as FkchartId,
		XmlDataTable.CashBook.value('@ChequeNo[1]','varchar(100)') as ChequeNo,
		XmlDataTable.CashBook.value('@ChequeDate[1]','Datetime')as ChequeDate,
		XmlDataTable.CashBook.value('@Remarks[1]','varchar(max)')as Remarks,
		XmlDataTable.CashBook.value('@FkUserID[1]','int')as FkuserId,
		1 as Active,
		GetDate() as ModifiedOn,
		XmlDataTable.CashBook.value('@Modified_By[1]','varchar(max)')as ModifiedBy 
		
FROM @xmlCashBookMaster.nodes('.//CashBook') as XmlDataTable(CashBook) 
	) as i

WHERE pk_CashBookID = (SELECT XmlDataTable.CashBook.value('@pk_CashBookID[1]','int') 
from @xmlCashBookMaster.nodes('.//CashBook') as XmlDataTable(CashBook))
  



Delete from CashBookDetail 
WHERE fk_CashBookId=(SELECT XmlDataTable.CashBook.value('@pk_CashBookID[1]','int') 
from @xmlCashBookMaster.nodes('.//CashBook') as XmlDataTable(CashBook)) 

     INSERT INTO CashBookDetail 
	 (	
	    fk_CashBookId,
		fk_ChartID,	
		[Description],
		Amount
	)

	Select	
			(
			SELECT
			XmlDataTable.CashBook.value('@pk_CashBookID[1]','int') from @xmlCashBookMaster.nodes('.//CashBook') as XmlDataTable(CashBook)),
			XmlUpdateDataTable.Detail.value('@fk_ChartID[1]','int'),
			XmlUpdateDataTable.Detail.value('@Description[1]','varchar(max)'),
			XmlUpdateDataTable.Detail.value('@Amount[1]','Decimal(18,5)')

	FROM @xmlCashBookDetail.nodes('//CashBookDetail') as XmlUpdateDataTable(Detail)
 
END 
--END

GO
