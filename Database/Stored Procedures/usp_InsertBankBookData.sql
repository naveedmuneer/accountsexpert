If Exists (Select *
   From   Information_Schema.Routines
   Where  Specific_Schema = N'dbo'
   And	   Specific_Name = N'usp_InsertBankBookData')
	Drop Procedure dbo.usp_InsertBankBookData

GO
/********************************************************************************************
Module       : usp_InsertBankBookData
Created By   : Abdul Wahab
CreatedOn   : July 29, 2016
Description  : 

*******************************Modification History*******************************************
*********************************************************************************************/
CREATE PROC [dbo].usp_InsertBankBookData
	@xmlBankBook xml,
	@xmlBankBookDetail xml
AS
BEGIN

Declare @rvcBankBookMasterID as int 
Declare @BankBookRefNo as varchar(50)
Declare @Inputtype as varchar(5)
Select @Inputtype = XmlDataTable.BankBook.value('@InputType[1]','Varchar(20)') FROM @xmlBankBook.nodes('.//BankBook') as XmlDataTable(BankBook)
Select @BankBookRefNo = @Inputtype + '-0000' + cast(ISNULL(MAX(pk_BankBookId),0) + 1 as varchar(50)) from BankBookMaster	
Declare @BankBookId as int
Select @BankBookId=cast(ISNULL(MAX(pk_BankBookId),0) +1  as int)   from BankBookMaster

INSERT INTO BankBookMaster(
    pk_BankBookId,
	InputType,
	fkCompanyID,
	BankBookRefNo,
	TransactionDate,
	fk_ChartID,
	ChequeNo,
	ChequeDate,
	Remarks,
	fkUserId,
	Active,
	Created_On,
	[Created_By]
	)
	   
	 Select	
	        @BankBookId,
			XmlDataTable.BankBook.value('@InputType[1]','Varchar(20)'),
			XmlDataTable.BankBook.value('@CompanyID[1]','int'),
			@BankBookRefNo,
			XmlDataTable.BankBook.value('@TransactionDate[1]','Datetime'),
			XmlDataTable.BankBook.value('@FkChartID[1]','int'),
			XmlDataTable.BankBook.value('@ChequeNo[1]','varchar(100)'),
			XmlDataTable.BankBook.value('@ChequeDate[1]','Datetime'),
			XmlDataTable.BankBook.value('@Remarks[1]','varchar(max)'),
			XmlDataTable.BankBook.value('@fkUserID[1]','int'),
			1,
			GetDate(),
			XmlDataTable.BankBook.value('@Created_By[1]','varchar(50)')
	
					
	FROM @xmlBankBook.nodes('.//BankBook') as XmlDataTable(BankBook)

 INSERT INTO BankBookDetail 
 (

	fk_BankBookId,
	fk_ChartID,
	[Description],
	Amount
)
    Select	
		@BankBookId,
		XmlDataTable.BankBookDetail.value('@Fk_ChartID[1]','int'),
		XmlDataTable.BankBookDetail.value('@Description[1]','varchar(max)'),
		XmlDataTable.BankBookDetail.value('@Amount[1]','Decimal(18,5)')
        FROM @xmlBankBookDetail.nodes('//BankBookDetail') as XmlDataTable(BankBookDetail)
	 END


