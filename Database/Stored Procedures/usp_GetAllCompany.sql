If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetAllCompany'
       )
 Drop Procedure usp_GetAllCompany
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetAllCompany
Author      : Muhammad Ammar
Create date : 24-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetAllCompany

AS
BEGIN
 SET NOCOUNT ON;

 Select * from Company WHERE Is_Active=1
End
GO

