If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetBankBookDetailMaxID'
       )
 Drop Procedure usp_GetBankBookDetailMaxID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetBankBookDetailMaxID
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetBankBookDetailMaxID

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

Select ISNULL(MAX(pk_BankBookDetailID),0) from BankBookDetail

END

GO

