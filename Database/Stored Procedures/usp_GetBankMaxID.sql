If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetBankMaxID'
       )
 Drop Procedure usp_GetBankMaxID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetBankMaxID
Author      : Naveed Muneer
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetBankMaxID

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

Select ISNULL(MAX(pk_BankBookId),0) from BankBookMaster

END

GO

