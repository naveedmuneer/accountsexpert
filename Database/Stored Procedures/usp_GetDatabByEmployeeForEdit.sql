If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDatabByEmployeeForEdit'
       )
 Drop Procedure usp_GetDatabByEmployeeForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetDatabByEmployeeForEdit
Author      : Muhammad Waleed
Create date : 3 July 2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDatabByEmployeeForEdit
(
	@EmployeeID INT 
)
AS	
BEGIN
 SET NOCOUNT ON;
 Select 
* FROM Employee
 WHERE pk_Employee_ID=@EmployeeID
 End
 GO


