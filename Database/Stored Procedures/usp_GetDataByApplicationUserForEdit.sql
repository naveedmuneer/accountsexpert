
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDataByApplicationUserForEdit'
       )
 Drop Procedure usp_GetDataByApplicationUserForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetDataByApplicationUserForEdit
Author      : Muneem Ahmed
Create date : 27-06-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDataByApplicationUserForEdit
(
	@applicationUserID INT 
)
AS	
BEGIN
 SET NOCOUNT ON;
 Select 
 appuser.pkUser_ID,
 AppUser.User_Name,
 AppUser.Password,
 AppUser.fkEmployee_ID,
 AppUser.Is_Active,
 Employee.pk_Employee_ID
 
 from ApplicationUser AS AppUser
 INNER JOIN Employee AS employee ON employee.pk_Employee_ID =AppUser.fkEmployee_ID
 WHERE pkUser_ID=@applicationUserID
 End
 GO


