
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateUserRoleMapping'
       )
 Drop Procedure usp_UpdateUserRoleMapping
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_UpdateUserRoleMapping
Author      : Muhammad Moiz
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
-- exec usp_UpdateUserRoleMapping 3,'14,15,16'
Create PROCEDURE [dbo].[usp_UpdateUserRoleMapping]
(
	@fkUser_ID int,
	@fkRole_ID Varchar(max)
)
    AS
	DECLARE @status BIT;
	SET NOCOUNT ON;
    IF  EXISTS(SELECT  fkUser_ID FROM UserRoleMapping WHERE fkUser_ID=@fkUser_ID)
    BEGIN
	
	Delete from UserRoleMapping WHERE fkUser_ID=@fkUser_ID
	Insert INTO UserRoleMapping(fkRole_ID,fkUser_ID) Select cast(Value as int),@fkUser_ID from dbo.Split(@fkRole_ID,',') 
    SET @status = 1;
    END


--ELSE  IF not   EXISTS(SELECT  fkUser_ID FROM UserRoleMapping WHERE fkUser_ID=@fkUser_ID)
--begin
	
	
--	Insert INTO UserRoleMapping(fkRole_ID,fkUser_ID) Select cast(Value as int),@fkUser_ID from dbo.Split(@fkRole_ID,',') 


--	SET @status = 1;

--END
   Else
   begin
   SET @status = 0;
   end
   SELECT @status;
   GO

