If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteBankBookDetail'
       )
 Drop Procedure usp_DeleteBankBookDetail
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteBankBookDetail
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteBankBookDetail
(
	@BankBookID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM BankBookDetail WHERE fk_BankBookId=@BankBookID)

	BEGIN
 	
	Delete FROM BankBookDetail WHERE fk_BankBookId=@BankBookID

	SET @status = 1;
    SELECT @status;
    END
    ELSE
    SET @status = 0;
    SELECT @status;	
    END

    GO

