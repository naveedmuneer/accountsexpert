If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDataByCashBookDetailIDForEdit'
       )
 Drop Procedure usp_GetDataByCashBookDetailIDForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetDataByCashBookDetailIDForEdit
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDataByCashBookDetailIDForEdit
(
	@CashBookDetailID INT 
)
AS
BEGIN
 SET NOCOUNT ON;

 Select * From CashBookDetail WHERE pk_CashBookDetailID=@CashBookDetailID
 
End
GO
