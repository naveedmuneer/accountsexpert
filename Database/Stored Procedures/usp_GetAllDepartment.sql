If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetAllDepartment'
       )
 Drop Procedure usp_GetAllDepartment
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetAllDepartment
Author      : Muhammad Waleed
Create date : 2 july 2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetAllDepartment

AS
BEGIN
 SET NOCOUNT ON;

 Select * from Department WHERE Is_Active=1
End
GO

