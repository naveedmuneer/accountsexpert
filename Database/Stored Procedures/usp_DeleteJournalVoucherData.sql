If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteJournalVoucherData'
       )
 Drop Procedure usp_DeleteJournalVoucherData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module	    : usp_DeleteJournalVoucherData
Author      : Abdul Wahab
Create date : aug-25-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteJournalVoucherData
(
	@JournalVoucherID	INT
)
AS
BEGIN
DECLARE @status BIT;

	IF EXISTS(SELECT 1 FROM JournalVoucherDetail WHERE fk_JournalVoucherID=@JournalVoucherID )

	BEGIN
 	
	Delete FROM JournalVoucherDetail WHERE fk_JournalVoucherID=@JournalVoucherID
    
	Delete FROM JournalVoucherMaster WHERE pk_JournalVoucherID=@JournalVoucherID

	SET @status = 1;
    SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END

	GO
