
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_Validateuser'
       )
	Drop Procedure usp_Validateuser
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_Validateuser
Author      : Naveed Muneer
Create date	: 21/6/2016
Description : Validate Data For Login
**********************************************************************************
Who				Date			Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_Validateuser
(
@strName as varchar(20),
@strPassword as varchar(20)
)
AS
BEGIN
DECLARE @status BIT ;
SET NOCOUNT ON;
IF EXISTS(
select 1 from ApplicationUser
where (User_Name = @strName COLLATE Latin1_General_CS_AS 
AND 
Password =@strPassword)
 )
 Begin
 set @status= 1;
 Select @status;
 end
 else 
 set @status= 0;
 Select @status;
end
GO
