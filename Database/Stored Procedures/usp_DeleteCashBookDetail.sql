If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteCashBookDetail'
       )
 Drop Procedure usp_DeleteCashBookDetail
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteCashBookDetail
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteCashBookDetail
(
	@CashBookID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM CashBookDetail WHERE fk_CashBookID=@CashBookID)

	BEGIN
 	
	Delete FROM CashBookDetail WHERE fk_CashBookID=@CashBookID

	SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;	
END

GO

