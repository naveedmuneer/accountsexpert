If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertChartOfAccount'
       )
 Drop Procedure usp_InsertChartOfAccount
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_InsertChartOfAccount
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertChartOfAccount
(
	@ChartID		INT,
	@ChartRefNo	Varchar(50),
	@ChartName	Varchar(50),
	@Description	Varchar(50),
	@GroupID		INT,
	@AccountNatureID INT,
	@Note		Varchar(50)=null,
	@Address		Varchar(500)=null,
	@Phone		Varchar(50)=null,
	@Fax			Varchar(50)=null,
	@Email		Varchar(50)=null,
	@NTNNO		Varchar(50)=null,
	@GSTNO		Varchar(50)=null,
	@Active		BIT,
	@CreatedOn	dateTime,
	@CreatedBy	Varchar(50)
	

)
AS
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF NOT EXISTS(SELECT 1 FROM Charts WHERE pk_ChartID=@ChartID)
	BEGIN

 INSERT INTO Charts 
 (
	pk_ChartID,
	ChartRefNo,
	ChartName,
	[Description],
	GroupID,
	fk_AccountNatureID,
	Note,
	[Address],
	Phone,
	Fax,
	Email,
	NTNNO,
	GSTNO,
	Active,
	Created_On,
	Created_By
)
values
(
	@ChartID,		
	@ChartRefNo,
	@ChartName,
	@Description,	
	@GroupID,		
	@AccountNatureID, 	
	@Note,		
	@Address,	
	@Phone,		
	@Fax,		
	@Email,		
	@NTNNO,		
	@GSTNO,		
	@Active,	
	@CreatedOn,
	@CreatedBy
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;

GO



GO

