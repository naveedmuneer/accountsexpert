
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetUserRoleMappings'
       )
 Drop Procedure [usp_GetUserRoleMappings]
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetUserRoleMappings
Author      : Moiz Saleem
Create date : 11-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create PROCEDURE [dbo].[usp_GetUserRoleMappings]
(
	@PageIndex INT = 1,
	@PageSize INT = 10,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

DECLARE @Table1 TABLE(UserID int,[User_Name] varchar(max), Role_Name varchar(max),RoleID int)
INSERT INTO @Table1	
Select	urm.fkUser_ID,
(Select User_Name from ApplicationUser  WHERE pkUser_ID= urm.fkUser_ID),
(Select Role_Name from Role  WHERE pkRole_ID= urm.fkRole_ID),fkRole_ID from UserRoleMapping urm


         SELECT  ROW_NUMBER() OVER
         (
         ORDER BY UserID ASC
         )
         AS RowNumber,
		UserID,[User_Name] 
       ,STUFF((SELECT ', ' + CAST(Role_Name AS VARCHAR(max)) [text()] 
		 FROM @Table1 
         WHERE [User_Name] = t.[User_Name]
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,2,' ') as Roles

		,STUFF((SELECT ', ' + CAST(RoleID AS VARCHAR(max)) [text()] 
		 FROM @Table1 
         WHERE [User_Name] = t.[User_Name]
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,2,' ') as RoleIDs
		  into #Results
		FROM @Table1 t
		GROUP BY [User_Name],UserID
		order by [User_Name]
		
		SELECT @RecordCount = COUNT(*)
		FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1

	  
     
      DROP TABLE #Results
	  end
	  GO