If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_BindAllDropDownsOfCashBook'
       )
 Drop Procedure usp_BindAllDropDownsOfCashBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_BindAllDropDownsOfCashBook
Author      : Waleed Saleem
Create date : 19-8-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_BindAllDropDownsOfCashBook

AS
BEGIN
 SET NOCOUNT ON;

 Select * from Company WHERE Is_Active=1
 Select * from dbo.Charts where fk_AccountNatureID = 3 AND GroupID IS NULL
 Select pk_ChartID ,ChartName from charts 
End
GO

