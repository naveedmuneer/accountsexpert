
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetEmployeeData'
       )
 Drop Procedure usp_GetEmployeeData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetEmployeeData
Author      : Waleed Saleem
Create date : 26-June-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetEmployeeData(
@PageIndex INT = 1,
 @PageSize INT = 10,
 @RecordCount INT OUTPUT
)

AS
BEGIN
SET NOCOUNT ON;
 SELECT ROW_NUMBER() OVER
      (
      ORDER BY Employee.pk_Employee_ID ASC
      )
      AS RowNumber,
pk_Employee_ID,      
Employee_Name,
NIC_No,
Email,
Company.Company_Name,
Department.Department_Name,
Designation.Designation_Name,
Contact_No,
Secondary_Contact_No,
Address,
Permenant_Address,
Employee.Is_Active,
Employee.Created_On
Into #Results
From
Employee INNER JOIN Company ON 
Employee.fkCompany_ID=Company.pkCompany_ID INNER JOIN 
Designation ON Employee.fkDepartment_ID=Designation.pkDesignation_ID INNER JOIN 
Department ON Employee.fkDepartment_ID= Department.pkDepartment_ID

  SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
     WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
      DROP TABLE #Results
End
GO



