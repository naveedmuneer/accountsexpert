If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertExceptionLoggingData'
       )
 Drop Procedure usp_InsertExceptionLoggingData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_InsertExceptionLoggingData
Author      : Abdul Wahab
Create date : 22-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create Procedure [dbo].usp_InsertExceptionLoggingData  
(  
@ExceptionMsg varchar(100)=null,  
@ExceptionType varchar(100)=null,  
@ExceptionSource nvarchar(max)=null,  
@ExceptionURL varchar(100)=null  
)  
as  
begin  
Insert into ExceptionLogging  
(  
ExceptionMsg ,  
ExceptionType,   
ExceptionSource,  
ExceptionURL,  
Logdate  
)  
select  
@ExceptionMsg,  
@ExceptionType,  
@ExceptionSource,  
@ExceptionURL,  
getdate()  
End 

GO
