If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCashBookReportData'
       )
 Drop Procedure usp_GetCashBookReportData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCashBook
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetCashBookReportData
(
	@CashBookID int
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT 
pk_CashBookID,
Company.Company_Name,
CMaster.CashBookRefNo,
CDetail.Description ,
Cast(CONVERT(date,CMaster.TransactionDate) AS varchar(12)) AS TransactionDate,
Charts.ChartName,
ChartMaster.ChartName As BookName,
CDetail.Amount,
CMaster.ChequeNo,
Cast(CONVERT(date,CMaster.ChequeDate)AS varchar(12)) AS ChequeDate ,
CMaster.Remarks from CashBookMaster AS CMaster 

INNER JOIN CashBookDetail AS CDetail ON CMaster.pk_CashBookID=CDetail.fk_CashBookID 
INNER JOIN Charts ON  Charts.pk_ChartID=CDetail.fk_ChartID 
INNER JOIN Company ON  Company.pkCompany_ID=Cmaster.fkCompanyID 
INNER JOIN Charts As ChartMaster ON ChartMaster.pk_ChartID=CMaster.fk_ChartID
Where pk_CashBookID=@CashBookID

End
GO
