If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCashBookLastInsertedID'
       )
 Drop Procedure usp_GetCashBookLastInsertedID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCashBookLastInsertedID
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetCashBookLastInsertedID
(
	@InputType varchar(50)
)
AS
BEGIN
 SET NOCOUNT ON;

 Select Max(pk_CashBookID) As CashBookID From CashBookMaster where InputType=@InputType
 
End
GO
