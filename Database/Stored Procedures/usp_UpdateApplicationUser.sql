If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateApplicationUser'
       )
 Drop Procedure usp_UpdateApplicationUser
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_UpdateApplicationUser
Author      : Muneem Ahmed
Create date : 27-06-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_UpdateApplicationUser
(
	@pk_UserID INT,
	@UserName varchar(100),
	@Password Varchar(100),
	@fkEmployeeID Int,
	@Is_Active Bit,
	@ModifiedOn	dateTime,
	@ModifiedBy	Varchar(50)
	

)
AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM ApplicationUser WHERE pkUser_ID = @pk_UserID)
	BEGIN
 Update ApplicationUser 
SET
	User_Name=@UserName,
	Password=@Password,
	fkEmployee_ID=@fkEmployeeID,
	Is_Active=@Is_Active,
	Modified_On=@ModifiedOn,
	Modified_By=@ModifiedBy
	WHERE pkUser_ID=@pk_UserID

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;
END
GO
