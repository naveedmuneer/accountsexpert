
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertBankBookDetail'
       )
 Drop Procedure usp_InsertBankBookDetail
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_InsertBankBookDetail
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertBankBookDetail
(
	--@BankBookDetailID INT,
	@fkBankBookID INT,
	@fkChartID INT,
	@Description varchar(max),
	@Amount Decimal(18,5)
	

)
AS
DECLARE @status BIT;
DECLARE @BankBookDetailID AS int 
select @BankBookDetailID =ISNULL(MAX(pk_BankBookDetailID),0)+1 from BankBookDetail
	SET NOCOUNT ON;
	IF NOT EXISTS(SELECT 1 FROM BankBookDetail WHERE pk_BankBookDetailID=@BankBookDetailID)
	BEGIN

 INSERT INTO BankBookDetail 
 (
	--pk_BankBookDetailID,
	fk_BankBookId,
	fk_ChartID,
	[Description],
	Amount
)
values
(
	--@BankBookDetailID,
	@fkBankBookID,
	@fkChartID,
	@Description,
	@Amount
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;

GO



GO

 