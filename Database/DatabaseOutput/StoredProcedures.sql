
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_Application_MenuData'
       )
	Drop Procedure usp_Application_MenuData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_Application_MenuData
Author      : Naveed Muneer		
Create date	: 5-13-2016
Description : Get MenuData
**********************************************************************************
Who				Date			Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_Application_MenuData
AS
BEGIN
SET NOCOUNT ON;
SELECT *
FROM Application_Menu  
End
GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteApplicationUser'
       )
 Drop Procedure usp_DeleteApplicationUser
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_DeleteApplicationUser
Author      : Muneem Ahmed
Create date : 27-06-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteApplicationUser
(
	@ApplicationUserID	INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM ApplicationUser where pkUser_ID=@ApplicationUserID)

	BEGIN
 	
	Delete FROM ApplicationUser WHERE pkUser_ID=@ApplicationUserID

	SET @status = 1;
    SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END

	GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteBankBookData'
       )
 Drop Procedure usp_DeleteBankBookData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteBankBookData
Author      : Abdul Wahab
Create date : 08-09-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteBankBookData
(
	@BankBookID		INT
)
AS
BEGIN
DECLARE @status BIT;

	IF EXISTS(SELECT 1 FROM BankBookDetail WHERE fk_BankBookId=@BankBookID)

	BEGIN
 	
	Delete FROM BankBookDetail WHERE fk_BankBookId=@BankBookID
    
	Delete FROM BankBookMaster WHERE pk_BankBookId=@BankBookID

	SET @status = 1;
    SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END

	GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteBankBookDetail'
       )
 Drop Procedure usp_DeleteBankBookDetail
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteBankBookDetail
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteBankBookDetail
(
	@BankBookID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM BankBookDetail WHERE fk_BankBookId=@BankBookID)

	BEGIN
 	
	Delete FROM BankBookDetail WHERE fk_BankBookId=@BankBookID

	SET @status = 1;
    SELECT @status;
    END
    ELSE
    SET @status = 0;
    SELECT @status;	
    END

    GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteBankBookMaster'
       )
 Drop Procedure usp_DeleteBankBookMaster
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteBankBookMaster
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteBankBookMaster
(
	@BankBookID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM BankBookMaster WHERE pk_BankBookId=@BankBookID)

	BEGIN
 	
	Delete FROM BankBookMaster WHERE pk_BankBookId=@BankBookID

	SET @status = 1;
    SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END

	GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteCashBookDetail'
       )
 Drop Procedure usp_DeleteCashBookDetail
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteCashBookDetail
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteCashBookDetail
(
	@CashBookID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM CashBookDetail WHERE fk_CashBookID=@CashBookID)

	BEGIN
 	
	Delete FROM CashBookDetail WHERE fk_CashBookID=@CashBookID

	SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;	
END

GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteCashBookMaster'
       )
 Drop Procedure usp_DeleteCashBookMaster
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteCashBookMaster
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteCashBookMaster
(
	@CashBookID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM CashBookMaster WHERE pk_CashBookID=@CashBookID)

	BEGIN
 	
	Delete FROM CashBookMaster WHERE pk_CashBookID=@CashBookID

	SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;	
END

GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteChartOfAccount'
       )
 Drop Procedure usp_DeleteChartOfAccount
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteChartOfAccount
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteChartOfAccount
(
	@ChartID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM Charts WHERE pk_ChartID=@ChartID And (Select Count(*) from Charts Where GroupID=@ChartID) = 0)

	BEGIN
 	
	Delete FROM Charts WHERE pk_ChartID=@ChartID And (Select Count(*) from Charts Where GroupID=@ChartID) = 0

	SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;	
END

GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteCompany'
       )
 Drop Procedure usp_DeleteCompany
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     usp_DeleteCompany
Author      : Abdul wahab
Create date : 14-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/


Create PROCEDURE [dbo].[usp_DeleteCompany]
(
	@CompanyID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT pkCompany_ID FROM Company WHERE pkCompany_ID=@CompanyID)

	BEGIN
 	
	Delete FROM Company WHERE pkCompany_ID=@CompanyID

	SET @status = 1;
    SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END


GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteEmployee'
       )
 Drop Procedure usp_DeleteEmployee
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_DeleteEmployee
Author      : Waleed Saleem
Create date : 30-6-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_DeleteEmployee
(
	@EmplpoyeeID		INT
)
AS
BEGIN
DELETE FROM Employee WHERE pk_Employee_ID = @EmplpoyeeID

END

    GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteRole'
       )
 Drop Procedure usp_DeleteRole
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     usp_DeleteRole
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/


create PROCEDURE [dbo].[usp_DeleteRole]
(
	@RoleID		INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT pkRole_ID FROM Role WHERE pkRole_ID=@RoleID)

	BEGIN
 	
	Delete FROM Role WHERE pkRole_ID=@RoleID

	SET @status = 1;
    SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END


GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_DeleteUserRoleMapping'
       )
 Drop Procedure [usp_DeleteUserRoleMapping]
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_DeleteUserRoleMapping
Author      : Moiz Saleem
Create date : 11-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create  PROCEDURE [dbo].[usp_DeleteUserRoleMapping]
(
	@fkUser_ID	INT
)
AS
BEGIN
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT fkUser_ID FROM UserRoleMapping WHERE fkUser_ID=@fkUser_ID)

	BEGIN
 	
	Delete FROM  UserRoleMapping where fkUser_ID=@fkUser_ID

	SET @status = 1;
   -- SELECT @status;
	END
	ELSE
	SET @status = 0;
	SELECT @status;	
	END
    GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetAccountNature'
       )
 Drop Procedure usp_GetAccountNature
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetAccountNature
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetAccountNature

AS
BEGIN
 SET NOCOUNT ON;

 Select pk_AccountNatureID AS AccountNatureID,AccountNatureName from AccountNature WHERE pk_AccountNatureID!=1
End
GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetActiveUsers'
       )
 Drop Procedure [usp_GetActiveUsers]
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetActiveUsers
Author      : Moiz Saleem
Create date : 11-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create PROCEDURE [dbo].[usp_GetActiveUsers]
AS	

BEGIN

SET NOCOUNT ON;
select  pkUser_ID ,[User_Name]  from ApplicationUser WHERE  Is_Active='true'

End
GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetAllAccountsBook'
       )
 Drop Procedure usp_GetAllAccountsBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetAllAccountsBook
Author      : Muhammad Ammar
Create date : 24-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetAllAccountsBook

AS
BEGIN
 SET NOCOUNT ON;

 Select * from AccountsBook WHERE Is_Active=1
End
GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetAllCompany'
       )
 Drop Procedure usp_GetAllCompany
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetAllCompany
Author      : Muhammad Ammar
Create date : 24-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetAllCompany

AS
BEGIN
 SET NOCOUNT ON;

 Select * from Company WHERE Is_Active=1
End
GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetAllDepartment'
       )
 Drop Procedure usp_GetAllDepartment
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetAllDepartment
Author      : Muhammad Waleed
Create date : 2 july 2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetAllDepartment

AS
BEGIN
 SET NOCOUNT ON;

 Select * from Department WHERE Is_Active=1
End
GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetAllDesignation'
       )
 Drop Procedure usp_GetAllDesignation
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetAllDesignation
Author      : Muhammad Waleed
Create date : 2 july 2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetAllDesignation

AS
BEGIN
 SET NOCOUNT ON;

 Select * from Designation WHERE Is_Active=1
End
GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetAllUser'
       )
 Drop Procedure usp_GetAllUser
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_GetAllUser
Author      : Muneem Ahmed
Create date : 26-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetAllUser

AS
BEGIN
 SET NOCOUNT ON;

 Select * from Employee WHERE Is_Active=1
End
GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetApplicationUserData'
       )
 Drop Procedure usp_GetApplicationUserData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetApplicationUserData
Author  : Muneem Ahmed
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetApplicationUserData
(
	@PageIndex INT = 1,
	@PageSize INT = 10,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT ROW_NUMBER() OVER
      (
      ORDER BY pkuser_id ASC
      )
      AS RowNumber
	  ,pkuser_id as pkUserID
	  ,ApplicationUser.User_Name as UserName
	  ,ApplicationUser.Password as Password
	  ,Employee.Employee_Name as EmployeeName
	  ,ApplicationUser.Is_Active as IsActive
	  into #Results
	  from ApplicationUser AS ApplicationUser
	  INNER JOIN Employee AS Employee ON applicationUser.fkEmployee_ID=employee.pk_Employee_ID
	  
      SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
      DROP TABLE #Results
End
GO




If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetBankBook'
       )
 Drop Procedure usp_GetBankBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetBankBook
Author  : Naveed Muneer
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetBankBook
(
	@PageIndex INT = 1,
	@PageSize INT = 10,
	@BankBookType varchar(10)=null,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT ROW_NUMBER() OVER
      (
      ORDER BY pk_BankBookId ASC
      )
      AS RowNumber
	  ,pk_BankBookId
	  ,Company.Company_Name
	  ,BMaster.BankBookRefNo
	  ,dbo.fn_GetDateFormat_DDMMMYYYY(TransactionDate) as TransactionDate 
	  ,ChartMaster.ChartName as VoucherBook
	   ,SUM(BDetail.Amount) AS Amount
	  ,BMaster.ChequeNo
	   ,dbo.fn_GetDateFormat_DDMMMYYYY(ChequeDate) as ChequeDate 
	  ,BMaster.Remarks
	  into #Results
	  from BankBookMaster AS BMaster
	  INNER JOIN BankBookDetail AS BDetail ON BMaster.pk_BankBookId=BDetail.fk_BankBookId
	  INNER JOIN Charts as ChartMaster ON  ChartMaster.pk_ChartID=BMaster.fk_ChartID
	  INNER JOIN Charts as ChartDetail ON  ChartDetail.pk_ChartID=BDetail.fk_ChartID
	  Left JOIN Company ON  Company.pkCompany_ID=BMaster.fkCompanyID
	  Where BMaster.InputType=@BankBookType
	  GROUP BY pk_BankBookId,Company_Name,BankBookRefNo,TransactionDate,ChartMaster.ChartName,ChequeNo,ChequeDate,Remarks

      SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
      DROP TABLE #Results
End
GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetBankBookDetailMaxID'
       )
 Drop Procedure usp_GetBankBookDetailMaxID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetBankBookDetailMaxID
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetBankBookDetailMaxID

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

Select ISNULL(MAX(pk_BankBookDetailID),0) from BankBookDetail

END

GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetBankBookLastInsertedID'
       )
 Drop Procedure usp_GetBankBookLastInsertedID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetBankBookLastInsertedID
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetBankBookLastInsertedID
(
	@InputType varchar(50)
)
AS
BEGIN
 SET NOCOUNT ON;

 Select Max(pk_BankBookID) As BankBookID From BankBookMaster where InputType=@InputType
 
End
GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetBankBookReportData'
       )
 Drop Procedure usp_GetBankBookReportData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCashBook
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetBankBookReportData
(
	@BankBookID int
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT 
  pk_BankBookId,
  Company.Company_Name,
  BMaster.BankBookRefNo As BankBookRefNo,
  BDetail.Description ,
  BMaster.ChequeDate,
  BMaster.ChequeNo,
Cast(CONVERT(date,BMaster.TransactionDate) AS varchar(12)) AS TransactionDate,
Charts.ChartName,
ChartMaster.ChartName As BookName ,
BDetail.Amount,
BMaster.ChequeNo,
Cast(CONVERT(date,BMaster.ChequeDate)AS varchar(12)) AS ChequeDate ,
BMaster.Remarks 
from BankBookMaster AS BMaster 
INNER JOIN BankBookDetail AS BDetail ON BMaster.pk_BankBookID=BDetail.fk_BankBookId 
INNER JOIN Charts ON  Charts.pk_ChartID=BDetail.fk_ChartID 
Left JOIN Company ON  Company.pkCompany_ID=BMaster.fkCompanyID 
INNER JOIN Charts  AS ChartMaster ON ChartMaster.pk_ChartID=BMaster.fk_ChartID
WHERE pk_BankBookId=@BankBookID
End
GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetBankMaxID'
       )
 Drop Procedure usp_GetBankMaxID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetBankMaxID
Author      : Naveed Muneer
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetBankMaxID

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

Select ISNULL(MAX(pk_BankBookId),0) from BankBookMaster

END

GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetBankVoucher'
       )
 Drop Procedure usp_GetBankVoucher
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetBankVoucher
Author      : Naveed Muneer
Create date : 24-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetBankVoucher

AS
BEGIN
 SET NOCOUNT ON;
 
Select * from dbo.Charts where fk_AccountNatureID = 3 AND GroupID IS NULL

End
GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCashBook'
       )
 Drop Procedure usp_GetCashBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCashBook
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetCashBook
(
	@PageIndex INT = 1,
	@PageSize INT = 10,
	@CashBookType varchar(10)=null,
	@TransactionStartDate varchar(100)=null,
	@TransactionENDDate varchar(100)=null,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT ROW_NUMBER() OVER
      (
            ORDER BY pk_CashBookID ASC
      )
     AS RowNumber
	  ,pk_CashBookID
	  ,CMaster.CashBookRefNo
	  ,dbo.fn_GetDateFormat_DDMMMYYYY(TransactionDate) as TransactionDate 
	  
	  ,ChartMaster.ChartName as VoucherBook
	  ,Sum(CDetail.Amount) AS Amount
	  ,CMaster.ChequeNo
	  ,dbo.fn_GetDateFormat_DDMMMYYYY(ChequeDate) as ChequeDate 
	  ,CMaster.Remarks
	  into #Results
	  from CashBookMaster AS CMaster
	  LEFT JOIN CashBookDetail AS CDetail ON CMaster.pk_CashBookID=CDetail.fk_CashBookID
	  INNER JOIN Charts as ChartMaster ON  ChartMaster.pk_ChartID=CMaster.fk_ChartID
	  INNER JOIN Charts as ChartDetail ON  ChartDetail.pk_ChartID=CDetail.fk_ChartID
	  Left JOIN Company ON  Company.pkCompany_ID=CMaster.fkCompanyID
      Where CMaster.InputType=@CashBookType And (CMaster.TransactionDate >= @TransactionStartDate OR @TransactionStartDate IS NULL) 
	  And (CMaster.TransactionDate <= @TransactionEndDate OR @TransactionEndDate IS NULL)
	  GROUP BY pk_CashBookID,Company_Name,CashBookRefNo,TransactionDate,ChartMaster.ChartName,ChequeNo,ChequeDate,Remarks

 SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
      DROP TABLE #Results
End
GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCashBookDetailMaxID'
       )
 Drop Procedure usp_GetCashBookDetailMaxID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCashBookMaxID
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetCashBookDetailMaxID

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

Select ISNULL(MAX(pk_CashBookDetailID),0) from CashBookDetail

END

GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCashBookLastInsertedID'
       )
 Drop Procedure usp_GetCashBookLastInsertedID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCashBookLastInsertedID
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetCashBookLastInsertedID
(
	@InputType varchar(50)
)
AS
BEGIN
 SET NOCOUNT ON;

 Select Max(pk_CashBookID) As CashBookID From CashBookMaster where InputType=@InputType
 
End
GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCashBookMaxID'
       )
 Drop Procedure usp_GetCashBookMaxID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCashBookMaxID
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetCashBookMaxID

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

Select ISNULL(MAX(pk_CashBookID),0) from CashBookMaster

END

GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCashBookReportData'
       )
 Drop Procedure usp_GetCashBookReportData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCashBook
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetCashBookReportData
(
	@CashBookID int
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT 
pk_CashBookID,
Company.Company_Name,
CMaster.CashBookRefNo,
CDetail.Description ,
Cast(CONVERT(date,CMaster.TransactionDate) AS varchar(12)) AS TransactionDate,
Charts.ChartName,
ChartMaster.ChartName As BookName,
CDetail.Amount,
CMaster.ChequeNo,
Cast(CONVERT(date,CMaster.ChequeDate)AS varchar(12)) AS ChequeDate ,
CMaster.Remarks from CashBookMaster AS CMaster 

INNER JOIN CashBookDetail AS CDetail ON CMaster.pk_CashBookID=CDetail.fk_CashBookID 
INNER JOIN Charts ON  Charts.pk_ChartID=CDetail.fk_ChartID 
INNER JOIN Company ON  Company.pkCompany_ID=Cmaster.fkCompanyID 
INNER JOIN Charts As ChartMaster ON ChartMaster.pk_ChartID=CMaster.fk_ChartID
Where pk_CashBookID=@CashBookID

End
GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetChartOfAccount'
       )
 Drop Procedure usp_GetChartOfAccount
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetChartOfAccount
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetChartOfAccount
(
	@ChartType varchar(50)=null,
	@PageIndex INT = 1,
	@PageSize INT = 10,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT ROW_NUMBER() OVER
      (
            ORDER BY pk_ChartID ASC
      )AS RowNumber
	  ,pk_ChartID AS [ChartID]
	  ,ChartRefNo
	  ,chart.Description
	  ,dbo.fn_ReverseChartOfAccountName(dbo.getChartAccountParent(pk_ChartID)) As ChartName
	  ,AC.AccountNatureName As AccountNature
	  ,chart.Active As [Status]
	  ,dbo.getChartOfAccountLevel(pk_ChartID) As [Level] 
	  INTO #Results
	  from charts chart
	  LEFT JOIN AccountNature AC On chart.fk_AccountNatureID=Ac.pk_AccountNatureID
		WHERE ((@ChartType is null) or (AC.Header1 = @ChartType))
	

 SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
      DROP TABLE #Results
End
GO

--Select * From charts
--Select * from accountNature
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetChartOfAccountMaxID'
       )
 Drop Procedure usp_GetChartOfAccountMaxID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetChartOfAccountMaxID
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetChartOfAccountMaxID

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

Select MAX(ISNULL(pk_ChartID,0)) from Charts

END

GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetChequeStatus'
       )
 Drop Procedure usp_GetChequeStatus
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetChequeStatus
Author      : Naveed Muneer
Create date : 24-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetChequeStatus

AS
BEGIN
 SET NOCOUNT ON;
 
Select * from EnumerationValue where fkEnumerationId= 1  

End
GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCompany'
       )
 Drop Procedure usp_GetCompany
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     [[usp_GetCompany]]
Author      : abdul wahab
Create date : 14-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/

Create PROCEDURE [dbo].[usp_GetCompany]
(
    @PageIndex INT = 1,
	@PageSize INT = 10,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT ROW_NUMBER() OVER
      (
      ORDER BY pkCompany_ID ASC
      )
      AS RowNumber,
	    pkCompany_ID 
		,[Company_Name]
		,[Description]
		,[Title],
		[Business],
		 dbo.fn_GetDateFormat_DDMMMYYYY(Establishment_Date) as Establishment_Date,
		 dbo.fn_GetDateFormat_DDMMMYYYY(Expiry_Date) as Expiry_Date, 
		[Is_Active] , 
		
		dbo.fn_GetDateFormat_DDMMMYYYY([Created_On]) as Created_On ,[Created_By]
	  into #Results
	  from Company 
	  
     

      SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1

	  
     
      DROP TABLE #Results				
End
GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetCompanyDataForEdit'
       )
 Drop Procedure usp_GetCompanyDataForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetCompanyDataForEdit
Author      : abdul wahab
Create date : 14-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create PROCEDURE [dbo].[usp_GetCompanyDataForEdit]
(
	@CompanyID INT 
)
AS	
BEGIN
 SET NOCOUNT ON;
 select *  from Company
 WHERE pkCompany_ID=@CompanyID
 End
 
Go
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDatabByEmployeeForEdit'
       )
 Drop Procedure usp_GetDatabByEmployeeForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetDatabByEmployeeForEdit
Author      : Muhammad Waleed
Create date : 3 July 2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDatabByEmployeeForEdit
(
	@EmployeeID INT 
)
AS	
BEGIN
 SET NOCOUNT ON;
 Select 
* FROM Employee
 WHERE pk_Employee_ID=@EmployeeID
 End
 GO



If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDataByApplicationUserForEdit'
       )
 Drop Procedure usp_GetDataByApplicationUserForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetDataByApplicationUserForEdit
Author      : Muneem Ahmed
Create date : 27-06-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDataByApplicationUserForEdit
(
	@applicationUserID INT 
)
AS	
BEGIN
 SET NOCOUNT ON;
 Select 
 appuser.pkUser_ID,
 AppUser.User_Name,
 AppUser.Password,
 AppUser.fkEmployee_ID,
 AppUser.Is_Active,
 Employee.pk_Employee_ID
 
 from ApplicationUser AS AppUser
 INNER JOIN Employee AS employee ON employee.pk_Employee_ID =AppUser.fkEmployee_ID
 WHERE pkUser_ID=@applicationUserID
 End
 GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDataByBankBookForEdit'
       )
 Drop Procedure usp_GetDataByBankBookForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetDataByBankBookForEdit
Author      : Muhammad Naveed
Create date : 05-25-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDataByBankBookForEdit
(
	@BankBookID INT 
)
AS	
BEGIN
 SET NOCOUNT ON;
 Select 
 BMaster.fkCompanyID,
 BMaster.BankBookRefNo,
 BMaster.TransactionDate,
 BMaster.ChequeNo,
 dbo.fn_GetDateFormat_DDMMMYYYY(ChequeDate) as ChequeDates, 
 dbo.fn_GetDateFormat_DDMMMYYYY(TransactionDate) as VoucherDate,
 BMaster.Remarks,
 BDetail.Description,
 BDetail.Amount,
 ChartMaster.pk_ChartID as VoucherBookID ,
 ChartDetail.ChartName as AccountsName,
 BDetail.fk_ChartID As AccountID
 from BankBookMaster AS BMaster
 INNER JOIN BankBookDetail AS BDetail ON BMaster.pk_BankBookId=BDetail.fk_BankBookId
 LEFT JOIN Charts as ChartDetail ON  ChartDetail.pk_ChartID=  BDetail.fk_ChartID
 INNER JOIN Charts as ChartMaster ON  ChartMaster.pk_ChartID= BMaster.fk_ChartID
 INNER JOIN Company ON  Company.pkCompany_ID=BMaster.fkCompanyID
 WHERE pk_BankBookID=@BankBookID
 End
 GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDataByCashBookDetailIDForEdit'
       )
 Drop Procedure usp_GetDataByCashBookDetailIDForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetDataByCashBookDetailIDForEdit
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDataByCashBookDetailIDForEdit
(
	@CashBookDetailID INT 
)
AS
BEGIN
 SET NOCOUNT ON;

 Select * From CashBookDetail WHERE pk_CashBookDetailID=@CashBookDetailID
 
End
GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDataByCashBookForEdit'
       )
 Drop Procedure usp_GetDataByCashBookForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetDataByCashBookForEdit
Author      : Muhammad Ammar
Create date : 05-25-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDataByCashBookForEdit
(
	@CashBookID INT 
)
AS	
BEGIN
 SET NOCOUNT ON;
 Select 
 CMaster.fkCompanyID,
 CMaster.CashBookRefNo,
 CMaster.TransactionDate,
 CMaster.ChequeNo,
 dbo.fn_GetDateFormat_DDMMMYYYY(ChequeDate) as ChequeDates ,
 dbo.fn_GetDateFormat_DDMMMYYYY(TransactionDate) as VoucherDate ,
 CMaster.Remarks,
 CDetail.Description,
 CDetail.Amount,
 ChartMaster.pk_ChartID as VoucherBook ,
 CDetail.fk_ChartID As AccountID,
 ChartMaster.ChartName as AccountsName
 from CashBookMaster AS CMaster
 INNER JOIN CashBookDetail AS CDetail on CMaster.pk_CashBookID=CDetail.fk_CashBookID
 LEFT JOIN Charts as ChartDetail ON  ChartDetail.pk_ChartID=  CDetail.fk_ChartID
 INNER JOIN Charts as ChartMaster ON  ChartMaster.pk_ChartID= CMaster.fk_ChartID
 INNER JOIN Company ON  Company.pkCompany_ID=CMaster.fkCompanyID
 WHERE pk_CashBookID=@CashBookID
 End
 GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDataByChartIDForEdit'
       )
 Drop Procedure usp_GetDataByChartIDForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetDataByChartIDForEdit
Author      : Muhammad Ammar
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDataByChartIDForEdit
(
	@ChartID INT 
)
AS
BEGIN
 SET NOCOUNT ON;

 Select * From Charts WHERE pk_ChartID=@ChartID
 
End
GO

--Select * From charts
--Select * from accountNature
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetDataFromChart'
       )
 Drop Procedure usp_GetDataFromChart
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetDataFromChart
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetDataFromChart

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

 Select pk_ChartID ,ChartName from charts 


END

GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetEmployeeData'
       )
 Drop Procedure usp_GetEmployeeData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetEmployeeData
Author      : Waleed Saleem
Create date : 26-June-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetEmployeeData(
@PageIndex INT = 1,
 @PageSize INT = 10,
 @RecordCount INT OUTPUT
)

AS
BEGIN
SET NOCOUNT ON;
 SELECT ROW_NUMBER() OVER
      (
      ORDER BY Employee.pk_Employee_ID ASC
      )
      AS RowNumber,
pk_Employee_ID,      
Employee_Name,
NIC_No,
Email,
Company.Company_Name,
Department.Department_Name,
Designation.Designation_Name,
Contact_No,
Secondary_Contact_No,
Address,
Permenant_Address,
Employee.Is_Active,
Employee.Created_On
Into #Results
From
Employee INNER JOIN Company ON 
Employee.fkCompany_ID=Company.pkCompany_ID INNER JOIN 
Designation ON Employee.fkDepartment_ID=Designation.pkDesignation_ID INNER JOIN 
Department ON Employee.fkDepartment_ID= Department.pkDepartment_ID

  SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
     WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1
     
      DROP TABLE #Results
End
GO




If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetGroupDataFromChart'
       )
 Drop Procedure usp_GetGroupDataFromChart
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetGroupDataFromChart
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetGroupDataFromChart

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

 Select pk_ChartID AS ChartID,ChartName from charts


END

GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetJounalVoucher'
       )
 Drop Procedure usp_GetJounalVoucher
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetJounalVoucher
Author      : Muhammad Naveed
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetJounalVoucher
(
@PageIndex INT = 1,
@PageSize INT = 10,
@RecordCount INT OUTPUT
)


AS
BEGIN

SELECT ROW_NUMBER() OVER
(
ORDER BY pk_JournalVoucherID ASC
)
AS RowNumber,
JournalVoucherRefNo,
dbo.fn_GetDateFormat_DDMMMYYYY(TransactionDate) as TransactionDate ,
Remarks as Description,
Sum(GVD.Debit) as Amount
into #Results
from 
JournalVoucherMaster GVM inner join JournalVoucherDetail GVD
on GVM.pk_JournalVoucherID = fk_JournalVoucherID
group by pk_JournalVoucherID,JournalVoucherRefNo,TransactionDate,Remarks

SELECT @RecordCount = COUNT(*)
FROM #Results
   
SELECT * FROM #Results
WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1

DROP TABLE #Results
END
GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetJournalVoucherMaxID'
       )
 Drop Procedure usp_GetJournalVoucherMaxID
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetJournalVoucherMaxID
Author      : Naveed Muneer
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetJournalVoucherMaxID

AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
		

Select ISNULL(MAX(pk_JournalVoucherID),0) from JournalVoucherMaster

END

GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetRoleDataForEdit'
       )
 Drop Procedure usp_GetRoleDataForEdit
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_GetRoleDataForEdit
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create PROCEDURE [dbo].[usp_GetRoleDataForEdit]
(
	@RoleID INT 
)
AS	
BEGIN
 SET NOCOUNT ON;
 select *  from Role
 WHERE pkRole_ID=@RoleID
 End
 
Go
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetRoles'
       )
 Drop Procedure usp_GetRoles
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     [usp_GetRoles]
Author      : Muhamad Moiz
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/


-- Exec [usp_GetBankBook] 1,10,5
Create PROCEDURE [dbo].[usp_GetRoles]
(
	@PageIndex INT = 1,
	@PageSize INT = 10,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT ROW_NUMBER() OVER
      (
      ORDER BY pkRole_ID ASC
      )
      AS RowNumber,
	    pkRole_ID ,[Role_Name],[Is_Active] , dbo.fn_GetDateFormat_DDMMMYYYY([Created_On]) as Created_On ,[Created_By]
	  into #Results
	  from Role 
	  
     

      SELECT @RecordCount = COUNT(*)
      FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1

	  
     
      DROP TABLE #Results
End

GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetSessionId'
       )
	Drop Procedure usp_GetSessionId
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_GetSessionId
Author      : Naveed Muneer
Create date	: 21/6/2016
Description : Get Session Id
**********************************************************************************
Who				Date			Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_GetSessionId
(
@strName as varchar(20)
)
AS
BEGIN

SET NOCOUNT ON;
Select pkUser_ID from ApplicationUser
where User_Name=@strName
end
GO





If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetUserRoleMappings'
       )
 Drop Procedure [usp_GetUserRoleMappings]
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetUserRoleMappings
Author      : Moiz Saleem
Create date : 11-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create PROCEDURE [dbo].[usp_GetUserRoleMappings]
(
	@PageIndex INT = 1,
	@PageSize INT = 10,
	@RecordCount INT OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON;

DECLARE @Table1 TABLE(UserID int,[User_Name] varchar(max), Role_Name varchar(max),RoleID int)
INSERT INTO @Table1	
Select	urm.fkUser_ID,
(Select User_Name from ApplicationUser  WHERE pkUser_ID= urm.fkUser_ID),
(Select Role_Name from Role  WHERE pkRole_ID= urm.fkRole_ID),fkRole_ID from UserRoleMapping urm


         SELECT  ROW_NUMBER() OVER
         (
         ORDER BY UserID ASC
         )
         AS RowNumber,
		UserID,[User_Name] 
       ,STUFF((SELECT ', ' + CAST(Role_Name AS VARCHAR(max)) [text()] 
		 FROM @Table1 
         WHERE [User_Name] = t.[User_Name]
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,2,' ') as Roles

		,STUFF((SELECT ', ' + CAST(RoleID AS VARCHAR(max)) [text()] 
		 FROM @Table1 
         WHERE [User_Name] = t.[User_Name]
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,2,' ') as RoleIDs
		  into #Results
		FROM @Table1 t
		GROUP BY [User_Name],UserID
		order by [User_Name]
		
		SELECT @RecordCount = COUNT(*)
		FROM #Results
           
      SELECT * FROM #Results
      WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1

	  
     
      DROP TABLE #Results
	  end
	  GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_GetUserRoleMappingsDataForEdit'
       )
 Drop Procedure [usp_GetUserRoleMappingsDataForEdit]
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_GetUserRoleMappingsDataForEdit
Author      : Moiz Saleem
Create date : 11-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/

Create PROCEDURE [dbo].[usp_GetUserRoleMappingsDataForEdit]
(
	@UserID int
)
AS
BEGIN
 SET NOCOUNT ON;

DECLARE @Table1 TABLE(UserID int,[User_Name] varchar(max), Role_Name varchar(max),RoleID int)
INSERT INTO @Table1			Select	urm.fkUser_ID,
							(Select User_Name from ApplicationUser  WHERE pkUser_ID= urm.fkUser_ID),
							(Select Role_Name from Role  WHERE pkRole_ID= urm.fkRole_ID),fkRole_ID from UserRoleMapping urm
--select *  from @Table1 t

SELECT  ROW_NUMBER() OVER
      (
      ORDER BY UserID ASC
      )
      AS RowNumber,
		UserID,[User_Name] 
       ,STUFF((SELECT ', ' + CAST(Role_Name AS VARCHAR(max)) [text()] 
		 FROM @Table1 
         WHERE [User_Name] = t.[User_Name]
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,2,' ') as Roles

		,STUFF((SELECT ', ' + CAST(RoleID AS VARCHAR(max)) [text()] 
		 FROM @Table1 
         WHERE [User_Name] = t.[User_Name]
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,2,' ') as RoleIDs
		FROM @Table1 t
		WHERE t.UserID=@UserID
		GROUP BY [User_Name],UserID
		order by [User_Name]
		End
        GO
		
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertApplicationUser'
       )
 Drop Procedure usp_InsertApplicationUser
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_InsertApplicationUser
Author      : Muneem Ahmed
Create date : 26-06-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertApplicationUser
(
	@pk_UserID INT,
	@UserName varchar(100),
	@Password Varchar(100),
	@fkEmployeeID Int,
	@Is_Active Bit,
	@createdBy varchar(100),
	@createdOn date
	)
	

AS
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF NOT EXISTS(SELECT 1 FROM ApplicationUser WHERE pkUser_ID=@pk_UserID)
	BEGIN
 INSERT INTO ApplicationUser 
 (
	
	User_Name,
	Password,
	fkEmployee_ID,
	Is_Active,
	Created_On,
	Created_By
)
values
(
	
	
	@UserName,
	@Password,
	@fkEmployeeID,
	@Is_Active,
	@createdOn,
	@createdBy

)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;

Go
Go

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertBankBook'
       )
 Drop Procedure usp_InsertBankBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     usp_InsertBankBook
Author      : Naveed Muneer
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertBankBook
(
	@BankBookID INT,
	@InputType Varchar(200),
	@CompanyID INT,
	@BankBookRefNo Varchar(100),
	@TransactionDate Datetime,
	@fkChartID INT,
	@ChequeNo varchar(100),
	@ChequeDate Datetime,
	@Remarks varchar(max),
	@fkUserID INT,
	@CreatedOn	dateTime,
	@CreatedBy	Varchar(50)
	

)
AS
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF NOT EXISTS(SELECT 1 FROM BankBookMaster WHERE pk_BankBookId=@BankBookID)
	BEGIN

 INSERT INTO BankBookMaster 
 (
	pk_BankBookId,
	InputType,
	fkCompanyID,
	BankBookRefNo,
	TransactionDate,
	fk_ChartID,
	ChequeNo,
	ChequeDate,
	Remarks,
	fkUserId,
	Created_On,
	Created_By
)
values
(
	@BankBookID,
	@InputType,
	@CompanyID,
	@BankBookRefNo,
	@TransactionDate,
	@fkChartID,
	@ChequeNo,
	@ChequeDate,
	@Remarks,
	@fkUserID,
	@CreatedOn,
	@CreatedBy
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;

GO



GO

If Exists (Select *
   From   Information_Schema.Routines
   Where  Specific_Schema = N'dbo'
   And	   Specific_Name = N'usp_InsertBankBookData')
	Drop Procedure dbo.usp_InsertBankBookData

GO
/********************************************************************************************
Module       : usp_InsertBankBookData
Created By   : Abdul Wahab
CreatedOn   : July 29, 2016
Description  : 

*******************************Modification History*******************************************
*********************************************************************************************/
CREATE PROC [dbo].usp_InsertBankBookData
	@xmlBankBook xml,
	@xmlBankBookDetail xml
AS
BEGIN

Declare @rvcBankBookMasterID as int 
Declare @BankBookRefNo as varchar(50)
Declare @Inputtype as varchar(5)
Select @Inputtype = XmlDataTable.BankBook.value('@InputType[1]','Varchar(20)') FROM @xmlBankBook.nodes('.//BankBook') as XmlDataTable(BankBook)
Select @BankBookRefNo = @Inputtype + '-0000' + cast(ISNULL(MAX(pk_BankBookId),0) + 1 as varchar(50)) from BankBookMaster	
Declare @BankBookId as int
Select @BankBookId=cast(ISNULL(MAX(pk_BankBookId),0) +1  as int)   from BankBookMaster

INSERT INTO BankBookMaster(
    pk_BankBookId,
	InputType,
	fkCompanyID,
	BankBookRefNo,
	TransactionDate,
	fk_ChartID,
	ChequeNo,
	ChequeDate,
	Remarks,
	fkUserId,
	Active,
	Created_On,
	[Created_By]
	)
	   
	 Select	
	        @BankBookId,
			XmlDataTable.BankBook.value('@InputType[1]','Varchar(20)'),
			XmlDataTable.BankBook.value('@CompanyID[1]','int'),
			@BankBookRefNo,
			XmlDataTable.BankBook.value('@TransactionDate[1]','Datetime'),
			XmlDataTable.BankBook.value('@FkChartID[1]','int'),
			XmlDataTable.BankBook.value('@ChequeNo[1]','varchar(100)'),
			XmlDataTable.BankBook.value('@ChequeDate[1]','Datetime'),
			XmlDataTable.BankBook.value('@Remarks[1]','varchar(max)'),
			XmlDataTable.BankBook.value('@fkUserID[1]','int'),
			1,
			GetDate(),
			XmlDataTable.BankBook.value('@Created_By[1]','varchar(50)')
	
					
	FROM @xmlBankBook.nodes('.//BankBook') as XmlDataTable(BankBook)

 INSERT INTO BankBookDetail 
 (

	fk_BankBookId,
	fk_ChartID,
	[Description],
	Amount
)
    Select	
		@BankBookId,
		XmlDataTable.BankBookDetail.value('@Fk_ChartID[1]','int'),
		XmlDataTable.BankBookDetail.value('@Description[1]','varchar(max)'),
		XmlDataTable.BankBookDetail.value('@Amount[1]','Decimal(18,5)')
        FROM @xmlBankBookDetail.nodes('//BankBookDetail') as XmlDataTable(BankBookDetail)
	 END

Go

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertBankBookDetail'
       )
 Drop Procedure usp_InsertBankBookDetail
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_InsertBankBookDetail
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertBankBookDetail
(
	--@BankBookDetailID INT,
	@fkBankBookID INT,
	@fkChartID INT,
	@Description varchar(max),
	@Amount Decimal(18,5)
	

)
AS
DECLARE @status BIT;
DECLARE @BankBookDetailID AS int 
select @BankBookDetailID =ISNULL(MAX(pk_BankBookDetailID),0)+1 from BankBookDetail
	SET NOCOUNT ON;
	IF NOT EXISTS(SELECT 1 FROM BankBookDetail WHERE pk_BankBookDetailID=@BankBookDetailID)
	BEGIN

 INSERT INTO BankBookDetail 
 (
	--pk_BankBookDetailID,
	fk_BankBookId,
	fk_ChartID,
	[Description],
	Amount
)
values
(
	--@BankBookDetailID,
	@fkBankBookID,
	@fkChartID,
	@Description,
	@Amount
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;

GO



GO

 If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertCashBook'
       )
 Drop Procedure usp_InsertCashBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_InsertCashBook
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertCashBook
(
	@CashBookID INT,
	@InputType Varchar(200),
	@CompanyID INT,
	@CashBookRefNo Varchar(100),
	@TransactionDate Datetime,
	@fkChartID INT,
	@ChequeNo varchar(100),
	@ChequeDate Datetime,
	@Remarks varchar(max),
	@fkUserID INT,
	@CreatedOn	dateTime,
	@CreatedBy	Varchar(50)
	

)
AS
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF NOT EXISTS(SELECT 1 FROM CashBookMaster WHERE pk_CashBookID=@CashBookID)
	BEGIN

 INSERT INTO CashBookMaster 
 (
	pk_CashBookID,
	InputType,
	fkCompanyID,
	CashBookRefNo,
	TransactionDate,
	fk_ChartID,
	ChequeNo,
	ChequeDate,
	Remarks,
	fkUserId,
	Created_On,
	Created_By
)
values
(
	@CashBookID,
	@InputType,
	@CompanyID,
	@CashBookRefNo,
	@TransactionDate,
	@fkChartID,
	@ChequeNo,
	@ChequeDate,
	@Remarks,
	@fkUserID,
	@CreatedOn,
	@CreatedBy
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;

GO



GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertCashBookDetail'
       )
 Drop Procedure usp_InsertCashBookDetail
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_InsertCashBookDetail
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertCashBookDetail
(
	@CashBookDetailID INT,
	@fkCashBookID INT,
	@fkChartID INT,
	@Description varchar(max),
	@Amount Decimal(18,5)
	

)
AS
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF NOT EXISTS(SELECT 1 FROM CashBookDetail WHERE pk_CashBookDetailID=@CashBookDetailID)
	BEGIN

 INSERT INTO CashBookDetail 
 (
	pk_CashBookDetailID,
	fk_CashBookID,
	fk_ChartID,
	[Description],
	Amount
)
values
(
	@CashBookDetailID,
	@fkCashBookID,
	@fkChartID,
	@Description,
	@Amount
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;

GO



GO

 If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertChartOfAccount'
       )
 Drop Procedure usp_InsertChartOfAccount
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_InsertChartOfAccount
Author      : Muhammad Ammar
Create date : 18-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertChartOfAccount
(
	@ChartID		INT,
	@ChartRefNo	Varchar(50),
	@ChartName	Varchar(50),
	@Description	Varchar(50),
	@GroupID		INT,
	@AccountNatureID INT,
	@Note		Varchar(50)=null,
	@Address		Varchar(500)=null,
	@Phone		Varchar(50)=null,
	@Fax			Varchar(50)=null,
	@Email		Varchar(50)=null,
	@NTNNO		Varchar(50)=null,
	@GSTNO		Varchar(50)=null,
	@Active		BIT,
	@CreatedOn	dateTime,
	@CreatedBy	Varchar(50)
	

)
AS
DECLARE @status BIT;
	SET NOCOUNT ON;
	IF NOT EXISTS(SELECT 1 FROM Charts WHERE pk_ChartID=@ChartID)
	BEGIN

 INSERT INTO Charts 
 (
	pk_ChartID,
	ChartRefNo,
	ChartName,
	[Description],
	GroupID,
	fk_AccountNatureID,
	Note,
	[Address],
	Phone,
	Fax,
	Email,
	NTNNO,
	GSTNO,
	Active,
	Created_On,
	Created_By
)
values
(
	@ChartID,		
	@ChartRefNo,
	@ChartName,
	@Description,	
	@GroupID,		
	@AccountNatureID, 	
	@Note,		
	@Address,	
	@Phone,		
	@Fax,		
	@Email,		
	@NTNNO,		
	@GSTNO,		
	@Active,	
	@CreatedOn,
	@CreatedBy
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;

GO



GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertCompany'
       )
 Drop Procedure usp_InsertCompany
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     [usp_InsertCompany]
Author      : abdul wahab
Create date : 14-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/

Create PROCEDURE [dbo].[usp_InsertCompany]
(
	
	@Company_Name Varchar(50),
	@Description varchar(50),
	@Title varchar(50),
	@Business varchar(50),
	@Establishment_Date Date,
	@Expiry_Date Date,
	@Is_Active BIT,
	@CreatedBy	Varchar(50)
	

)
AS
	DECLARE @status BIT;
	SET NOCOUNT ON;


	IF NOT EXISTS(SELECT  Company_Name FROM Company WHERE Company_Name=@Company_Name)
	BEGIN

 INSERT INTO Company 
 (
	
	Company_Name ,
	Description,
	Title,
	Business,
	Establishment_Date,
	Expiry_Date,
	Is_Active,
	Created_On,
	Created_By,
	Machine_Created
)
values
(
	@Company_Name,
	@Description,
	@Title,
	@Business,
	@Establishment_Date,
	@Expiry_Date,
	@Is_Active,
	getdate(),
	@CreatedBy,
	getdate()
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;


Go
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertEmplpyee'
       )
 Drop Procedure usp_InsertEmplpyee
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_InsertEmplpyee
Author      : Muhammad Waleed
Create date : 2 July 2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_InsertEmplpyee
(
	
	@EmployeeName Varchar(50),
	@NIC Varchar(50),
	@CompanyID INT,
	@DepartmentID INT,
	@DesignationID INT,
	@Email Varchar(50),
	@ContactNo Varchar(50) ,
	@SecondaryConatctNo Varchar(50),
	@Address varchar(100),
	@PermenantAddress varchar(100),
	@Active	BIT,
	@CreatedOn	dateTime,
	@CreatedBy	Varchar(50)
)
AS
Begin
	SET NOCOUNT ON;

	

 INSERT INTO Employee 
 (
	Employee_Name,
	NIC_No,
	fkCompany_ID,
	fkDepartment_ID,
	fkDesignation_ID,
	Email,
	Contact_No,
	Secondary_Contact_No,
	Address,
	Permenant_Address,
	Is_Active,
	Created_On,
	Created_By
)
values
(
	@EmployeeName,
	@NIC, 
	@CompanyID ,
	@DepartmentID, 
	@DesignationID, 
	@Email ,
	@ContactNo,
	@SecondaryConatctNo, 
	@Address ,
	@PermenantAddress, 
	@Active,		
	@CreatedOn,	
	@CreatedBy	
)
End

GO





If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertExceptionLoggingData'
       )
 Drop Procedure usp_InsertExceptionLoggingData
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_InsertExceptionLoggingData
Author      : Abdul Wahab
Create date : 22-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create Procedure [dbo].usp_InsertExceptionLoggingData  
(  
@ExceptionMsg varchar(100)=null,  
@ExceptionType varchar(100)=null,  
@ExceptionSource nvarchar(max)=null,  
@ExceptionURL varchar(100)=null  
)  
as  
begin  
Insert into ExceptionLogging  
(  
ExceptionMsg ,  
ExceptionType,   
ExceptionSource,  
ExceptionURL,  
Logdate  
)  
select  
@ExceptionMsg,  
@ExceptionType,  
@ExceptionSource,  
@ExceptionURL,  
getdate()  
End 
Go

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertRole'
       )
 Drop Procedure usp_InsertRole
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     [usp_InsertRole]
Author      : Muhamad Moiz
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/

create PROCEDURE [dbo].[usp_InsertRole]
(
	
	@Role_Name Varchar(50),
	@Is_Active BIT,
	@CreatedBy	Varchar(50)
	

)
AS
	DECLARE @status BIT;
	SET NOCOUNT ON;


	IF NOT EXISTS(SELECT  Role_Name FROM Role WHERE Role_Name=@Role_Name)
	BEGIN

 INSERT INTO Role 
 (
	
	Role_Name,
	Is_Active,
	Created_On,
	Created_By
)
values
(
	@Role_Name,
	@Is_Active,
	getdate(),
	@CreatedBy
)

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;


GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_InsertUserRoleMapping'
       )
 Drop Procedure [usp_InsertUserRoleMapping]
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module      : usp_InsertUserRoleMapping
Author      : Moiz Saleem
Create date : 11-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
Create PROCEDURE [dbo].[usp_InsertUserRoleMapping]
(
	@fkUser_ID int,
	@fkRole_ID Varchar(max)
	
	
	

)
AS
	DECLARE @status BIT;
	SET NOCOUNT ON;



	IF NOT EXISTS(SELECT  fkUser_ID FROM UserRoleMapping WHERE fkUser_ID=@fkUser_ID)
	BEGIN

Insert INTO UserRoleMapping(fkRole_ID,fkUser_ID) Select cast(Value as int),@fkUser_ID from dbo.Split(@fkRole_ID,',') 


SET @status = 1;
--SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;
GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateApplicationUser'
       )
 Drop Procedure usp_UpdateApplicationUser
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_UpdateApplicationUser
Author      : Muneem Ahmed
Create date : 27-06-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_UpdateApplicationUser
(
	@pk_UserID INT,
	@UserName varchar(100),
	@Password Varchar(100),
	@fkEmployeeID Int,
	@Is_Active Bit,
	@ModifiedOn	dateTime,
	@ModifiedBy	Varchar(50)
	

)
AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM ApplicationUser WHERE pkUser_ID = @pk_UserID)
	BEGIN
 Update ApplicationUser 
SET
	User_Name=@UserName,
	Password=@Password,
	fkEmployee_ID=@fkEmployeeID,
	Is_Active=@Is_Active,
	Modified_On=@ModifiedOn,
	Modified_By=@ModifiedBy
	WHERE pkUser_ID=@pk_UserID

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;
END
GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateBankBook'
       )
 Drop Procedure usp_UpdateBankBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_UpdateBankBook
Author      : Muhammad Ammar
Create date : 25-06-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_UpdateBankBook
(
	@BankBookID INT,
	@CompanyID INT,
	@TransactionDate Datetime,
	@fkChartID INT,
	@ChequeNo varchar(100),
	@ChequeDate Datetime,
	@Remarks varchar(max),
	@UserID INT,
	@ModifiedOn	dateTime,
	@ModifiedBy	Varchar(50)
	

)
AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM BankBookMaster WHERE pk_BankBookID = @BankBookID)
	BEGIN
 Update BankBookMaster 
SET

	fkCompanyID=@CompanyID,
	TransactionDate=@TransactionDate,
	fk_ChartID=@fkChartID,
	ChequeNo=@ChequeNo,
	ChequeDate=@ChequeDate,
	Remarks=@Remarks,
	fkUserId=@UserID,
	Modified_On=@ModifiedOn,
	Modified_By=@ModifiedBy
	WHERE pk_BankBookID = @BankBookID

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;
END
GO
	
	If Exists (Select *
	   From   Information_Schema.Routines
	   Where  Specific_Schema = N'dbo'
	   And	   Specific_Name = N'usp_UpdateBankBookData')
	   Drop Procedure dbo.usp_UpdateBankBookData

	GO
/********************************************************************************************
Module       : usp_UpdateBankBookData
Created By   : Abdul Wahab
CreatedOn   : July 29, 2016
Description  : 

*******************************Modification History*******************************************
*********************************************************************************************/
CREATE PROC [dbo].usp_UpdateBankBookData

(	@xmlBankBookMaster xml,
	@xmlBanakBookDetail xml
)
	AS
	BEGIN
	-- DECLARE @status BIT;
	--SET NOCOUNT ON;
	  --BEGIN
    UPDATE BankBookMaster 
	SET 
	InputType = i.InputType, 
	fkCompanyID = i.CompanyId,
	TransactionDate = i.TrasactionDate,
	fk_ChartID = i.FkchartId, 
	ChequeNo = i.ChequeNo,
	ChequeDate = i.ChequeDate, 
	Remarks = i.Remarks,
	fkUserId = i.FkuserId, 
	Active = i.Active,
	Modified_On=i.ModifiedOn,
	Modified_By=i.ModifiedBy

	FROM
	 (
  		SELECT 
		XmlDataTable.BankBook.value('@InputType[1]','varchar(5)') as InputType,
		XmlDataTable.BankBook.value('@CompanyID[1]','int') as CompanyId,
		XmlDataTable.BankBook.value('@TransactionDate','Datetime')as TrasactionDate ,
		XmlDataTable.BankBook.value('@FkChartID[1]','int')as FkchartId,
		XmlDataTable.BankBook.value('@ChequeNo[1]','varchar(100)') as ChequeNo,
		XmlDataTable.BankBook.value('@ChequeDate[1]','Datetime')as ChequeDate,
		XmlDataTable.BankBook.value('@Remarks[1]','varchar(max)')as Remarks,
		XmlDataTable.BankBook.value('@FkUserID[1]','int')as FkuserId,
		1 as Active,
		GetDate() as ModifiedOn,
		XmlDataTable.BankBook.value('@Modified_By[1]','varchar(max)')as ModifiedBy 
		
FROM @xmlBankBookMaster.nodes('.//BankBook') as XmlDataTable(BankBook) 
	) as i

WHERE pk_BankBookID = (SELECT XmlDataTable.BankBook.value('@pk_BankBookID[1]','int') 
from @xmlBankBookMaster.nodes('.//BankBook') as XmlDataTable(BankBook))
  



Delete from BankBookDetail 
WHERE fk_BankBookId=(SELECT XmlDataTable.BankBook.value('@pk_BankBookID[1]','int') 
from @xmlBankBookMaster.nodes('.//BankBook') as XmlDataTable(BankBook)) 

     INSERT INTO BankBookDetail 
	 (	
	    fk_BankBookId,
		fk_ChartID,	
		[Description],
		Amount
	)

	Select	
			(
			SELECT
			XmlDataTable.BankBook.value('@pk_BankBookID[1]','int') from @xmlBankBookMaster.nodes('.//BankBook') as XmlDataTable(BankBook)),
			XmlUpdateDataTable.Detail.value('@Fk_ChartID[1]','int'),
			XmlUpdateDataTable.Detail.value('@Description[1]','varchar(max)'),
			XmlUpdateDataTable.Detail.value('@Amount[1]','Decimal(18,5)')

	FROM @xmlBanakBookDetail.nodes('//BankBookDetail') as XmlUpdateDataTable(Detail)
 
END 
--END

GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateCashBook'
       )
 Drop Procedure usp_UpdateCashBook
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_UpdateCashBook
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_UpdateCashBook
(
	@CashBookID INT,
	@CompanyID INT,
	@TransactionDate Datetime,
	@fkAccountBookID INT,
	@ChequeNo varchar(100),
	@ChequeDate Datetime,
	@Remarks varchar(max),
	@UserID INT,
	@ModifiedOn	dateTime,
	@ModifiedBy	Varchar(50)
	

)
AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM CashBookMaster WHERE pk_CashBookID = @CashBookID)
	BEGIN
 Update CashBookMaster 
SET

	fkCompanyID=@CompanyID,
	TransactionDate=@TransactionDate,
	fk_ChartID=@fkAccountBookID,
	ChequeNo=@ChequeNo,
	ChequeDate=@ChequeDate,
	Remarks=@Remarks,
	fkUserId=@UserID,
	Modified_On=@ModifiedOn,
	Modified_By=@ModifiedBy
	WHERE pk_CashBookID=@CashBookID

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;
END
GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateCashBookDetail'
       )
 Drop Procedure usp_UpdateCashBookDetail
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_UpdateCashBookDetail
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_UpdateCashBookDetail
(
	@CashBookDetailID INT,
	@fkCashBookID INT,
	@fkChartID INT,
	@Description varchar(max),
	@Amount Decimal(18,5)
)
AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM CashBookDetail WHERE pk_CashBookDetailID = @CashBookDetailID)
	BEGIN
 Update CashBookDetail 
SET

	fk_CashBookID=@fkCashBookID,
	fk_ChartID=@fkChartID,
	[Description]=@Description,
	Amount=@Amount
	WHERE pk_CashBookDetailID=@CashBookDetailID

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;
END
GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateChartOfAccount'
       )
 Drop Procedure usp_UpdateChartOfAccount
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_UpdateChartOfAccount
Author      : Muhammad Ammar
Create date : 19-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_UpdateChartOfAccount
(
	@ChartID		INT,
	@ChartRefNo	Varchar(50),
	@ChartName	Varchar(50),
	@Description	Varchar(50),
	@GroupID		INT,
	@AccountNatureID INT,
	@Note		Varchar(50)=null,
	@Address		Varchar(500)=null,
	@Phone		Varchar(50)=null,
	@Fax			Varchar(50)=null,
	@Email		Varchar(50)=null,
	@NTNNO		Varchar(50)=null,
	@GSTNO		Varchar(50)=null,
	@Active		BIT,
	@ModifiedOn	dateTime,
	@ModifiedBy	Varchar(50)
	

)
AS
BEGIN
 DECLARE @status BIT;
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM Charts WHERE pk_ChartID = @ChartID)
	BEGIN
 Update Charts 
SET
	ChartRefNo=@ChartRefNo,
	ChartName=@ChartName,
	[Description]=@Description,
	GroupID=@GroupID,
	fk_AccountNatureID=@AccountNatureID,
	Note=@Note,
	[Address]=@Address,
	Phone=@Phone,
	Fax=@Fax,
	Email=@Email,
	NTNNO=@NTNNO,
	GSTNO=@GSTNO,
	Active=@Active,
	Modified_On=@ModifiedOn,
	Modified_By=@ModifiedBy
	WHERE pk_ChartID=@ChartID

SET @status = 1;
SELECT @status;
END
ELSE
SET @status = 0;
SELECT @status;
END
GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateCompany'
       )
 Drop Procedure usp_UpdateCompany
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     [usp_UpdateCompany]
Author      : abdul wahab
Create date : 14-07-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/

Create PROCEDURE [dbo].[usp_UpdateCompany]
(
	@pkCompany_ID int,
	@Company_Name Varchar(50),
	@Description varchar(50),
	@Title varchar(50),
	@Business varchar(50),
	@Establishment_Date Date,
	@Expiry_Date Date,
	@Is_Active BIT
)
AS
	DECLARE @status BIT;
	SET NOCOUNT ON;


	IF  EXISTS(SELECT  pkCompany_ID FROM Company WHERE pkCompany_ID=@pkCompany_ID)
    BEGIN
	IF  EXISTS(SELECT  Company_Name FROM Company WHERE pkCompany_ID!=@pkCompany_ID and Company_Name=@Company_Name )
    BEGIN
	
    SET @status = 0;

    END
    ELSE

    BEGIN
		
	Update [Company]  set
	Company_Name=@Company_Name,
	Description=@Description,
	Title=@Title,
	Business=@Business,
	Establishment_Date=@Establishment_Date,
	Expiry_Date=@Expiry_Date,
	Is_Active=@Is_Active,
	Modified_On=getdate() 
	WHERE pkCompany_ID=@pkCompany_ID
	SET @status = 1;
	end
		
		
	END

	ELSE

	BEGIN

	SET @status = 0;

	END



	SELECT @status;


GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateEmployee'
       )
 Drop Procedure usp_UpdateEmployee
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_UpdateEmployee
Author      : Muhammad Waleed
Create date : 3 July 2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_UpdateEmployee
(   
    @EmployeeID INT,
	@EmployeeName Varchar(50),
	@NIC Varchar(50),
	@CompanyID INT,
	@DepartmentID INT,
	@DesignationID INT,
	@Email Varchar(50),
	@ContactNo Varchar(50) ,
	@SecondaryConatctNo Varchar(50),
	@Address varchar(100),
	@PermenantAddress varchar(100),
	@Active	BIT,
	@ModifiedOn	DateTime,
	@ModifiedBy	Varchar(50)
	

)
AS
BEGIN
SET NOCOUNT ON;
Update Employee 
SET
Employee_Name = @EmployeeName,
NIC_No = @NIC,
fkCompany_ID = @CompanyID,
fkDepartment_ID = @DepartmentID,
fkDesignation_ID = @DesignationID,
Email = @Email,
Contact_No = @ContactNo,
Secondary_Contact_No = @SecondaryConatctNo,
Address = @Address,
Permenant_Address = @PermenantAddress,
Is_Active = @Active,
Modified_On = @ModifiedOn,
Modified_By = @ModifiedBy
WHERE @EmployeeID = pk_Employee_ID


END
GO
If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateRole'
       )
 Drop Procedure usp_UpdateRole
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  :     [usp_UpdateRole]
Author      : Muhamad Moiz
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/

 create PROCEDURE [dbo].[usp_UpdateRole]
(
	@pkRole_ID int,
	@Role_Name Varchar(50),
	@Is_Active BIT
	
	

)
AS
	DECLARE @status BIT;
	SET NOCOUNT ON;


	IF  EXISTS(SELECT  pkRole_ID FROM Role WHERE pkRole_ID=@pkRole_ID)
BEGIN
	IF  EXISTS(SELECT  Role_Name FROM Role WHERE pkRole_ID!=@pkRole_ID and Role_Name=@Role_Name )
BEGIN
SET @status = 0;

END
ELSE

BEGIN
		
		Update [Role]  set Role_Name=@Role_Name,Is_Active=@Is_Active,Modified_On=getdate() WHERE pkRole_ID=@pkRole_ID
		SET @status = 1;
		end
		
		
END

	ELSE

BEGIN

	SET @status = 0;

END



SELECT @status;


GO

If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_UpdateUserRoleMapping'
       )
 Drop Procedure usp_UpdateUserRoleMapping
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module  : usp_UpdateUserRoleMapping
Author      : Muhammad Moiz
Create date : 14-05-2016

**********************************************************************************
Who    Date   Description
**********************************************************************************

**********************************************************************************/
-- exec usp_UpdateUserRoleMapping 3,'14,15,16'
Create PROCEDURE [dbo].[usp_UpdateUserRoleMapping]
(
	@fkUser_ID int,
	@fkRole_ID Varchar(max)
)
    AS
	DECLARE @status BIT;
	SET NOCOUNT ON;
    IF  EXISTS(SELECT  fkUser_ID FROM UserRoleMapping WHERE fkUser_ID=@fkUser_ID)
    BEGIN
	
	Delete from UserRoleMapping WHERE fkUser_ID=@fkUser_ID
	Insert INTO UserRoleMapping(fkRole_ID,fkUser_ID) Select cast(Value as int),@fkUser_ID from dbo.Split(@fkRole_ID,',') 
    SET @status = 1;
    END


--ELSE  IF not   EXISTS(SELECT  fkUser_ID FROM UserRoleMapping WHERE fkUser_ID=@fkUser_ID)
--begin
	
	
--	Insert INTO UserRoleMapping(fkRole_ID,fkUser_ID) Select cast(Value as int),@fkUser_ID from dbo.Split(@fkRole_ID,',') 


--	SET @status = 1;

--END
   Else
   begin
   SET @status = 0;
   end
   SELECT @status;
   GO


If Exists (Select *
       From   Information_Schema.Routines
       Where  Specific_Name = N'usp_Validateuser'
       )
	Drop Procedure usp_Validateuser
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************************************************
Module		: usp_Validateuser
Author      : Naveed Muneer
Create date	: 21/6/2016
Description : Validate Data For Login
**********************************************************************************
Who				Date			Description
**********************************************************************************

**********************************************************************************/
CREATE PROCEDURE dbo.usp_Validateuser
(
@strName as varchar(20),
@strPassword as varchar(20)
)
AS
BEGIN
DECLARE @status BIT ;
SET NOCOUNT ON;
IF EXISTS(
select 1 from ApplicationUser
where (User_Name = @strName COLLATE Latin1_General_CS_AS 
AND 
Password =@strPassword)
 )
 Begin
 set @status= 1;
 Select @status;
 end
 else 
 set @status= 0;
 Select @status;
end
GO
